package quantumepoch.window;

import static org.lwjgl.opengl.GL11.glViewport;
import static org.lwjgl.opengl.GL30.GL_FRAMEBUFFER;
import static org.lwjgl.opengl.GL30.glBindFramebuffer;

import java.awt.Canvas;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.PixelFormat;

import quantumepoch.core.Core;

public class LWJGLWindow extends Window
{

	public static final int BYTES_PER_PIXEL = 4;
	public static final int SAMPLES = 0;

	public LWJGLWindow()
	{
		if (Core.getWindow() != null)
		{
			throw new IllegalStateException("Can not make more than one instance of Window");
		}
	}

	@Override
	public void update()
	{
		Display.update();
	}

	@Override
	public void create()
	{
		try
		{
			Display.create(new PixelFormat(8, 8, 1, SAMPLES));
		}
		catch (LWJGLException e)
		{
			System.out.println("Error creating window");
			e.printStackTrace();
		}
	}

	@Override
	public void dispose()
	{
		Display.destroy();
	}

	@Override
	public String getTitle()
	{
		return Display.getTitle();
	}

	@Override
	public int getWidth()
	{
		return Display.getWidth();
	}

	@Override
	public int getHeight()
	{
		return Display.getHeight();
	}

	public float getAspectRatio()
	{
		return (float) getWidth() / getHeight();
	}

	public boolean isResizable()
	{
		return Display.isResizable();
	}

	@Override
	public boolean isCloseRequested()
	{
		return Display.isCloseRequested();
	}

	@Override
	public boolean isVisible()
	{
		return Display.isVisible();
	}

	@Override
	public boolean wasResized()
	{
		return Display.wasResized();
	}

	@Override
	public void setTitle(String title)
	{
		Display.setTitle(title);
	}

	@Override
	public void setSize(int width, int height)
	{
		try
		{
			Display.setDisplayMode(new DisplayMode(width, height));
		}
		catch (LWJGLException e)
		{
			System.err.println("Error changing the size of the window");
			e.printStackTrace();
		}
	}

	public void setFullScreen(boolean fullScreen)
	{
		try
		{
			Display.setFullscreen(fullScreen);
		}
		catch (LWJGLException e)
		{
			e.printStackTrace();
		}
	}

	public void setLocation(int x, int y)
	{
		Display.setLocation(x, y);
	}

	public void setViewport(int x, int y, int width, int height)
	{
		bindAsRenderTarget();
		glViewport(x, y, width, height);
	}

	@Override
	public void setResizable(boolean resizable)
	{
		Display.setResizable(resizable);
	}

	public void setParent(Canvas parent)
	{
		try
		{
			Display.setParent(parent);
		}
		catch (LWJGLException e)
		{
			e.printStackTrace();
		}
	}

	public void bindAsRenderTarget()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

}
