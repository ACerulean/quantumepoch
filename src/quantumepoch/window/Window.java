package quantumepoch.window;

/**
 * Abstract window class
 *
 */
public abstract class Window
{

	public abstract String getTitle();

	public abstract int getWidth();

	public abstract int getHeight();

	public abstract boolean isCloseRequested();

	public abstract boolean wasResized();

	public abstract void setSize(int width, int height);

	public abstract void setResizable(boolean resizable);

	public abstract boolean isVisible();

	public abstract void setTitle(String title);

	public abstract void update();

	public abstract void create();

	public abstract void dispose();

}
