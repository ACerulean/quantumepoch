package quantumepoch.window;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * makes the crash window and crash report when the program has problems and use
 * the use to more easily fix code and find issues
 *
 */
public class CrashWindow extends JFrame implements ActionListener
{

	private static final long serialVersionUID = 4045852164875260678L;

	private String crashReport;
	private JScrollPane scrollPane;
	private JTextArea textArea;
	private JButton copyButton;

	/**
	 * constructor that sets the values of the crash window
	 * 
	 * @param title
	 *            the title of the window
	 * @param width
	 *            the width of the window
	 * @param height
	 *            the height of the window
	 */
	public CrashWindow(String title, int width, int height)
	{
		this(null, title, width, height);
	}

	/**
	 * constructor that sets the values of the window and what is in the window
	 * frame
	 * 
	 * @param crashReport
	 *            report printed inside the window report
	 * @param title
	 *            the title of the window
	 * @param width
	 *            the width of the window
	 * @param height
	 *            the height of the window
	 */
	public CrashWindow(String crashReport, String title, int width, int height)
	{
		this.crashReport = crashReport;

		setTitle(title);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setLayout(new BorderLayout());
		setSize(width, height);

		copyButton = new JButton("Copy to clipboard");
		copyButton.addActionListener(this);
		textArea = new JTextArea(crashReport);
		textArea.setEditable(false);
		scrollPane = new JScrollPane(textArea);
		add(scrollPane, BorderLayout.CENTER);
		add(copyButton, BorderLayout.SOUTH);

		setForeground(Color.PINK);
		setBackground(Color.PINK);

		setVisible(true);
	}

	/**
	 * gets the crash report to output to the screen
	 * 
	 * @return returns the crash report string
	 */
	public String getCrashReport()
	{
		return crashReport;
	}

	/**
	 * sets the crash report string to the window
	 * 
	 * @param crashReport
	 *            the report to set
	 */
	public void setCrashReport(String crashReport)
	{
		this.crashReport = crashReport;
		textArea.setText(crashReport);
	}

	/**
	 * override method of the action performed in the event
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == copyButton)
		{
			StringSelection selection = new StringSelection(textArea.getText());
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(selection, null);
		}
	}

}
