package quantumepoch.window;

import java.awt.Canvas;
import java.awt.Graphics2D;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.AWTGLCanvas;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.PixelFormat;

/**
 * class that creates the window for the crash report and game to be displayed
 * on.
 *
 */
public class JFrameWindow extends Window implements WindowListener
{

	public static final int SAMPLES = 8;

	private boolean visible;
	private boolean closeRequested;
	private JFrame frame;
	private AWTGLCanvas canvas;
	private BufferStrategy buffer;
	private Graphics2D g;

	/**
	 * constructor that creates the the window to display
	 */
	public JFrameWindow()
	{
		frame = new JFrame("Game");
		try
		{
			canvas = new AWTGLCanvas();
		}
		catch (LWJGLException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * gets the graphics used in the game window
	 * 
	 * @return returns the graphics
	 */
	public Graphics2D getGraphics()
	{
		return g;
	}

	/**
	 * gets the frame value of the window
	 * 
	 * @return returns the Jframe
	 */
	public JFrame getFrame()
	{
		return frame;
	}

	/**
	 * gets the canvas to display the game and report on
	 * 
	 * @return returns the canvas to display
	 */
	public Canvas getCanvas()
	{
		return canvas;
	}

	/**
	 * sets the icon for the image to buffer and display
	 * 
	 * @param image
	 */
	public void setIcon(BufferedImage image)
	{
	}

	/**
	 * override method that gets the title of the frame
	 * 
	 * @return returns the title of the frame
	 */
	@Override
	public String getTitle()
	{
		return frame.getTitle();
	}

	/**
	 * override method that gets the width of the frame
	 * 
	 * @return returns the width of the frame
	 */
	@Override
	public int getWidth()
	{
		return canvas.getWidth();
	}

	/**
	 * override method that gets the height of the frame
	 * 
	 * @return returns the height of the frame
	 */
	@Override
	public int getHeight()
	{
		return canvas.getHeight();
	}

	/**
	 * override method that sets the size of the frame
	 * 
	 * @param width
	 *            the width size to set
	 * @param height
	 *            the height size to set
	 */
	@Override
	public void setSize(int width, int height)
	{
		canvas.setSize(width, height);
		// not currently available
	}

	/**
	 * sets the title of the frame used
	 * 
	 * @param title
	 *            the title to set for the frame
	 */
	@Override
	public void setTitle(String title)
	{
		frame.setTitle(title);
	}

	/**
	 * updates the frame of the game and report
	 */
	@Override
	public void update()
	{
		Display.update();
		buffer.show();
		g = (Graphics2D) buffer.getDrawGraphics();
	}

	/**
	 * creates the frame for the game and reports
	 */
	@Override
	public void create()
	{
		frame.addWindowListener(this);
		frame.add(canvas);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		canvas.createBufferStrategy(2);
		buffer = canvas.getBufferStrategy();
		g = (Graphics2D) buffer.getDrawGraphics();
		frame.setVisible(true);
		try
		{
			Display.setParent(canvas);
			Display.create(new PixelFormat(8, 8, 1, SAMPLES));
			canvas.initContext(0, 0, 0);
		}
		catch (LWJGLException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * destroys the frame window
	 */
	@Override
	public void dispose()
	{
		Display.destroy();
	}

	/**
	 * sets the resizable value of the values
	 * 
	 * @param resizable
	 *            the resizable boolean value
	 */
	@Override
	public void setResizable(boolean resizable)
	{
		frame.setResizable(resizable);
	}

	/**
	 * asks if the window is visible
	 * 
	 * @returns returns boolean value if visible or not
	 */
	@Override
	public boolean isVisible()
	{
		return visible;
	}

	/**
	 * activates the window
	 * 
	 * @param WindowEvent
	 *            event that opens/activates the window
	 */
	@Override
	public void windowActivated(WindowEvent e)
	{

	}

	/**
	 * closes the window
	 * 
	 * @param WindowEvent
	 *            event that closes the window
	 */
	@Override
	public void windowClosed(WindowEvent e)
	{
		frame.dispose();
	}

	/**
	 * 
	 */
	@Override
	public void windowClosing(WindowEvent e)
	{
		closeRequested = true;
	}

	@Override
	public void windowDeactivated(WindowEvent e)
	{

	}

	@Override
	public void windowDeiconified(WindowEvent e)
	{
		visible = true;
	}

	@Override
	public void windowIconified(WindowEvent e)
	{
		visible = false;
	}

	@Override
	public void windowOpened(WindowEvent e)
	{

	}

	@Override
	public boolean isCloseRequested()
	{
		return closeRequested;
	}

	@Override
	public boolean wasResized()
	{
		return false;
	}

}
