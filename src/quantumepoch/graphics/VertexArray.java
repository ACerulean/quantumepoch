package quantumepoch.graphics;

import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

import java.util.ArrayList;
import java.util.List;

/**
 * An OpenGL VAO, which stores multiple VBOS which data can be rendered. This class acts as an interface between OpenGL and the engine.
 */
public class VertexArray
{

	/** the handle of this VAO */
	private int handle;
	/** the list of vertex attributes this VAo can expect */
	private List<VertexAttrib> vertexAttributes;

	/**
	 * Creates a new vertex array.
	 */
	public VertexArray()
	{
		handle = glGenVertexArrays();
		vertexAttributes = new ArrayList<VertexAttrib>();
	}

	/**
	 * @return the handle of this vertex array
	 */
	public int getHandle()
	{
		return handle;
	}

	/**
	 * @return the list of vertex attributes this VAo expects
	 */
	public List<VertexAttrib> getVertexAttributes()
	{
		return vertexAttributes;
	}

	/**
	 * @return The offset between multiple vertex attributes in an interleaved VBO.
	 */
	public int getStride()
	{
		int stride = 0;

		for (VertexAttrib attrib : vertexAttributes)
		{
			stride += attrib.getDataType().totalSize();
		}

		return stride;
	}

	/**
	 * Calculates the starting offset for a attribute.
	 * 
	 * @param attribIndex
	 *            The index of the attribute
	 * @return The offset in bytes
	 */
	public int getOffset(int attribIndex)
	{
		int offset = 0;
		for (int i = 0; i < attribIndex; i++)
		{
			offset += vertexAttributes.get(i).getDataType().totalSize();
		}
		return offset;
	}

	/**
	 * Binds this VAO to be used with OpenGL
	 */
	public void bind()
	{
		glBindVertexArray(handle);
	}

	/**
	 * Updates this VAO with the specified VBOs data.
	 * 
	 * @param vbo
	 *            The VBO to update this VAOS data with
	 */
	public void update(VertexBuffer vbo)
	{

		bind();
		{
			int stride = getStride();

			vbo.bind();

			for (int i = 0; i < vertexAttributes.size(); i++)
			{
				VertexAttrib a = vertexAttributes.get(i);
				glVertexAttribPointer(a.getLocation(), a.getDataType().getElementCount(), a.getDataType().getGlType(), false, stride, getOffset(i));
			}
			vbo.sendData();

			vbo.unbind();

		}
		unbind();
	}

	/**
	 * Unbinds any already bound VAOs from OpenGL.
	 */
	public void unbind()
	{
		glBindVertexArray(0);
	}

	/**
	 * Disposes of this VAO and any resources.
	 */
	public void dispose()
	{
		glDeleteVertexArrays(handle);
	}

}
