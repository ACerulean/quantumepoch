package quantumepoch.graphics;

import static org.lwjgl.opengl.GL11.GL_LINES;
import static org.lwjgl.opengl.GL11.GL_LINE_LOOP;
import static org.lwjgl.opengl.GL11.GL_LINE_STRIP;
import static org.lwjgl.opengl.GL11.GL_POINTS;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_QUAD_STRIP;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_TRIANGLE_FAN;
import static org.lwjgl.opengl.GL11.GL_TRIANGLE_STRIP;

/**
 * Stores multiple primitives that OpenGL can work with when rendering.
 */
public enum PrimitiveShape
{

	POINT(GL_POINTS), LINE(GL_LINES), LINE_STRIP(GL_LINE_STRIP), LINE_LOOP(GL_LINE_LOOP), TRIANGLE(GL_TRIANGLES), TRIANGLE_STRIP(GL_TRIANGLE_STRIP), TRIANGLE_FAN(GL_TRIANGLE_FAN), QUAD(GL_QUADS), QUAD_STRIP(GL_QUAD_STRIP);

	/** the opengl param for this primivive */
	private int glConst;

	/**
	 * Creates a new opengl primitive shape.
	 * 
	 * @param glConst
	 *            the opengl parameter for this primitive.
	 */
	private PrimitiveShape(int glConst)
	{
		this.glConst = glConst;
	}

	/**
	 * @return the opengl param for this primitive
	 */
	public int getGlConst()
	{
		return glConst;
	}

}
