package quantumepoch.graphics.gui;

import quantumepoch.audio.AudioSource;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.batch.SpriteBatch;
import quantumepoch.graphics.font.FontRenderer;
import quantumepoch.graphics.font.TrueTypeFont;
import quantumepoch.graphics.texture.TextureRegion;
import quantumepoch.physics.BoundingBox2D;

/**
 * A button with an image and text on it.
 */
public class ImageTextButton extends ImageButton
{

	private String text;
	private TrueTypeFont font;
	private Color tint;

	public ImageTextButton()
	{
		super();
	}

	public ImageTextButton(Component parent, int relativeX, int relativeY, TextureRegion normalTexture, TextureRegion hoveredTexture, TextureRegion pressedTexture, BoundingBox2D boundingBox, AudioSource blip)
	{
		super(parent, relativeX, relativeY, normalTexture, hoveredTexture, pressedTexture, boundingBox, blip);
	}

	public ImageTextButton(Component parent, int relativeX, int relativeY, TextureRegion normalTexture, TextureRegion hoveredTexture, TextureRegion pressedTexture, String text, TrueTypeFont font, Color tint, AudioSource blip)
	{
		super(parent, relativeX, relativeY, normalTexture, hoveredTexture, pressedTexture, blip);
		setText(text);
		setFont(font);
		setTint(tint);
	}

	public String getText()
	{
		return text;
	}

	public void setText(String text)
	{
		this.text = text;
	}

	public TrueTypeFont getFont()
	{
		return font;
	}

	public Color getTint()
	{
		return tint;
	}

	public void setFont(TrueTypeFont font)
	{
		this.font = font;
	}

	public void setTint(Color tint)
	{
		this.tint = tint;
	}

	public void render(SpriteBatch spriteBatch, FontRenderer frender)
	{
		if (isHovering())
		{
			spriteBatch.setTint(tint.scale(0.5f));
		} else
		{
			spriteBatch.setTint(tint);
		}

		super.render(spriteBatch);
		spriteBatch.reset();
		if (text != null && font != null)
		{
			int textWidth = font.getStringWidth(text);
			int textHeight = font.getCharacterHeight();

			int textX = (int) (getNormalTexture().getWidth() * getScaleX() - textWidth) / 2;
			int textY = (int) (getNormalTexture().getHeight() * getScaleY() - textHeight) / 2;

			frender.setColor(Color.WHITE);
			frender.render(font, text, getScreenX() + textX, getScreenY() + textY);
		}
	}
}
