package quantumepoch.graphics.gui;

import org.lwjgl.input.Keyboard;

import quantumepoch.core.Core;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.batch.SpriteBatch;
import quantumepoch.graphics.font.FontRenderer;
import quantumepoch.graphics.font.TrueTypeFont;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * A basic text field used to enter text data.
 */
public class BasicTextField extends Component
{

	public static final int HORIZONTAL_SPACING = 10;

	private int width;
	private int height;
	private String text;
	private TextureRegion bg;
	private float scaleX;
	private float scaleY;
	private TrueTypeFont font;
	private Color color;

	private Color textColor;

	public BasicTextField(Component parent, int x, int y, int width, int height, String text, TrueTypeFont font, Color color, Color textColor, TextureRegion bg, float scaleX, float scaleY)
	{
		super(parent, x, y);
		this.width = width;
		this.height = height;
		this.text = text;
		this.font = font;
		this.color = color;
		this.textColor = textColor;
		this.bg = bg;
		this.scaleX = scaleX;
		this.scaleY = scaleY;
	}

	public float getScaleX()
	{
		return scaleX;
	}

	public TextureRegion getBg()
	{
		return bg;
	}

	public void setBg(TextureRegion bg)
	{
		this.bg = bg;
	}

	public void setScaleX(float scaleX)
	{
		this.scaleX = scaleX;
	}

	public float getScaleY()
	{
		return scaleY;
	}

	public void setScaleY(float scaleY)
	{
		this.scaleY = scaleY;
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public String getText()
	{
		return text;
	}

	public TrueTypeFont getFont()
	{
		return font;
	}

	public Color getColor()
	{
		return color;
	}

	public Color getTextColor()
	{
		return textColor;
	}

	public void setWidth(int width)
	{
		this.width = width;
	}

	public void setHeight(int height)
	{
		this.height = height;
	}

	public void setText(String text)
	{
		this.text = text;
	}

	public void setFont(TrueTypeFont font)
	{
		this.font = font;
	}

	public void setColor(Color color)
	{
		this.color = color;
	}

	public void setTextColor(Color textColor)
	{
		this.textColor = textColor;
	}

	@Override
	public void update()
	{
		int mx = Core.getInput().getMouse().getX();
		int my = Core.getInput().getMouse().getY();

		if (Core.getInput().getMouse().wasButtonPressed(0))
		{
			if (mx >= getScreenX() && mx <= getScreenX() + width && my >= getScreenY() && my <= getScreenY() + height)
			{
				setFocused(this);

				// clear out the key events
				while (org.lwjgl.input.Keyboard.next())
				{

				}
			}
		}

		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_RETURN))
		{
			setFocused(getParent());
		}

		while (org.lwjgl.input.Keyboard.next())
		{
			char c = org.lwjgl.input.Keyboard.getEventCharacter();
			if (c != 0 && c != '\n' && c != '\r')
			{
				if (c == 8)
				{
					if (text.length() > 0)
					{
						text = text.substring(0, text.length() - 1);
					}
				} else
				{
					text += c;
				}
			}
		}
	}

	public void render(SpriteBatch batch, FontRenderer fontRenderer)
	{

		batch.setTint(Color.WHITE);
		batch.render(bg, 0, 0, getScreenX(), getScreenY(), 0, scaleX, scaleY);

		int width = font.getStringWidth(text);
		String display = "";

		if (width >= this.width)
		{
			int totalWidth = 0;
			for (int i = text.length() - 1; i >= 0 && totalWidth <= this.width - 2 * HORIZONTAL_SPACING; i--)
			{
				char c = text.charAt(i);
				totalWidth += font.getCharacterWidth(c);
				display = c + display;
			}
		} else
		{
			display = text;
		}

		batch.reset();

		fontRenderer.setColor(textColor);
		fontRenderer.render(font, display, getScreenX() + HORIZONTAL_SPACING, getScreenY());
	}

}
