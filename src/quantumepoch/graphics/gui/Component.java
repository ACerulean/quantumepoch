package quantumepoch.graphics.gui;

import java.util.ArrayList;
import java.util.List;

/**
 * An abstraction over all GUI components which can have children and relative positions.
 */
public abstract class Component
{

	private static Component focused;

	private int relativeX;
	private int relativeY;
	private Component parent;
	private List<Component> children;
	private boolean hidden;

	public Component()
	{
		this(null, 0, 0);
	}

	public Component(Component parent, int relativeX, int relativeY)
	{
		this.relativeX = relativeX;
		this.relativeY = relativeY;
		this.parent = parent;
		this.children = new ArrayList<Component>();
		this.hidden = false;
	}

	public static Component getFocused()
	{
		return focused;
	}

	public static void setFocused(Component focused)
	{
		Component.focused = focused;
	}

	public int getRelativeX()
	{
		return relativeX;
	}

	public int getRelativeY()
	{
		return relativeY;
	}

	public int getScreenX()
	{
		if (parent == null)
		{
			return relativeX;
		} else
		{
			return parent.getScreenX() + relativeX;
		}
	}

	public int getScreenY()
	{
		if (parent == null)
		{
			return relativeY;
		} else
		{
			return parent.getScreenY() + relativeY;
		}
	}

	public List<Component> getChildren()
	{
		return children;
	}

	public Component getParent()
	{
		return parent;
	}

	public boolean isHidden()
	{
		return hidden;
	}

	public void setRelativeX(int relativeX)
	{
		this.relativeX = relativeX;
	}

	public void setRelativeY(int relativeY)
	{
		this.relativeY = relativeY;
	}

	public void setHidden(boolean hidden)
	{
		this.hidden = hidden;
	}

	public abstract void update();

}
