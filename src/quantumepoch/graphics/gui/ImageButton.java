package quantumepoch.graphics.gui;

import quantumepoch.audio.AudioSource;
import quantumepoch.core.Core;
import quantumepoch.graphics.batch.SpriteBatch;
import quantumepoch.graphics.texture.TextureRegion;
import quantumepoch.physics.BoundingBox2D;
import quantumepoch.utils.math.Vector2f;

/**
 * A button with a single image on it.
 */
public class ImageButton extends Component
{

	private TextureRegion normalTexture;
	private TextureRegion hoveredTexture;
	private TextureRegion pressedTexture;
	private boolean hovering;
	private boolean pressed;
	private boolean wasPressed;
	private BoundingBox2D boundingBox;
	private float scaleX = 1, scaleY = 1;
	private AudioSource blip;

	public ImageButton()
	{
		this(null, 0, 0, null, null, null, null);
	}

	public ImageButton(Component parent, int relativeX, int relativeY, TextureRegion normalTexture, TextureRegion hoveredTexture, TextureRegion pressedTexture, AudioSource blip)
	{
		this(parent, relativeX, relativeY, normalTexture, hoveredTexture, pressedTexture, new BoundingBox2D(new Vector2f(relativeX, relativeY), normalTexture.getWidth(), normalTexture.getHeight()), blip);
	}

	public ImageButton(Component parent, int relativeX, int relativeY, TextureRegion normalTexture, TextureRegion hoveredTexture, TextureRegion pressedTexture, BoundingBox2D boundingBox, AudioSource blip)
	{
		super(parent, relativeX, relativeY);
		this.normalTexture = normalTexture;
		this.hoveredTexture = hoveredTexture;
		this.pressedTexture = pressedTexture;
		this.boundingBox = boundingBox;
		this.blip = blip;
	}

	public float getScaleX()
	{
		return scaleX;
	}

	public void setScaleX(float scaleX)
	{
		this.scaleX = scaleX;
	}

	public float getScaleY()
	{
		return scaleY;
	}

	public void setScaleY(float scaleY)
	{
		this.scaleY = scaleY;
	}

	public TextureRegion getNormalTexture()
	{
		return normalTexture;
	}

	public TextureRegion getHoveredTexture()
	{
		return hoveredTexture;
	}

	public TextureRegion getPressedTexture()
	{
		return pressedTexture;
	}

	public boolean isHovering()
	{
		return hovering;
	}

	public boolean isPressed()
	{
		return pressed;
	}

	public boolean wasPressed()
	{
		return wasPressed;
	}

	public BoundingBox2D getBoundingBox()
	{
		return boundingBox;
	}

	public void setNormalTexture(TextureRegion normalTexture)
	{
		this.normalTexture = normalTexture;
	}

	public void setHoveredTexture(TextureRegion hoveredTexture)
	{
		this.hoveredTexture = hoveredTexture;
	}

	public void setPressedTexture(TextureRegion pressedTexture)
	{
		this.pressedTexture = pressedTexture;
	}

	public void setBoundingBox(BoundingBox2D boundingBox)
	{
		this.boundingBox = boundingBox;
	}

	public void setHovering(boolean hovering)
	{
		this.hovering = hovering;
	}

	public void setPressed(boolean p)
	{
		pressed = p;
	}

	@Override
	public void update()
	{
		if (getFocused() == null || getFocused() == this || getFocused() == getParent())
		{

			int mx = Core.getInput().getMouse().getX();
			int my = Core.getInput().getMouse().getY();
			int x = getScreenX();
			int y = getScreenY();
			int w = (int) boundingBox.getWidth();
			int h = (int) boundingBox.getHeight();

			boolean intersects = true;
			if (mx < x || my < y || mx > x + w * scaleX || my > y + h * scaleY)
			{
				intersects = false;
			}
			if (!hovering && intersects)
				blip.play();
			hovering = intersects;
			if (hovering && Core.getInput().getMouse().isButtonDown(0))
			{
				pressed = true;
			} else
			{
				pressed = false;
			}

			if (hovering && Core.getInput().getMouse().wasButtonPressed(0))
				wasPressed = true;
			else
				wasPressed = false;
		} else
		{
			hovering = false;
			pressed = false;
		}
	}

	public void render(SpriteBatch spriteBatch)
	{
		TextureRegion toRender = null;
		if (!hovering)
		{
			toRender = normalTexture;
		} else if (hovering && !pressed)
		{
			toRender = hoveredTexture;
		} else if (pressed)
		{
			toRender = pressedTexture;
		}
		spriteBatch.render(toRender, 0, 0, getScreenX(), getScreenY(), 0, scaleX, scaleY);
	}

}
