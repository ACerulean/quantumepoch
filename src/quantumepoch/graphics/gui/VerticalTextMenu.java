package quantumepoch.graphics.gui;

import java.util.ArrayList;
import java.util.List;

import quantumepoch.graphics.Color;
import quantumepoch.graphics.font.FontRenderer;
import quantumepoch.graphics.font.FontType;
import quantumepoch.graphics.font.TrueTypeFont;

/**
 * A menu that will allow the user to select from a list of text.
 */
public class VerticalTextMenu
{

	private int x;
	private int y;
	private int verticleSpacing;
	private TrueTypeFont notSelectedFont;
	private TrueTypeFont selectedFont;
	private Color selectedColor;
	private Color notSelectedColor;
	private List<String> entries;
	private int selected;

	public VerticalTextMenu()
	{
		entries = new ArrayList<>();
		selected = 0;

		notSelectedFont = new TrueTypeFont(FontType.COMIC_SANS, 40, true);
		selectedFont = new TrueTypeFont(FontType.COMIC_SANS, 45, true);

		selectedColor = Color.RED;
		notSelectedColor = Color.WHITE;

	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	public int getVerticleSpacing()
	{
		return verticleSpacing;
	}

	public TrueTypeFont getNotSelectedFont()
	{
		return notSelectedFont;
	}

	public TrueTypeFont getSelectedFont()
	{
		return selectedFont;
	}

	public Color getSelectedColor()
	{
		return selectedColor;
	}

	public Color getNotSelectedColor()
	{
		return notSelectedColor;
	}

	public List<String> getEntries()
	{
		return entries;
	}

	public int getSelected()
	{
		return selected;
	}

	public void setX(int x)
	{
		this.x = x;
	}

	public void setY(int y)
	{
		this.y = y;
	}

	public void setVerticleSpacing(int verticleSpacing)
	{
		this.verticleSpacing = verticleSpacing;
	}

	public void setNotSelectedFont(TrueTypeFont notSelectedFont)
	{
		this.notSelectedFont = notSelectedFont;
	}

	public void setSelectedFont(TrueTypeFont selectedFont)
	{
		this.selectedFont = selectedFont;
	}

	public void setSelectedColor(Color selectedColor)
	{
		this.selectedColor = selectedColor;
	}

	public void setNotSelectedColor(Color notSelectedColor)
	{
		this.notSelectedColor = notSelectedColor;
	}

	public void setEntries(List<String> entries)
	{
		this.entries = entries;
	}

	public void setSelected(int selected)
	{
		this.selected = selected;
	}

	public void render(FontRenderer fontRenderer)
	{
		int currentY = y;
		for (int i = entries.size() - 1; i >= 0; i--)
		{
			if (i == selected)
			{
				fontRenderer.setColor(selectedColor);
				fontRenderer.render(selectedFont, entries.get(i), x, currentY);
			} else
			{
				fontRenderer.setColor(notSelectedColor);
				fontRenderer.render(notSelectedFont, entries.get(i), x, currentY);
			}
			currentY += verticleSpacing;
		}
	}

}
