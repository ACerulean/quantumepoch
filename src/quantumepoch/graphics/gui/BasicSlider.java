package quantumepoch.graphics.gui;

import quantumepoch.core.Core;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.batch.ShapeBatch;
import quantumepoch.utils.math.Vector2f;

/**
 * A slider used for adjusting options in-game.
 */
public class BasicSlider extends Component
{

	public static final int SLIDER_PIECE_WIDTH = 10;
	public static final int SLIDER_PIECE_HEIGHT = 20;

	private int pieceX;
	private int width;
	private Color lineColor;
	private Color sliderPieceColor;
	private boolean mouseDown;

	public BasicSlider(int width, int sliderX, int sliderY, Color lineColor, Color sliderPieceColor)
	{
		super(null, sliderX, sliderY);
		this.width = width;
		this.lineColor = lineColor;
		this.sliderPieceColor = sliderPieceColor;
		pieceX = sliderX;
	}

	public int getPieceX()
	{
		return pieceX;
	}

	public void setPieceX(int pieceX)
	{
		this.pieceX = pieceX;
	}

	public int getWidth()
	{
		return width;
	}

	public void setWidth(int width)
	{
		this.width = width;
	}

	public Color getLineColor()
	{
		return lineColor;
	}

	public void setLineColor(Color lineColor)
	{
		this.lineColor = lineColor;
	}

	public Color getSliderPieceColor()
	{
		return sliderPieceColor;
	}

	public void setSliderPieceColor(Color sliderPieceColor)
	{
		this.sliderPieceColor = sliderPieceColor;
	}

	@Override
	public void update()
	{
		boolean before = mouseDown;
		Vector2f mouse = new Vector2f(Core.getInput().getMouse().getX(), Core.getInput().getMouse().getY());
		if (!mouseDown && Core.getInput().getMouse().isButtonDown(0) && mouse.getX() >= getScreenX() && mouse.getX() < getScreenX() + width && mouse.getY() >= getScreenY() && mouse.getY() < getScreenY() + SLIDER_PIECE_HEIGHT)
			mouseDown = true;
		else if (mouseDown && !Core.getInput().getMouse().isButtonDown(0))
			mouseDown = false;

		if (!before && mouseDown)
		{
			Core.getInput().getMouse().resetDX();
			Core.getInput().getMouse().resetDY();
		}

		if (mouseDown)
		{
			// in bounds
			pieceX += Core.getInput().getMouse().getDX();
			if (pieceX < getScreenX())
				pieceX = getScreenX();
			if (pieceX > getScreenX() + width)
				pieceX = getScreenX() + width;
		}
	}

	public void setNormalizedSliderPosition(float pos)
	{
		if (pos < 0)
			pieceX = 0;
		else if (pos > 1)
			pieceX = getScreenX() + width;
		else
		{
			pieceX = getScreenX() + (int) (width * pos);
		}
	}

	public float getNormalizedSliderPosition()
	{
		return (float) (pieceX - getScreenX()) / width;
	}

	public void render(ShapeBatch shapeBatch)
	{
		shapeBatch.reset();
		shapeBatch.setColor(lineColor);
		shapeBatch.renderLine(getScreenX(), getScreenY() + SLIDER_PIECE_HEIGHT / 2, getScreenX() + width, getScreenY() + SLIDER_PIECE_HEIGHT / 2);

		shapeBatch.setColor(sliderPieceColor);
		shapeBatch.renderFilledRect(pieceX - SLIDER_PIECE_WIDTH / 2, getScreenY(), SLIDER_PIECE_WIDTH, SLIDER_PIECE_HEIGHT);
		shapeBatch.end();
	}
}
