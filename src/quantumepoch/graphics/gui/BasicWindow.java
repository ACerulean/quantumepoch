package quantumepoch.graphics.gui;

import quantumepoch.core.Core;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.batch.ShapeBatch;
import quantumepoch.graphics.batch.SpriteBatch;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * A basic draggable window that can have sub components.
 */
public class BasicWindow extends Component
{

	public static final int TOP_BAR_HEIGHT = 20;

	private int width;
	private int height;
	protected TextureRegion bg;
	protected float scaleX;
	protected float scaleY;
	private Color topBarColor;
	private Color windowColor;
	private boolean grabbed = false;
	private int prevX = Core.getInput().getMouse().getX();
	private int prevY = Core.getInput().getMouse().getY();

	public BasicWindow(Color topBarColor, Color windowColor, int x, int y, int width, int height, TextureRegion bg, float sx, float sy)
	{
		super(null, x, y);
		this.width = width;
		this.height = height;
		this.topBarColor = topBarColor;
		this.windowColor = windowColor;
		this.bg = bg;
		this.scaleX = sx;
		this.scaleY = sy;
	}

	public int getWidth()
	{
		return width;
	}

	public void setWidth(int width)
	{
		this.width = width;
	}

	public int getHeight()
	{
		return height;
	}

	public void setHeight(int height)
	{
		this.height = height;
	}

	public Color getTopBarColor()
	{
		return topBarColor;
	}

	public void setTopBarColor(Color topBarColor)
	{
		this.topBarColor = topBarColor;
	}

	public Color getWindowColor()
	{
		return windowColor;
	}

	public void setWindowColor(Color windowColor)
	{
		this.windowColor = windowColor;
	}

	public static int getTopBarHeight()
	{
		return TOP_BAR_HEIGHT;
	}

	@Override
	public void update()
	{

		// diff
		int mx = Core.getInput().getMouse().getX();
		int my = Core.getInput().getMouse().getY();

		if (!grabbed && !Core.getInput().getMouse().isButtonDown(0))
		{
			grabbed = true;
			prevX = mx;
			prevY = my;
		} else
		{
			grabbed = false;
		}

		if (Core.getInput().getMouse().wasButtonPressed(0))
		{
			if (mx >= getScreenX() && mx <= getScreenX() + width && my >= getScreenY() && my <= getScreenY() + height)
			{
				setFocused(this);
			} else
			{
				setFocused(null);
			}
		}

		if (getFocused() == this)
		{
			if (Core.getInput().getMouse().isButtonDown(0))
			{

				int dx = mx - prevX;
				int dy = my - prevY;
				setRelativeX(getRelativeX() + dx);
				setRelativeY(getRelativeY() + dy);

				prevX = mx;
				prevY = my;
			}
		}

	}

	public void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch)
	{
		spriteBatch.setTint(Color.WHITE);
		spriteBatch.render(bg, 0, 0, getScreenX(), getScreenY(), 0, scaleX, scaleY);
		if (getFocused() == this)
		{
			shapeBatch.setColor(Color.BLUE);
			shapeBatch.renderRect(getScreenX() - 1, getScreenY() - 1, width + 1, height + 1, 0, 0, 0);
		}
	}

}
