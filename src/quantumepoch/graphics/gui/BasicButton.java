package quantumepoch.graphics.gui;

import quantumepoch.audio.AudioSource;
import quantumepoch.core.Core;
import quantumepoch.game.Game;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.batch.ShapeBatch;
import quantumepoch.graphics.font.FontRenderer;
import quantumepoch.graphics.font.TrueTypeFont;

/**
 * A button that has text, a color and some text.
 */
public class BasicButton extends Component
{

	private String text;
	private TrueTypeFont font;
	private Color color;
	private int width;
	private int height;
	private boolean hovering;
	private boolean pressed;
	private AudioSource blip;
	private boolean playedBlip;

	public BasicButton(Component parent, int x, int y, String name, Color col, int pad)
	{
		super(parent, x, y);

		text = name;

		font = Game.CALIBRI_30;
		color = col;

		width = font.getStringWidth(name) + pad;
		height = font.getCharacterHeight();

		this.blip = Game.getInstance().getBlip();
	}

	public BasicButton(Component parent, int x, int y, String text, TrueTypeFont font, Color color, int width, int height)
	{
		super(parent, x, y);
		this.text = text;
		this.font = font;
		this.color = color;
		this.width = width;
		this.height = height;
		this.blip = Game.getInstance().getBlip();
	}

	public String getText()
	{
		return text;
	}

	public TrueTypeFont getFont()
	{
		return font;
	}

	public Color getColor()
	{
		return color;
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public boolean isHovering()
	{
		return hovering;
	}

	public boolean isPressed()
	{
		return pressed;
	}

	public void setText(String text)
	{
		this.text = text;
	}

	public void setFont(TrueTypeFont font)
	{
		this.font = font;
	}

	public void setColor(Color color)
	{
		this.color = color;
	}

	public void setWidth(int width)
	{
		this.width = width;
	}

	public void setHeight(int height)
	{
		this.height = height;
	}

	public void setHovering(boolean hovering)
	{
		this.hovering = hovering;
	}

	public void setPressed(boolean pressed)
	{
		this.pressed = pressed;
	}

	@Override
	public void update()
	{
		if (getFocused() == null || (getParent() != null && getFocused() == getParent()) || (getParent() == null && getFocused() == null))
		{
			int mx = Core.getInput().getMouse().getX();
			int my = Core.getInput().getMouse().getY();
			boolean intersects = true;
			if (mx < getScreenX() || my < getScreenY() || mx > getScreenX() + width || my > getScreenY() + height)
			{
				intersects = false;
			}
			hovering = intersects;

			if (hovering && Core.getInput().getMouse().isButtonDown(0))
			{
				pressed = true;
			} else
			{
				pressed = false;
			}
		} else
		{
			hovering = false;
			pressed = false;
		}

		if (hovering)
		{
			if (!playedBlip)
			{
				playedBlip = true;
				blip.play();
			}
		} else
		{
			playedBlip = false;
		}
	}

	public void render(ShapeBatch batch, FontRenderer fontRenderer)
	{
		batch.setColor(color);
		batch.renderFilledRect(getScreenX(), getScreenY(), width, height);

		if (hovering)
		{
			batch.setColor(Color.BLUE);
			batch.renderRect(getScreenX() - 1, getScreenY() - 1, width + 1, height + 1, 0, 0, 0);
		}

		batch.reset();

		int textWidth = font.getStringWidth(text);
		int textHeight = font.getCharacterHeight();

		int textX = (width - textWidth) / 2;
		int textY = (height - textHeight) / 2;

		fontRenderer.setColor(Color.WHITE);
		fontRenderer.render(font, text, getScreenX() + textX, getScreenY() + textY);
	}
}
