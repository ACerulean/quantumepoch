package quantumepoch.graphics.shader.forward;

import quantumepoch.graphics.VertexAttrib;
import quantumepoch.graphics.shader.ShaderProgram;
import quantumepoch.utils.math.Matrix4f;

/**
 * Used in the forward rendering pipeline to render text to the screen.
 */
public class ForwardText extends ShaderProgram
{

	/**
	 * Creates a new forward text shader.
	 */
	public ForwardText()
	{
		super(new VertexAttrib[] { ATTRIBUTE_POSITION, ATTRIBUTE_COLOR, ATTRIBUTE_TEXCOORD, ATTRIBUTE_NORMAL });
	}

	/**
	 * Creates a new forward text shader
	 * 
	 * @param vertexShaderPath
	 * @param fragmentShaderPath
	 * @param link
	 */
	public ForwardText(String vertexShaderPath, String fragmentShaderPath, boolean link)
	{
		super(new VertexAttrib[] { ATTRIBUTE_POSITION, ATTRIBUTE_COLOR, ATTRIBUTE_TEXCOORD, ATTRIBUTE_NORMAL }, vertexShaderPath, fragmentShaderPath, link);
	}

	/**
	 * Updates existing uniforms
	 * 
	 * @param viewProjectionMatrix
	 * @param transform
	 * @param mode
	 */
	public void updateUniforms(Matrix4f viewProjectionMatrix, Matrix4f transform, int mode)
	{
		super.updateUniforms(viewProjectionMatrix, transform);
		setInteger("mode", mode);
	}

}
