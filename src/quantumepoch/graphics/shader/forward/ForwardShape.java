package quantumepoch.graphics.shader.forward;

import quantumepoch.graphics.VertexAttrib;
import quantumepoch.graphics.shader.ShaderProgram;

/**
 * Used in the forward rendering pipeline to render specific shapes.
 */
public class ForwardShape extends ShaderProgram
{

	/**
	 * Creates a new forward shape shader.
	 */
	public ForwardShape()
	{
		super(new VertexAttrib[] { ATTRIBUTE_POSITION, ATTRIBUTE_COLOR, ATTRIBUTE_TEXCOORD, ATTRIBUTE_NORMAL });
	}

	/**
	 * Creates a new forward shape shader.
	 * 
	 * @param vertexShaderPath
	 * @param fragmentShaderPath
	 * @param link
	 */
	public ForwardShape(String vertexShaderPath, String fragmentShaderPath, boolean link)
	{
		super(new VertexAttrib[] { ATTRIBUTE_POSITION, ATTRIBUTE_COLOR, ATTRIBUTE_TEXCOORD, ATTRIBUTE_NORMAL }, vertexShaderPath, fragmentShaderPath, link);
	}
}
