package quantumepoch.graphics.shader.forward;

import quantumepoch.graphics.VertexAttrib;
import quantumepoch.graphics.shader.ShaderProgram;
import quantumepoch.utils.math.Matrix4f;
import quantumepoch.utils.math.Vector3f;

/**
 * A shader used to render 2d sprites and ambient lighting.
 */
public class ForwardAmbient extends ShaderProgram
{

	/** the ambient uniform name */
	public static final String UNIFORM_AMBIENT = "u_ambient";

	/**
	 * Creates a new forward ambient shader
	 */
	public ForwardAmbient()
	{
		super(new VertexAttrib[] { ATTRIBUTE_POSITION, ATTRIBUTE_COLOR, ATTRIBUTE_TEXCOORD, ATTRIBUTE_NORMAL });
	}

	/**
	 * Creates a new forward ambient shader.
	 * 
	 * @param vertexShaderPath
	 * @param fragmentShaderPath
	 * @param link
	 */
	public ForwardAmbient(String vertexShaderPath, String fragmentShaderPath, boolean link)
	{
		super(new VertexAttrib[] { ATTRIBUTE_POSITION, ATTRIBUTE_COLOR, ATTRIBUTE_TEXCOORD, ATTRIBUTE_NORMAL }, vertexShaderPath, fragmentShaderPath, link);
	}

	/**
	 * Updates shader uniform values
	 * 
	 * @param viewProjectionMatrix
	 * @param transform
	 * @param ambient
	 */
	public void updateUniforms(Matrix4f viewProjectionMatrix, Matrix4f transform, Vector3f ambient)
	{
		super.updateUniforms(viewProjectionMatrix, transform);
		setVector3f(UNIFORM_AMBIENT, ambient);
	}

}
