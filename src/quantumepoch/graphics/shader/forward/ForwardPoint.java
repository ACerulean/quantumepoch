package quantumepoch.graphics.shader.forward;

import quantumepoch.graphics.VertexAttrib;
import quantumepoch.graphics.lighting.PointLight;
import quantumepoch.graphics.shader.ShaderProgram;
import quantumepoch.utils.math.Matrix4f;
import quantumepoch.utils.math.Vector2f;

/**
 * Used in the forward rendering pipeline to render point lights over existing scenes.
 */
public class ForwardPoint extends ShaderProgram
{

	public static final String UNIFORM_POSITION = "u_position";
	public static final String UNIFORM_COLOR = "u_color";
	public static final String UNIFORM_TRANSLATE = "u_translate";
	public static final String UNIFORM_INTENSITY = "u_intensity";
	public static final String UNIFORM_QUADRATIC_ATTEN = "u_quadraticAtten";
	public static final String UNIFORM_LINEAR_ATTEN = "u_linearAtten";
	public static final String UNIFORM_CONSTANT_ATTEN = "u_constantAtten";

	/**
	 * Creates a new forward point light shader.
	 */
	public ForwardPoint()
	{
		super(new VertexAttrib[] { ATTRIBUTE_POSITION, ATTRIBUTE_COLOR, ATTRIBUTE_TEXCOORD, ATTRIBUTE_NORMAL });
	}

	/**
	 * Creates a new forward point light shader
	 * 
	 * @param vertexShaderPath
	 * @param fragmentShaderPath
	 * @param link
	 */
	public ForwardPoint(String vertexShaderPath, String fragmentShaderPath, boolean link)
	{
		super(new VertexAttrib[] { ATTRIBUTE_POSITION, ATTRIBUTE_COLOR, ATTRIBUTE_TEXCOORD, ATTRIBUTE_NORMAL }, vertexShaderPath, fragmentShaderPath, link);
	}

	/**
	 * Updates the existing shader uniform variables.
	 * 
	 * @param viewProjectionMatrix
	 * @param model
	 * @param light
	 */
	public void updateUniforms(Matrix4f viewProjectionMatrix, Matrix4f model, PointLight light)
	{
		updateUniforms(viewProjectionMatrix, model, light, null);
	}

	/**
	 * Updates the existing shader uniform variables.
	 * 
	 * @param viewProjectionMatrix
	 * @param model
	 * @param light
	 * @param translate
	 */
	public void updateUniforms(Matrix4f viewProjectionMatrix, Matrix4f model, PointLight light, Vector2f translate)
	{
		super.updateUniforms(viewProjectionMatrix, model);
		setVector3f(UNIFORM_POSITION, light.getPosition());
		setVector3f(UNIFORM_COLOR, light.getColor().toVector3f());
		if (translate != null)
		{
			setVector2f(UNIFORM_TRANSLATE, translate);
		}
		setFloat(UNIFORM_INTENSITY, light.getIntensity());
		setFloat(UNIFORM_QUADRATIC_ATTEN, light.getQuadAtten());
		setFloat(UNIFORM_LINEAR_ATTEN, light.getLinearAtten());
		setFloat(UNIFORM_CONSTANT_ATTEN, light.getConstAtten());
	}

}
