package quantumepoch.graphics.shader;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_INT;
import static org.lwjgl.opengl.GL20.GL_BOOL;

/**
 * Stores all possible OpenGL data types.
 */
public enum DataType
{

	VOID(0, 0, 0), BOOL(1, 1, GL_BOOL), INT(1, 4, GL_INT), FLOAT(1, 4, GL_FLOAT), VEC2F(2, 4, GL_FLOAT), VEC2I(2, 4, GL_INT), VEC3F(3, 4, GL_FLOAT), VEC3I(3, 4, GL_INT), VEC4F(4, 4, GL_FLOAT), VEC4I(4, 4, GL_INT);

	/** the amount of elements contained within this gl data type */
	private int elementCount;
	/** the size of each element contained within this data type */
	private int elementSize;
	/** the gl type of this data */
	private int glType;

	/**
	 * Constructs a new data type.
	 * 
	 * @param elementCount
	 *            The amount of elements in this data type
	 * @param elementSize
	 *            The size of each element
	 * @param glType
	 *            The GLSL type of element
	 */
	DataType(int elementCount, int elementSize, int glType)
	{
		this.elementCount = elementCount;
		this.elementSize = elementSize;
		this.glType = glType;
	}

	/**
	 * @return The amount of elements in this data type
	 */
	public int getElementCount()
	{
		return elementCount;
	}

	/**
	 * @return the size of each element in this data type
	 */
	public int getElementSize()
	{
		return elementSize;
	}

	/**
	 * @return the GLSL int id of the data type
	 */
	public int getGlType()
	{
		return glType;
	}

	/**
	 * @return the total size of this data type in bytes
	 */
	public int totalSize()
	{
		return elementCount * elementSize;
	}

}
