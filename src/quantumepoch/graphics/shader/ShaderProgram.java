package quantumepoch.graphics.shader;

import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL20.GL_COMPILE_STATUS;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_LINK_STATUS;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glBindAttribLocation;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glGetProgramInfoLog;
import static org.lwjgl.opengl.GL20.glGetProgrami;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;
import static org.lwjgl.opengl.GL20.glGetShaderi;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glUniform1f;
import static org.lwjgl.opengl.GL20.glUniform1i;
import static org.lwjgl.opengl.GL20.glUniform2f;
import static org.lwjgl.opengl.GL20.glUniform3f;
import static org.lwjgl.opengl.GL20.glUniform4f;
import static org.lwjgl.opengl.GL20.glUniformMatrix2;
import static org.lwjgl.opengl.GL20.glUniformMatrix3;
import static org.lwjgl.opengl.GL20.glUniformMatrix4;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL20.glValidateProgram;
import quantumepoch.core.Core;
import quantumepoch.graphics.VertexArray;
import quantumepoch.graphics.VertexAttrib;
import quantumepoch.utils.IOUtils;
import quantumepoch.utils.math.Matrix2f;
import quantumepoch.utils.math.Matrix3f;
import quantumepoch.utils.math.Matrix4f;
import quantumepoch.utils.math.Vector2f;
import quantumepoch.utils.math.Vector3f;
import quantumepoch.utils.math.Vector4f;

/**
 * A shader is a program which can be executed on the GPU with the aid of OpenGL. This class acts as an intuitive interface between OpenGL's native shader methods and the end user for easy use.
 */
public class ShaderProgram
{

	/** the maximum error log size */
	public static final int MAX_ERR_LOG_SIZE = 10000;

	// Uniforms
	public static final String UNIFORM_MODEL_MATRIX = "u_modelMatrix";
	public static final String UNIFORM_VIEWPROJECTION_MATRIX = "u_viewProjectionMatrix";
	public static final String UNIFORM_MODE = "mode";

	// Attributes
	public static final VertexAttrib ATTRIBUTE_POSITION = new VertexAttrib("a_position", 0, DataType.VEC3F);
	public static final VertexAttrib ATTRIBUTE_COLOR = new VertexAttrib("a_color", 1, DataType.VEC4F);
	public static final VertexAttrib ATTRIBUTE_TEXCOORD = new VertexAttrib("a_texCoord", 2, DataType.VEC2F);
	public static final VertexAttrib ATTRIBUTE_NORMAL = new VertexAttrib("a_normal", 3, DataType.VEC3F);

	/** the opengl handle of this shader program */
	private int programHandle;
	/** the opengl handle of the vertex shader */
	private int vertexShaderHandle;
	/** the opengl handle of the fragment shader */
	private int fragmentShaderHandle;
	/** the general log output of this shader */
	private String log;
	/** the vertex array this shader uses */
	private VertexArray vao;
	/** the source code of the vertex shader */
	private String vertexShaderSource;
	/** the source code of the fragment shader */
	private String fragmentShaderSource;

	/**
	 * Creates a new shader program with no source and vertex attribs.
	 */
	public ShaderProgram()
	{
		this(null);
	}

	/**
	 * Creates a new shader program.
	 * 
	 * @param vertexAttribs
	 *            The attributes for every vertex passed into this shader
	 */
	public ShaderProgram(VertexAttrib[] vertexAttribs)
	{
		this(vertexAttribs, null, null, false);
	}

	/**
	 * Creates a new shader program.
	 * 
	 * @param vertexAttribs
	 *            The attributes for every vertex passed into this shader
	 * @param vertexShaderPath
	 *            The path to the vertex shader source code
	 * @param fragmentShaderPath
	 *            The path to the fragment shader source code.
	 * @param link
	 *            Whether or not to link the GLSL program on construction
	 */
	public ShaderProgram(VertexAttrib[] vertexAttribs, String vertexShaderPath, String fragmentShaderPath, boolean link)
	{
		this.log = "";
		this.vao = new VertexArray();

		createProgram();

		if (vertexAttribs != null)
		{
			for (VertexAttrib attrib : vertexAttribs)
			{
				vao.getVertexAttributes().add(attrib);
				bindVertexAttrib(attrib);
			}
		}

		if (vertexShaderPath != null)
		{
			createAndCompileVertexShader(vertexShaderPath);
		}

		if (fragmentShaderPath != null)
		{
			createAndCompileFragmentShader(fragmentShaderPath);
		}

		if (link)
		{
			linkProgram();
		}
	}

	/**
	 * @return the general output log of this shader as a string
	 */
	public String getProgramLog()
	{
		return log;
	}

	/**
	 * @return the vertex array this shader uses for data
	 */
	public VertexArray getVertexArray()
	{
		return vao;
	}

	/**
	 * Creates the program handle for this shader program.
	 */
	public void createProgram()
	{
		programHandle = glCreateProgram();
	}

	/**
	 * Binds a vertex attribute to the shader program.
	 * 
	 * @param vertexAttrib
	 *            The attrib to bind
	 */
	public void bindVertexAttrib(VertexAttrib vertexAttrib)
	{
		glBindAttribLocation(programHandle, vertexAttrib.getLocation(), vertexAttrib.getName());
	}

	/**
	 * Creates and compiles the vertex shader with the specified source. This method will output on compile time failure.
	 * 
	 * @param vertexShaderPath
	 *            The path to the source code
	 */
	public void createAndCompileVertexShader(String vertexShaderPath)
	{

		vertexShaderHandle = glCreateShader(GL_VERTEX_SHADER);
		vertexShaderSource = IOUtils.loadInternalFileAsString(vertexShaderPath);

		glShaderSource(vertexShaderHandle, vertexShaderSource);
		glCompileShader(vertexShaderHandle);

		glValidateProgram(programHandle);

		if (glGetShaderi(vertexShaderHandle, GL_COMPILE_STATUS) == GL_FALSE)
		{
			String result = glGetShaderInfoLog(vertexShaderHandle, MAX_ERR_LOG_SIZE);
			Core.getLog().fatal("Error compiling vertex shader: " + result);
			log += result;
			Core.getCore().stop();
		} else
		{
			// Core.getLog().info(IOUtils.getFileName(vertexShaderPath) + " (vertex) sucessfully compiled");
		}

		glAttachShader(programHandle, vertexShaderHandle);
	}

	/**
	 * Creates and compiles the fragment shader with the specified source. This method will output on compile time failure.
	 * 
	 * @param fragmentShaderPath
	 *            The path to the source code
	 */
	public void createAndCompileFragmentShader(String fragmentShaderPath)
	{

		fragmentShaderHandle = glCreateShader(GL_FRAGMENT_SHADER);
		fragmentShaderSource = IOUtils.loadInternalFileAsString(fragmentShaderPath);

		glShaderSource(fragmentShaderHandle, fragmentShaderSource);
		glCompileShader(fragmentShaderHandle);

		glValidateProgram(programHandle);

		if (glGetShaderi(fragmentShaderHandle, GL_COMPILE_STATUS) == GL_FALSE)
		{
			String result = glGetShaderInfoLog(fragmentShaderHandle, MAX_ERR_LOG_SIZE);
			Core.getLog().fatal("Error compiling fragment shader: " + result);
			log += result;
			Core.getCore().stop();
		} else
		{
			// Core.getLog().info(IOUtils.getFileName(fragmentShaderPath) + " (fragment) sucessfully compiled");
		}

		glAttachShader(programHandle, fragmentShaderHandle);
	}

	/**
	 * Links this shader program's various shaders together into a final GPU executable. This method will output on failure.
	 */
	public void linkProgram()
	{
		glLinkProgram(programHandle);

		glValidateProgram(programHandle);

		if (glGetProgrami(programHandle, GL_LINK_STATUS) == GL_FALSE)
		{
			String result = glGetProgramInfoLog(programHandle, MAX_ERR_LOG_SIZE);
			Core.getLog().fatal("Error whiles linking shader program:" + result);
			log += result;
			Core.getCore().stop();
		} else
		{
			// Core.getLog().info("Shader with handle " + programHandle + " sucessfully linked");
		}

	}

	/**
	 * Updates the value of shader uniforms.
	 * 
	 * @param viewProjectionMatrix
	 *            The viewProjection matrix to update the existing one with
	 * @param transform
	 *            The transformation matrix to apply to all vertices
	 */
	public void updateUniforms(Matrix4f viewProjectionMatrix, Matrix4f transform)
	{
		bind();
		setMatrix4f(UNIFORM_VIEWPROJECTION_MATRIX, viewProjectionMatrix);
		setMatrix4f(UNIFORM_MODEL_MATRIX, transform);
	}

	/**
	 * Binds this shader for OpenGL use.
	 */
	public void bind()
	{
		glUseProgram(programHandle);
	}

	/**
	 * @param name
	 *            The name of the uniform
	 * @return The index location of the uniform
	 */
	public int getUniformLocation(String name)
	{
		return glGetUniformLocation(programHandle, name);
	}

	/**
	 * Sets an int uniform in the shader
	 * 
	 * @param name
	 *            The name of the unifrm
	 * @param value
	 *            The new value
	 */
	public void setInteger(String name, int value)
	{
		glUniform1i(getUniformLocation(name), value);
	}

	/**
	 * Sets a float uniform in the shader
	 * 
	 * @param name
	 *            The name of the uniform
	 * @param value
	 *            The new value
	 */
	public void setFloat(String name, float value)
	{
		glUniform1f(getUniformLocation(name), value);
	}

	/**
	 * Sets a vector2f uniform in the shader
	 * 
	 * @param name
	 *            The name of the uniform
	 * @param value
	 *            The new value
	 */
	public void setVector2f(String name, Vector2f value)
	{
		glUniform2f(getUniformLocation(name), value.getX(), value.getY());
	}

	/**
	 * Sets a vector3f uniform in the shader
	 * 
	 * @param name
	 *            The name of the uniform
	 * @param value
	 *            The new value
	 */
	public void setVector3f(String name, Vector3f value)
	{
		glUniform3f(getUniformLocation(name), value.getX(), value.getY(), value.getZ());
	}

	/**
	 * Sets a vector4f uniform in the shader
	 * 
	 * @param name
	 *            The name of the uniform
	 * @param value
	 *            The new value
	 */
	public void setVector4f(String name, Vector4f value)
	{
		glUniform4f(getUniformLocation(name), value.getX(), value.getY(), value.getZ(), value.getW());
	}

	/**
	 * Sets a matrix2f uniform in the shader
	 * 
	 * @param name
	 *            The name of the uniform
	 * @param value
	 *            The new value
	 */
	public void setMatrix2f(String name, Matrix2f value)
	{
		glUniformMatrix2(getUniformLocation(name), false, IOUtils.mat2ToFloatBuffer(value));
	}

	/**
	 * Sets a matrix3f uniform in the shader
	 * 
	 * @param name
	 *            The name of the uniform
	 * @param value
	 *            The new value
	 */
	public void setMatrix3f(String name, Matrix3f value)
	{
		glUniformMatrix3(getUniformLocation(name), false, IOUtils.mat3ToFloatBuffer(value));
	}

	/**
	 * Sets a matrix4f uniform in the shader
	 * 
	 * @param name
	 *            The name of the uniform
	 * @param value
	 *            The new value
	 */
	public void setMatrix4f(String name, Matrix4f value)
	{
		glUniformMatrix4(getUniformLocation(name), false, IOUtils.mat4ToFloatBuffer(value));
	}

}
