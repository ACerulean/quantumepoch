package quantumepoch.graphics;

import quantumepoch.utils.math.Vector3f;

/**
 * Defines how a color should be represented in this engine. In this case, as an
 * RGBA value.
 */
public class Color
{

	public static final Color BLACK = new Color(0xFF000000);
	public static final Color WHITE = new Color(0xFFFFFFFF);
	public static final Color RED = new Color(0xFFFF0000);
	public static final Color GREEN = new Color(0xFF00FF00);
	public static final Color BLUE = new Color(0xFF0000FF);
	public static final Color LIGHT_BLUE = new Color(0xFFC8FFFF);
	public static final Color CYAN = new Color(0xFF00FFFF);
	public static final Color FUCHSIA = new Color(0xFFFF77FF);
	public static final Color LAVENDER = new Color(0xFFB57EDC);
	public static final Color INDIGO = new Color(0xFF4B0082);
	public static final Color GOLD = new Color(0xFFFFD700);
	public static final Color LIME_GREEN = new Color(0xFFBFFF00);
	public static final Color OLIVE = new Color(0xFF808000);
	public static final Color CORNFLOWER_BLUE = new Color(0xFF6495ED);
	public static final Color GRAY = new Color(0xFF808080);
	public static final Color PURPLE = new Color(0xFF800080);
	public static final Color ORANGE = new Color(0xFFFFA500);
	
	/** the red color value */
	private float red;
	/** the green color value */
	private float green;
	/** the blue color value */
	private float blue;
	/** the alpha color value */
	private float alpha;

	/**
	 * Creates a new color from an int. Format of int in little endian is as
	 * follows - first 8 bits define red, second 8 bits define red, third 8 bits
	 * define green and last 8 bits define blue.
	 * 
	 * @param hex
	 *            The value of the color
	 */
	public Color(int hex)
	{
		this((hex >> 16) & 0xFF, (hex >> 8) & 0xFF, (hex >> 0) & 0xFF, (hex >> 24) & 0xFF);
	}

	/**
	 * Creates a new color with a default alpha of 1
	 * 
	 * @param red
	 *            The red value
	 * @param green
	 *            The green value
	 * @param blue
	 *            The blue value
	 */
	public Color(float red, float green, float blue)
	{
		this(red, green, blue, 1.0f);
	}

	/**
	 * Creates a new color.
	 * 
	 * @param red
	 *            The red value
	 * @param green
	 *            The green value
	 * @param blue
	 *            The blue value
	 * @param alpha
	 *            The alpha value
	 */
	public Color(float red, float green, float blue, float alpha)
	{
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.alpha = alpha;

		if (red > 1)
			this.red /= 255f;
		if (green > 1)
			this.green /= 255f;
		if (blue > 1)
			this.blue /= 255f;
		if (alpha > 1)
			this.alpha /= 255f;

	}

	/**
	 * @return The hex color value
	 */
	public int getRGB()
	{
		return ((int) (alpha * 255) << 24) | ((int) (red * 255) << 16) | ((int) (green * 255) << 8) | ((int) (blue * 255) << 0);
	}

	/**
	 * @return the red value
	 */
	public float getRed()
	{
		return red;
	}

	/**
	 * @return the green value
	 */
	public float getGreen()
	{
		return green;
	}

	/**
	 * @return the blue value
	 */
	public float getBlue()
	{
		return blue;
	}

	/**
	 * @return the alpha value
	 */
	public float getAlpha()
	{
		return alpha;
	}

	/**
	 * Sets the value of this color to the specified int hex value.
	 * 
	 * @param hex
	 *            The int hex value for the color
	 */
	public void setRGB(int hex)
	{
		this.alpha = ((hex >> 24) & 0xFF) / 255;
		this.red = ((hex >> 16) & 0xFF) / 255;
		this.green = ((hex >> 8) & 0xFF) / 255;
		this.blue = ((hex >> 0) & 0xFF) / 255;
	}

	/**
	 * Sets the new red value of this color.
	 * 
	 * @param red
	 *            The new red color
	 */
	public void setRed(float red)
	{
		this.red = red;
	}

	/**
	 * Sets the new green color of this color.
	 * 
	 * @param green
	 *            The new green color
	 */
	public void setGreen(float green)
	{
		this.green = green;
	}

	/**
	 * Sets the new blue color of this color.
	 * 
	 * @param green
	 *            The new blue color
	 */
	public void setBlue(float blue)
	{
		this.blue = blue;
	}

	/**
	 * Sets the new alpha color of this color.
	 * 
	 * @param green
	 *            The new alpha color
	 */
	public void setAlpha(float alpha)
	{
		this.alpha = alpha;
	}

	/**
	 * @return A vector representation of this color
	 */
	public Vector3f toVector3f()
	{
		return new Vector3f(red, green, blue);
	}

	/**
	 * @param rgb
	 *            The RGB to extract the red value from
	 * @return The red value contained in the hex RGB
	 */
	public static int getRedFromRGB(int rgb)
	{
		return (rgb >> 16) & 0xFF;
	}

	/**
	 * @param rgb
	 *            The RGB to extract the green value from
	 * @return The green value contained in the hex RGB
	 */
	public static int getGreenFromRGB(int rgb)
	{
		return (rgb >> 8) & 0xFF;
	}

	/**
	 * @param rgb
	 *            The RGB to extract the blue value from
	 * @return The blue value contained in the hex RGB
	 */
	public static int getBlueFromRGB(int rgb)
	{
		return (rgb >> 0) & 0xFF;
	}

	/**
	 * Scales(multiplies) this color by the specified scale factor. Scaling
	 * works the same as vector scaling, where all components are multiplied by
	 * the specified value. This method does not mutate the data.
	 * 
	 * @param scale
	 *            The scale factor for the color
	 * @return Thw new color
	 */
	public Color scale(float scale)
	{
		return new Color(red * scale, green * scale, blue * scale, alpha * scale);
	}

	/**
	 * @return a String representation of this color as [a, r, g, b]
	 */
	@Override
	public String toString()
	{
		return (int) (getRed() * 255) + " " + (int) (getGreen() * 255) + " " + (int) (getBlue() * 255);
	}
}
