package quantumepoch.graphics;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import quantumepoch.core.Core;
import quantumepoch.utils.IOUtils;
import quantumepoch.utils.math.Vector3f;

/**
 * Represents a rectangular array of pixels. This class also contains a variety
 * of useful methods to make manipulations on bitmaps. Data is stored in
 * integers, containing the R G and B value of the pixel. This is used as a
 * simple form of compression. Most of the functionality in this class is
 * designed to make it easy to chain calls.
 */
public class Bitmap
{

	/** the width of the bitmap in pixels */
	private int width;
	/** the height of the bitmap in pixels */
	private int height;
	/** the data of the bitmap stored as an integer */
	private int[] data;

	/**
	 * Creates a new bitmap/
	 * 
	 * @param width
	 *            The width of the bitmap in pixels
	 * @param height
	 *            The height of the bitmap in pixels
	 */
	public Bitmap(int width, int height)
	{
		this.width = width;
		this.height = height;
		allocate();
	}

	/**
	 * Creates a new bitmap from an input stream. The input stream is assumed to
	 * be valid.
	 */
	public Bitmap(InputStream is) throws IOException
	{
		this(ImageIO.read(is), true);
	}

	/**
	 * Creates a new bitmap from the specified path of an image.
	 * 
	 * @param path
	 *            The path to the bitmap image
	 */
	public Bitmap(String path)
	{
		BufferedImage image = IOUtils.loadInteralImage(path);
		width = image.getWidth();
		height = image.getHeight();
		allocate();
		image.getRGB(0, 0, width, height, data, 0, width); // x,y,width,height,array,offset,scan
															// size

		flipY();
	}

	/**
	 * Creates a bitmap from an image, with additional parameters to specify a
	 * sub image.
	 * 
	 * @param path
	 *            The path of the image
	 * @param startX
	 *            The starting x of the sub image
	 * @param startY
	 *            The starting y of the sub image
	 * @param width
	 *            The width of the sub image
	 * @param height
	 *            The height of the sub image
	 */
	public Bitmap(String path, int startX, int startY, int width, int height)
	{
		this(IOUtils.loadInteralImage(path).getSubimage(startX, startY, width, height), true);
	}

	/**
	 * Copies one bitmap into this bitmap's data.
	 * 
	 * @param other
	 *            The other bitmap to copy
	 */
	public Bitmap(Bitmap other)
	{
		width = other.getWidth();
		height = other.getHeight();
		allocate();
		other.getData(data);
	}

	/**
	 * Creates a bitmap from an existing buffered iamge.
	 * 
	 * @param awtImage
	 *            The existing buffered image
	 * @param flipY
	 *            Whether or not to flip this bitmap Sover the y axis.
	 */
	public Bitmap(BufferedImage awtImage, boolean flipY)
	{
		width = awtImage.getWidth();
		height = awtImage.getHeight();
		allocate();
		awtImage.getRGB(0, 0, width, height, data, 0, width);
		if (flipY)
		{
			flipY();
		}
	}

	/**
	 * @return the width of this bitmap in pixels
	 */
	public int getWidth()
	{
		return width;
	}

	/**
	 * @return the height of this bitmap in pixels
	 */
	public int getHeight()
	{
		return height;
	}

	/**
	 * @return the area of this bitmap in square pixels.
	 */
	public int size()
	{
		return width * height;
	}

	/**
	 * @return a new array of data exactly the same as this bitmap's data
	 */
	public int[] getData()
	{
		int[] res = new int[data.length];
		getData(res);
		return res;
	}

	/**
	 * Copies the data of this bitmap into a specified destination array.
	 * 
	 * @param dst
	 *            The destination array
	 */
	public void getData(int[] dst)
	{
		if (dst.length != data.length)
		{
			// throw error
			Core.getLog().fatal("Destination array not the same length as data array");
		}

		for (int i = 0; i < data.length; i++)
		{
			dst[i] = data[i];
		}
	}

	/**
	 * @param x
	 *            The x pixel
	 * @param y
	 *            The y pixel
	 * @return The color of a pixel at the specified x,y location
	 */
	public int getPixel(int x, int y)
	{
		return data[x + (y * width)];
	}

	/**
	 * @param x
	 *            The x pixel
	 * @param y
	 *            The y pixel
	 * @return The red value of a specified pixel
	 */
	public int getRed(int x, int y)
	{
		return (getPixel(x, y) >> 16) & 0xFF;
	}

	/**
	 * @param x
	 *            The x pixel
	 * @param y
	 *            The y pixel
	 * @return The green value of a specified pixel
	 */
	public int getGreen(int x, int y)
	{
		return (getPixel(x, y) >> 8) & 0xFF;
	}

	/**
	 * @param x
	 *            The x pixel
	 * @param y
	 *            The y pixel
	 * @return The blue value of a specified pixel
	 */
	public int getBlue(int x, int y)
	{
		return (getPixel(x, y) >> 0) & 0xFF;
	}

	/**
	 * @param x
	 *            The x pixel
	 * @param y
	 *            The y pixel
	 * @return The alpha value of a specified pixel
	 */
	public int getAlpha(int x, int y)
	{
		return (getPixel(x, y) >> 24) & 0xFF;
	}

	/**
	 * Allocates a brand new data store for this bitmap in RAM.
	 */
	public void allocate()
	{
		data = new int[width * height];
	}

	/**
	 * Resizes this bitmap to the new specified size.
	 * 
	 * @param width
	 *            The new width of the bitmap
	 * @param height
	 *            The new height of the bitmap.
	 * @return The new, resized, bitmap.
	 */
	public Bitmap resize(int width, int height)
	{
		int[] newAlloc = new int[width * height];

		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				int dataValue = 0;
				if (x < this.width && y < this.height)
				{
					dataValue = getPixel(x, y);

				}
				newAlloc[x + (y * width)] = dataValue;
			}
		}

		this.width = width;
		this.height = height;
		allocate();
		setData(newAlloc);

		return this;
	}

	/**
	 * Blends the entire bitmap with the specified RGB color multiplicatively.
	 * This basically muliplies every RGB component in the Bitmap with the
	 * specified color.
	 * 
	 * @param rgb
	 *            The blend color
	 * @return The new, blended, bitmap
	 */
	public Bitmap multiplicativeBlend(int rgb)
	{
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				multiplicativeBlendPixel(x, y, rgb);
			}
		}
		return this;
	}

	/**
	 * Gives the entire bitmap a specified transparency.
	 * 
	 * @param alpha
	 *            The new alpha of this bitmap
	 * @return The new, transparentized, bitmap
	 */
	public Bitmap transparentize(int alpha)
	{
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				if (data[x + y * width] != 0xFFFF00FF)
				{
					data[x + y * width] = (alpha << 24) | (getRed(x, y) << 16 | getGreen(x, y) << 8 | getBlue(x, y));
				}
			}
		}

		return this;
	}

	/**
	 * Additively blends this bitmap with the specified RGB. This basically adds
	 * every pixel in the entire bitmap with the specified RGB.
	 * 
	 * @param rgb
	 *            The rgb to blend with
	 * @return The new, blended, bitmap.
	 */
	public Bitmap additiveBlend(int rgb)
	{
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				additiveBlendPixel(x, y, rgb);
			}
		}
		return this;
	}

	/**
	 * Copies the speicifed data buffer into this bitmap.
	 * 
	 * @param data
	 *            The new data store
	 * @return The new bitmap
	 */
	public Bitmap setData(int[] data)
	{
		if (data.length != this.data.length)
		{
			// throw error
		}
		for (int i = 0; i < data.length; i++)
		{
			this.data[i] = data[i];
		}
		return this;
	}

	/**
	 * Sets the data to the data from the image source specified by the path.
	 * 
	 * @param imagePath
	 *            The image path to the new image data store
	 * @return The new bitmap
	 */
	public Bitmap setData(String imagePath)
	{
		BufferedImage image = IOUtils.loadInteralImage(imagePath);
		width = image.getWidth();
		height = image.getHeight();

		int[] res = new int[width * height];
		image.getRGB(0, 0, width, height, res, 0, width); // x,y,width,height,array,offset,scan
															// size

		for (int i = 0; i < data.length; i++)
		{
			if (i > data.length)
			{
				Core.getLog().warning("The loaded image was larger than the current allocated data");
				break;
			}
			data[i] = res[i];
		}

		flipY();
		return this;
	}

	/**
	 * Sets the pixel at the specified location to the specified value.
	 * 
	 * @param x The x position
	 * @param y The y position
	 * @param value
	 * @return
	 */
	public Bitmap setPixel(int x, int y, int value)
	{
		data[x + (y * width)] = value;
		return this;
	}
	
	/**
	 * Multiplies the correct RGB with Pixel color to get correct
	 * blend color.
	 * 
	 * @param x the x position
	 * @param y the y position
	 * @param rgb the rgb to blend with
	 */
	public void multiplicativeBlendPixel(int x, int y, int rgb)
	{
		int blendRed = (rgb >> 16) & 0xFF;
		int blendGreen = (rgb >> 8) & 0xFF;
		int blendBlue = (rgb >> 0) & 0xFF;

		int red = (((getRed(x, y) * blendRed) / 0xFF) << 16);
		int green = (((getGreen(x, y) * blendGreen) / 0xFF) << 8);
		int blue = (((getBlue(x, y) * blendBlue) / 0xFF) << 0);

		setPixel(x, y, red | green | blue);
	}

	/**
	 * Adds the correct RGB with pixels to get blend
	 * 
	 * @param x the x position
	 * @param y the y position
	 * @param rgb the rgb to blend with
	 */
	public void additiveBlendPixel(int x, int y, int rgb)
	{
		int blendRed = (rgb >> 16) & 0xFF;
		int blendGreen = (rgb >> 8) & 0xFF;
		int blendBlue = (rgb >> 0) & 0xFF;

		int red = (((getRed(x, y) + blendRed) << 16));
		int green = (((getGreen(x, y) + blendGreen) << 8));
		int blue = (((getBlue(x, y) + blendBlue) << 0));

		setPixel(x, y, red | green | blue);
	}

	/**
	 * fills the bitmap with the rgb value
	 * 
	 * @param rgb the correct rgb to blend with
	 * @return returns the filled bitmap with rgb
	 */
	public Bitmap fill(int rgb)
	{
		for (int i = 0; i < data.length; i++)
		{
			data[i] = rgb;
		}
		return this;
	}

	/**
	 * flips the x value of the bitmap
	 * 
	 * @return returns the new flipped bitmap
	 */
	public Bitmap flipX()
	{
		int[] res = new int[width * height];
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				res[x + (y * width)] = data[(width - x - 1) + (y * width)];
			}
		}
		setData(res);
		return this;
	}

	/**
	 * replaces magenta with transparency 
	 *
	 * @return returns new bitmap
	 */
	public Bitmap replaceMagentaWithTransparancy()
	{
		return replaceColorWithTransparency(0xFFFF00FF);
	}

	/**
	 * replaces color with transparency
	 * 
	 * @param color the color to replace
	 * @return returns the new bitmap
	 */
	public Bitmap replaceColorWithTransparency(int color)
	{
		for (int i = 0; i < data.length; i++)
		{
			if (data[i] == color)
			{
				data[i] = 0;
			}
		}
		return this;
	}

	/**
	 * flips the bitmap on the y axis
	 * 
	 * @return returns the new bitmap
	 */
	public Bitmap flipY()
	{
		int[] res = new int[width * height];
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				res[x + (y * width)] = data[x + ((height - y - 1) * width)];
			}
		}
		setData(res);
		return this;
	}

	/**
	 * merges the different layers of the bitmap together
	 * 
	 * @param layers the layers to merge together
	 * @return returns the new merged bitmap
	 */
	public static Bitmap mergeLayers(Bitmap[] layers)
	{
		Bitmap result = new Bitmap(layers[0]);

		for (int i = 1; i < layers.length; i++)
		{
			if (layers[i].getWidth() != result.getWidth() || layers[i].getHeight() != result.getHeight())
			{
				throw new IllegalArgumentException("All layers must have the same size!");
			} else
			{
				int[] newData = new int[result.getWidth() * result.getHeight()];

				for (int x = 0; x < result.getWidth(); x++)
				{
					for (int y = 0; y < result.getHeight(); y++)
					{
						// additive blend the current layer to the existing
						// result image
						Vector3f srcColor = new Vector3f(layers[i].getRed(x, y) / 255f, layers[i].getGreen(x, y) / 255f, layers[i].getBlue(x, y) / 255f);
						Vector3f destColor = new Vector3f(result.getRed(x, y) / 255f, result.getGreen(x, y) / 255f, result.getBlue(x, y) / 255f);
						float alpha = layers[i].getAlpha(x, y) / 255f;

						srcColor.scale(alpha);
						destColor.scale(1 - alpha);

						Vector3f resultColor = destColor.add(srcColor);

						int finalAlpha = Math.max(result.getAlpha(x, y), layers[i].getAlpha(x, y));

						int finalColor = (finalAlpha << 24) | ((int) (resultColor.getX() * 255) << 16) | ((int) (resultColor.getY() * 255) << 8) | ((int) (resultColor.getZ() * 255) << 0);
						newData[x + (y * result.getWidth())] = finalColor;
					}
				}

				result.setData(newData);
			}
		}

		return result;
	}
}
