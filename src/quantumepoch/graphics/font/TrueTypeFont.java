package quantumepoch.graphics.font;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;

import quantumepoch.core.Core;
import quantumepoch.graphics.Bitmap;
import quantumepoch.graphics.texture.Texture;
import quantumepoch.utils.IOUtils;
import quantumepoch.utils.MathUtils;

/**
 * Represents a true type font which can be used in a FontRenderer to draw text to the screen. This statically generates a bitmap font to be used within the program whenever needed.
 */
public class TrueTypeFont
{

	/** the constant width of the fontmap generated */
	public static final int FONT_TEXTURE_WIDTH = 512;
	/** the type of this font */
	private FontType fontType;
	/** the point size of this font */
	private int fontSize;
	/** whether or not this font is anti aliased */
	private boolean antiAlias;
	/** the characters of this font */
	private FontCharacter[] fontCharacters;
	/** the height of each character on this font */
	private int characterHeight;
	/** the generated fontmap texture */
	private Texture fontTexture;
	/** whether or not this font could be found */
	private boolean found;

	/**
	 * Creates a new true type font.
	 * 
	 * @param fontType
	 *            The type of font
	 * @param fontSize
	 *            The point size of the font
	 * @param antiAlias
	 *            Whether or not to anti alias when generating the font map.
	 */
	public TrueTypeFont(FontType fontType, int fontSize, boolean antiAlias)
	{

		if (!fontType.exists())
		{
			found = false;
			Core.getLog().warning(fontType.getFontName() + " was not found, loading Arial by default");
			fontType = FontType.ARIAL;
		} else
		{
			found = true;
		}

		this.fontType = fontType;
		this.fontSize = fontSize;
		this.antiAlias = antiAlias;
		this.fontCharacters = new FontCharacter[256];

	}

	/**
	 * Generates the font map.
	 */
	public void loadFont()
	{

		BufferedImage renderedFontImage = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = renderedFontImage.createGraphics();
		Font awtFont = new Font(found ? fontType.getFontName() : "Arial", Font.PLAIN, fontSize);
		FontMetrics awtFontMetrics = g.getFontMetrics(awtFont);

		// get largest character height
		characterHeight = awtFontMetrics.getMaxAscent() + awtFontMetrics.getMaxDescent() + 4;

		// get row count and position characters
		int rowCount = 0;

		for (int i = 0, x = 0, y = 0; i < 256; i++)
		{

			int width = awtFontMetrics.charWidth((char) i) + 3;
			int widthNext = awtFontMetrics.charWidth((char) (i + 1)) + 3;

			fontCharacters[i] = new FontCharacter((char) i, width - 3, x, y - 4);

			x += width;

			if (x + widthNext >= FONT_TEXTURE_WIDTH)
			{
				x = 0;
				y += characterHeight;
				rowCount++;
			}

		}

		// find correct image height, needs to be a power of 2
		int height = rowCount * characterHeight;

		if (!MathUtils.isPowerOfTwo(rowCount * characterHeight))
		{
			int pow = 0;

			// find the next power of 2
			for (int power = 0; Math.pow(2, power - 1) < height; power++)
			{
				pow = power;
			}

			height = (int) Math.pow(2, pow);
		}

		renderedFontImage = new BufferedImage(FONT_TEXTURE_WIDTH, height, BufferedImage.TYPE_INT_RGB);
		g = (Graphics2D) renderedFontImage.getGraphics();

		if (antiAlias)
		{
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		}

		// render characters to buffered image
		for (int i = 0; i < 256; i++)
		{
			g.setColor(Color.WHITE);
			g.setFont(awtFont);
			g.drawString(String.valueOf((char) i), fontCharacters[i].getX(), fontCharacters[i].getY() - awtFontMetrics.getMaxDescent());
		}

		fontTexture = new Texture(new Bitmap(renderedFontImage, true));
		fontTexture.load();

	}

	public FontType getFontType()
	{
		return fontType;
	}

	public int getFontSize()
	{
		return fontSize;
	}

	public int getCharacterHeight()
	{
		return characterHeight;
	}

	public FontCharacter[] getFontCharacters()
	{
		return fontCharacters;
	}

	public Texture getFontTexture()
	{
		return fontTexture;
	}

	public int getCharacterX(char c)
	{
		return fontCharacters[c].getX();
	}

	public int getCharacterY(char c)
	{
		return fontCharacters[c].getY();
	}

	public int getCharacterWidth(char c)
	{
		return fontCharacters[c].getWidth();
	}

	public boolean isAntiAliased()
	{
		return antiAlias;
	}

	/**
	 * Calculates the width of a string in pixels if it were rendered with this font.
	 * 
	 * @param string
	 *            The string to calculate the width of
	 * @return The width of the specified string in pixels
	 */
	public int getStringWidth(String string)
	{
		int width = 0;
		for (char c : string.toCharArray())
		{
			width += getCharacterWidth(c);
		}
		return width;
	}

	/**
	 * Checks if the system has the specified font type
	 * 
	 * @param fontType
	 * @return Whether the system has the specified font type
	 */
	public static boolean doesSystemHaveFont(FontType fontType)
	{
		GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
		String[] fonts = genv.getAvailableFontFamilyNames();
		for (String font : fonts)
		{
			if (fontType.getFontName().equals(font))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Loads a non system font.
	 * 
	 * @param path
	 *            The path to the external font resource
	 */
	public static void loadFont(String path)
	{
		try
		{
			GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(Font.createFont(Font.TRUETYPE_FONT, IOUtils.getInternalResourceAsStream(path)));
		} catch (FontFormatException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
