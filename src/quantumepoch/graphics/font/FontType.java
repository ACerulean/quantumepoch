package quantumepoch.graphics.font;

import java.awt.GraphicsEnvironment;

/**
 * A type of font, used when creating fonts. This class is essentially an adapter over a string, but provides extra functionality.
 */
public class FontType
{

	public static final FontType ARIAL = new FontType("Arial");
	public static final FontType CALIBRI = new FontType("Calibri");
	public static final FontType COMIC_SANS = new FontType("Comic Sans MS");

	/** the name of the font */
	private String fontName;

	/**
	 * Creates a new FontType.
	 * 
	 * @param fontName
	 *            The name of the font
	 */
	public FontType(String fontName)
	{
		this.fontName = fontName;
	}

	public String getFontName()
	{
		return fontName;
	}

	/**
	 * @return Whether this font type exists
	 */
	public boolean exists()
	{
		for (String x : GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames())
		{
			if (x.equals(fontName))
			{
				return true;
			}
		}
		return false;
	}
}
