package quantumepoch.graphics.font;

import quantumepoch.core.Core;
import quantumepoch.graphics.BlendFunction;
import quantumepoch.graphics.BufferUsage;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.PrimitiveShape;
import quantumepoch.graphics.VertexBuffer;
import quantumepoch.graphics.shader.forward.ForwardText;
import quantumepoch.graphics.texture.TextureRegion;
import quantumepoch.utils.MathUtils;
import quantumepoch.utils.math.Matrix4f;
import quantumepoch.utils.math.Vector2f;

/**
 * A Core component of the rendering system used to render text to the screen via fonts.
 */
public class FontRenderer
{

	/** the shader used to render fonts to the screen */
	private ForwardText shader;
	/** the vbo where font vertex data will be stored */
	private VertexBuffer vbo;
	/** the color of the text to be rendered to the screen */
	private Color color;
	/** the matrix that defines the view and projection space of the rendering area */
	private Matrix4f viewProjectionMatrix;
	/** the matrix that defines the global translations of the text */
	private Matrix4f modelMatrix;
	/** the rendering mode of the font. 0 = render black areas on characters, 1 = don't render black areas on characters */
	private int mode;

	public FontRenderer()
	{

		shader = new ForwardText("shaders/forward-vertex.vs", "shaders/forward-text.fs", true);

		vbo = new VertexBuffer(shader.getVertexArray(), 50000, PrimitiveShape.QUAD, BufferUsage.STREAM);

		viewProjectionMatrix = MathUtils.orthoMatrix(0, Core.getWindow().getWidth(), 0, Core.getWindow().getHeight(), -1, 1);
		modelMatrix = new Matrix4f();
		modelMatrix.setIdentity();

		color = Color.WHITE;
	}

	public int getMode()
	{
		return mode;
	}

	public void setMode(int mode)
	{
		this.mode = mode;
	}

	public Color getColor()
	{
		return color;
	}

	public void setColor(Color color)
	{
		this.color = color;
	}

	/**
	 * Renders a string to the screen.
	 * 
	 * @param font
	 *            The font to use when rendering
	 * @param text
	 *            The text to render to the screen
	 * @param x
	 * @param y
	 */
	public void render(TrueTypeFont font, String text, float x, float y)
	{
		render(font, text, x, y, true);
	}

	/**
	 * Renders a string to the screen
	 * 
	 * @param font
	 *            The font to use when rendering
	 * @param text
	 *            The text to render to the screen
	 * @param x
	 * @param y
	 * @param blend
	 *            Whether or not to use blending when rendering the text
	 */
	public void render(TrueTypeFont font, String text, float x, float y, boolean blend)
	{

		BlendFunction temp = Core.getGraphics().getBlendFunction();

		if (blend)
		{
			Core.getGraphics().setBlendingEnabled(true);
			Core.getGraphics().setBlendFunction(BlendFunction.ADDITIVE);
		} else
		{
			Core.getGraphics().setBlendingEnabled(true);
			// Core.getGraphics().setBlendFunction(BlendFunction.ALPHA_BLEND);
		}

		shader.updateUniforms(viewProjectionMatrix, modelMatrix, mode);
		Core.getGraphics().setBlendingEnabled(true);

		font.getFontTexture().bind();

		float currentX = x;
		int cHeight = font.getCharacterHeight();

		for (char c : text.toCharArray())
		{

			int cWidth = font.getCharacterWidth(c);
			int charX = font.getCharacterX(c);
			int charY = font.getFontTexture().getHeight() - font.getCharacterY(c);

			TextureRegion region = new TextureRegion(font.getFontTexture(), charX, charY, cWidth, cHeight);

			Vector2f uv1 = region.getUv1();
			Vector2f uv2 = region.getUv2();
			Vector2f uv3 = region.getUv3();
			Vector2f uv4 = region.getUv4();

			vbo.vertex(currentX, y, 0, color.getRed(), color.getGreen(), color.getBlue(), 1, uv1.getX(), uv1.getY(), 0, 0, 1);
			vbo.vertex(currentX, y + cHeight, 0, color.getRed(), color.getGreen(), color.getBlue(), 1, uv2.getX(), uv2.getY(), 0, 0, 1);
			vbo.vertex(currentX + cWidth, y + cHeight, 0, color.getRed(), color.getGreen(), color.getBlue(), 1, uv3.getX(), uv3.getY(), 0, 0, 1);
			vbo.vertex(currentX + cWidth, y, 0, color.getRed(), color.getGreen(), color.getBlue(), 1, uv4.getX(), uv4.getY(), 0, 0, 1);

			currentX += cWidth;
		}

		vbo.updateBufferData();
		vbo.render(text.length() * 4);
		vbo.clearBufferData();

		if (temp != null)
			Core.getGraphics().setBlendFunction(temp);
	}
}
