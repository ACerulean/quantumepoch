package quantumepoch.graphics.font;

/**
 * A character of a font.
 */
public class FontCharacter
{

	/** the character of the font */
	private char character;
	/** the width of the character on the font sheet */
	private int width;
	/** the x position of the character on the font sheet */
	private int x;
	/** the y position of the character on the font sheet */
	private int y;

	/**
	 * Creates a new font character
	 * 
	 * @param character
	 * @param width
	 * @param x
	 * @param y
	 */
	public FontCharacter(char character, int width, int x, int y)
	{
		this.character = character;
		this.width = width;
		this.x = x;
		this.y = y;
	}

	/**
	 * @return the character on the font sheet
	 */
	public char getCharacter()
	{
		return character;
	}

	/**
	 * @return the width of the character on the font sheet
	 */
	public int getWidth()
	{
		return width;
	}

	/**
	 * @return the x position of the character on the font sheet
	 */
	public int getX()
	{
		return x;
	}

	/**
	 * @return the y position of the character on the font sheets
	 */
	public int getY()
	{
		return y;
	}
}
