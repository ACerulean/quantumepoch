package quantumepoch.graphics;

import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;

import java.nio.IntBuffer;

import quantumepoch.utils.IOUtils;

/**
 * This class provides an abstract utility over OpenGL for indexing an OpenGL
 * buffer.
 */
public class IndexBuffer
{

	/** the size of the index buffer */
	private int size;
	/** the opengl handle of the buffer */
	private int handle;
	/** the indices stored in this buffer */
	private IntBuffer indices;
	/** the usage hint of this buffer */
	private BufferUsage usage;

	/**
	 * Creates a new index buffer
	 * 
	 * @param size
	 *            The size of this buffer
	 * @param usage
	 *            The OpenGL usage hint of this buffer.
	 */
	public IndexBuffer(int size, BufferUsage usage)
	{
		this.size = size;
		this.handle = glGenBuffers();
		this.usage = usage;
		allocate();
	}

	/**
	 * @return the OpenGL handle of this buffer
	 */
	public int getHandle()
	{
		return handle;
	}

	/**
	 * @return the size of this buffer
	 */
	public int getSize()
	{
		return size;
	}

	/**
	 * @return the opengl hint of this buffer
	 */
	public BufferUsage getUsage()
	{
		return usage;
	}

	/**
	 * Sets the new opengl usage of this buffer
	 * 
	 * @param usage
	 *            The new opengl usage
	 */
	public void setUsage(BufferUsage usage)
	{
		this.usage = usage;
	}

	/**
	 * Binds this buffer to be used with OpenGL.
	 */
	public void bind()
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle);
	}

	/**
	 * Unbinds any currently bound index buffer.
	 */
	public void unbind()
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	/**
	 * Allocates a new data store for this index buffer.
	 */
	public void allocate()
	{
		indices = IOUtils.createIntBuffer(size);
	}

	/**
	 * Sets the indices stored within this index buffer.
	 * 
	 * @param indices
	 *            The indices to store in this buffer
	 */
	public void setIndices(int[] indices)
	{
		this.indices.clear();
		this.indices.put(indices);
	}

	/**
	 * Sends the index data to opengl.
	 */
	public void sendData()
	{
		bind();
		indices.flip();
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices, usage.getGlConst());
	}

	/**
	 * Adds a new index to this index buffer/
	 * 
	 * @param index
	 *            The new index to add to this buffer
	 */
	public void addIndex(int index)
	{
		indices.put(index);
	}

}
