package quantumepoch.graphics.lighting;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import quantumepoch.core.Core;
import quantumepoch.graphics.BlendFunction;
import quantumepoch.graphics.BufferUsage;
import quantumepoch.graphics.PrimitiveShape;
import quantumepoch.graphics.VertexBuffer;
import quantumepoch.graphics.batch.ShapeBatch;
import quantumepoch.graphics.shader.forward.ForwardPoint;
import quantumepoch.physics.BoundingBox2D;
import quantumepoch.utils.MathUtils;
import quantumepoch.utils.math.Matrix4f;
import quantumepoch.utils.math.Vector2f;

/**
 * A class used as a system of rendering lights throught the forward rendering system. This class also handles geometric shadows.
 */
public class Light2DRenderer
{

	/** the scale factor of shadows */
	public static final float SHADOW_SCALE_FACTOR = 800f;

	/** the point light shader used to render point lights */
	private ForwardPoint pointShader;
	/** the VBO containing point data to be rendered */
	private VertexBuffer pointVBO;
	/** the shape batch used to render occluders to the stencil buffer */
	private ShapeBatch occluderRenderer;
	/** the view and projection spaces defined in a matrix */
	private Matrix4f viewProjectionMatrix;
	/** the matrix transforming every vertex */
	private Matrix4f modelMatrix;

	/** a list of occluders which will block out light sources */
	private List<BoundingBox2D> occluders;

	/**
	 * Creates a new light renderer
	 */
	public Light2DRenderer()
	{
		pointShader = new ForwardPoint("shaders/forward-vertex.vs", "shaders/forward-point.fs", true);

		// change later to batch lights, this as it is is really inefficient
		pointVBO = new VertexBuffer(pointShader.getVertexArray(), 4, PrimitiveShape.QUAD, BufferUsage.STREAM);

		occluderRenderer = new ShapeBatch();

		viewProjectionMatrix = MathUtils.orthoMatrix(0, Core.getWindow().getWidth(), 0, Core.getWindow().getHeight(), -1, 1);
		modelMatrix = new Matrix4f();
		modelMatrix.setIdentity();

		occluders = new ArrayList<BoundingBox2D>();
	}

	/**
	 * @return all bounding boxes that will block out light
	 */
	public List<BoundingBox2D> getOccluders()
	{
		return occluders;
	}

	/**
	 * Renders a point light without shadows
	 * 
	 * @param light
	 *            The light to render
	 */
	public void renderPointLight(PointLight light)
	{
		renderPointLight(light, false);
	}

	/**
	 * Renders a point light
	 * 
	 * @param light
	 *            The light to render
	 * @param shadows
	 *            Whether or not to render shadows
	 */
	public void renderPointLight(PointLight light, boolean shadows)
	{
		renderPointLight(light, null, shadows);
	}

	public static int count = 0;

	/**
	 * Renders a point light
	 * 
	 * @param light
	 *            The light to render
	 * @param translate
	 *            The translation of the light or null if none
	 * @param shadows
	 *            Whether or not to render shadows
	 */
	public void renderPointLight(PointLight light, Vector2f translate, boolean shadows)
	{

		count++;
		if (shadows)
		{
			// set up stenciling
			GL11.glEnable(GL11.GL_STENCIL_TEST);
			GL11.glClearStencil(0);
			GL11.glClear(GL11.GL_STENCIL_BUFFER_BIT);
			GL11.glStencilFunc(GL11.GL_ALWAYS, 1, 1);
			GL11.glStencilOp(GL11.GL_REPLACE, GL11.GL_REPLACE, GL11.GL_REPLACE);
			GL11.glColorMask(false, false, false, false);
			GL11.glDepthMask(true);

			occluderRenderer.begin();

			for (BoundingBox2D box : occluders)
			{
				Vector2f[] vertices = new Vector2f[4];
				vertices[0] = new Vector2f(box.getPosition().getX(), box.getPosition().getY());
				vertices[1] = new Vector2f(box.getPosition().getX(), box.getPosition().getY() + box.getHeight());
				vertices[2] = new Vector2f(box.getPosition().getX() + box.getWidth(), box.getPosition().getY() + box.getHeight());
				vertices[3] = new Vector2f(box.getPosition().getX() + box.getWidth(), box.getPosition().getY());

				Vector2f lightPos = new Vector2f(light.getPosition().getX(), light.getPosition().getY());

				for (int i = 0; i < vertices.length; i++)
				{
					Vector2f vert = vertices[i];
					Vector2f next = vertices[(i + 1) % vertices.length];
					Vector2f edge = next.subtract(vert);
					Vector2f normal = new Vector2f(-edge.getY(), edge.getX());
					Vector2f lightToVertex = vert.subtract(lightPos).normalized();
					if (normal.dotProduct(lightToVertex) < 0)
					{
						Vector2f point1 = vert.add(vert.subtract(lightPos).multiply(SHADOW_SCALE_FACTOR));
						Vector2f point2 = next.add(next.subtract(lightPos).multiply(SHADOW_SCALE_FACTOR));

						occluderRenderer.renderQuad(next, point2, point1, vert);
					}
				}

			}

			occluderRenderer.end();

			GL11.glColorMask(true, true, true, true);
			GL11.glDepthMask(false);
			GL11.glStencilFunc(GL11.GL_NOTEQUAL, 1, 1);
			GL11.glStencilOp(GL11.GL_KEEP, GL11.GL_KEEP, GL11.GL_KEEP);
		}

		Core.getGraphics().setBlendingEnabled(true);
		Core.getGraphics().setBlendFunction(BlendFunction.ADDITIVE);

		if (translate != null)
		{
			pointShader.updateUniforms(viewProjectionMatrix, modelMatrix, light, translate);
		} else
		{
			pointShader.updateUniforms(viewProjectionMatrix, modelMatrix, light);
		}
		pointShader.bind();

		pointVBO.vertex(0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, -1);
		pointVBO.vertex(0, Core.getWindow().getHeight(), 0, 1, 1, 1, 1, 0, 0, 0, 0, -1);
		pointVBO.vertex(Core.getWindow().getWidth(), Core.getWindow().getHeight(), 0, 1, 1, 1, 1, 0, 0, 0, 0, -1);
		pointVBO.vertex(Core.getWindow().getWidth(), 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, -1);

		pointVBO.updateBufferData();
		pointVBO.render();
		pointVBO.clearBufferData();
		Core.getGraphics().setBlendFunction(BlendFunction.ALPHA_BLEND);
		GL11.glDisable(GL11.GL_STENCIL_TEST);
	}
}
