package quantumepoch.graphics.lighting;

import quantumepoch.graphics.Color;

/**
 * An ambient light is a type of light which just has a color, but should be seperately identified.
 */
public class AmbientLight extends Light
{

	/**
	 * Creates a new ambient light
	 * 
	 * @param color
	 *            The ambient light's color
	 */
	public AmbientLight(Color color)
	{
		super(color);
	}

}
