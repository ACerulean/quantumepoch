package quantumepoch.graphics.lighting;

import quantumepoch.graphics.Color;

/**
 * An abstract representation of a light, which has just a color.
 */
public abstract class Light
{

	/** the color of the light */
	private Color color;

	/**
	 * Creates a new Light
	 * 
	 * @param color
	 *            The color of the light
	 */
	public Light(Color color)
	{
		this.color = color;
	}

	/**
	 * @return the color of the light
	 */
	public Color getColor()
	{
		return color;
	}

	/**
	 * Sets the new color of the light
	 * 
	 * @param color
	 *            The new light color
	 */
	public void setColor(Color color)
	{
		this.color = color;
	}

}
