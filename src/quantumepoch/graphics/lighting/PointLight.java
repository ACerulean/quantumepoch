package quantumepoch.graphics.lighting;

import quantumepoch.graphics.Color;
import quantumepoch.utils.math.Vector3f;

/**
 * A point light is a light that has a definite position, an intensity, and an attenuation factor. This class is a data structure that contains that definition.
 */
public class PointLight extends Light
{

	/** the location of the point light */
	private Vector3f position;
	/** the intensity of the light, or brightness */
	private float intensity;
	/** the constant attenuation factor */
	private float constAtten;
	/** the linear attenuation factor */
	private float linearAtten;
	/** the quadratic attenuation factor */
	private float quadAtten;

	/**
	 * Creates a new point light
	 * 
	 * @param position
	 * @param color
	 * @param intensity
	 * @param constAtten
	 * @param linearAtten
	 * @param quadAtten
	 */
	public PointLight(Vector3f position, Color color, float intensity, float constAtten, float linearAtten, float quadAtten)
	{
		super(color);
		this.position = position;
		this.intensity = intensity;
		this.constAtten = constAtten;
		this.linearAtten = linearAtten;
		this.quadAtten = quadAtten;
	}

	public Vector3f getPosition()
	{
		return position;
	}

	public float getIntensity()
	{
		return intensity;
	}

	public float getConstAtten()
	{
		return constAtten;
	}

	public float getLinearAtten()
	{
		return linearAtten;
	}

	public float getQuadAtten()
	{
		return quadAtten;
	}

	public void setPosition(Vector3f position)
	{
		this.position = position;
	}

	public void setIntensity(float intensity)
	{
		this.intensity = intensity;
	}

	public void setConstAtten(float constAtten)
	{
		this.constAtten = constAtten;
	}

	public void setLinearAtten(float linearAtten)
	{
		this.linearAtten = linearAtten;
	}

	public void setQuadAtten(float quadAtten)
	{
		this.quadAtten = quadAtten;
	}

}
