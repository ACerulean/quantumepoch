package quantumepoch.graphics.animation;

import quantumepoch.graphics.texture.TextureRegion;

/**
 * Represents one frame of a sprite animation.
 */
public class Frame
{

	/** the texture of the frame */
	private TextureRegion texture;
	/** the length of this frame */
	private long length;

	/**
	 * Creates a new frame
	 * 
	 * @param texture
	 *            The texture of the frame
	 * @param length
	 *            The length of the frame
	 */
	public Frame(TextureRegion texture, long length)
	{
		this.texture = texture;
		this.length = length;
	}

	/**
	 * @return the texture of the frame
	 */
	public TextureRegion getTexture()
	{
		return texture;
	}

	/**
	 * @return the length of the frame
	 */
	public long getLength()
	{
		return length;
	}

	/**
	 * Sets the texture of the frame.
	 * 
	 * @param texture
	 *            the texture of the frame
	 */
	public void setTexture(TextureRegion texture)
	{
		this.texture = texture;
	}

	/**
	 * Sets the length of the frame.
	 * 
	 * @param length
	 *            The length of the frame.
	 */
	public void setLength(long length)
	{
		this.length = length;
	}

}
