package quantumepoch.graphics.animation;

import java.util.ArrayList;
import java.util.List;

import quantumepoch.graphics.texture.Texture;
import quantumepoch.graphics.texture.TextureRegion;
import quantumepoch.utils.Delay;

/**
 * A sprite animation which can be easily utilized to create fluid looking movement.
 */
public class TextureAnimation
{

	/** the list of frames to be used */
	private List<Frame> frames;
	/** the current frame */
	private int currentFrame;
	/** the current delay until the next frame */
	private Delay frameDelay;
	/** stores whether the animation has started */
	private boolean started;
	/** stores whether the animation is paused */
	private boolean paused;

	/**
	 * Creates a new texture animation.
	 */
	public TextureAnimation()
	{
		frames = new ArrayList<Frame>();
		currentFrame = 0;
		frameDelay = new Delay(0);
		started = false;
		paused = false;
	}

	/**
	 * Creates a new texture animation by stripping frames by the specified texture.
	 * 
	 * @param texture
	 *            The texture of the animation
	 * @param frameWidth
	 *            The width of each frame
	 * @param frameHeight
	 *            The height of each frame
	 * @param frameLength
	 *            The length of each frame
	 */
	public TextureAnimation(Texture texture, int frameWidth, int frameHeight, long frameLength)
	{
		this(texture, frameWidth, frameHeight, 0, 0, texture.getWidth(), texture.getHeight(), frameLength, null);
	}

	/**
	 * Creates a new texture animation by stripping frames by the specified texture.
	 * 
	 * @param texture
	 *            The texture of the animation
	 * @param frameWidth
	 *            The width of each frame
	 * @param frameHeight
	 *            The height of each frame
	 * @param frameLength
	 *            The length of each frame
	 * @param indicies
	 *            The indices to index frames so they can be reused
	 */
	public TextureAnimation(Texture texture, int frameWidth, int frameHeight, long frameLength, int[] indicies)
	{
		this(texture, frameWidth, frameHeight, 0, 0, texture.getWidth(), texture.getHeight(), frameLength, indicies);
	}

	/**
	 * Creates a new texture animation by stripping frames by the specified texture.
	 * 
	 * @param texture
	 *            The texture to create the animation from
	 * @param frameWidth
	 *            The width of each frame
	 * @param frameHeight
	 *            the height of each frame
	 * @param startX
	 *            The startx on the texture of the animation
	 * @param startY
	 *            The starty on the texture of the animatin
	 * @param width
	 *            The width of each animation frame
	 * @param height
	 *            The height of each animation frame
	 * @param frameLength
	 *            The length of each animation frame
	 * @param indices
	 *            The indices of the loaded frames to be used
	 */
	public TextureAnimation(Texture texture, int frameWidth, int frameHeight, int startX, int startY, int width, int height, long frameLength, int[] indices)
	{
		frames = new ArrayList<Frame>();
		List<Frame> unindexedFrames = new ArrayList<Frame>();

		for (int y = startY; y <= startY + height; y += frameHeight)
		{
			for (int x = startX; x <= startX + width; x += frameWidth)
			{
				unindexedFrames.add(new Frame(new TextureRegion(texture, x, y, frameWidth, frameHeight), frameLength));
			}
		}

		if (indices != null)
		{
			for (int i : indices)
			{
				frames.add(unindexedFrames.get(i));
			}
		} else
		{
			frames = unindexedFrames;
		}

		frameDelay = new Delay(frameLength);
	}

	/**
	 * @return whether this animation has started
	 */
	public boolean isStarted()
	{
		return started;
	}

	/**
	 * @return the animation frames
	 */
	public List<Frame> getFrames()
	{
		return frames;
	}

	/**
	 * @return the current frame of the animation
	 */
	public Frame getCurrentFrame()
	{
		return frames.get(currentFrame);
	}

	/**
	 * @return the current frame index
	 */
	public int getCurrentFrameIndex()
	{
		return currentFrame;
	}

	/**
	 * Starts this texture animation
	 * 
	 * @return The started texture animation
	 */
	public TextureAnimation start()
	{
		started = true;
		return this;
	}

	/**
	 * Stops the texture animation.
	 * 
	 * @return The stopped texture animation
	 */
	public TextureAnimation stop()
	{
		started = false;
		currentFrame = 0;
		frameDelay.setDelay(getCurrentFrame().getLength());
		return this;
	}

	/**
	 * Pauses the texture animation
	 * 
	 * @return The paused texture animation
	 */
	public TextureAnimation pause()
	{
		paused = true;
		return this;
	}

	/**
	 * Resumes the texture animation from a paused state.
	 * 
	 * @return The resumed texture animation
	 */
	public TextureAnimation resume()
	{
		paused = false;
		return this;
	}

	/**
	 * Sets the current frame index of the animation/
	 * 
	 * @param index
	 *            The index of the frame.
	 */
	public void setCurrentFrame(int index)
	{
		currentFrame = index;
	}

	/**
	 * Updates this texture animation and changes frames as needed. If the animation has not been started, or is paused, this method will effectively do nothing to the current state of the animation.
	 */
	public void update()
	{
		if (frames.size() > 0 && started && !paused)
		{
			if (frameDelay == null)
			{
				frameDelay = new Delay(frames.get(currentFrame).getLength());
			}
			if (frameDelay.isReady())
			{
				currentFrame++;

				if (currentFrame >= frames.size())
				{
					currentFrame = 0;
				}
			}
		} else
		{
			// exception
		}
	}

}
