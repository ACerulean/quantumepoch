package quantumepoch.graphics.texture;

import static org.lwjgl.opengl.GL11.GL_LINEAR;
import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_RGBA8;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_S;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_T;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameteri;
import static org.lwjgl.opengl.GL11.glTexSubImage2D;
import static org.lwjgl.opengl.GL12.GL_CLAMP_TO_EDGE;

import java.nio.ByteBuffer;

import quantumepoch.graphics.Bitmap;
import quantumepoch.utils.IOUtils;

/**
 * A texture is an OpenGL bitmap. The data for the texture will be stored on the GPU where OpenGL can access it at quick speeds. Textures can be rendered using sprite batching, see SpriteBatch.java for more.
 */
public class Texture
{

	/** the last bitmap loaded to this texture */
	private Bitmap bitmap;
	/** the opengl id of this texture */
	private int textureID;

	/**
	 * Creates a new texture
	 * 
	 * @param path
	 *            The path to the image file defining the image data
	 */
	public Texture(String path)
	{
		this(new Bitmap(path));
	}

	/**
	 * Creates a new texture
	 * 
	 * @param bitmap
	 *            The bitmap containing the texture data
	 */
	public Texture(Bitmap bitmap)
	{
		this.bitmap = bitmap;
	}

	/**
	 * @return the width of the texture
	 */
	public int getWidth()
	{
		return bitmap.getWidth();
	}

	/**
	 * @return the height of the bitmap
	 */
	public int getHeight()
	{
		return bitmap.getHeight();
	}

	/**
	 * @return the bitmap containing the data of this texture
	 */
	public Bitmap getBitmap()
	{
		return bitmap;
	}

	/**
	 * @return the opengl id of this texture
	 */
	public int getID()
	{
		return textureID;
	}

	/**
	 * Sets the texture up to be used with OpenGL.
	 */
	public void bind()
	{
		glBindTexture(GL_TEXTURE_2D, textureID);
	}

	/**
	 * Removes any existing texture from being used with OpenGL on active texture unit zero.
	 */
	public void unbind()
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	/**
	 * Updates the data of this texture with another bitmap's array of texture data.
	 * 
	 * @param buffer
	 *            The source buffer containing the new data
	 * @return This texture, updated with the new data
	 */
	public Texture updateData(Bitmap buffer)
	{
		bind();

		ByteBuffer pixelData = IOUtils.createByteBuffer(buffer.getWidth() * buffer.getHeight() * 4);

		int[] data = buffer.getData();

		for (int i : data)
		{
			pixelData.put((byte) ((i >> 16) & 0xFF));
			pixelData.put((byte) ((i >> 8) & 0xFF));
			pixelData.put((byte) ((i >> 0) & 0xFF));
			pixelData.put((byte) ((i >> 24) & 0xFF));
		}

		pixelData.flip();

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		glTexSubImage2D(textureID, 0, 0, 0, buffer.getWidth(), buffer.getHeight(), GL_RGBA8, GL_UNSIGNED_BYTE, pixelData);

		return this;

	}

	/**
	 * Updates this texture's data with the data contained in a byte buffer
	 * 
	 * @param data
	 *            The source data of the texture
	 */
	public void updateData(ByteBuffer data)
	{
		glTexSubImage2D(textureID, 0, 0, 0, bitmap.getWidth(), bitmap.getHeight(), GL_RGBA8, GL_UNSIGNED_BYTE, data);
	}

	/**
	 * Loads the data in this texture to the GPU for OpenGL use. This method MUST be invoked before any rendering of thhis texture.
	 * 
	 * @return This texture, loaded.
	 */
	public Texture load()
	{
		textureID = glGenTextures();

		bind();

		ByteBuffer pixelData = IOUtils.createByteBuffer(bitmap.getWidth() * bitmap.getHeight() * 4);

		int[] data = bitmap.getData();

		for (int i : data)
		{
			pixelData.put((byte) ((i >> 16) & 0xFF));
			pixelData.put((byte) ((i >> 8) & 0xFF));
			pixelData.put((byte) ((i >> 0) & 0xFF));
			pixelData.put((byte) ((i >> 24) & 0xFF));
		}

		pixelData.flip();

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, bitmap.getWidth(), bitmap.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, pixelData);

		return this;

	}

	/**
	 * Converts this texture to a texture region covering the entire texture.
	 * 
	 * @return A texture region containing the whole texture
	 */
	public TextureRegion toTextureRegion()
	{
		return new TextureRegion(this);
	}
}
