package quantumepoch.graphics;

import quantumepoch.utils.math.Vector2f;
import quantumepoch.utils.math.Vector3f;

/**
 * A data structure for a vertex to be sent to the GPU via opengl.
 */
public class Vertex
{

	/** the position attribute of the vertex */
	private Vector3f position;
	/** the color attribute of the vertex */
	private Color color;
	/** the tex coord attribute of the vertex */
	private Vector2f texCoord;

	/**
	 * Creates a new vertex with default attributes.
	 */
	public Vertex()
	{
		this(new Vector3f(), new Color(0, 0, 0), new Vector2f());
	}

	/**
	 * Creates a new vertex with the specified position attribute.
	 * 
	 * @param position
	 *            The position attribute
	 */
	public Vertex(Vector3f position)
	{
		this(position, new Color(0, 0, 0), new Vector2f());
	}

	/**
	 * Creates a new vertex with the specified position and color attributes.
	 * 
	 * @param position
	 *            The position attribute
	 * @param color
	 *            The color attribute
	 */
	public Vertex(Vector3f position, Color color)
	{
		this(position, color, new Vector2f());
	}

	/**
	 * Creates a new vertex with the specified position, color and texCoord
	 * attributes.
	 * 
	 * @param position
	 *            The position attribute
	 * @param color
	 *            The color attribute
	 * @param texCoord
	 *            The tex coord attribute
	 */
	public Vertex(Vector3f position, Color color, Vector2f texCoord)
	{
		this.position = position;
		this.color = color;
		this.texCoord = texCoord;
	}

	/**
	 * @return the position attribute
	 */
	public Vector3f getPosition()
	{
		return position;
	}

	/**
	 * @return the color attribute.
	 */
	public Color getColor()
	{
		return color;
	}

	/**
	 * @return the tex coord attribute.
	 */
	public Vector2f getTexCoord()
	{
		return texCoord;
	}

	/**
	 * Sets the position attribute.
	 * 
	 * @param position
	 *            The new position attribute
	 */
	public void setPosition(Vector3f position)
	{
		this.position = position;
	}

	/**
	 * Sets the vertex attribute color.
	 * 
	 * @param color
	 *            The color attribute
	 */
	public void setColor(Color color)
	{
		this.color = color;
	}

	/**
	 * Sets the tex coord attribute.
	 * 
	 * @param texCoord
	 *            The tex coord attribute
	 */
	public void setTexCoord(Vector2f texCoord)
	{
		this.texCoord = texCoord;
	}

}
