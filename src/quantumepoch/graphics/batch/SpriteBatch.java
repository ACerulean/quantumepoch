package quantumepoch.graphics.batch;

import quantumepoch.core.Core;
import quantumepoch.graphics.BufferUsage;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.IndexBuffer;
import quantumepoch.graphics.PrimitiveShape;
import quantumepoch.graphics.VertexBuffer;
import quantumepoch.graphics.lighting.AmbientLight;
import quantumepoch.graphics.shader.ShaderProgram;
import quantumepoch.graphics.shader.forward.ForwardAmbient;
import quantumepoch.graphics.texture.Texture;
import quantumepoch.graphics.texture.TextureRegion;
import quantumepoch.utils.MathUtils;
import quantumepoch.utils.math.Matrix4f;
import quantumepoch.utils.math.Vector2f;
import quantumepoch.utils.math.Vector4f;

/**
 * A core component of the rendering system used to render sprites to the screen.
 */
public class SpriteBatch
{

	/** whether the sprite batch is in rendering mode */
	private boolean began;
	/** the maximum amount of draws the sprite batch will make before it force batches */
	private int maxDraws;
	/** the current amount of draw calls */
	private int drawCount;
	/** the current amount of flush calls */
	private int flushCount;
	/** the current texture to render with */
	private Texture currentTexture;
	/** shader used to apply ambient light to sprites and render them to the screen */
	private ForwardAmbient shader;
	/** the vertex buffer where vertex data of the bounds of the sprites are stored */
	private VertexBuffer vbo;
	/** the matrix that defines the view and projection space of the rendering area */
	private Matrix4f viewProjectionMatrix;
	/** the matrix applied to every sprite to transform quads */
	private Matrix4f modelMatrix;
	/** the ambient to be applied to every rendered sprite */
	private AmbientLight ambient;
	/** the color to tint sprites */
	private Color tint = Color.WHITE;

	/**
	 * Creates a new sprite batch with a default max vertex count of 50K.
	 */
	public SpriteBatch()
	{
		this(50000);
	}

	/**
	 * Creates a new sprite batch.
	 * 
	 * @param maxDraws
	 *            The max amount of draw calls before the sprite batch force renders
	 */
	public SpriteBatch(int maxDraws)
	{
		this.maxDraws = maxDraws;

		shader = new ForwardAmbient("shaders/forward-vertex.vs", "shaders/forward-ambient.fs", true);

		vbo = new VertexBuffer(shader.getVertexArray(), maxDraws * 4, PrimitiveShape.TRIANGLE, BufferUsage.STREAM);

		IndexBuffer indexBuffer = new IndexBuffer(maxDraws * 6, BufferUsage.STATIC);

		int[] indices = new int[maxDraws * 6];

		for (int i = 0, j = 0; i < maxDraws * 6; i += 6, j += 4)
		{
			indices[i + 0] = j + 0;
			indices[i + 1] = j + 1;
			indices[i + 2] = j + 2;
			indices[i + 3] = j + 2;
			indices[i + 4] = j + 3;
			indices[i + 5] = j + 0;
		}

		indexBuffer.setIndices(indices);
		indexBuffer.sendData();

		vbo.setIndexBuffer(indexBuffer);

		viewProjectionMatrix = MathUtils.orthoMatrix(0, Core.getWindow().getWidth(), 0, Core.getWindow().getHeight(), -1, 1);

		modelMatrix = new Matrix4f();
		modelMatrix.setIdentity();

		ambient = new AmbientLight(Color.WHITE);
	}

	/**
	 * @return whether the sprite batch is in rendering mode
	 */
	public boolean hasBegan()
	{
		return began;
	}

	/**
	 * @return the max amount of calls before the sprite batch force renders
	 */
	public int getMaxDraws()
	{
		return maxDraws;
	}

	/**
	 * @return the current amount of sprites that are drawn
	 */
	public int getDrawCount()
	{
		return drawCount;
	}

	/**
	 * @return the amount of times the sprite batch has rendered to the screen
	 */
	public int getFlushCount()
	{
		return flushCount;
	}

	/**
	 * @return the current bound texture
	 */
	public Texture getCurrentTexture()
	{
		return currentTexture;
	}

	/**
	 * @return the current shader used to render sprites
	 */
	public ShaderProgram getShader()
	{
		return shader;
	}

	/**
	 * @return the vbo used to store sprite data
	 */
	public VertexBuffer getVBO()
	{
		return vbo;
	}

	/**
	 * @return the matrix used to define the view projection space of the rendering area
	 */
	public Matrix4f getViewProjectionMatrix()
	{
		return viewProjectionMatrix;
	}

	/**
	 * @return the matrix that every sprite is transformed by.
	 */
	public Matrix4f getModelMatrix()
	{
		return modelMatrix;
	}

	/**
	 * @return the ambient light applied to every sprite
	 */
	public AmbientLight getAmbient()
	{
		return ambient;
	}

	/**
	 * @return the current tint applied to every rendered sprite
	 */
	public Color getTint()
	{
		return tint;
	}

	/**
	 * Sets the tint of sprites to be rendered./
	 * 
	 * @param tint
	 *            The new sprite tint
	 */
	public void setTint(Color tint)
	{
		this.tint = tint;
	}

	/**
	 * Puts this sprite batch into rendering mode. This method shuld be invoked before the user renders any sprites.
	 */
	public void begin()
	{
		if (!began)
		{
			began = true;

			drawCount = 0;

			shader.updateUniforms(viewProjectionMatrix, modelMatrix, ambient.getColor().toVector3f());
		} else
		{

		}
	}

	/**
	 * Stops this shape batch from being in rendering mode. This method should be invoked after the user renders any sprites.
	 */
	public void end()
	{
		if (began)
		{
			began = false;
			flush();
			flushCount = 0;
		} else
		{

		}
	}

	/**
	 * Renders a sprite to the screen
	 * 
	 * @param tex
	 *            The texture of the sprite
	 * @param x
	 * @param y
	 */
	public void render(Texture tex, float x, float y)
	{
		render(tex, x, y, 0);
	}

	/**
	 * Renders a sprite to the screen
	 * 
	 * @param tex
	 *            The texture region of this sprite
	 * @param x
	 * @param y
	 */
	public void render(TextureRegion tex, float x, float y)
	{
		render(tex, 0, 0, x, y, 0, 1, 1);
	}

	/**
	 * Renders a sprite to the screen.
	 * 
	 * @param tex
	 *            The texture of the sprite
	 * @param x
	 * @param y
	 * @param rotation
	 *            The rotation of the sprite, relative to the middle
	 */
	public void render(Texture tex, float x, float y, float rotation)
	{
		render(tex, x, y, rotation, 1, 1);
	}

	/**
	 * Renders a sprite to the screen.
	 * 
	 * @param tex
	 *            The texture of the sprite/
	 * @param x
	 * @param y
	 * @param rotation
	 * @param scaleX
	 *            Scaling on the x axis
	 * @param scaleY
	 *            Scaling on the y axis
	 */
	public void render(Texture tex, float x, float y, float rotation, float scaleX, float scaleY)
	{
		render(tex, 0, 0, x, y, rotation, scaleX, scaleY);
	}

	/**
	 * Renders a sprite to the screen.
	 * 
	 * @param tex
	 *            The texture of the sprite
	 * @param originX
	 *            The point relative to whether rotation and scaling will be accomplished
	 * @param originY
	 *            The point relative to whether rotation and scaling will be accomplished
	 * @param x
	 * @param y
	 * @param rotation
	 * @param scaleX
	 * @param scaleY
	 */
	public void render(Texture tex, float originX, float originY, float x, float y, float rotation, float scaleX, float scaleY)
	{
		render(new TextureRegion(tex, 0, 0, tex.getWidth(), tex.getHeight()), originX, originY, x, y, rotation, scaleX, scaleY);
	}

	/**
	 * Renders a sprite to the screen
	 * 
	 * @param region
	 *            The texture region of the sprite
	 * @param originX
	 *            The point relative to whether rotation and scaling will be accomplished
	 * @param originY
	 *            The point relative to whether rotation and scaling will be accomplished
	 * @param x
	 * @param y
	 * @param rotation
	 * @param scaleX
	 * @param scaleY
	 */
	public void render(TextureRegion region, float originX, float originY, float x, float y, float rotation, float scaleX, float scaleY)
	{
		render(region.getTexture(), new Vector2f[] { region.getUv1(), region.getUv2(), region.getUv3(), region.getUv4() }, originX, originY, x, y, rotation, scaleX, scaleY);
	}

	/**
	 * Renders a sprite to the screen.
	 * 
	 * @param tex
	 *            The texture of the sprite.
	 * @param texCoords
	 *            The texture coordinates of each vertex
	 * @param originX
	 *            The point relative to whether rotation and scaling will be accomplished
	 * @param originY
	 *            The point relative to whether rotation and scaling will be accomplished
	 * @param x
	 * @param y
	 * @param rotation
	 * @param scaleX
	 * @param scaleY
	 */
	public void render(Texture tex, Vector2f[] texCoords, float originX, float originY, float x, float y, float rotation, float scaleX, float scaleY)
	{

		if (!began)
		{
			return;
		}

		if (tex == null)
		{
			return;
		}

		float width = (texCoords[2].getX() - texCoords[0].getX()) * tex.getWidth();
		float height = (texCoords[2].getY() - texCoords[0].getY()) * tex.getHeight();

		// check if in bounds
		if (x + width < 0 || x > Core.getWindow().getWidth() || y + height < 0 || y > Core.getWindow().getHeight())
		{
			return;
		}

		// check if there is not a current texture to bind
		if (currentTexture == null)
		{
			currentTexture = tex;
		}

		if (currentTexture == tex && drawCount < maxDraws)
		{

			// define 4 vertices using homogenous coordinates
			Vector4f V1 = new Vector4f(-originX, -originY, 0, 1);
			Vector4f V2 = new Vector4f(-originX, height - originY, 0, 1);
			Vector4f V3 = new Vector4f(width - originX, height - originY, 0, 1);
			Vector4f V4 = new Vector4f(width - originX, -originY, 0, 1);

			// scale vertices relate to origin x, origin y
			V1.mult(scaleX, scaleY, 1, 1);
			V2.mult(scaleX, scaleY, 1, 1);
			V3.mult(scaleX, scaleY, 1, 1);
			V4.mult(scaleX, scaleY, 1, 1);

			// rotate the vertices by multiplying a vector 4f by a rotation
			// matrix 4f
			if (rotation != 0)
			{
				Matrix4f mat = MathUtils.rotationZ(rotation);
				V1.set(V1.multiply(mat));
				V2.set(V2.multiply(mat));
				V3.set(V3.multiply(mat));
				V4.set(V4.multiply(mat));
			}

			// translate vertices to correct position
			V1.translate(originX + x, originY + y, 0, 0);
			V2.translate(originX + x, originY + y, 0, 0);
			V3.translate(originX + x, originY + y, 0, 0);
			V4.translate(originX + x, originY + y, 0, 0);

			// send the vertex data to the vertex buffer
			vbo.vertex(V1.getX(), V1.getY(), 0, tint.getRed(), tint.getGreen(), tint.getBlue(), tint.getAlpha(), texCoords[0].getX(), texCoords[0].getY(), 0, 0, -1);
			vbo.vertex(V2.getX(), V2.getY(), 0, tint.getRed(), tint.getGreen(), tint.getBlue(), tint.getAlpha(), texCoords[1].getX(), texCoords[1].getY(), 0, 0, -1);
			vbo.vertex(V3.getX(), V3.getY(), 0, tint.getRed(), tint.getGreen(), tint.getBlue(), tint.getAlpha(), texCoords[2].getX(), texCoords[2].getY(), 0, 0, -1);
			vbo.vertex(V4.getX(), V4.getY(), 0, tint.getRed(), tint.getGreen(), tint.getBlue(), tint.getAlpha(), texCoords[3].getX(), texCoords[3].getY(), 0, 0, -1);

			drawCount++;

		} else
		{
			// flush the current texture, then start the rendering of the recent
			// texture
			flush();
			drawCount = 0;
			currentTexture = tex;
			render(tex, texCoords, originX, originY, x, y, rotation, scaleX, scaleY);
		}

	}

	/**
	 * This method renders all current data to the screen, but stays in rendering mode.
	 */
	public void reset()
	{
		end();
		begin();
	}

	/**
	 * Renders all data to the screen. This method should not be called externally.
	 */
	private void flush()
	{
		shader.bind();

		if (currentTexture != null)
		{
			currentTexture.bind();
		}

		vbo.updateBufferData();
		vbo.render(drawCount * 6);
		vbo.clearBufferData();

		flushCount++;
	}

}
