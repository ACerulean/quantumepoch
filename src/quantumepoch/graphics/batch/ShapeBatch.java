package quantumepoch.graphics.batch;

import java.util.HashMap;
import java.util.Map;

import quantumepoch.core.Core;
import quantumepoch.graphics.BufferUsage;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.PrimitiveShape;
import quantumepoch.graphics.VertexBuffer;
import quantumepoch.graphics.shader.forward.ForwardShape;
import quantumepoch.utils.MathUtils;
import quantumepoch.utils.math.Matrix4f;
import quantumepoch.utils.math.Vector2f;
import quantumepoch.utils.math.Vector4f;

/**
 * A core component of the rendering system used to render shapes to the screen. This class heavily utilizes OpenGL to make rendering calls.
 */
public class ShapeBatch
{

	/** the amount of vertices when rendering a circle */
	public static final int CIRCLE_VERTICES = 30;

	/** whether the shape batch is in rendering mode */
	private boolean began;
	/** the maximum amount of draws before the current data is batched */
	private int maxDraws;
	/** the current amount of draws */
	private int draws;
	/** the amount of times this batch has been flushed */
	private int flushCount;
	/** the shader used to render the shapes in the forward rendering pipeline */
	private ForwardShape shader;
	/** the vertex buffer used to store shape vertex data */
	private VertexBuffer vbo;
	/** the matrix used to define the view and projection space of the rendering area */
	private Matrix4f viewProjectionMatrix;
	/** the matrix used to transform a shape */
	private Matrix4f modelMatrix;
	/** the current rendering color of the shape batch */
	private Color color;
	/** the current primitive type being rendered */
	private PrimitiveShape primitive;
	/** a mapping optimization using a cache for trigonometric operations when rendering circles */
	private Map<Float, Double> cosCache, sinCache;

	/**
	 * Creates a new shape batch.
	 */
	public ShapeBatch()
	{
		this(50000);
		cosCache = new HashMap<Float, Double>();
		sinCache = new HashMap<Float, Double>();
	}

	/**
	 * Creates a new shape batch.
	 * 
	 * @param maxDraws
	 *            The maximum amount of draws before the batch force renders
	 */
	public ShapeBatch(int maxDraws)
	{

		shader = new ForwardShape("shaders/shapeShader.vs", "shaders/shapeShader.fs", true);

		primitive = PrimitiveShape.QUAD;
		vbo = new VertexBuffer(shader.getVertexArray(), maxDraws * 4, primitive, BufferUsage.STREAM);

		viewProjectionMatrix = MathUtils.orthoMatrix(0, Core.getWindow().getWidth(), 0, Core.getWindow().getHeight(), -1, 1);
		modelMatrix = new Matrix4f();
		modelMatrix.setIdentity();
		color = Color.WHITE;

	}

	/*
	 * Cache system recommended by a friend. Since the calculations tend not to vary much, they repeat often. So, we cache the calculations.
	 */

	/**
	 * Returns the possibly cached cosine value of the specified value. This method caches the cos value if not already.
	 * 
	 * @param angle
	 *            The angle to calculate the cos of.
	 * @return The cached value of the cos.
	 */
	public double ccos(float angle)
	{
		if (!cosCache.containsKey(angle))
		{
			double cos = Math.cos(angle);
			cosCache.put(angle, cos);
			return cos;
		} else
		{
			return cosCache.get(angle);
		}
	}

	/**
	 * Returns the possibly cached sine value of the specified value. This method caches the sin value if not already.
	 * 
	 * @param angle
	 *            The angle to calculate the sin of.
	 * @return The cached value of the sin.
	 */
	public double csin(float angle)
	{
		if (!sinCache.containsKey(angle))
		{
			double sin = Math.sin(angle);
			sinCache.put(angle, sin);
			return sin;
		} else
		{
			return sinCache.get(angle);
		}
	}

	/**
	 * @return whether the shape batch is in rendering mode
	 */
	public boolean hasBegan()
	{
		return began;
	}

	/**
	 * @return the maximum amount of draw before the shape batch force renders
	 */
	public int getMaxDraws()
	{
		return maxDraws;
	}

	/**
	 * @return the current amount of draws the shape batch is scheduled to make.
	 */
	public int getDraws()
	{
		return draws;
	}

	/**
	 * @return the amount of times the shape batch has flushed data.
	 */
	public int getFlushCount()
	{
		return flushCount;
	}

	/**
	 * Sets the rendering color of the shape batch.
	 * 
	 * @param color
	 *            The new rendering color
	 */
	public void setColor(Color color)
	{
		this.color = color;
	}

	/**
	 * @return the color of the drawing mode in this shape batch
	 */
	public Color getColor()
	{
		return color;
	}

	// rendering methods

	/**
	 * Renders a line segment between 2 poitns.
	 * 
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 */
	public void renderLine(float x1, float y1, float x2, float y2)
	{

		if (!began)
			return;

		if (primitive == PrimitiveShape.LINE)
		{
			vbo.vertex(x1, y1, 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);
			vbo.vertex(x2, y2, 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);
		} else
		{
			flush();
			primitive = PrimitiveShape.LINE;
			renderLine(x1, y1, x2, y2);
		}

	}

	/**
	 * Renders a continuous line strip between multiple points. this method should be called multiple times in order to render lines. Every new vertex defines a new line
	 * 
	 * @param x
	 * @param y
	 */
	public void renderLineStrip(float x, float y)
	{
		if (!began)
			return;

		if (primitive == PrimitiveShape.LINE_STRIP)
		{
			vbo.vertex(x, y, 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);
		} else
		{
			flush();
			primitive = PrimitiveShape.LINE_STRIP;
			renderLineStrip(x, y);
		}
	}

	/**
	 * Renders a line loop. This method behaves like the line strip renderer, except this connects the last point to the first point.
	 * 
	 * @param x
	 * @param y
	 */
	public void renderLineLoop(float x, float y)
	{
		if (!began)
			return;

		if (primitive == PrimitiveShape.LINE_LOOP)
		{
			vbo.vertex(x, y, 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);
		} else
		{
			flush();
			primitive = PrimitiveShape.LINE_LOOP;
			renderLineLoop(x, y);
		}
	}

	/**
	 * Renders a non filled quad defined by 4 arbitrary points.
	 * 
	 * @param point1
	 * @param point2
	 * @param point3
	 * @param point4
	 */
	public void renderQuad(Vector2f point1, Vector2f point2, Vector2f point3, Vector2f point4)
	{
		if (!began)
			return;

		if (primitive == PrimitiveShape.QUAD)
		{
			vbo.vertex(point1.getX(), point1.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);
			vbo.vertex(point2.getX(), point2.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);
			vbo.vertex(point3.getX(), point3.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);
			vbo.vertex(point4.getX(), point4.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);
		} else
		{
			flush();
			primitive = PrimitiveShape.QUAD;
			renderQuad(point1, point2, point3, point4);
		}
	}

	/**
	 * Renders a non filled rectangle with the specified bounds and rotation.
	 * 
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param originX
	 *            The reference point whether scales and rotations will be performed relative to
	 * @param originY
	 *            The reference point whether scales and rotations will be performed relative to
	 * 
	 * @param rot
	 */
	public void renderRect(float x, float y, float width, float height, float originX, float originY, float rot)
	{
		if (!began)
			return;

		if (primitive == PrimitiveShape.LINE)
		{
			Vector4f v1 = new Vector4f(0, 0, 0, 1);
			Vector4f v2 = new Vector4f(0, height, 0, 1);
			Vector4f v3 = new Vector4f(width, height, 0, 1);
			Vector4f v4 = new Vector4f(width, 0, 0, 1);

			if (rot != 0)
			{

				v1.translate(-originX, -originY, 0, 0);
				v2.translate(-originX, -originY, 0, 0);
				v3.translate(-originX, -originY, 0, 0);
				v4.translate(-originX, -originY, 0, 0);

				Matrix4f mat = MathUtils.rotationZ(rot);
				v1.set(v1.multiply(mat));
				v2.set(v2.multiply(mat));
				v3.set(v3.multiply(mat));
				v4.set(v4.multiply(mat));

				v1.translate(originX, originY, 0, 0);
				v2.translate(originX, originY, 0, 0);
				v3.translate(originX, originY, 0, 0);
				v4.translate(originX, originY, 0, 0);
			}

			v1.translate(x, y, 0, 0);
			v2.translate(x, y, 0, 0);
			v3.translate(x, y, 0, 0);
			v4.translate(x, y, 0, 0);

			vbo.vertex(v1.getX(), v1.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);
			vbo.vertex(v2.getX(), v2.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);

			vbo.vertex(v2.getX(), v2.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);
			vbo.vertex(v3.getX(), v3.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);

			vbo.vertex(v3.getX(), v3.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);
			vbo.vertex(v4.getX(), v4.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);

			vbo.vertex(v4.getX(), v4.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);
			vbo.vertex(v1.getX(), v1.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);

		} else
		{
			flush();
			primitive = PrimitiveShape.LINE;
			renderRect(x, y, width, height, originX, originY, rot);
		}
	}

	/**
	 * Renders a filled rectangle starting at [x,y] with a width and height of [w,h].
	 * 
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public void renderFilledRect(float x, float y, float width, float height)
	{

		if (!began)
			return;

		if (primitive == PrimitiveShape.QUAD)
		{
			vbo.vertex(x, y, 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);
			vbo.vertex(x, y + height, 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);
			vbo.vertex(x + width, y + height, 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);
			vbo.vertex(x + width, y, 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);
		} else
		{
			flush();
			primitive = PrimitiveShape.QUAD;
			renderFilledRect(x, y, width, height);
		}
	}

	/**
	 * Renders a non filled circle at the specified [x,y] position with the specified radius. This implementation uses caching to speed up trig operations.
	 * 
	 * @param x
	 * @param y
	 * @param rad
	 *            the radius of the circle in pixels.
	 */
	public void renderCircle(float x, float y, float rad)
	{

		if (!began)
			return;

		if (primitive == PrimitiveShape.LINE)
		{
			float inc = 360f / CIRCLE_VERTICES;
			for (float i = 0; i <= 360; i += inc)
			{
				float x1 = x + (float) ccos((float) Math.toRadians(i)) * rad;
				float y1 = y + (float) csin((float) Math.toRadians(i)) * rad;
				float x2 = x + (float) ccos((float) Math.toRadians(i - inc)) * rad;
				float y2 = y + (float) csin((float) Math.toRadians(i - inc)) * rad;

				vbo.vertex(x1, y1, 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);
				vbo.vertex(x2, y2, 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);
			}
		} else
		{
			flush();
			primitive = PrimitiveShape.LINE;
			renderCircle(x, y, rad);

		}

	}

	/**
	 * Renders a non filled polygon.
	 * 
	 * @param vertices
	 *            The vertices that define the polygon
	 */
	public void renderPolygon(Vector2f[] vertices)
	{
		if (!began)
			return;

		for (int i = 0; i < vertices.length; i++)
		{
			Vector2f v1 = vertices[i];
			Vector2f v2 = null;
			vbo.vertex(v1.getX(), v1.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);

			if (i == vertices.length - 1)
			{
				v2 = vertices[0];
			} else
			{
				v2 = vertices[i + 1];
			}

			vbo.vertex(v2.getX(), v2.getY(), 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);

		}
	}

	/**
	 * Renders a filled circle at [x,]y with the specified radius.This implementation uses caching to speed up trig operations.
	 * 
	 * @param x
	 * @param y
	 * @param rad
	 *            The radius of the circle
	 */
	public void renderFilledCircle(float x, float y, float rad)
	{
		if (!began)
			return;

		if (primitive == PrimitiveShape.TRIANGLE_STRIP)
		{
			float inc = 360f / CIRCLE_VERTICES;

			vbo.vertex(x, y, 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);
			for (float i = inc; i <= 360 + inc; i += inc)
			{

				float x1 = x + (float) ccos((float) Math.toRadians(inc)) * rad;
				float y1 = y + (float) csin((float) Math.toRadians(inc)) * rad;
				float x2 = x + (float) ccos((float) Math.toRadians(i - inc)) * rad;
				float y2 = y + (float) csin((float) Math.toRadians(i - inc)) * rad;
				vbo.vertex(x1, y1, 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);
				vbo.vertex(x2, y2, 0, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha(), 0, 0, 0, 0, 1);
			}
		} else
		{
			flush();
			primitive = PrimitiveShape.TRIANGLE_STRIP;
			renderFilledCircle(x, y, rad);
		}
	}

	/**
	 * Resets the shape batch. This batches all current data and goes back into rendering mode.
	 */
	public void reset()
	{
		end();
		begin();
	}

	/**
	 * Puts the shape batch into rendering mode. This should be invoked before the user renders any shape to the screen.
	 */
	public void begin()
	{
		if (!began)
		{
			began = true;
			draws = 0;
			shader.updateUniforms(viewProjectionMatrix, modelMatrix);
		} else
		{
			// throw exception
		}
	}

	/**
	 * Takes the shape batch out of rendering mode. This should be called when the user wants to render data to the screen.
	 */
	public void end()
	{
		if (began)
		{
			began = false;
			flush();
			flushCount++;
		} else
		{
			// throw exception
		}
	}

	/**
	 * Flushes all data to the GPU. This shouldn't be called externally.
	 */
	private void flush()
	{
		shader.bind();

		vbo.setPrimitive(primitive);
		vbo.updateBufferData();
		vbo.render();
		vbo.clearBufferData();

		draws = 0;

		flushCount++;
	}
}
