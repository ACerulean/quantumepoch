package quantumepoch.graphics;

import static org.lwjgl.opengl.ARBImaging.GL_TABLE_TOO_LARGE;
import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_EQUAL;
import static org.lwjgl.opengl.GL11.GL_FILL;
import static org.lwjgl.opengl.GL11.GL_FRONT_AND_BACK;
import static org.lwjgl.opengl.GL11.GL_GEQUAL;
import static org.lwjgl.opengl.GL11.GL_GREATER;
import static org.lwjgl.opengl.GL11.GL_INVALID_ENUM;
import static org.lwjgl.opengl.GL11.GL_INVALID_OPERATION;
import static org.lwjgl.opengl.GL11.GL_INVALID_VALUE;
import static org.lwjgl.opengl.GL11.GL_LEQUAL;
import static org.lwjgl.opengl.GL11.GL_LESS;
import static org.lwjgl.opengl.GL11.GL_LINE;
import static org.lwjgl.opengl.GL11.GL_NO_ERROR;
import static org.lwjgl.opengl.GL11.GL_OUT_OF_MEMORY;
import static org.lwjgl.opengl.GL11.GL_POINT;
import static org.lwjgl.opengl.GL11.GL_STACK_OVERFLOW;
import static org.lwjgl.opengl.GL11.GL_STACK_UNDERFLOW;
import static org.lwjgl.opengl.GL11.GL_VENDOR;
import static org.lwjgl.opengl.GL11.GL_VERSION;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glDepthFunc;
import static org.lwjgl.opengl.GL11.glDepthMask;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glGetError;
import static org.lwjgl.opengl.GL11.glGetString;
import static org.lwjgl.opengl.GL11.glPolygonMode;
import static org.lwjgl.opengl.GL30.GL_INVALID_FRAMEBUFFER_OPERATION;
import quantumepoch.core.Core;

/**
 * Provides functionality for interfacing with opengl using lwjgl's binding.
 * This class should be a singleton, and the only instance should be stored in
 * core.
 */
public class RenderCore
{

	/** the current opengl blend function */
	private BlendFunction bfunc;

	/**
	 * Creates a new singleton of render core.
	 */
	public RenderCore()
	{
		if (Core.getGraphics() != null)
		{
			throw new IllegalStateException("Can not make more than one instance of RenderCore");

		}
	}

	/**
	 * @return the opengl version currently on this machine
	 */
	public String getGLVersion()
	{
		return glGetString(GL_VERSION);
	}

	/**
	 * @return the opengl vendor currently on this machine
	 */
	public String getVendor()
	{
		return glGetString(GL_VENDOR);
	}

	/**
	 * @return the blend function currently beign used by opengl
	 */
	public BlendFunction getBlendFunction()
	{
		return bfunc;
	}

	/**
	 * @return the int code for the current opengl error, if any
	 */
	public int getGLError()
	{
		return glGetError();
	}

	/**
	 * Clears the contents currently stored in the opengl color buffer.
	 */
	public void clearColorBuffer()
	{
		glClear(GL_COLOR_BUFFER_BIT);
	}

	/**
	 * Checks for any opengl errors. If there are any, the program should crash
	 * with an error message.
	 */
	public void checkGLErrors()
	{

		int errorId = getGLError();
		boolean errorOccurred = false;

		int errorCount = 0;

		while (errorId != GL_NO_ERROR)
		{
			errorOccurred = true;

			String glErrorMessage = "";

			if (errorId == GL_INVALID_ENUM)
			{
				glErrorMessage = "GL_INVALID_ENUM";
			} else if (errorId == GL_INVALID_FRAMEBUFFER_OPERATION)
			{
				glErrorMessage = "GL_INVALID_FRAMEBUFFER_OPERATION";
			} else if (errorId == GL_INVALID_OPERATION)
			{
				glErrorMessage = "GL_INVALID_OPERATION";
			} else if (errorId == GL_INVALID_VALUE)
			{
				glErrorMessage = "GL_INVALID_VALUE";
			} else if (errorId == GL_OUT_OF_MEMORY)
			{
				glErrorMessage = "GL_OUT_OF_MEMORY";
			} else if (errorId == GL_STACK_OVERFLOW)
			{
				glErrorMessage = "GL_STACK_OVERFLOW";
			} else if (errorId == GL_STACK_UNDERFLOW)
			{
				glErrorMessage = "GL_STACK_UNDERFLOW";
			} else if (errorId == GL_TABLE_TOO_LARGE)
			{
				glErrorMessage = "GL_TABLE_TOO_LARGE";
			} else
			{
				// error not found
				glErrorMessage = "NOT FOUND";
			}

			Core.getLog().fatal("Error " + errorCount + ": " + glErrorMessage);
			errorId = glGetError();
			errorCount++;
		}

		if (errorOccurred)
		{
			Core.getLog().fatal("OpenGL error list end");
			System.exit(1);
			// crash
		}
	}

	/**
	 * Sets the color that opengl should clear the screen with every frame.
	 * 
	 * @param clearColor
	 *            The new opengl clear color
	 */
	public void setClearColor(Color clearColor)
	{
		glClearColor(clearColor.getRed(), clearColor.getGreen(), clearColor.getBlue(), clearColor.getAlpha());
	}

	/**
	 * Sets whether opengl blending should be enabled in the pipeline
	 * 
	 * @param blend
	 *            Whether oengl blending should be enabled.
	 */
	public void setBlendingEnabled(boolean blend)
	{
		if (blend)
		{
			glEnable(GL_BLEND);
		} else
		{
			glDisable(GL_BLEND);
		}
	}

	/**
	 * Sets the blend function that opengl should utilize when rendering
	 * 
	 * @param function
	 *            The blend function to use when rendering
	 */
	public void setBlendFunction(BlendFunction function)
	{
		bfunc = function;
		glBlendFunc(function.getParam1(), function.getParam2());
	}

}
