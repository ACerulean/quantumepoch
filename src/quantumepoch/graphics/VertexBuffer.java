package quantumepoch.graphics;

import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glDeleteBuffers;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;

import java.nio.FloatBuffer;

import quantumepoch.utils.IOUtils;

/**
 * Stores vertex data and interfaces with OpenGL to send the data to the GPU to render.
 */
public class VertexBuffer
{

	/** the OpenGL handle of the vertex buffer */
	private int handle;
	/** the size of the buffer */
	private int bufferSize;
	/** the amount of vertices currently in this buffer */
	private int vertexCount;
	/** the data store of this buffer */
	private FloatBuffer data;
	/** an index buffer, if any, to index vertices */
	private IndexBuffer indexBuffer;
	/** the parent VAO of this VBO. */
	private VertexArray vao;
	/** the primitive shape this VBO will render with */
	private PrimitiveShape prim;
	/** the opengl usage of this buffer */
	private BufferUsage usage;

	/**
	 * Creates a new vertex buffer
	 * 
	 * @param vao
	 * @param bufferSize
	 * @param prim
	 * @param usage
	 */
	public VertexBuffer(VertexArray vao, int bufferSize, PrimitiveShape prim, BufferUsage usage)
	{
		this.vao = vao;
		this.handle = glGenBuffers();
		this.bufferSize = bufferSize;
		this.usage = usage;
		this.prim = prim;
		allocate();
	}

	public int getHandle()
	{
		return handle;
	}

	public int getBufferSize()
	{
		return bufferSize;
	}

	public int getVertexCount()
	{
		return vertexCount;
	}

	public FloatBuffer getBufferData()
	{
		return data;
	}

	public IndexBuffer getIndexBuffer()
	{
		return indexBuffer;
	}

	public VertexArray getVao()
	{
		return vao;
	}

	public PrimitiveShape getPrimitive()
	{
		return prim;
	}

	public BufferUsage getUsage()
	{
		return usage;
	}

	public void setVao(VertexArray vao)
	{
		this.vao = vao;
	}

	public void setPrimitive(PrimitiveShape prim)
	{
		this.prim = prim;
	}

	public void setBufferSize(int bufferSize)
	{
		this.bufferSize = bufferSize;
		allocate();
	}

	public void setIndexBuffer(IndexBuffer indexBuffer)
	{
		this.indexBuffer = indexBuffer;
	}

	/**
	 * Removes the index buffer, if any, from this VBO.
	 */
	public void removeIndexBuffer()
	{
		indexBuffer = null;
	}

	/**
	 * Clears the data associated with this VBO.
	 */
	public void clearBufferData()
	{
		data.clear();
		vertexCount = 0;
	}

	/**
	 * Binds this buffer for OpenGL usage.
	 */
	public void bind()
	{
		glBindBuffer(GL_ARRAY_BUFFER, handle);
	}

	/**
	 * Unbinds any currently bound VBOS from OpenGL.
	 */
	public void unbind()
	{
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	/**
	 * Sends an array of data to this VBO.
	 * 
	 * @param data
	 *            The array of data to put in this data store
	 */
	public void vertex(float[] data)
	{
		if (vertexCount < bufferSize)
		{
			for (float f : data)
			{
				this.data.put(f);
			}
			vertexCount++;
		}
	}

	/**
	 * Loads a vertex into data that holds a position, color, texture coordinate and normal vector.
	 * 
	 * @param x
	 * @param y
	 * @param z
	 * @param r
	 * @param g
	 * @param b
	 * @param a
	 * @param u
	 * @param v
	 * @param nx
	 * @param ny
	 * @param nz
	 */
	public void vertex(float x, float y, float z, float r, float g, float b, float a, float u, float v, float nx, float ny, float nz)
	{
		if (vertexCount < bufferSize)
		{
			data.put(x);
			data.put(y);
			data.put(z);
			data.put(r);
			data.put(g);
			data.put(b);
			data.put(a);
			data.put(u);
			data.put(v);
			data.put(nx);
			data.put(ny);
			data.put(nz);
			vertexCount++;
		}
	}

	/**
	 * Inserts a vertex into this buffer
	 * 
	 * @param vert
	 */
	public void vertex(Vertex vert)
	{
		if (vert != null)
		{
			vertex(vert.getPosition().getX(), vert.getPosition().getY(), vert.getPosition().getZ(), vert.getColor().getRed(), vert.getColor().getGreen(), vert.getColor().getBlue(), vert.getColor().getAlpha(), vert.getTexCoord().getX(), vert.getTexCoord().getY(), 0, 0, 0);
		}
	}

	/**
	 * Uploads the data in this buffer to the GPU.
	 */
	public void sendData()
	{
		glBufferData(GL_ARRAY_BUFFER, data, usage.getGlConst());
	}

	/**
	 * Allocates data for this VBO.
	 */
	public void allocate()
	{
		int elementCount = 0;
		for (VertexAttrib attrib : vao.getVertexAttributes())
		{
			elementCount += attrib.getDataType().getElementCount();
		}
		data = IOUtils.createFloatBuffer(bufferSize * elementCount);
	}

	/**
	 * Disposes of this VBO and any resources associated with it.
	 */
	public void dispose()
	{
		glDeleteBuffers(handle);
	}

	/**
	 * Updates the VBO with the last data stored.
	 */
	public void updateBufferData()
	{
		data.flip();

		vao.update(this);
	}

	/**
	 * Renders this vbo to the screen
	 */
	public void render()
	{
		if (indexBuffer == null)
		{
			render(vertexCount);
		} else
		{
			render(indexBuffer.getSize());
		}
	}

	/**
	 * Renders this VBO to the screen
	 * 
	 * @param indexCount
	 *            The amount of indices to use when rendering
	 */
	public void render(int indexCount)
	{

		// TODO: optimize
		bind();
		{
			vao.bind();

			for (VertexAttrib a : vao.getVertexAttributes())
			{
				glEnableVertexAttribArray(a.getLocation());
			}

			if (indexBuffer == null)
			{
				glDrawArrays(prim.getGlConst(), 0, indexCount);
			} else
			{
				indexBuffer.bind();
				glDrawElements(prim.getGlConst(), indexCount, GL_UNSIGNED_INT, 0);
			}

			for (VertexAttrib a : vao.getVertexAttributes())
			{
				glDisableVertexAttribArray(a.getLocation());
			}

			vao.unbind();
		}
		unbind();
	}
}
