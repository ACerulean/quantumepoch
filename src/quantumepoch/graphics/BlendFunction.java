package quantumepoch.graphics;

import static org.lwjgl.opengl.GL11.GL_DST_COLOR;
import static org.lwjgl.opengl.GL11.GL_ONE;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_ZERO;

/**
 * Defines multiple blend functions for OpenGL interfacing.
 *
 */
public enum BlendFunction
{
	ADDITIVE(GL_ONE, GL_ONE), MULTIPLICATIVE(GL_DST_COLOR, GL_ZERO), ALPHA_BLEND(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	/** the first opengl blending parameter */
	private int param1;
	/** the second opengl blending parameter */
	private int param2;

	/**
	 * Creates a new blend function
	 * 
	 * @param param1 The first OpenGL parameter
	 * @param param2 The second OpenGl parameter
	 */
	private BlendFunction(int param1, int param2)
	{
		this.param1 = param1;
		this.param2 = param2;
	}

	/**
	 * @return the first opengl parameter
	 */
	public int getParam1()
	{
		return param1;
	}

	/**
	 * @return the second opengl parameter
	 */
	public int getParam2()
	{
		return param2;
	}
}
