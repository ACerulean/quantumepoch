package quantumepoch.graphics;

import quantumepoch.graphics.shader.DataType;

/**
 * Holds data associated with vertices.
 */
public class VertexAttrib
{

	/** the name id of the vertex attrib */
	private String name;
	/** the index location of the vertex attrib */
	private int location;
	/** the data type of this vertex attrib */
	private DataType type;

	/**
	 * Creates a new vertex attribute
	 * 
	 * @param name
	 * @param location
	 * @param type
	 */
	public VertexAttrib(String name, int location, DataType type)
	{
		this.name = name;
		this.location = location;
		this.type = type;
	}

	public String getName()
	{
		return name;
	}

	public int getLocation()
	{
		return location;
	}

	public DataType getDataType()
	{
		return type;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setLocation(int location)
	{
		this.location = location;
	}

	public void setDataType(DataType type)
	{
		this.type = type;
	}
}
