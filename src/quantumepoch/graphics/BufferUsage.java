package quantumepoch.graphics;

import static org.lwjgl.opengl.GL15.GL_DYNAMIC_DRAW;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.GL_STREAM_DRAW;

/**
 * Defines multiple buffer usages for interfacing with OpenGL.
 */
public enum BufferUsage
{

	STATIC(GL_STATIC_DRAW), DYNAMIC(GL_DYNAMIC_DRAW), STREAM(GL_STREAM_DRAW);

	/** the constant opengl uses for the buffer usage */
	private int glConst;

	/**
	 * Creates a new buffer usage.
	 * 
	 * @param glConst The gl param for the usage
	 */
	BufferUsage(int glConst)
	{
		this.glConst = glConst;
	}

	/**
	 * @return The OpenGL parameter for buffer usage
	 */
	public int getGlConst()
	{
		return glConst;
	}

}
