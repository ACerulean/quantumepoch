package quantumepoch.core;

/**
 * An interface that defines common behavior for any application.
 */
public interface Application
{

	/**
	 * Initializes the application. Load all game content here.
	 */
	public void init();

	/**
	 * Called when the window has been resized.
	 */
	public void resized();

	/**
	 * Updates the application.
	 */
	public void update();

	/**
	 * Renders the application.
	 */
	public void render();

	/**
	 * Disposes the application.
	 */
	public void dispose();

}
