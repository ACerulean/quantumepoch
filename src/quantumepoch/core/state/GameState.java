package quantumepoch.core.state;

/**
 * An abstract representation of a game state. A game state is an independent
 * section of a Game that renders and updates seperately from other game states.
 */
public abstract class GameState
{

	/** the id of this game state */
	private int id;

	/**
	 * Creates a new game state with the specified ID.
	 * 
	 * @param id
	 *            The ID of the game state.
	 */
	public GameState(int id)
	{
		this.id = id;
	}

	/**
	 * @return the id of this game state
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * Sets this game state's ID.
	 * 
	 * @param id
	 *            The id of the game state.
	 */
	public void setId(int id)
	{
		this.id = id;
	}

	/** initializes this game state */
	public abstract void init();

	/** event invoked when the game window has been resized */
	public abstract void resized();

	/** updates this game state, this is not the place to render */
	public abstract void update();

	/** renders this game state, this is not the place to update */
	public abstract void render();

	/** disposes resources of this game state */
	public abstract void dispose();

}
