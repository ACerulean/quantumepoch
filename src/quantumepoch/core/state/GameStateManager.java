package quantumepoch.core.state;

import java.util.ArrayList;
import java.util.List;

import quantumepoch.graphics.gui.Component;

/**
 * Manages multiple game states.
 */
public class GameStateManager
{

	/** A list of game states for this game state manager */
	private List<GameState> appStates;
	/** The current loaded game state for this game state */
	private GameState currentState;

	/**
	 * Creates a new game state manager.
	 */
	public GameStateManager()
	{
		this.appStates = new ArrayList<GameState>();
	}

	/**
	 * @return the game states relevant to this game state manager
	 */
	public List<GameState> getAppStates()
	{
		return appStates;
	}

	/**
	 * @param id
	 *            The ID of the retreivable game state.
	 * @return The game state with the specified ID. If no game state exists,
	 *         this method will return null.
	 */
	public GameState getGameState(int id)
	{
		for (GameState state : appStates)
		{
			if (state.getId() == id)
			{
				return state;
			}
		}
		return null;
	}

	/**
	 * @return the current loaded game state
	 */
	public GameState getCurrentState()
	{
		return currentState;
	}

	/**
	 * Adds a game state to this game state manager's collection of managable
	 * game states.
	 * 
	 * @param state
	 *            The GameState to add.
	 */
	public void addGameState(GameState state)
	{
		this.appStates.add(state);
	}

	/**
	 * Sets the current game state of this game state manager.
	 * 
	 * @param id
	 *            The ID of the game state.
	 */
	public void setCurrentState(int id)
	{
		for (GameState state : appStates)
		{
			if (state.getId() == id)
			{
				if (currentState != null)
				{
					currentState.dispose();
				}
				Component.setFocused(null);
				currentState = state;
				currentState.init();
			}
		}
	}

}
