package quantumepoch.core;

import java.awt.Canvas;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import quantumepoch.audio.Audio;
import quantumepoch.core.debug.ProfileTimer;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.RenderCore;
import quantumepoch.input.Input;
import quantumepoch.utils.DateUtils;
import quantumepoch.utils.IOUtils;
import quantumepoch.utils.Logger;
import quantumepoch.window.CrashWindow;
import quantumepoch.window.LWJGLWindow;
import quantumepoch.window.Window;

/**
 * The Core engine.
 */
public class Core
{

	/**
	 * Singleton instance of the Window class.
	 */
	private static Window window;

	/**
	 * Singleton instance of the RenderCore class.
	 */
	private static RenderCore graphics;

	/**
	 * Singleton instance of the Audio class.
	 */
	private static Audio audio;

	/**
	 * Singleton instance of the Core class.
	 */
	private static Core core;

	/**
	 * Singleton instance of the Input class.
	 */
	private static Input input;

	/**
	 * Singleton instance of the Logger class.
	 */
	private static Logger log;

	/**
	 * Represents if the engine is running.
	 */
	private boolean running;

	/**
	 * The application instance.
	 */
	private Application app;

	/**
	 * The current amount of FPS the engine is executing at.
	 */
	private int fps;

	/**
	 * The current amount of UPS the engine is executing at.
	 */
	private int ups;

	/**
	 * The current delta time between this and last frame.
	 */
	private long delta;

	/**
	 * The target FPS when the application window is not minimized.
	 */
	private int foregroundFps;

	/**
	 * The target FPS when the application window is minimized.
	 */
	private int backgroundFps;

	/**
	 * The target UPS when the application window is not minimized.
	 */
	private int foregroundUps;

	/**
	 * The target UPS when the application window is minimized.
	 */
	private int backgroundUps;

	/**
	 * The color that the application window is cleared to every frame.
	 */
	private Color clearColor;

	/**
	 * The update profiler that the engine uses for updating.
	 */
	private ProfileTimer updateProfiler;

	/**
	 * The render profiler that the engine uses for rendering.
	 */
	private ProfileTimer renderProfiler;

	private List<SplashScreen> splashScreens;
	private int current = 0;

	/**
	 * Constructs a new Core instance. Only one Core instance may be made throughout the application's lifetime.
	 */
	public Core(List<SplashScreen> splashScreens, Application app, boolean start, String title, int width, int height, boolean resizable, int fgFps, int fgUps, int bgFps, int bgUps)
	{
		this(splashScreens, app, null, start, title, width, height, resizable, fgFps, fgUps, bgFps, bgUps);
	}

	/**
	 * Constructs a new Core instance. Only one Core instance may be made throughout the application's lifetime.
	 */
	public Core(List<SplashScreen> splashScreens, Application app, Canvas parent, boolean start, String title, int width, int height, boolean resizable, int fgFps, int fgUps, int bgFps, int bgUps)
	{
		if (core == null)
		{

			this.splashScreens = splashScreens;

			core = this;

			this.app = app;

			// initialize window
			window = new LWJGLWindow();
			window.setTitle(title);
			window.setSize(width, height);
			window.setResizable(resizable);
			window.create();

			// initialize rendering engine
			graphics = new RenderCore();

			foregroundFps = fgFps;
			foregroundUps = fgUps;
			backgroundFps = bgFps;
			backgroundUps = bgUps;

			// initialize audio
			audio = new Audio();
			audio.create();

			clearColor = Color.BLACK;

			if (start)
			{
				start();
			}

		} else
		{
			throw new IllegalStateException("Can not make more than one instance of Core");
		}
	}

	/**
	 * @return The creation status of this singleton.
	 */
	public static boolean created()
	{
		if (core == null)
		{
			return false;
		} else
		{
			return true;
		}
	}

	/**
	 * @return the engine's window singleton
	 */
	public static Window getWindow()
	{
		return window;
	}

	/**
	 * @return the main opengl graphics class
	 */
	public static RenderCore getGraphics()
	{
		return graphics;
	}

	/**
	 * @return the main audio engine class
	 */
	public static Audio getAudio()
	{
		return audio;
	}

	/**
	 * @return the core mechanics singleton
	 */
	public static Core getCore()
	{
		return core;
	}

	/**
	 * @return the input subsystem
	 */
	public static Input getInput()
	{
		return input;
	}

	/**
	 * @return the logger for the engine
	 */
	public static Logger getLog()
	{
		return log;
	}

	/**
	 * @return The running status of the Core engine.
	 */
	public boolean isRunning()
	{
		return running;
	}

	/**
	 * @return The application instance attached to the engine.
	 */
	public Application getApplication()
	{
		return app;
	}

	/**
	 * @return The amount of frames per second the application is running at.
	 */
	public int getFps()
	{
		return fps;
	}

	/**
	 * @return The amount of updates per second the application is running at.
	 */
	public int getUps()
	{
		return ups;
	}

	/**
	 * @return The color that the screen is being cleared to every frame.
	 */
	public Color getClearColor()
	{
		return clearColor;
	}

	/**
	 * @return The amount of time passed between this frame and last frame.
	 */
	public long getDelta()
	{
		return delta;
	}

	/**
	 * @return The amount of frames per second the engine will target when the game window is in focus.
	 */
	public int getForegroundFps()
	{
		return foregroundFps;
	}

	/**
	 * @return The amount of frames per second the engine will target when the game window is not in focus.
	 */
	public int getBackgroundFps()
	{
		return backgroundFps;
	}

	/**
	 * @return The amount of updates per second the engine will target when the game window is in focus.
	 */
	public int getForegroundUps()
	{
		return foregroundUps;
	}

	/**
	 * @return The amount of updates per second the engine will target when the game window is not in focus.
	 */
	public int getBackgroundUps()
	{
		return backgroundUps;
	}

	/**
	 * @return The profiler for updating the application.
	 */
	public ProfileTimer getUpdateTimer()
	{
		return updateProfiler;
	}

	/**
	 * @return The profiler for rendering the application.
	 */
	public ProfileTimer getRenderTimer()
	{
		return renderProfiler;
	}

	/**
	 * Sets the amount of frames per second that the engine will target when the window is focused.
	 */
	public void setForegroundFps(int fps)
	{
		this.foregroundFps = fps;
	}

	/**
	 * Sets the amount of frames per second that the engine will target when the window is not focused.
	 */
	public void setBackgroundFps(int fps)
	{
		this.backgroundFps = fps;
	}

	/**
	 * Sets the amount of updates per second that the engine will target when the window is focused.
	 */
	public void setForegroundUps(int ups)
	{
		this.foregroundUps = ups;
	}

	/**
	 * Sets the amount of frames per second that the engine will target when the window is not focused.
	 */
	public void setBackgroundUps(int ups)
	{
		this.backgroundUps = ups;
	}

	/**
	 * Sets the color that the screen will clear to every frame.
	 */
	public void setClearColor(Color clearColor)
	{
		this.clearColor = clearColor;
	}

	/**
	 * Starts the engine.
	 */
	public void start()
	{
		running = true;
		run();
	}

	/**
	 * Stops the engine.
	 */
	public void stop()
	{
		running = false;
	}

	/**
	 * Start the main game loop.
	 */
	private void run()
	{
		// will catch any exceptions and generate a crash error
		try
		{
			// initialize the engine
			init();
			Core.getLog().info("Starting engine");

			long lastSecond = System.nanoTime();
			long thisFrame = System.nanoTime();
			long lastFrame = System.nanoTime();
			double updateTime = 1e9f / foregroundFps;
			double renderTime = 1e9f / foregroundUps;
			double accumulativeUpdateTime = 0;
			double accumulativeFrameTime = 0;
			long sleepTime = 0;
			int updateCount = 0;
			int frameCount = 0;

			while (running)
			{

				// Check if the game needs to stop.
				if (window.isCloseRequested())
				{
					stop();
				}

				// Get the delta between this frame and last frame
				thisFrame = System.nanoTime();
				delta = thisFrame - lastFrame;
				lastFrame = thisFrame;

				// Accumulate the delta in the update and render accumulate
				// variables.
				accumulativeUpdateTime += delta;
				accumulativeFrameTime += delta;

				int targetUps = 0;
				int targetFps = 0;

				if (window.isVisible())
				{
					targetFps = foregroundFps;
					targetUps = foregroundUps;
				} else
				{
					targetFps = backgroundFps;
					targetUps = backgroundUps;
				}

				updateTime = 1e9f / targetUps;
				renderTime = 1e9f / targetFps;

				// Check if window was resized
				if (window.wasResized())
				{
					app.resized();
				}

				// Update and take into account any missed frames.
				if (targetUps != -1)
				{
					while (accumulativeUpdateTime >= updateTime)
					{
						updateProfiler.start();
						update();
						updateProfiler.end();
						updateCount++;
						accumulativeUpdateTime -= updateTime;
					}
				} else
				{
					updateProfiler.start();
					update();
					updateProfiler.end();
					updateCount++;
					accumulativeUpdateTime = 0;
				}

				// Render if possible.
				if (targetFps != -1)
				{
					if (accumulativeFrameTime >= renderTime)
					{
						renderProfiler.start();
						render();
						renderProfiler.end();
						frameCount++;
						accumulativeFrameTime = 0;
					} else
					{

						// Calculate sleep time and sleep for that amount.

						sleepTime = (int) ((renderTime - accumulativeFrameTime) / (1e6f));

						try
						{
							Thread.sleep(sleepTime);
						} catch (InterruptedException e)
						{
							e.printStackTrace();
						}

					}
				} else
				{
					renderProfiler.start();
					render();
					renderProfiler.end();
					frameCount++;
					accumulativeFrameTime = 0;
				}

				// Check if a second has passed, and if so, update fps and ups.
				if (System.nanoTime() - lastSecond >= 1e9)
				{
					fps = frameCount;
					ups = updateCount;
					frameCount = 0;
					updateCount = 0;
					lastSecond = System.nanoTime();
				}

			}

			dispose();
			System.exit(0);
		} catch (Exception e)
		{
			Core.getLog().fatal("Crashing engine...");

			String crashReport = getStackTrace(e);
			if (e.getCause() != null)
			{
				crashReport += getStackTrace(e.getCause());
			}

			Exception disposeException = dispose();
			if (disposeException != null)
			{
				crashReport += "\n" + " Problems with disposing game with this exception!";
			}

			/*
			 * LOG THE CRASH TO A FILE
			 */

			try
			{
				File parent = new File(IOUtils.getRootParentPath() + File.separator + "error_logs");

				if (!parent.exists())
					if (!parent.mkdir())
						System.err.println("Could not make crash report directory!");

				String title = "crash_report_" + DateUtils.getMonth() + "-" + DateUtils.getDayOfMonth() + "-" + DateUtils.getYear() + "_at_" + DateUtils.getHours12() + "-" + DateUtils.getMinutes() + "-" + DateUtils.getSeconds();
				File file = new File(parent.getAbsolutePath() + File.separator + title);

				file.createNewFile();
				FileOutputStream os = new FileOutputStream(file);
				os.write(crashReport.getBytes());
				os.close();

				System.err.println("Logged crash");

			} catch (FileNotFoundException e1)
			{
				e1.printStackTrace();
			} catch (IOException e1)
			{
				e1.printStackTrace();
			}

			new CrashWindow(crashReport, "QuantumEpoch Crash Report", 400, 400);

		}

	}

	/**
	 * A utility method to get the stack trace as a string.
	 * 
	 * @param the
	 *            throwable to get the stack trace of
	 * @return the stack trace as a string
	 */
	private String getStackTrace(Throwable e)
	{
		StringBuilder crashReport = new StringBuilder();
		crashReport.append(e.toString() + "\n");
		StackTraceElement[] elem = e.getStackTrace();
		for (int i = 0; i < elem.length; i++)
		{
			if (i > 0)
			{
				crashReport.append("\t");
			}

			crashReport.append(elem[i].toString());

			crashReport.append("\n");
		}
		return crashReport.toString();
	}

	/**
	 * Initialize the engine and application.
	 */
	private void init()
	{

		ProfileTimer initProfiler = new ProfileTimer();
		initProfiler.start();

		// //////////////////////////////////////////////////

		// initialize logger with standard output stream
		try
		{
			log = new Logger(new OutputStream[] { System.out, new FileOutputStream(new File("./last_log_out.dat")) });
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}

		// initialize engine components

		Core.getLog().info("Initializing input subsystems");
		input = new Input();
		input.create();

		updateProfiler = new ProfileTimer();
		renderProfiler = new ProfileTimer();

		Core.getLog().info("Initializing application");
		// initialize application
		app.init();

		// //////////////////////////////////////////////////

		initProfiler.end();

		log.info("Engine initialization took " + (initProfiler.getDelta() / 1e6f) + " ms");
	}

	/**
	 * Update the engine and application.
	 */
	private void update()
	{
		if (splashScreens == null)
			current = -1;
		if (current != -1)
		{
			splashScreens.get(current).update();

			// by the time we have updated the splash screen, we need to check if the user hasn't skipped
			if (current != -1)
				if (splashScreens.get(current).getDuration().isReady())
				{
					current = (current + 1) % splashScreens.size();
					splashScreens.get(current).getDuration().reset();
					if (current == 0)
					{
						current = -1;
					}
				}
		} else
		{
			app.update();
		}
		input.update();

	}

	/**
	 * Render the engine and application.
	 */
	private void render()
	{

		graphics.clearColorBuffer();

		if (current != -1 && splashScreens != null)
		{
			splashScreens.get(current).render();
		} else
		{
			app.render();
		}

		// check for an opengl and openal errors after application renders
		graphics.checkGLErrors();
		audio.checkALErrors();

		window.update();

	}

	public void skipSplash()
	{
		current = -1;
	}

	/**
	 * Dispose the engine and application.
	 */
	private Exception dispose()
	{

		Exception ret = null;
		try
		{
			app.dispose();
		} catch (Exception e)
		{
			ret = e;
		}

		window.dispose();

		audio.dispose();

		return ret;

	}

}
