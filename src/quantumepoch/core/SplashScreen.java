package quantumepoch.core;

import quantumepoch.utils.Delay;

/** An abstraction for a splash screen. */
public abstract class SplashScreen
{

	/** the duration of the splash screen */
	private Delay duration;

	/** Constructs a splash screen with the specified duration */
	public SplashScreen(Delay duration)
	{
		this.duration = duration;
	}

	/** @return the duration of the splash screen */
	public Delay getDuration()
	{
		return duration;
	}

	/** sets the duration of the splash screen */
	public void setDuration(Delay duration)
	{
		this.duration = duration;
	}

	/** updates this splash screen */
	public abstract void update();

	/** renders this splash screen */
	public abstract void render();

}
