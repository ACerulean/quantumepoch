package quantumepoch.core.debug;

/**
 * A basic memory profiler to test memory usage.
 */
public class ProfileMemory
{

	/** the amount of free memory on the system */
	private long freeMemory;
	/** the maximum amount of memory the JVM will use */
	private long maxMemory;
	/** the current amount of memory used */
	private long memoryUsed;

	/**
	 * Creates a new memory profiler.
	 */
	public ProfileMemory()
	{

	}

	/**
	 * Updates the memory profiler with new statistics.
	 */
	public void update()
	{
		freeMemory = Runtime.getRuntime().freeMemory();
		maxMemory = Runtime.getRuntime().maxMemory();
		memoryUsed = maxMemory - freeMemory;
	}

	/**
	 * @return The amount of free memory in the current JVM.
	 */
	public long getFreeMemory()
	{
		return freeMemory;
	}

	/**
	 * @return The maximum amount of memory the JVM will use.
	 */
	public long getMaxMemory()
	{
		return maxMemory;
	}

	/**
	 * @return The current amount of memory the NVM has used.
	 */
	public long getMemoryUsed()
	{
		return memoryUsed;
	}

}
