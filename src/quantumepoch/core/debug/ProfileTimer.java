package quantumepoch.core.debug;

/**
 * A basic timer for profiling how long a section of code takes to execute. This can be used to test for bottlenecks in performant code.
 */
public class ProfileTimer
{

	/** stores whether this profiler timer has started */
	private boolean started;
	/** stores the start time in nanos of the profile timer */
	private long startInvocation;
	/** stores the end time in nanos of the profile timer */
	private long endInvocation;
	/** stores the change in time of the section of code that has been executed */
	private long delta;
	/** accumulative delta that stores the sum of all deltas */
	private long accumDelta;
	/** the amount of times a piece of code has been tested */
	private int invocationCount;

	/**
	 * @return wether this profile timer has started
	 */
	public boolean isStarted()
	{
		return started;
	}

	/**
	 * @return the start time of this profile timer
	 */
	public long getStartInvocation()
	{
		return startInvocation;
	}

	/**
	 * @return the end time of this profiler
	 */
	public long getEndInvocation()
	{
		return endInvocation;
	}

	/**
	 * @return the change in time of a section of code that has been tested
	 */
	public long getDelta()
	{
		return delta;
	}

	/**
	 * @return the sum of all deltas
	 */
	public long getAccumDelta()
	{
		return accumDelta;
	}

	/**
	 * @return the amount of time a piece of code has been tested
	 */
	public int getInvocationCount()
	{
		return invocationCount;
	}

	/**
	 * Starts this profile timer. An invocation to end will store timer statistics about the code that was ran.
	 */
	public void start()
	{
		if (!started)
		{
			started = true;
			startInvocation = System.nanoTime();
		} else
		{
			throw new IllegalStateException("Profile timer already started");
		}

	}

	/**
	 * Ends this profile timer. Stores statistics about how long this last piece of code ran took.
	 */
	public void end()
	{
		if (started)
		{
			endInvocation = System.nanoTime();
			delta = endInvocation - startInvocation;
			accumDelta += delta;
			invocationCount++;
			started = false;
		} else
		{
			throw new IllegalStateException("Must start profile timer before invocating end");
		}
	}

	/**
	 * Resets the statistics of this profile timer.
	 */
	public void reset()
	{
		started = false;
		startInvocation = 0;
		endInvocation = 0;
		delta = 0;
		accumDelta = 0;
		invocationCount = 0;
	}

}
