package quantumepoch.physics;

import quantumepoch.utils.math.Vector2f;

/**
 * Class binding circle part of the volume class, extends the 2D volume binding
 * class.
 *
 */
public class BoundingCircle extends BoundingVolume2D
{

	private float radius;

	/**
	 * empty constructor taking no parameters
	 */
	public BoundingCircle()
	{

	}

	/**
	 * constructor taking the position and radius of the circle
	 * 
	 * @param position
	 *            the position of the circle
	 * @param radius
	 *            the radius of the circle
	 */
	public BoundingCircle(Vector2f position, float radius)
	{
		super(position);
		this.radius = radius;
	}

	/**
	 * gets the radius of the circle
	 * 
	 * @return returns the radius
	 */
	public float getRadius()
	{
		return radius;
	}

	/**
	 * sets the radius of the circle
	 * 
	 * @param radius
	 *            the radius to be set
	 */
	public void setRadius(float radius)
	{
		this.radius = radius;
	}

}
