package quantumepoch.physics;

import quantumepoch.utils.math.Vector2f;

/**
 * Abstract class to set standards for the other binding classes and methods to
 * use for binding processes
 *
 */
public abstract class BoundingVolume2D
{

	protected Vector2f position;

	/**
	 * constructor taking no params setting position of binding
	 */
	public BoundingVolume2D()
	{
		position = new Vector2f();
	}

	/**
	 * constructor taking in the position to set
	 * 
	 * @param position
	 */
	public BoundingVolume2D(Vector2f position)
	{
		this.position = position;
	}

	/**
	 * gets the position of the vector for binding
	 * 
	 * @return returns the position
	 */
	public Vector2f getPosition()
	{
		return position;
	}

	/**
	 * sets the position of the binding vector
	 * 
	 * @param position
	 *            the position to set
	 */
	public void setPosition(Vector2f position)
	{
		this.position = position;
	}

	/**
	 * method if intersecting binding vectors, makes new vectors for binding
	 * process.
	 * 
	 * @param other
	 *            the other binding vector to check for intersection
	 * @return returns true if intersecting vectors
	 */
	public boolean intersects(BoundingVolume2D other)
	{
		if (this instanceof BoundingBox2D)
		{
			if (other instanceof BoundingBox2D)
			{
				return boxBox((BoundingBox2D) this, (BoundingBox2D) other);
			} else if (other instanceof BoundingCircle)
			{
				return boxCircle((BoundingBox2D) this, (BoundingCircle) other);
			} else
			{
				return false;
			}
		} else if (this instanceof BoundingCircle)
		{
			if (other instanceof BoundingBox2D)
			{
				return boxCircle((BoundingBox2D) other, (BoundingCircle) this);
			} else if (other instanceof BoundingCircle)
			{
				return circleCircle((BoundingCircle) this, (BoundingCircle) other);
			} else
			{
				return false;
			}
		} else
		{
			return false;
		}
	}

	/**
	 * compares the two boxes for binding
	 * 
	 * @param box1
	 *            first box to compare
	 * @param box2
	 *            second box to compare
	 * @return returns true if same
	 */
	private boolean boxBox(BoundingBox2D box1, BoundingBox2D box2)
	{
		Vector2f p1 = box1.position;
		Vector2f p2 = box2.position;
		if (p1.getX() > (p2.getX() + box2.getWidth()) || (p1.getX() + box1.getWidth()) < p2.getX() || p1.getY() > (p2.getY() + box2.getHeight()) || (p1.getY() + box1.getHeight()) < p2.getY())
		{
			return false;
		} else
		{
			return true;
		}
	}

	/**
	 * compares the circle of the volume for binding
	 * 
	 * @param c1
	 *            first circle to compare
	 * @param c2
	 *            second circle to compare
	 * @return returns true if same
	 */
	private boolean circleCircle(BoundingCircle c1, BoundingCircle c2)
	{
		float radiusSum = c1.getRadius() + c2.getRadius();
		float dx = c1.position.getX() - c2.position.getX();
		float dy = c1.position.getY() - c2.position.getY();

		if (dx * dx + dy * dy <= radiusSum * radiusSum)
		{
			return true;
		} else
		{
			return false;
		}

	}

	/**
	 * compares both box and circles for binding if they are the same
	 * 
	 * @param box
	 *            the box to compare
	 * @param circle
	 *            the circle to compare
	 * @return
	 */
	private boolean boxCircle(BoundingBox2D box, BoundingCircle circle)
	{
		for (float x = box.position.getX(); x < box.position.getX() + box.getWidth(); x += box.getWidth() / 10f)
		{
			for (float y = box.position.getY(); y < box.position.getY() + box.getHeight(); y += box.getHeight() / 10f)
			{
				float dx = x - circle.position.getX();
				float dy = y - circle.position.getY();

				if (dx * dx + dy * dy <= circle.getRadius() * circle.getRadius())
				{
					return true;
				}
			}
		}
		return false;
	}
}
