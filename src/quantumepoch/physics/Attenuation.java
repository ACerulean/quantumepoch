package quantumepoch.physics;

/**
 * Stores information about a quadratic.
 * 
 * term1 constant of the quadratic 
 * term2 linear part of the quadratic 
 * term3 the quadratic of the equation
 *
 */
public class Attenuation
{

	private float constant;
	private float linear;
	private float quadratic;

	/**
	 * constructor of Attenuation, requires all parts of equation
	 * 
	 * @param constant
	 *            the constant of the equation
	 * @param linear
	 *            the linear variable of the quadratic
	 * @param quadratic
	 *            squared variable of the equation
	 */
	public Attenuation(float constant, float linear, float quadratic)
	{
		this.constant = constant;
		this.linear = linear;
		this.quadratic = quadratic;
	}

	/**
	 * gets the constant of the quadratic
	 * 
	 * @return returns the constant
	 */
	public float getConstant()
	{
		return constant;
	}

	/**
	 * gets the linear variable of the equation
	 * 
	 * @return returns the linear
	 */
	public float getLinear()
	{
		return linear;
	}

	/**
	 * gets the quadratic of the equation
	 * 
	 * @return returns the quadratic
	 */
	public float getQuadratic()
	{
		return quadratic;
	}

	/**
	 * finds the quadratic at the distance/location
	 * 
	 * @param distance
	 *            variable for the quadratic
	 * @return returns the new attenuation
	 */
	public float attenuationAt(float distance)
	{
		return 1.0f / (quadratic * distance * distance + linear * distance + constant);
	}

	/**
	 * sets the constant of the quadratic
	 * 
	 * @param constant
	 *            the constant to be set
	 */
	public void setConstant(float constant)
	{
		this.constant = constant;
	}

	/**
	 * sets the linear of the quadratic
	 * 
	 * @param linear
	 *            the linear to be set
	 */
	public void setLinear(float linear)
	{
		this.linear = linear;
	}

	/**
	 * sets the quadratic of the equation
	 * 
	 * @param quadratic
	 *            the quadratic to be set
	 */
	public void setQuadratic(float quadratic)
	{
		this.quadratic = quadratic;
	}

}
