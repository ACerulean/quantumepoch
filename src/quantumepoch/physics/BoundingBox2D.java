package quantumepoch.physics;

import quantumepoch.utils.math.Vector2f;

/**
 * Class for binding the dimensions of the screen and tiles of the game. Extends
 * the 2dVolume binding class.
 *
 */
public class BoundingBox2D extends BoundingVolume2D
{

	private float width;
	private float height;

	/**
	 * Empty constructor taking no params for BoundingBox2D
	 */
	public BoundingBox2D()
	{

	}

	/**
	 * Constructor taking in all the dimension of the box
	 * 
	 * @param position
	 *            position of box
	 * @param width
	 *            width of box
	 * @param height
	 *            height of box
	 */
	public BoundingBox2D(Vector2f position, float width, float height)
	{
		super(position);
		this.width = width;
		this.height = height;
	}

	/**
	 * gets the width of the box
	 * 
	 * @return returns width
	 */
	public float getWidth()
	{
		return width;
	}

	/**
	 * gets the height of the box
	 * 
	 * @return returns the height
	 */
	public float getHeight()
	{
		return height;
	}

	/**
	 * sets the width of the box
	 * 
	 * @param width
	 *            width to set
	 */
	public void setWidth(float width)
	{
		this.width = width;
	}

	public void setHeight(float height)
	{
		this.height = height;
	}

}
