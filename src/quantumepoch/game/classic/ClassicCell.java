package quantumepoch.game.classic;

import quantumepoch.game.tetris.Cell;
import quantumepoch.game.tetris.CellLocation;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * Represents a tetris cell of the classic mode tetris game.
 *
 */
public class ClassicCell extends Cell
{

	/**
	 * classic cell constructor that creates the cell block and calls to super class to instantiate in grid 
	 * @param location the location to create block
	 * @param tetrisBlock the tetris block to create
	 * @param color the color of tetris block
	 * @param texture the texture to display the block
	 */
	public ClassicCell(CellLocation location, boolean tetrisBlock, Color color, TextureRegion texture)
	{
		super(location, tetrisBlock, color, texture);
	}

}
