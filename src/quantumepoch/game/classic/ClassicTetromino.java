package quantumepoch.game.classic;

import quantumepoch.game.tetris.CellLocation;
import quantumepoch.game.tetris.TetrisGrid;
import quantumepoch.game.tetris.Tetromino;
import quantumepoch.game.tetris.TetrominoType;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * The classic tetris blocks specifically for the classic mode game, extension of
 * the tetromino tetris block class
 *
 */
public class ClassicTetromino extends Tetromino
{

	/**
	 * classic tetris block constructor that chooses the starting block attributes and calls to instantiate
	 * it in the grid
	 * @param grid the grid to create the blocks in
	 * @param start the starting block in the game
	 * @param type the type of block next
	 * @param texture the texture of the tetris block
	 * @param color the color of the tetris block
	 */
	public ClassicTetromino(TetrisGrid grid, CellLocation start, TetrominoType type, TextureRegion texture, Color color)
	{
		super(grid, start, type, color, texture);

		// int index = -1;
		// // find the correct index
		// while (TetrominoType.values()[index++] != type)
		// ;
		//
		// setTexture(TetrisGrid.BLOCK_TEXTURE);
		// setColor(ClassicGrid.TETROMINO_COLORS[index]);
	}
}
