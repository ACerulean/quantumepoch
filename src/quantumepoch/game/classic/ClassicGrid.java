package quantumepoch.game.classic;

import quantumepoch.game.tetris.Cell;
import quantumepoch.game.tetris.CellLocation;
import quantumepoch.game.tetris.TetrisGrid;
import quantumepoch.game.tetris.Tetromino;
import quantumepoch.game.tetris.TetrominoType;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.batch.ShapeBatch;
import quantumepoch.graphics.batch.SpriteBatch;

/**
 * Represents a tetris grid of the classic mode tetris game.
 */
public class ClassicGrid extends TetrisGrid
{
	/** Color of the grid */
	public static final Color[] TETROMINO_COLORS = new Color[] { Color.RED, Color.BLUE, Color.GREEN, Color.INDIGO, Color.CYAN, Color.CORNFLOWER_BLUE, Color.GOLD };

	/**
	 * Creates a new classic grid
	 * 
	 * @param width
	 *            The width of the grid
	 * @param height
	 *            The height of the grid
	 */
	public ClassicGrid(int width, int height)
	{
		super(width, height, true);
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				cells[x + (y * width)] = new ClassicCell(new CellLocation(x, y), false, Color.WHITE, BLOCK_TEXTURE);
			}
		}
	}

	/**
	 * override method to save the tetris blocks to the grid as save state
	 */
	@Override
	public void saveTetrominoToGrid()
	{
		for (CellLocation location : currentTetromino.getBlockLocations())
		{
			cells[location.getX() + (location.getY() * width)].setTetrisBlock(true);
			((ClassicCell) getCellAt(location.getX(), location.getY())).setColor(((ClassicTetromino) currentTetromino).getColor());
		}
	}

	/**
	 * override method to generate the tetris block and pick random block to instantiate in grid
	 * @return returns the tetris block to be generated.
	 */
	@Override
	public Tetromino generateTetromino()
	{
		int index = rand.nextInt(TetrominoType.values().length);

		TetrominoType gen = TetrominoType.values()[index];
		while (isTetrominoOnQueue(gen))
		{
			index = (index + 1) % TetrominoType.values().length;
			gen = TetrominoType.values()[index];
		}
		// // check if we just had that tetromino
		// if (currentTetromino != null && (currentTetromino.getType() ==
		// TetrominoType.values()[index]))
		// {
		// // increment to the next tetromino
		// }

		Color randColor = TETROMINO_COLORS[index];
		return new ClassicTetromino(this, new CellLocation(width / 2, height - 2), gen, BLOCK_TEXTURE, randColor);
	}

	/**
	 * the keyboard command to instantly drop the tetris block into the grid row
	 * @param linesToDrop the lines the block is in to drop
	 * @param minComplete the minimum blocks to drop
	 */
	@Override
	public void dropRows(int linesToDrop, int minComplete)
	{
		for (int i = 0; i < linesToDrop; i++)
		{
			for (int y = minComplete + 1; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					if (getCellAt(x, y).isTetrisBlock())
					{
						ClassicCell current = (ClassicCell) getCellAt(x, y);
						ClassicCell under = (ClassicCell) getCellAt(x, y - 1);
						under.setTetrisBlock(true);
						under.setColor(current.getColor());
						current.setTetrisBlock(false);
					}
				}
			}
		}
	}

	/**
	 * method to render the classic mode grid of tetris
	 * @param spriteBatch the batch file to pull the tetris block sprites from
	 * @param shapeBatch the batch file to pull the tetris shapes from
	 * @param xPosition the x position to pull the block from
	 * @param yPosition the x position to pull the block from
	 */
	@Override
	public void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch, int xPosition, int yPosition)
	{
		super.render(spriteBatch, shapeBatch, xPosition, yPosition);
		
		spriteBatch.setTint(Color.WHITE);
		if (getCurrentTetromino() != null)
			for (CellLocation loc : getCurrentFall())
			{
				spriteBatch.render(((ClassicTetromino) currentTetromino).getTexture(), xPosition + loc.getX() * Cell.SIZE, yPosition + loc.getY() * Cell.SIZE);
			}

		if (getCurrentTetromino() != null)
			spriteBatch.setTint(((ClassicTetromino) currentTetromino).getColor());
		if (getCurrentTetromino() != null)

			for (CellLocation location : currentTetromino.getBlockLocations())
			{
				spriteBatch.render(((ClassicTetromino) currentTetromino).getTexture(), xPosition + location.getX() * Cell.SIZE, yPosition + location.getY() * Cell.SIZE);
			}
		for (Cell cell : cells)
		{
			if (cell.isTetrisBlock())
			{
				spriteBatch.setTint(((ClassicCell) cell).getColor());
				spriteBatch.render(((ClassicCell) cell).getTexture(), xPosition + cell.getLocation().getX() * Cell.SIZE, yPosition + cell.getLocation().getY() * Cell.SIZE);
			}
		}
	}

}
