package quantumepoch.game;

import quantumepoch.graphics.Color;
import quantumepoch.graphics.texture.TextureRegion;
import quantumepoch.utils.math.Vector3f;

/**
 * Represents a single tetris starfield piece.
 */
public class StarfieldPiece
{

	/** the position of the starfield piece */
	private Vector3f pos;
	/** the texture of the starfield piece */
	private TextureRegion texture;
	/** the color of the starfield piece */
	private Color color;
	/** the rotation of the starfield piece */
	private float rot;
	/** the rotational velocity of the starfield piece */
	private float rotVel;

	/**
	 * Creates a new starfield piece
	 * 
	 * @param pos
	 *            The position
	 * @param texture
	 *            The texture
	 * @param color
	 *            The color
	 * @param rot
	 *            The rotation
	 * @param rotVel
	 *            The rotational velocity
	 */
	public StarfieldPiece(Vector3f pos, TextureRegion texture, Color color, float rot, float rotVel)
	{
		this.pos = pos;
		this.texture = texture;
		this.color = color;
		this.rot = rot;
		this.rotVel = rotVel;
	}

	/**
	 * @return the position
	 */
	public Vector3f getPos()
	{
		return pos;
	}

	/**
	 * Sets the position
	 * 
	 * @param pos
	 *            The new position
	 */
	public void setPos(Vector3f pos)
	{
		this.pos = pos;
	}

	/**
	 * @return the texture
	 */
	public TextureRegion getTexture()
	{
		return texture;
	}

	/**
	 * Sets the texture
	 * 
	 * @param texture
	 *            The new texture
	 */
	public void setTexture(TextureRegion texture)
	{
		this.texture = texture;
	}

	/**
	 * @return the color
	 */
	public Color getColor()
	{
		return color;
	}

	/**
	 * Sets the color
	 * 
	 * @param color
	 *            The new color
	 */
	public void setColor(Color color)
	{
		this.color = color;
	}

	/**
	 * @return the rotation
	 */
	public float getRot()
	{
		return rot;
	}

	/**
	 * Sets the rotation
	 * 
	 * @param rot
	 *            The new rotation
	 */
	public void setRot(float rot)
	{
		this.rot = rot;
	}

	/**
	 * @return the rotational velocity
	 */
	public float getRotVel()
	{
		return rotVel;
	}

	/**
	 * Sets the rotation velocity
	 * 
	 * @param rotVel
	 *            The new rotational velocity
	 */
	public void setRotVel(float rotVel)
	{
		this.rotVel = rotVel;
	}

	/**
	 * Updates the state
	 */
	public void update()
	{
		rot += rotVel;
	}

}
