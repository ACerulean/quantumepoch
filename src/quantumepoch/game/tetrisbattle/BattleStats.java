package quantumepoch.game.tetrisbattle;

/**
 * A data structure storing stats for a tetris battle.
 */
public class BattleStats
{

	/** whether the tetris battle has ended */
	private boolean battleOver;
	/** whether the player has won */
	private boolean playerWon;
	/** the amount of experiance the player has earned */
	private int expEarned;

	/**
	 * @return whether the tetris battle has ended
	 */
	public boolean isBattleOver()
	{
		return battleOver;
	}

	/**
	 * Sets whether the tetris battle is over
	 * 
	 * @param battleOver
	 *            Whether the tetris battle is over
	 */
	public void setBattleOver(boolean battleOver)
	{
		this.battleOver = battleOver;
	}

	/**
	 * @return Whether the player has won the battle
	 */
	public boolean hasPlayerWon()
	{
		return playerWon;
	}

	/**
	 * Sets if the player has won.
	 * 
	 * @param playerWon
	 *            Whether the player has won
	 */
	public void setPlayerWon(boolean playerWon)
	{
		this.playerWon = playerWon;
	}

	/**
	 * @return the amount of experiance the player has earned from this battle
	 */
	public int getExpEarned()
	{
		return expEarned;
	}

	/**
	 * @param expEarned
	 *            the amount of exp the player has earned from this battle
	 */
	public void setExpEarned(int expEarned)
	{
		this.expEarned = expEarned;
	}

}
