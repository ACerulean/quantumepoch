package quantumepoch.game.tetrisbattle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.UnsupportedAudioFileException;

import quantumepoch.audio.AudioBuffer;
import quantumepoch.audio.AudioSource;
import quantumepoch.audio.WaveAudio;
import quantumepoch.core.Core;
import quantumepoch.game.Game;
import quantumepoch.game.Sounds;
import quantumepoch.game.Textures;
import quantumepoch.game.adventure.AdventureGrid;
import quantumepoch.game.adventure.GridListener;
import quantumepoch.game.classic.ClassicCell;
import quantumepoch.game.tetris.Cell;
import quantumepoch.game.tetris.CellLocation;
import quantumepoch.game.tetris.StandardControl;
import quantumepoch.game.tetris.TetrisGrid;
import quantumepoch.game.world.World;
import quantumepoch.game.world.entity.imment.ImmobileEntity;
import quantumepoch.game.world.entity.mob.MobileEntity;
import quantumepoch.game.world.entity.mob.battle.BattleEnemy;
import quantumepoch.game.world.entity.mob.battle.BattlePlayer;
import quantumepoch.game.world.entity.particle.Particle;
import quantumepoch.game.world.entity.particle.TetrisParticle;
import quantumepoch.game.world.entity.projectile.Projectile;
import quantumepoch.graphics.Color;
import quantumepoch.physics.BoundingBox2D;
import quantumepoch.utils.math.Vector2f;

/**
 * Represents and handles tetris battles that occur in adventure mode.
 */
public class TetrisBattle implements GridListener
{

	/** the world the battle is happening in */
	private World world;
	/** the battle player of the battle */
	private BattlePlayer player;
	/** the battle enemy of the battle */
	private BattleEnemy opponent;
	/** the grid of the player */
	private AdventureGrid playerGrid;
	/** the list of mobs in the tetris battle */
	private List<MobileEntity> mobs;
	/** the list of projectiles in the tetris battle */
	private List<Projectile> projectiles;
	/** the list of particles in the tetris battle */
	private List<Particle> particles;
	/** the list of immobile entities in the tetris battle */
	private List<ImmobileEntity> immobileEntities;
	/** whether the battle has ended */
	private boolean battleOver;
	/** the tetris theme song */
	private AudioSource source, chargedSound, lineCleared;
	/** standard controls for tetris gameplay */
	private StandardControl stdControl;

	/**
	 * Creates a new tetris battle
	 * 
	 * @param world
	 *            The world the battle is occuring in
	 */
	public TetrisBattle(World world)
	{
		this.world = world;
		playerGrid = new AdventureGrid(10, 21, true);

		playerGrid.addGridListener(this);

		mobs = new ArrayList<MobileEntity>();
		projectiles = new ArrayList<Projectile>();
		particles = new ArrayList<Particle>();
		immobileEntities = new ArrayList<ImmobileEntity>();

		WaveAudio track = null;
		try
		{
			track = new WaveAudio("music/tetris_theme.wav");
		} catch (UnsupportedAudioFileException | IOException e)
		{
			e.printStackTrace();
		}
		source = new AudioSource(new AudioBuffer(track.getFormat(), track.getData(), track.getSampleRate()));

		chargedSound = new AudioSource(new AudioBuffer(Sounds.CHARGED.getTrack().getFormat(), Sounds.CHARGED.getTrack().getData(), Sounds.CHARGED.getTrack().getSampleRate()));
		lineCleared = new AudioSource(new AudioBuffer(Sounds.LINE_CLEARED.getTrack().getFormat(), Sounds.LINE_CLEARED.getTrack().getData(), Sounds.LINE_CLEARED.getTrack().getSampleRate()));

		AudioSource.stopAll();
		source.play();
		source.setLooping(true);

		stdControl = new StandardControl();
	}

	/**
	 * @return the world the battle is occurring in
	 */
	public World getWorld()
	{
		return world;
	}

	/**
	 * @return the opponent of the battle
	 */
	public BattleEnemy getOpponent()
	{
		return opponent;
	}

	/**
	 * Initializes the battle with the specified player and opponent.
	 * 
	 * @param player
	 *            The player of the battle
	 * @param opponent
	 *            The opponent of the battle
	 */
	public void init(BattlePlayer player, BattleEnemy opponent)
	{
		this.player = player;
		this.opponent = opponent;

		mobs.add(player);
		mobs.add(opponent);
	}

	/**
	 * @return the player battling
	 */
	public BattlePlayer getPlayer()
	{
		return player;
	}

	/**
	 * @return the player's tetris grid
	 */
	public TetrisGrid getPlayerTetrisGrid()
	{
		return playerGrid;
	}

	/**
	 * @return the background audio playing in this battle
	 */
	public AudioSource getAudio()
	{
		return source;
	}

	//
	// /**
	// * @return the enemie's tetris grid
	// */
	// public TetrisGrid getEnemyTetrisGrid()
	// {
	// return enemyGrid;
	// }
	//
	/**
	 * @return the list of mobs in this tetris battle
	 */
	public List<MobileEntity> getMobs()
	{
		return mobs;
	}

	/**
	 * @return the list of projectiles in this tetris battle
	 */
	public List<Projectile> getProjectiles()
	{
		return projectiles;
	}

	/**
	 * @return the list of particles in this tetris battle
	 */
	public List<Particle> getParticles()
	{
		return particles;
	}

	/**
	 * @return the list of immobile entities in this tetris battle
	 */
	public List<ImmobileEntity> getImmobileEntities()
	{
		return immobileEntities;
	}

	/**
	 * Updates this tetris battle. This updates all entities within the battle, and the tetris grid involved.
	 */
	public void update()
	{
		if (!battleOver)
		{
			if (playerGrid.hasOverflown())
			{
				source.stop();
				getWorld().getBattleStats().setBattleOver(true);
				getWorld().getBattleStats().setPlayerWon(false);
				battleOver = true;
			}
			for (int i = 0; i < mobs.size(); i++)
			{
				mobs.get(i).update();
			}
			for (int i = 0; i < projectiles.size(); i++)
			{
				projectiles.get(i).update();
			}
			for (int i = 0; i < particles.size(); i++)
			{
				particles.get(i).update();
			}
			for (int i = 0; i < immobileEntities.size(); i++)
			{
				immobileEntities.get(i).update();
			}

			stdControl.update(playerGrid);
			playerGrid.update();

			Game.getInstance().getLightRenderer().getOccluders().clear();

			for (Cell cell : playerGrid.getCells())
			{
				if (cell.isTetrisBlock())
				{
					Game.getInstance().getLightRenderer().getOccluders().add(new BoundingBox2D(new Vector2f((Core.getWindow().getWidth() - (playerGrid.getWidth() * Cell.SIZE)) / 2 + cell.getLocation().getX() * Cell.SIZE, 10 + cell.getLocation().getY() * Cell.SIZE), Cell.SIZE, Cell.SIZE));
				}
			}

			for (CellLocation cell : playerGrid.getCurrentTetromino().getBlockLocations())
			{
				Game.getInstance().getLightRenderer().getOccluders().add(new BoundingBox2D(new Vector2f((Core.getWindow().getWidth() - (playerGrid.getWidth() * Cell.SIZE)) / 2 + cell.getX() * Cell.SIZE, 10 + cell.getY() * Cell.SIZE), Cell.SIZE, Cell.SIZE));
			}

			if (player.getHealth() <= 0)
			{
				// enemy wins
				getWorld().getBattleStats().setBattleOver(true);
				getWorld().getBattleStats().setPlayerWon(false);
				battleOver = true;
				source.stop();
				Game.getInstance().getLightRenderer().getOccluders().clear();
			} else if (opponent.getHealth() <= 0)
			{
				// player wins
				getWorld().getBattleStats().setBattleOver(true);
				getWorld().getBattleStats().setPlayerWon(true);
				battleOver = true;
				source.stop();
				Game.getInstance().getLightRenderer().getOccluders().clear();
			}
		} else
		{
		}
	}

	/**
	 * Renders the tetris battle to the screen.
	 */
	public void render()
	{
		if (!battleOver)
		{
			Game.getInstance().getSpriteBatch().setTint(Color.WHITE);
			Game.getInstance().getSpriteBatch().render(Textures.BATTLE_BG.getTexture(), 0, 0);
			player.setRotation((float) Math.toRadians(180));

			for (MobileEntity mob : mobs)
			{
				mob.render();
			}
			for (Projectile proj : projectiles)
			{
				proj.render();
			}

			for (ImmobileEntity imment : immobileEntities)
			{
				imment.render();
			}
			Game.getInstance().getSpriteBatch().reset();

			int w = playerGrid.getWidth() * Cell.SIZE;

			playerGrid.render(Game.getInstance().getSpriteBatch(), Game.getInstance().getShapeBatch(), (Core.getWindow().getWidth() - w) / 2, 10);

			// render particles over the grid
			for (Particle particle : particles)
			{
				particle.render();
			}

			// enemyGrid.render(Game.getInstance().getSpriteBatch(),
			// Game.getInstance().getShapeBatch(), Core.getWindow().getWidth() -
			// Cell.SIZE * enemyGrid.getWidth() - 300, 10);
		}
	}

	@Override
	public void rowDropped(AdventureGrid source, ClassicCell[] cells)
	{
		float y = 0;
		for (ClassicCell cell : cells)
			if (cell != null)
			{
				y = cell.getLocation().getY() * Cell.SIZE;
				break;
			}

		lineCleared.play();

		spawnFireworks((Core.getWindow().getWidth() - (playerGrid.getWidth() * Cell.SIZE)) / 2, y, 4);

		getWorld().getBattleStats().setExpEarned(getWorld().getBattleStats().getExpEarned() + 1);

		player.getAbility().setChargePercent(Math.min(player.getAbility().getChargePercent() + 0.4f, 1.0f));
		if (player.getAbility().getChargePercent() >= 1)
			chargedSound.play();
	}

	/**
	 * Spawns count amount of firework particles at [x,y]
	 * 
	 * @param x
	 * @param y
	 * @param count
	 *            The amount of particles
	 */
	public void spawnFireworks(float x, float y, float count)
	{
		for (int i = 0; i < count; i++)
		{
			for (float x_ = x; x_ <= x + playerGrid.getWidth() * Cell.SIZE; x_ += (playerGrid.getWidth() * Cell.SIZE) / count)
				getParticles().add(new TetrisParticle(world, x_, y, 10, 500));
		}
	}
}
