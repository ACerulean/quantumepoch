package quantumepoch.game;

import quantumepoch.game.world.entity.Entity;
import quantumepoch.graphics.Color;

/**
 * A few render utils for the game
 */
public class RenderUtils
{

	/**
	 * Renders a health bar above an entity's head/
	 * 
	 * @param e
	 *            The entity
	 * @param percent
	 *            The percent of health
	 * @param inColor
	 *            The inside color of the heatlh bar
	 * @param outColor
	 *            The outside color of the health bar
	 */
	public static void renderHealthBar(Entity e, float percent, Color inColor, Color outColor)
	{
		renderHealthBar(e.getX() - e.getWidth() / 2, e.getY() + e.getHeight() + 5, 2 * e.getWidth(), 10, percent, inColor, outColor);
	}

	/**
	 * Renders a health bar at the specified position and size.
	 * 
	 * @param x
	 *            The health bar x
	 * @param y
	 *            The health bar y
	 * @param w
	 *            The health bar width
	 * @param h
	 *            The health bar height
	 * @param percent
	 *            The percent of health
	 * @param inColor
	 *            The inside color of the health bar
	 * @param outColor
	 *            The outside color of the health bar
	 */
	public static void renderHealthBar(float x, float y, float w, float h, float percent, Color inColor, Color outColor)
	{
		if (!Game.getInstance().getShapeBatch().hasBegan())
			Game.getInstance().getShapeBatch().begin();

		Game.getInstance().getShapeBatch().setColor(inColor);
		Game.getInstance().getShapeBatch().renderFilledRect(x, y, w, h);

		Game.getInstance().getShapeBatch().setColor(outColor);
		Game.getInstance().getShapeBatch().renderFilledRect(x, y, percent * w, h);
	}

}
