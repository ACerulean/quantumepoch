package quantumepoch.game.inventory;

import quantumepoch.game.world.entity.Entity;

/** Class for an entities inventory */
public class Inventory
{
	/** Size of inventory */
	private int size;
	/** The entity that has the inventory */
	private Entity holder;
	/** Array that holds what's in the inventory */
	private InventoryEntry[] entries;

	/**
	 * Creates a new inventory
	 * 
	 * @param size
	 *            The inventory size
	 */
	public Inventory(int size)
	{
		this(size, null);
	}

	/**
	 * Creates a new inventory
	 * 
	 * @param size
	 *            The inventory size
	 * @param holder
	 *            The entity for the inventory
	 */
	public Inventory(int size, Entity holder)
	{
		this.size = size;
		this.holder = holder;
		this.entries = new ItemStack[size];
	}

	/**
	 * @return the size of the inventory
	 */
	public int getSize()
	{
		return size;
	}

	/**
	 * @return the entity that the inventory is for
	 */
	public Entity getHolder()
	{
		return holder;
	}

	/**
	 * @return the the array that holds what is in the inventory
	 */
	public InventoryEntry[] getEntries()
	{
		return entries;
	}

	/**
	 * Sets the size of the inventory
	 * 
	 * @param size
	 *            The new inventory size
	 */
	public void setSize(int size)
	{
		this.size = size;
		InventoryEntry[] newStack = new InventoryEntry[size];

		for (int i = 0; i < newStack.length; i++)
		{
			if (i < entries.length)
			{
				newStack[i] = entries[i];
			}
		}

		entries = newStack;
	}

	/**
	 * @param index
	 *            Location of the item in the array.
	 * @return the item at index
	 */
	public InventoryEntry getItemAt(int index)
	{
		return entries[index];
	}

	/**
	 * Adds an item to to the inventory
	 * 
	 * @param item
	 *            The item being added
	 * @return The index where the item was added or -1 if the inventory was
	 *         full
	 */
	public int addEntry(InventoryEntry item)
	{
		for (int i = 0; i < size; i++)
		{
			if (entries[i] == null)
			{
				entries[i] = item;
				return i;
			}
		}
		return -1;
	}

	/**
	 * Sets the entity for the inventory
	 * 
	 * @param holder
	 *            The entity for the inventory
	 */
	public void setHolder(Entity holder)
	{
		this.holder = holder;
	}

}
