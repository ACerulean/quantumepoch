package quantumepoch.game.inventory;

import quantumepoch.audio.AudioBuffer;
import quantumepoch.audio.AudioSource;
import quantumepoch.game.Sounds;
import quantumepoch.game.world.entity.Entity;
import quantumepoch.game.world.entity.projectile.WizardBall;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * Wizard damage attack ability that will shoot a blue magic ball at the target,
 * causing damage based on the amount of charge loaded up.
 */
public class PlasmaBall extends DamageAttack
{

	/**
	 * Creates a new plasma ball ability for the wizard.
	 * 
	 * @param name
	 *            The name of the ability.
	 * @param abilityTexture
	 *            The texture of the ability.
	 * @param chargePercent
	 *            The percentage of the charge.
	 * @param holder
	 *            The entity that has the ability.
	 * @param damage
	 *            The max damage this ability projectile will do.
	 */
	public PlasmaBall(String name, TextureRegion abilityTexture, float chargePercent, Entity holder, float damage)
	{
		super(name, abilityTexture, chargePercent, holder, damage);
		sound = new AudioSource(new AudioBuffer(Sounds.WIZARD_BALL.getTrack().getFormat(), Sounds.WIZARD_BALL.getTrack().getData(), Sounds.WIZARD_BALL.getTrack().getSampleRate()));
	}

	/**
	 * Will use the ability if the charge has a percentage higher than 0. Also
	 * will shoot a wizard ball if the requirement is met.
	 */
	@Override
	public void use()
	{
		if (getChargePercent() > 0)
		{
			getHolder().getWorld().getBattle().getProjectiles().add(new WizardBall(getHolder().getWorld(), getHolder().getX() + getHolder().getWidth() / 2, getHolder().getY() + getHolder().getHeight() / 2, 0, getChargePercent() * getDamage(), getHolder(), Color.CORNFLOWER_BLUE));
			playAudio();
		}
	}
}
