package quantumepoch.game.inventory;

/** Class for the health potion item */
public class HealthPotion extends Item
{

	/**
	 * Creates a health potion
	 */
	public HealthPotion()
	{
		super(0, "Health Potion");
	}

	@Override
	public void use(Inventory inv)
	{
		
	}

}
