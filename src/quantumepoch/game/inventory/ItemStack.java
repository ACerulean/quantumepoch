package quantumepoch.game.inventory;

/** Class used to create and change stacks of items */
public class ItemStack extends InventoryEntry
{
	/** The items in the stack */
	private Item item;
	/** The amount of an item in the stack */
	private int count;

	/**
	 * Creates a new item stack
	 */
	public ItemStack()
	{
		this(null, 0);
	}

	/**
	 * Creates a new item stack
	 * 
	 * @param item
	 *            The items in the stack
	 * @param count
	 *            The amount of an item in the stack
	 */
	public ItemStack(Item item, int count)
	{
		this.item = item;
		this.count = count;
	}

	/**
	 * @return the items in the stack
	 */
	public Item getItem()
	{
		return item;
	}

	/**
	 * Sets the items in the stack
	 * 
	 * @param item
	 *            The new items in the stack
	 */
	public void setItem(Item item)
	{
		this.item = item;
	}

	/**
	 * @return the amount of items in the stack
	 */
	public int getCount()
	{
		return count;
	}

	/**
	 * Set the amount of items in the stack
	 * 
	 * @param count
	 *            The new amount
	 */
	public void setCount(int count)
	{
		this.count = count;
	}

	/**
	 * Uses the item in the inventory and decreases the item stack by the amount
	 * set
	 * 
	 * @param inv
	 *            The inventory the stack is located in
	 * @param decrease
	 *            The amount being used
	 */
	public void use(Inventory inv, boolean decrease)
	{
		item.use(inv);
		if (decrease)
		{
			count--;
		}
	}

}
