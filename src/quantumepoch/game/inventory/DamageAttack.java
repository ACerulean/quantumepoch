package quantumepoch.game.inventory;

import quantumepoch.game.world.entity.Entity;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * Abstract class for abilities that do damage.
 */
public abstract class DamageAttack extends Ability
{
	/** How much damage the ability will do */
	private float damage;

	/**
	 * Creates a new damage attack.
	 * 
	 * @param name
	 *            The name of this ability.
	 * @param abilityTexture
	 *            The texture of this ability.
	 * @param chargePercent
	 *            The starting charge percent of this ability.
	 * @param holder
	 *            The entity that is using this ability.
	 * @param damage
	 *            The amount of damage the projectile of this ability will do.
	 */
	public DamageAttack(String name, TextureRegion abilityTexture, float chargePercent, Entity holder, float damage)
	{
		super(name, abilityTexture, chargePercent, holder);
		this.damage = damage;
	}

	/**
	 * @return the damage
	 */
	public float getDamage()
	{
		return damage;
	}

	/**
	 * Sets how much damage this ability will do on a target
	 * 
	 * @param damage
	 *            the damage to set
	 */
	public void setDamage(float damage)
	{
		this.damage = damage;
	}

	/** Abstract method that will allow the holder to use this ability */
	public abstract void use();
}
