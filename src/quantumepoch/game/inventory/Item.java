package quantumepoch.game.inventory;

/** Abstract class used to create items */
public abstract class Item
{
	/** Item ID */
	private int id;
	/** The name of the item */
	private String name;

	/**
	 * Creates a new item
	 * 
	 * @param id
	 *            Item ID
	 */
	public Item(int id)
	{
		this(0, null);
	}

	/**
	 * Creates a new item
	 * 
	 * @param id
	 *            The item ID
	 * @param name
	 *            The item name
	 */
	public Item(int id, String name)
	{
		this.id = id;
		this.name = name;
	}

	/**
	 * @return the item ID
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * Sets the ID
	 * 
	 * @param id
	 *            The new ID
	 */
	public void setId(int id)
	{
		this.id = id;
	}

	/**
	 * @return the item name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Sets the item name
	 * 
	 * @param name
	 *            The new name
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * Abstract method that will allow the item located in an inventory to be
	 * used
	 */
	public abstract void use(Inventory inv);

}
