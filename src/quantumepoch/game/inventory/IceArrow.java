package quantumepoch.game.inventory;

import quantumepoch.audio.AudioBuffer;
import quantumepoch.audio.AudioSource;
import quantumepoch.game.Sounds;
import quantumepoch.game.Textures;
import quantumepoch.game.world.entity.Entity;
import quantumepoch.game.world.entity.projectile.ArcherArrow;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * Archer damage attack ability that will deal a set initial amount of damage
 * and will leave the target frozen for a certain amount of time.
 */
public class IceArrow extends DamageAttack
{
	/** Time that this ice arrow will freeze the target */
	private int freezeTime;

	/**
	 * Creates the ability to shoot an ice arrow.
	 * 
	 * @param name
	 *            The name of the ability.
	 * @param abilityTexture
	 *            The texture of the ability.
	 * @param chargePercent
	 *            The percentage of the charge.
	 * @param holder
	 *            The entity that has the ability.
	 * @param damage
	 *            The amount of initial damage this ability projectile will do.
	 * @param freezeTime
	 *            The amount of time the target will be frozen.
	 */
	public IceArrow(String name, TextureRegion abilityTexture, float chargePercent, Entity holder, float damage, int freezeTime)
	{
		super(name, abilityTexture, chargePercent, holder, damage);
		this.freezeTime = freezeTime;
		sound = new AudioSource(new AudioBuffer(Sounds.ARCHER_ARROW.getTrack().getFormat(), Sounds.ARCHER_ARROW.getTrack().getData(), Sounds.ARCHER_ARROW.getTrack().getSampleRate()));
	}

	/**
	 * @return the freeze time
	 */
	public int getFreezeTime()
	{
		return freezeTime;
	}

	/**
	 * Sets the time the ice arrow will freeze the target.
	 * 
	 * @param timeFrozen
	 *            The new freeze time.
	 */
	public void setFreezeTime(int freezeTime)
	{
		this.freezeTime = freezeTime;
	}

	/**
	 * Will use the ability if the charge has a percentage higher than or equal
	 * to 1. Also will shoot a archer arrow if the requirement is met.
	 */
	@Override
	public void use()
	{
		if (getChargePercent() >= 1)
		{
			getHolder().getWorld().getBattle().getProjectiles().add(new ArcherArrow(getHolder().getWorld(), getHolder().getX() + getHolder().getWidth() / 2, getHolder().getY() + getHolder().getHeight() / 2, new TextureRegion(Textures.PROJECTILES.getTexture(), 16 * 4, 16 * 0, 16, 16), "Ice", getDamage(), getHolder(), 0f));
			playAudio();
		}
	}
}
