package quantumepoch.game.inventory;

import quantumepoch.audio.AudioSource;
import quantumepoch.game.world.entity.Entity;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * An abstract class that will be used to create abilities for the entities
 */

public abstract class Ability extends InventoryEntry
{

	/** ability name */
	private String name;
	/** entity that has the ability */
	private Entity holder;
	/** entity that the ability is being used on */
	private Entity target;
	/** the ability's texture */
	private TextureRegion abilityTexture;
	/** percentage of charge */
	private float chargePercent;

	private float scale = 1;

	private float rotation;

	protected AudioSource sound;

	/**
	 * Creates a new ability
	 * 
	 * @param name
	 *            The name of the ability
	 * @param abilityTexture
	 *            The texture for the ability
	 * @param chargePercent
	 *            The charge percentage
	 * @param holder
	 *            The entity that has the ability
	 */
	public Ability(String name, TextureRegion abilityTexture, float chargePercent, Entity holder)
	{
		this.name = name;
		this.abilityTexture = abilityTexture;
		this.chargePercent = chargePercent;
		this.holder = holder;
	}

	/**
	 * @return the ability name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @return the entity that has the ability
	 */
	public Entity getHolder()
	{
		return holder;
	}

	/**
	 * @return the entity that the ability is being used on
	 */
	public Entity getTarget()
	{
		return target;
	}

	/**
	 * Sets the name of the ability
	 * 
	 * @param name
	 *            The name being set for the ability
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * Sets the entity that uses the ability
	 * 
	 * @param h
	 *            The entity that has/is using the ability
	 */
	public void setHolder(Entity h)
	{
		this.holder = h;
	}

	/**
	 * Sets the entity the ability is being used on
	 * 
	 * @param t
	 *            The targeted entity
	 */
	public void setTarget(Entity t)
	{
		this.target = t;
	}

	/**
	 * @return the texture of the texture
	 */
	public TextureRegion getAbilityTexture()
	{
		return abilityTexture;
	}

	/**
	 * Sets the texture of the ability
	 * 
	 * @param abilityTexture
	 *            The texture being set
	 */
	public void setAbilityTexture(TextureRegion abilityTexture)
	{
		this.abilityTexture = abilityTexture;
	}

	/**
	 * @return the percentage charged up
	 */
	public float getChargePercent()
	{
		return chargePercent;
	}

	/**
	 * Adds the percentage of the charge by a certain magnitude
	 * 
	 * @param mag
	 *            The magnitude at which the percent will change
	 */
	public void changeChargePercent(float mag)
	{
		this.chargePercent += mag;
	}

	/**
	 * Sets the percentage of the charge
	 * 
	 * @param chargePercent
	 *            The percent to change the charge to
	 */
	public void setChargePercent(float chargePercent)
	{
		this.chargePercent = chargePercent;
	}

	/**
	 * @return the scale
	 */
	public float getScale()
	{
		return scale;
	}

	/**
	 * @param scale
	 *            the scale to set
	 */
	public void setScale(float scale)
	{
		this.scale = scale;
	}

	/**
	 * @return the rotation
	 */
	public float getRotation()
	{
		return rotation;
	}

	/**
	 * @param rotation
	 *            the rotation to set
	 */
	public void setRotation(float rotation)
	{
		this.rotation = rotation;
	}

	/** Abstract method that will allow an entity to use the ability */
	public abstract void use();

	public void playAudio()
	{
		if (sound != null)
			sound.play();
	}

}
