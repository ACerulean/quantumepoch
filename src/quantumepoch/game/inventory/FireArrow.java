package quantumepoch.game.inventory;

import quantumepoch.audio.AudioBuffer;
import quantumepoch.audio.AudioSource;
import quantumepoch.game.Sounds;
import quantumepoch.game.Textures;
import quantumepoch.game.world.entity.Entity;
import quantumepoch.game.world.entity.projectile.ArcherArrow;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * Archer fire attack ability that shoots an arrow that does a set damage and
 * leaves the target burned.
 */
public class FireArrow extends FireAttack
{
	/**
	 * Creates a new ability to shoot a fire arrow for the archer.
	 * 
	 * @param name
	 *            The name of this ability.
	 * @param abilityTexture
	 *            The texture of this ability.
	 * @param chargePercent
	 *            The percentage of this charge.
	 * @param holder
	 *            The entity that has this ability.
	 * @param damage
	 *            The amount of initial damage this ability projectile will do.
	 * @param burnDamage
	 *            The amount of damage done each second while burning.
	 * @param burnTime
	 *            The amount of time burned.
	 */
	public FireArrow(String name, TextureRegion abilityTexture, float chargePercent, Entity holder, float damage, float burnDamage, int burnTime)
	{
		super(name, abilityTexture, chargePercent, holder, damage, burnDamage, burnTime);
		sound = new AudioSource(new AudioBuffer(Sounds.ARCHER_ARROW.getTrack().getFormat(), Sounds.ARCHER_ARROW.getTrack().getData(), Sounds.ARCHER_ARROW.getTrack().getSampleRate()));
	}

	/**
	 * Will use the ability if the charge has a percentage higher than or equal
	 * to 1. Also will shoot a archer arrow if the requirement is met.
	 */
	@Override
	public void use()
	{
		if (getChargePercent() >= 1)
		{
			getHolder().getWorld().getBattle().getProjectiles().add(new ArcherArrow(getHolder().getWorld(), getHolder().getX() + getHolder().getWidth() / 2, getHolder().getY() + getHolder().getHeight() / 2, new TextureRegion(Textures.PROJECTILES.getTexture(), 16 * 3, 16 * 0, 16, 16), "Fire", getDamage(), getHolder(), 0f));
			playAudio();
		}
	}
}
