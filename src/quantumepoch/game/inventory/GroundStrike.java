package quantumepoch.game.inventory;

import quantumepoch.audio.AudioBuffer;
import quantumepoch.audio.AudioSource;
import quantumepoch.game.Sounds;
import quantumepoch.game.world.entity.Entity;
import quantumepoch.game.world.entity.projectile.SwordSlash;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * Knight damage attack ability that will deal damage a large amount of damage.
 */
public class GroundStrike extends DamageAttack
{

	/**
	 * Creates a new ground strike ability.
	 * 
	 * @param name
	 *            The name of this ability.
	 * @param abilityTexture
	 *            The texture of this ability.
	 * @param chargePercent
	 *            The percentage of this charge.
	 * @param holder
	 *            The entity that has this ability.
	 * @param damage
	 *            The amount of initial damage this ability projectile will do.
	 */
	public GroundStrike(String name, TextureRegion abilityTexture, float chargePercent, Entity holder, float damage)
	{
		super(name, abilityTexture, chargePercent, holder, damage);
		sound = new AudioSource(new AudioBuffer(Sounds.KNIGHT_SWORD.getTrack().getFormat(), Sounds.KNIGHT_SWORD.getTrack().getData(), Sounds.KNIGHT_SWORD.getTrack().getSampleRate()));
	}

	/**
	 * Will use the ability if the charge has a percentage higher than or equal
	 * to 1. Also will shoot a sword slash if the requirement is met.
	 */
	@Override
	public void use()
	{
		if (getChargePercent() >= 1)
		{
			getHolder().getWorld().getBattle().getProjectiles().add(new SwordSlash(getHolder().getWorld(), getHolder().getX() + getHolder().getWidth() / 2, getHolder().getY() + getHolder().getHeight() / 2, getDamage(), "Normal", getHolder()));
			playAudio();
		}
	}
}
