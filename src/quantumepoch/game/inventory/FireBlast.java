package quantumepoch.game.inventory;

import quantumepoch.audio.AudioBuffer;
import quantumepoch.audio.AudioSource;
import quantumepoch.game.Sounds;
import quantumepoch.game.world.entity.Entity;
import quantumepoch.game.world.entity.projectile.WizardBall;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * Wizard fire attack ability that will shoot a flaming wizard ball at the
 * target leaving them burned.
 */
public class FireBlast extends FireAttack
{

	/**
	 * Creates a new fire blast ability.
	 * 
	 * @param name
	 *            The name of this ability.
	 * @param abilityTexture
	 *            The texture of this ability.
	 * @param chargePercent
	 *            The percentage of this charge.
	 * @param holder
	 *            The entity that has this ability.
	 * @param damage
	 *            The amount of intitial damage this ability projectile will do.
	 * @param burnDamage
	 *            The amount of damage done each second while burning.
	 * @param burnTime
	 *            The amount of time burned.
	 */
	public FireBlast(String name, TextureRegion abilityTexture, float chargePercent, Entity holder, float damage, float burnDamage, int burnTime)
	{
		super(name, abilityTexture, chargePercent, holder, damage, burnDamage, burnTime);
		sound = new AudioSource(new AudioBuffer(Sounds.WIZARD_BALL.getTrack().getFormat(), Sounds.WIZARD_BALL.getTrack().getData(), Sounds.WIZARD_BALL.getTrack().getSampleRate()));
	}

	/**
	 * Will use the ability if the charge has a percentage higher than or equal
	 * to 1. Also will shoot a wizard ball if the requirement is met.
	 */
	@Override
	public void use()
	{
		if (getChargePercent() >= 1)
		{
			getHolder().getWorld().getBattle().getProjectiles().add(new WizardBall(getHolder().getWorld(), getHolder().getX() + getHolder().getWidth() / 2, getHolder().getY() + getHolder().getHeight() / 2, 0f, getDamage(), "Fire", getHolder(), Color.RED));
			playAudio();
		}
	}
}
