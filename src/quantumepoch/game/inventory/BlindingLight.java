package quantumepoch.game.inventory;

import quantumepoch.audio.AudioBuffer;
import quantumepoch.audio.AudioSource;
import quantumepoch.game.Sounds;
import quantumepoch.game.world.entity.Entity;
import quantumepoch.game.world.entity.projectile.LightedProjectile;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * Wizard ability that blinds the target, leaving them unable to attack for a
 * certain period of time.
 */
public class BlindingLight extends Ability
{
	/** Time the target will be stunned. */
	private int stunTime;

	/**
	 * Creates a new blinding light ability.
	 * 
	 * @param name
	 *            The name of this ability.
	 * @param abilityTexture
	 *            The texture of this ability.
	 * @param chargePercent
	 *            The charge percent.
	 * @param holder
	 *            The entity that has this ability.
	 * @param stunTime
	 *            How long this ability will stun the target.
	 */
	public BlindingLight(String name, TextureRegion abilityTexture, float chargePercent, Entity holder, int stunTime)
	{
		super(name, abilityTexture, chargePercent, holder);
		this.stunTime = stunTime;
		sound = new AudioSource(new AudioBuffer(Sounds.WIZARD_BALL.getTrack().getFormat(), Sounds.WIZARD_BALL.getTrack().getData(), Sounds.WIZARD_BALL.getTrack().getSampleRate()));
	}

	/**
	 * @return the stunTime
	 */
	public int getStunTime()
	{
		return stunTime;
	}

	/**
	 * Sets how long this ability will stun the target.
	 * 
	 * @param stunTime
	 *            the stunTime to set
	 */
	public void setStunTime(int stunTime)
	{
		this.stunTime = stunTime;
	}

	/**
	 * Will use the ability if the charge has a percentage higher than or equal
	 * to 1. Also will shoot a lighted projectile if the requirement is met.
	 */
	@Override
	public void use()
	{
		if (getChargePercent() >= 1)
		{
			getHolder().getWorld().getBattle().getProjectiles().add(new LightedProjectile(getHolder().getWorld(), getHolder().getX() + getHolder().getWidth() / 2, getHolder().getY() + getHolder().getHeight() / 2, 10f, 0f, 500f, 0f, "Disable", getHolder(), Color.WHITE));
			playAudio();
		}
	}
}
