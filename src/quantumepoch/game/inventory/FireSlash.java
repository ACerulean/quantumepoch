package quantumepoch.game.inventory;

import quantumepoch.audio.AudioBuffer;
import quantumepoch.audio.AudioSource;
import quantumepoch.game.Sounds;
import quantumepoch.game.world.entity.Entity;
import quantumepoch.game.world.entity.projectile.SwordSlash;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * Knight fire attack ability that does an set initial amount of damage and
 * leaves the target burned.
 */
public class FireSlash extends FireAttack
{

	/**
	 * Creates a new fire slash ability.
	 * 
	 * @param name
	 *            The name of this ability.
	 * @param abilityTexture
	 *            The texture of this ability.
	 * @param chargePercent
	 *            The percentage of this charge.
	 * @param holder
	 *            The entity that has this ability.
	 * @param damage
	 *            The amount of intitial damage this ability projectile will do.
	 * @param burnDamage
	 *            The amount of damage done each second while burning.
	 * @param burnTime
	 *            The amount of time burned.
	 */
	public FireSlash(String name, TextureRegion abilityTexture, float chargePercent, Entity holder, float damage, float burnDamage, int burnTime)
	{
		super(name, abilityTexture, chargePercent, holder, damage, burnDamage, burnTime);
		sound = new AudioSource(new AudioBuffer(Sounds.KNIGHT_SWORD.getTrack().getFormat(), Sounds.KNIGHT_SWORD.getTrack().getData(), Sounds.KNIGHT_SWORD.getTrack().getSampleRate()));
	}

	/**
	 * Will use the ability if the charge has a percentage higher than or equal
	 * to 1. Also will shoot a sword slash if the requirement is met.
	 */
	@Override
	public void use()
	{
		if (getChargePercent() >= 1)
		{
			getHolder().getWorld().getBattle().getProjectiles().add(new SwordSlash(getHolder().getWorld(), getHolder().getX() + getHolder().getWidth() / 2, getHolder().getY() + getHolder().getHeight() / 2, getDamage(), "Fire", getHolder()));
			playAudio();
		}
	}
}
