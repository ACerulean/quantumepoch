package quantumepoch.game.inventory;

/**
 * abstract class of the inventory for the adventure mode to be implemented
 *
 */
public abstract class InventoryEntry
{

	/**
	 * class instance variables for height and width
	 */
	public static final int ENTRY_HEIGHT = 25;
	public static final int ENTRY_WIDTH = 250;

}
