package quantumepoch.game.inventory;

import quantumepoch.audio.AudioBuffer;
import quantumepoch.audio.AudioSource;
import quantumepoch.game.Sounds;
import quantumepoch.game.world.entity.Entity;
import quantumepoch.game.world.entity.projectile.SwordSlash;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * Knight damage attack ability that will deal an initial damage and leave the
 * target stunned.
 */
public class SwordStun extends DamageAttack
{
	/** Time the target will be stunned */
	private int stunTime;

	/**
	 * Creates a new slash ability for the knight.
	 * 
	 * @param name
	 *            The name of the ability.
	 * @param abilityTexture
	 *            The texture of the ability.
	 * @param chargePercent
	 *            The percentage of the charge.
	 * @param holder
	 *            The entity that has the ability.
	 * @param damage
	 *            The max damage this ability projectile will do.
	 * @param stunTime
	 *            How long this ability will stun the target.
	 */
	public SwordStun(String name, TextureRegion abilityTexture, float chargePercent, Entity holder, float damage, int stunTime)
	{
		super(name, abilityTexture, chargePercent, holder, damage);
		this.stunTime = stunTime;
		sound = new AudioSource(new AudioBuffer(Sounds.KNIGHT_SWORD.getTrack().getFormat(), Sounds.KNIGHT_SWORD.getTrack().getData(), Sounds.KNIGHT_SWORD.getTrack().getSampleRate()));
	}

	/**
	 * @return the stunTime
	 */
	public int getStunTime()
	{
		return stunTime;
	}

	/**
	 * Sets a new stun time.
	 * 
	 * @param stunTime
	 *            The new stun time to set.
	 */
	public void setStunTime(int stunTime)
	{
		this.stunTime = stunTime;
	}

	/**
	 * Will use the ability if the charge has a percentage higher than or equal
	 * to 1. Also will shoot a sword slash if the requirement is met.
	 */
	@Override
	public void use()
	{
		if (getChargePercent() >= 1)
		{
			getHolder().getWorld().getBattle().getProjectiles().add(new SwordSlash(getHolder().getWorld(), getHolder().getX() + getHolder().getWidth() / 2, getHolder().getY() + getHolder().getHeight() / 2, getDamage(), "Disable", getHolder()));
			playAudio();
		}
	}
}
