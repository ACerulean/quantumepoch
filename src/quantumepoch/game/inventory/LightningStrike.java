package quantumepoch.game.inventory;

import quantumepoch.audio.AudioBuffer;
import quantumepoch.audio.AudioSource;
import quantumepoch.game.Sounds;
import quantumepoch.game.world.entity.Entity;
import quantumepoch.game.world.entity.projectile.WizardBall;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * Wizard damage attack ability that will deal a large amount of damage to the
 * target; it will also stun the enemy for a set period of time..
 */
public class LightningStrike extends DamageAttack
{
	/** Time the target will be stunned */
	private int stunTime;

	/**
	 * Creates a new lightning stil.
	 * 
	 * @param name
	 *            The name of the ability.
	 * @param abilityTexture
	 *            The texture of the ability.
	 * @param chargePercent
	 *            The percentage of the charge.
	 * @param holder
	 *            The entity that has the ability.
	 * @param damage
	 *            The amount of initial damage this ability projectile will do.
	 * @param stunTime
	 *            The amount of time the target will be stunned.
	 */
	public LightningStrike(String name, TextureRegion abilityTexture, float chargePercent, Entity holder, float damage, int stunTime)
	{
		super(name, abilityTexture, chargePercent, holder, damage);
		this.stunTime = stunTime;
		sound = new AudioSource(new AudioBuffer(Sounds.WIZARD_BALL.getTrack().getFormat(), Sounds.WIZARD_BALL.getTrack().getData(), Sounds.WIZARD_BALL.getTrack().getSampleRate()));

	}

	/**
	 * @return the stun time 
	 * 	
	 */
	public int getStunTime()
	{
		return stunTime;
	}

	/**
	 * Set a new stun time.
	 * 
	 * @param stunTime
	 *            The new stunTime to set.
	 */
	public void setStunTime(int stunTime)
	{
		this.stunTime = stunTime;
	}

	/**
	 * Will use the ability if the charge has a percentage higher than or equal
	 * to 1. Also will shoot a wizard ball if the requirement is met.
	 */
	@Override
	public void use()
	{
		if (getChargePercent() >= 1)
		{
			getHolder().getWorld().getBattle().getProjectiles().add(new WizardBall(getHolder().getWorld(), getHolder().getX() + getHolder().getWidth() / 2, getHolder().getY() + getHolder().getHeight() / 2, 0, getDamage(), "Disable", getHolder(), Color.INDIGO));
			playAudio();
		}
	}
}
