package quantumepoch.game.inventory;

import quantumepoch.game.world.entity.Entity;
import quantumepoch.graphics.texture.TextureRegion;

/** Abstract ability class for attacks that burn the target */
public abstract class FireAttack extends DamageAttack
{

	/** The amount of damage that will be inflicted on the target */
	private float burnDamage;
	/** Time the target will take burn damage */
	private int burnTime;

	/**
	 * Creates a new fire attack ability.
	 * 
	 * @param name
	 *            The name of this ability.
	 * @param abilityTexture
	 *            The texture of this ability.
	 * @param chargePercent
	 *            The percentage of this charge.
	 * @param holder
	 *            The entity that has this ability.
	 * @param damage
	 *            The amount of intitial damage this ability projectile will do.
	 * @param burnDamage
	 *            The amount of damage done each second while burning.
	 * @param burnTime
	 *            The amount of time burned.
	 */
	public FireAttack(String name, TextureRegion abilityTexture, float chargePercent, Entity holder, float damage, float burnDamage, int burnTime)
	{
		super(name, abilityTexture, chargePercent, holder, damage);
		this.burnDamage = burnDamage;
		this.burnTime = burnTime;
	}

	/**
	 * @return time that the enemy will take burn damage
	 */
	public int getBurnTime()
	{
		return burnTime;
	}

	/**
	 * @return amount of damage the target will take each second
	 */
	public float getBurnDamage()
	{
		return burnDamage;
	}

	/**
	 * @param bt
	 *            The new time the target will burn
	 */
	public void setburnTime(int bt)
	{
		burnTime = bt;
	}

	/**
	 * @param bd
	 *            The new damage that will be taken when burned
	 */
	public void setBurnDamage(int bd)
	{
		burnDamage = bd;
	}

	/** Abstract method that will allow an entity to use the ability */
	public abstract void use();
}
