package quantumepoch.game.inventory;

import quantumepoch.audio.AudioBuffer;
import quantumepoch.audio.AudioSource;
import quantumepoch.game.Sounds;
import quantumepoch.game.world.entity.Entity;
import quantumepoch.game.world.entity.projectile.ArcherArrow;
import quantumepoch.game.world.entity.projectile.SwordSlash;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * Knight damage attack ability that will deal damage based on the amount of
 * charge loaded.
 */
public class Slash extends DamageAttack
{

	/**
	 * Creates a new slash ability for the knight.
	 * 
	 * @param name
	 *            The name of the ability.
	 * @param abilityTexture
	 *            The texture of the ability.
	 * @param chargePercent
	 *            The percentage of the charge.
	 * @param holder
	 *            The entity that has the ability.
	 * @param damage
	 *            The max damage this ability projectile will do.
	 */
	public Slash(String name, TextureRegion abilityTexture, float chargePercent, Entity holder, float damage)
	{
		super(name, abilityTexture, chargePercent, holder, damage);
		sound = new AudioSource(new AudioBuffer(Sounds.KNIGHT_SWORD.getTrack().getFormat(), Sounds.KNIGHT_SWORD.getTrack().getData(), Sounds.KNIGHT_SWORD.getTrack().getSampleRate()));
	}

	/**
	 * Will use the ability if the charge has a percentage higher than 0. Also
	 * will shoot a sword slash if the requirement is met.
	 */
	@Override
	public void use()
	{
		if (getChargePercent() > 0)
		{
			getHolder().getWorld().getBattle().getProjectiles().add(new SwordSlash(getHolder().getWorld(), getHolder().getX() + getHolder().getWidth() / 2, getHolder().getY() + getHolder().getHeight() / 2, getChargePercent() * getDamage(), "Normal", getHolder()));
			playAudio();
		}
	}
}
