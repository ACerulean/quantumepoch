package quantumepoch.game.tetris;

import java.util.Random;

import quantumepoch.game.TetrisStarfield;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.batch.ShapeBatch;
import quantumepoch.graphics.batch.SpriteBatch;
import quantumepoch.graphics.texture.Texture;
import quantumepoch.graphics.texture.TextureRegion;
import quantumepoch.utils.Delay;

/**
 * An abstract representation of a tetris grid. This class handles all logic for any tetris grid.
 */
public abstract class TetrisGrid
{

	public static final int NEXT_BUFFER_SIZE = 4;
	private Tetromino nextBuffer[] = new Tetromino[NEXT_BUFFER_SIZE];

	/** the block piece texture */
	public static final Texture BLOCK_PIECES = new Texture("textures/sprites/BlockPieces.png");
	/** the texture of a block */
	public static final TextureRegion BLOCK_TEXTURE = new TextureRegion(BLOCK_PIECES, 24 * 1, 24 * 3, 24, 24);

	/** a random instance */
	protected Random rand;
	/** width of the grid */
	protected int width;
	/** height of the grid */
	protected int height;
	/** the cells */
	protected Cell[] cells;
	/** current tetromino */
	protected Tetromino currentTetromino;
	/** fall delay of tetris pieces */
	protected Delay fallDelay;
	/** slide delay of tetris pieces */
	protected Delay slideDelay;
	/** whether the current tetris piece is sliding */
	protected boolean sliding;
	/** the count of lines cleared */
	protected int linesCleared;

	protected int nextBufferOffset = 0;

	private boolean renderNextBuffer;

	private boolean hasOverflown;

	/**
	 * Creates a new tetris grid
	 * 
	 * @param width
	 *            The width in cells
	 * @param height
	 *            The height in cells
	 */
	public TetrisGrid(int width, int height, boolean renderNextBuffer)
	{
		this.rand = new Random();
		this.width = width;
		this.height = height;

		this.cells = new Cell[width * height];
		this.slideDelay = new Delay(1000);

		this.fallDelay = new Delay(250);

		BLOCK_PIECES.load();

		for (int i = 0; i < NEXT_BUFFER_SIZE; i++)
			generateNextBufferPiece();

		this.renderNextBuffer = renderNextBuffer;
	}

	/**
	 * Updates the state of this tetris grid
	 */
	public void update()
	{

		if (currentTetromino == null)
			generateNextBufferPiece();

		if (fallDelay.isReady())
		{
			if (!sliding)
			{
				currentTetromino.translate(0, -1);
			}
		}

		// check if current tetromino is about to hit anything
		if (!canTranslate(0, -1))
		{

			// fall down immediately and ignore sliding
			if (!sliding)
			{
				sliding = true;
				slideDelay.reset();
			} else if (slideDelay.isReady())
			{
				sliding = false;

				if (highestTetrominoY() > height - 3)
				{
					hasOverflown = true;
				} else
				{
					saveTetrominoToGrid();
					generateNextBufferPiece();
				}
			}

		} else
		{
			if (sliding)
			{
				sliding = false;
			}
		}

		clearAndDropRows();

	}

	public boolean hasOverflown()
	{
		return hasOverflown;
	}

	public void setNextBufferOffset(int nbo)
	{
		nextBufferOffset = nbo;
	}

	public boolean isTetrominoOnQueue(TetrominoType type)
	{
		for (Tetromino tet : getNextBuffer())
			if (tet != null && tet.getType() == type)
				return true;
		return false;
	}

	public void generateNextBufferPiece()
	{
		Tetromino next = generateTetromino();

		currentTetromino = nextBuffer[0];

		// shift other tetrominos down in array
		for (int i = 0; i < NEXT_BUFFER_SIZE - 1; i++)
		{
			nextBuffer[i] = nextBuffer[i + 1];
		}

		nextBuffer[NEXT_BUFFER_SIZE - 1] = next;
	}

	public Tetromino[] getNextBuffer()
	{
		return nextBuffer;
	}

	public long calcFallSpeed()
	{
		return (long) (1000 * (Math.pow(0.75, getLinesCleared() / 10)));
	}

	/**
	 * Translates the current tetromino by the specified amount
	 * 
	 * @param x
	 *            The x amount
	 * @param y
	 *            The y amount
	 */
	public void translate(int x, int y)
	{
		if (canTranslate(x, y))
		{
			currentTetromino.translate(x, y);
		}
	}

	/**
	 * Rotates the current tetromino by 90 degrees
	 */
	public void rotateTetromino()
	{
		if (canRotate())
		{
			currentTetromino.rotate90();
		}
	}

	/**
	 * Instantly drops the current tetromino
	 */
	public void instantDrop()
	{
		CellLocation[] instantFall = getCurrentFall();
		for (int i = 0; i < instantFall.length; i++)
		{
			currentTetromino.getBlockLocations()[i] = new CellLocation(instantFall[i]);
		}

		saveTetrominoToGrid();
		generateNextBufferPiece();
	}

	/**
	 * @return the highest y value of the current tetromino
	 */
	private int highestTetrominoY()
	{
		int y = Integer.MIN_VALUE;
		for (CellLocation loc : currentTetromino.getBlockLocations())
		{
			if (loc.getY() > y)
			{
				y = loc.getY();
			}
		}
		return y;
	}

	/**
	 * Gets the instant fall of the specified cell locations.
	 * 
	 * @param locs
	 *            The cell locations
	 * @return The location the instant drop would be in, as a celllocation array
	 */
	public CellLocation[] getFall(CellLocation[] locs)
	{
		int minTranslation = Integer.MAX_VALUE;
		for (int i = 0; i < locs.length; i++)
		{
			int originalY = locs[i].getY();
			for (int j = locs[i].getY(); j >= 0; j--)
			{
				int yDown = originalY - j;

				if (j <= 0 || cells[locs[i].getX() + ((j - 1) * width)].isTetrisBlock())
				{
					if (yDown < minTranslation)
					{
						minTranslation = yDown;
					}
				}
			}
		}
		for (CellLocation loc : locs)
		{
			loc.setY(loc.getY() - minTranslation);
		}
		return locs;
	}

	/**
	 * @return The current tetromino fall
	 */
	public CellLocation[] getCurrentFall()
	{
		CellLocation[] locs = new CellLocation[4];
		for (int i = 0; i < locs.length; i++)
		{
			locs[i] = new CellLocation(currentTetromino.getBlockLocations()[i]);
		}
		return getFall(locs);
	}

	/**
	 * Returns whether the current tetromino can translate the specified x and y.
	 * 
	 * @param x
	 *            The x translation
	 * @param y
	 *            The y translation
	 * @return Whether the current tetromino can translate the specified amount
	 */
	public boolean canTranslate(int x, int y)
	{
		boolean canTranslate = true;
		for (CellLocation loc : currentTetromino.getBlockLocations())
		{
			int afterX = loc.getX() + x;
			int afterY = loc.getY() + y;

			if (afterX < 0 || afterX >= width || afterY < 0 || afterY >= height || cells[afterX + (afterY * width)].isTetrisBlock())
			{
				canTranslate = false;
			}
		}
		return canTranslate;
	}

	/**
	 * @return Whether the current tetromino can rotate by 90 degrees.
	 */
	public boolean canRotate()
	{
		boolean canRotate = true;
		CellLocation[] resultSet = currentTetromino.rotateResult(1);
		for (CellLocation loc : resultSet)
		{
			if (loc.getX() < 0 || loc.getX() >= width || loc.getY() < 0 || loc.getY() >= height || cells[loc.getX() + (loc.getY() * width)].isTetrisBlock())
			{
				canRotate = false;
			}
		}
		return canRotate;
	}

	/**
	 * Clears and drops all rows possible.
	 */
	public void clearAndDropRows()
	{
		// Cell[][] droppedCells = null;

		// this stores the bottom most row that has been complete
		int minComplete = Integer.MAX_VALUE;
		int linesToDrop = 0;

		for (int y = 0; y < height; y++)
		{

			boolean complete = true;

			// check if the row is complete
			for (int x = 0; x < width; x++)
			{
				if (!cells[x + (y * width)].isTetrisBlock())
				{
					complete = false;
				}
			}

			// the row has been completed
			if (complete)
			{
				linesCleared++;
				linesToDrop++;
				if (y < minComplete)
				{
					minComplete = y;
				}

				// set the tetris blocks to off
				for (int x = 0; x < width; x++)
				{
					cells[x + (y * width)].setTetrisBlock(false);
				}

			}
		}

		// drop the rows above
		dropRows(linesToDrop, minComplete);
	}

	public void saveTetrominoToGrid()
	{
		for (CellLocation location : currentTetromino.getBlockLocations())
		{
			cells[location.getX() + (location.getY() * width)].setTetrisBlock(true);
			getCellAt(location.getX(), location.getY()).setColor(currentTetromino.getColor());
		}
	}

	/** generates a new tetromino */
	public abstract Tetromino generateTetromino();

	/** drops all current rows that need to be dropped */
	public abstract void dropRows(int linesToDrop, int minComplete);

	/**
	 * @return the cells of this grid
	 */
	public Cell[] getCells()
	{
		return cells;
	}

	/**
	 * @param x
	 *            The x position of the cell
	 * @param y
	 *            The y position of the cell
	 * @return The cell at the (x,y) position
	 */
	public Cell getCellAt(int x, int y)
	{
		return cells[x + y * width];
	}

	/**
	 * Sets the amount of lines cleared
	 * 
	 * @param count
	 *            The new amount of lines cleared
	 */
	public void setLinesCleared(int count)
	{
		this.linesCleared = count;
	}

	/**
	 * @return the width in cells
	 */
	public int getWidth()
	{
		return width;
	}

	/**
	 * @return the height in cells
	 */
	public int getHeight()
	{
		return height;
	}

	/**
	 * @return the count of lines cleared
	 */
	public int getLinesCleared()
	{
		return linesCleared;
	}

	/**
	 * @return the current tetromino
	 */
	public Tetromino getCurrentTetromino()
	{
		return currentTetromino;
	}

	/**
	 * @return the current fall speed
	 */
	public long getFallSpeed()
	{
		return fallDelay.getDelay();
	}

	/**
	 * Sets the cell at the specified position to the specified value.
	 * 
	 * @param x
	 *            The x position of the cell to set
	 * @param y
	 *            The y position of the cell to set
	 * @param value
	 *            The new value of the cell location
	 */
	public void setCellAt(int x, int y, Cell value)
	{
		cells[x + y * width] = value;
	}

	/**
	 * Sets the current tetromino
	 * 
	 * @param currentTetromino
	 *            The new tetromino
	 */
	public void setCurrentTetromino(Tetromino currentTetromino)
	{
		this.currentTetromino = currentTetromino;
	}

	/**
	 * Sets the fall speed
	 * 
	 * @param fallSpeed
	 *            The new fall speed
	 */
	public void setFallSpeed(long fallSpeed)
	{
		fallDelay.setDelay(fallSpeed);
	}

	/**
	 * Renders this tetris grid.
	 * 
	 * @param spriteBatch
	 *            The spritebatch used to render textures
	 * @param shapeBatch
	 *            The shapebatch used to render shapes
	 * @param xPosition
	 *            The x position of the grid
	 * @param yPosition
	 *            The y position of this grid
	 */
	public void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch, int xPosition, int yPosition)
	{

		shapeBatch.setColor(Color.BLACK);
		shapeBatch.renderFilledRect(xPosition, yPosition, width * Cell.SIZE, height * Cell.SIZE);

		// render horizontal lines
		shapeBatch.setColor(Color.WHITE);
		for (int y = 0; y <= height; y++)
		{
			int yy = y * Cell.SIZE;
			shapeBatch.renderLine(xPosition, yPosition + yy, xPosition + Cell.SIZE * width, yPosition + yy);
		}

		// render verticle lines
		shapeBatch.setColor(Color.WHITE);
		for (int x = 0; x <= width; x++)
		{
			int xx = x * Cell.SIZE;
			shapeBatch.renderLine(xPosition + xx, yPosition, xPosition + xx, yPosition + Cell.SIZE * height);
		}

		shapeBatch.reset();

		if (renderNextBuffer)
			for (int i = 0; i < NEXT_BUFFER_SIZE; i++)
			{
				for (int j = 0; j < TetrominoType.values().length; j++)
				{
					if (TetrominoType.values()[j] == nextBuffer[i].getType())
					{
						TextureRegion texture = TetrisStarfield.PIECES[j];
						Color c = TetrisStarfield.COLORS[j];
						int y = yPosition + i * 24 * 4;
						int x = xPosition - 24 * 5 + nextBufferOffset;
						spriteBatch.setTint(c);
						spriteBatch.render(texture, texture.getWidth() / 2, texture.getHeight() / 2, x, y, (float) Math.toRadians(180), 1, 1);
						break;
					}
				}
			}
	}
}
