package quantumepoch.game.tetris;

import org.lwjgl.input.Keyboard;

import quantumepoch.core.Core;

public class MultiplayerControl
{

	private byte timeFix1, timeFix2;

	public MultiplayerControl()
	{

	}

	public void update(TetrisGrid grid1, TetrisGrid grid2)
	{

		// player one
		// translate tetromino down
		if (Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_S))
		{
			grid1.setFallSpeed(50);
		} else
		{
			grid1.setFallSpeed(grid1.calcFallSpeed());
		}
		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_SPACE))
		{
			grid1.instantDrop();
		}
		// translate tetromino the left
		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_A))
		{
			// Make it scroll left with time interval.
			timeFix1 = 1;
		}
		// translate tetromino the right
		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_D))
		{
			// Make it scroll right with time interval
			timeFix1 = -1;
		}
		// rotate tetromino clockwise
		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_W))
		{
			grid1.rotateTetromino();
		}

		if (!Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_D) && !Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_A))
		{
			// set it to zero, because it isn't scrolling anything at all
			timeFix1 = 0;
		} else if (timeFix1 > 0) // scroll left
		{
			if (timeFix1 % 8 == 1)
			{
				grid1.translate(-1, 0);
			}
			timeFix1++;
		} else if (timeFix1 < 0) // scroll right
		{
			if (timeFix1 % 8 == -1)
			{

				grid1.translate(1, 0);
			}

			timeFix1--;
		}

		// player two
		// translate tetromino down
		if (Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_DOWN))
		{
			grid2.setFallSpeed(50);
		} else
		{
			grid2.setFallSpeed(grid2.calcFallSpeed());
		}
		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_NUMPAD0))
		{
			grid2.instantDrop();
		}
		// translate tetromino the left
		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_LEFT))
		{
			// Make it scroll left with time interval.
			timeFix2 = 1;
		}
		// translate tetromino the right
		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_RIGHT))
		{
			// Make it scroll right with time interval
			timeFix2 = -1;
		}
		// rotate tetromino clockwise
		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_UP))
		{
			grid2.rotateTetromino();
		}

		if (!Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_RIGHT) && !Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_LEFT))
		{
			// set it to zero, because it isn't scrolling anything at all
			timeFix2 = 0;
		} else if (timeFix2 > 0) // scroll left
		{
			if (timeFix2 % 8 == 1)
			{
				grid2.translate(-1, 0);
			}
			timeFix2++;
		} else if (timeFix2 < 0) // scroll right
		{
			if (timeFix2 % 8 == -1)
			{

				grid2.translate(1, 0);
			}
			timeFix2--;
		}
	}

}
