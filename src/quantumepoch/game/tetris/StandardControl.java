package quantumepoch.game.tetris;

import org.lwjgl.input.Keyboard;

import quantumepoch.core.Core;

public class StandardControl
{

	private byte timeFix;

	public StandardControl()
	{
	}

	public void update(TetrisGrid grid)
	{

		// translate tetromino down
		if (Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_DOWN) || Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_S))
		{
			grid.setFallSpeed(50);
		} else
		{
			grid.setFallSpeed(grid.calcFallSpeed());
		}
		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_SPACE))
		{
			grid.instantDrop();
		}
		// translate tetromino the left
		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_LEFT) || Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_A))
		{
			// Make it scroll left with time interval.
			timeFix = 1;
		}
		// translate tetromino the right
		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_RIGHT) || Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_D))
		{
			// Make it scroll right with time interval
			timeFix = -1;
		}
		// rotate tetromino clockwise
		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_UP) || Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_W))
		{
			grid.rotateTetromino();
		}

		if (!Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_RIGHT) && !Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_LEFT) && !Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_D) && !Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_A))
		{
			// set it to zero, because it isn't scrolling anything at all
			timeFix = 0;
		} else if (timeFix > 0) // scroll left
		{
			if (timeFix % 8 == 1)
			{
				grid.translate(-1, 0);
			}
			timeFix++;
		} else if (timeFix < 0) // scroll right
		{
			if (timeFix % 8 == -1)
			{

				grid.translate(1, 0);
			}

			timeFix--;
		}
	}

}
