package quantumepoch.game.tetris;

/**
 * Represents an (x, y) cell location on a grid.
 */
public class CellLocation
{

	/** the x position of this cell */
	private int x;
	/** the y position of this cell */
	private int y;

	/**
	 * Create a new cell location at (0,0)
	 */
	public CellLocation()
	{
		x = y = 0;
	}

	/**
	 * Creates a new cell location based on an existing CellLocation's location.
	 * 
	 * @param other
	 *            The other cell location to make a copy of.
	 */
	public CellLocation(CellLocation other)
	{
		this(other.getX(), other.getY());
	}

	/**
	 * Creates a new CellLocation at the specified coordinates.
	 * 
	 * @param x
	 *            The x coordinate of this cell.
	 * @param y
	 *            The y coordinate of this cell.
	 */
	public CellLocation(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	/**
	 * @return the x coordinate of this cell
	 */
	public int getX()
	{
		return x;
	}

	/**
	 * @return the y coordinate of this cell
	 */
	public int getY()
	{
		return y;
	}

	/**
	 * Sets this cell's x coordinate.
	 * 
	 * @param x
	 *            The new x coordinate of this cell.
	 */
	public void setX(int x)
	{
		this.x = x;
	}

	/**
	 * Sets this cell's y coordinate.
	 * 
	 * @param y
	 *            The new y coordinate of this cell.
	 */
	public void setY(int y)
	{
		this.y = y;
	}

}
