package quantumepoch.game.tetris;

import quantumepoch.graphics.Color;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * A tetris tetomino, composed of 4 orthogonal tetris pieces.
 */
public class Tetromino
{

	/** the tetris grid of this tetromino */
	private TetrisGrid grid;
	/** the block locations of this tetromino */
	private CellLocation[] blockLocations;
	/** the pivot point of this tetromino */
	private CellLocation pivot;
	/** the type of tetromino */
	private TetrominoType type;

	private TextureRegion texture;
	private Color color;

	/**
	 * Creates a new tetromino
	 * 
	 * @param grid
	 *            The grid of this tetromino
	 * @param start
	 *            The start pivot point
	 * @param type
	 *            The type of tetromino
	 */
	public Tetromino(TetrisGrid grid, CellLocation start, TetrominoType type, Color color, TextureRegion texture)
	{
		this.grid = grid;
		this.type = type;
		this.texture = texture;
		this.color = color;

		pivot = new CellLocation(start);
		blockLocations = new CellLocation[4];

		switch (type)
		{
		case I:
			blockLocations[0] = new CellLocation(pivot.getX() - 1, pivot.getY());
			blockLocations[1] = new CellLocation(pivot.getX() - 0, pivot.getY());
			blockLocations[2] = new CellLocation(pivot.getX() + 1, pivot.getY());
			blockLocations[3] = new CellLocation(pivot.getX() + 2, pivot.getY());
			break;
		case J:
			blockLocations[0] = new CellLocation(pivot.getX(), pivot.getY() + 1);
			blockLocations[1] = new CellLocation(pivot.getX(), pivot.getY());
			blockLocations[2] = new CellLocation(pivot.getX(), pivot.getY() - 1);
			blockLocations[3] = new CellLocation(pivot.getX() - 1, pivot.getY() - 1);
			break;
		case L:
			blockLocations[0] = new CellLocation(pivot.getX(), pivot.getY() + 1);
			blockLocations[1] = new CellLocation(pivot.getX(), pivot.getY());
			blockLocations[2] = new CellLocation(pivot.getX(), pivot.getY() - 1);
			blockLocations[3] = new CellLocation(pivot.getX() + 1, pivot.getY() - 1);
			break;
		case O:
			blockLocations[0] = new CellLocation(pivot.getX(), pivot.getY());
			blockLocations[1] = new CellLocation(pivot.getX() - 1, pivot.getY());
			blockLocations[2] = new CellLocation(pivot.getX(), pivot.getY() - 1);
			blockLocations[3] = new CellLocation(pivot.getX() - 1, pivot.getY() - 1);
			break;
		case S:
			blockLocations[0] = new CellLocation(pivot.getX(), pivot.getY());
			blockLocations[1] = new CellLocation(pivot.getX() - 1, pivot.getY());
			blockLocations[2] = new CellLocation(pivot.getX() - 1, pivot.getY() + 1);
			blockLocations[3] = new CellLocation(pivot.getX(), pivot.getY() - 1);
			break;
		case Z:
			blockLocations[0] = new CellLocation(pivot.getX(), pivot.getY());
			blockLocations[1] = new CellLocation(pivot.getX() + 1, pivot.getY());
			blockLocations[2] = new CellLocation(pivot.getX() + 1, pivot.getY() + 1);
			blockLocations[3] = new CellLocation(pivot.getX(), pivot.getY() - 1);
			break;
		case T:
			blockLocations[0] = new CellLocation(pivot.getX(), pivot.getY());
			blockLocations[1] = new CellLocation(pivot.getX(), pivot.getY() + 1);
			blockLocations[2] = new CellLocation(pivot.getX() - 1, pivot.getY());
			blockLocations[3] = new CellLocation(pivot.getX() + 1, pivot.getY());
			break;
		default:
			break;
		}
	}

	/**
	 * @return The grid
	 */
	public TetrisGrid getGrid()
	{
		return grid;
	}

	/**
	 * @return The type of tetromino
	 */
	public TetrominoType getType()
	{
		return type;
	}

	/**
	 * @return the pivot point
	 */
	public CellLocation getPivot()
	{
		return pivot;
	}

	/**
	 * @return the block locations
	 */
	public CellLocation[] getBlockLocations()
	{
		return blockLocations;
	}

	/**
	 * Sets the grid
	 * 
	 * @param grid
	 *            The new grid
	 */
	public void setGrid(TetrisGrid grid)
	{
		this.grid = grid;
	}

	/**
	 * Sets the pivot point
	 * 
	 * @param pivot
	 *            The new pivot
	 */
	public void setPivot(CellLocation pivot)
	{
		this.pivot = pivot;
	}

	/**
	 * Sets the block locations
	 * 
	 * @param blockLocations
	 *            The new block locations
	 */
	public void setBlockLocations(CellLocation[] blockLocations)
	{
		this.blockLocations = blockLocations;
	}

	/**
	 * Translates this tetromino
	 * 
	 * @param x
	 *            The x translation
	 * @param y
	 *            The y translation
	 */
	public void translate(int x, int y)
	{
		pivot.setX(pivot.getX() + x);
		pivot.setY(pivot.getY() + y);

		for (CellLocation loc : blockLocations)
		{
			loc.setX(loc.getX() + x);
			loc.setY(loc.getY() + y);
		}
	}

	/**
	 * Rotates this tetromino 90 degrees
	 */
	public void rotate90()
	{

		CellLocation[] resultSet = rotateResult(1);

		for (int i = 0; i < resultSet.length; i++)
		{
			blockLocations[i] = new CellLocation(resultSet[i]);
		}
	}

	/**
	 * Gets the result of rotating this tetromino the specified amount of times
	 * 
	 * @param rotations
	 *            The amount of rotations
	 * @return The result of rotating
	 */
	public CellLocation[] rotateResult(int rotations)
	{
		CellLocation[] resultSet = new CellLocation[4];

		for (int i = 0; i < resultSet.length; i++)
		{
			resultSet[i] = new CellLocation(blockLocations[i]);
		}

		for (int j = 0; j < rotations; j++)
		{
			if (type == TetrominoType.O)
			{
				return resultSet;
			} else
			{
				int originX = grid.getWidth() / 2;
				int originY = grid.getHeight() / 2;
				int pivotX = pivot.getX() - originX;
				int pivotY = pivot.getY() - originY;

				originX += pivotX;
				originY += pivotY;

				for (int i = 0; i < blockLocations.length; i++)
				{
					int tetX = resultSet[i].getX() - originX;
					int tetY = resultSet[i].getY() - originY;

					int rotatedX = tetY;
					int rotatedY = -tetX;

					int finalX = originX + rotatedX;
					int finalY = originY + rotatedY;

					resultSet[i] = new CellLocation(finalX, finalY);
				}

			}
		}

		return resultSet;

	}

	public TextureRegion getTexture()
	{
		return texture;
	}

	public void setTexture(TextureRegion texture)
	{
		this.texture = texture;
	}

	public Color getColor()
	{
		return color;
	}

	public void setColor(Color color)
	{
		this.color = color;
	}

}
