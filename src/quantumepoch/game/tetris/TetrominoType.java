package quantumepoch.game.tetris;

/**
 * Stores the possition types of tetrominos
 */
public enum TetrominoType
{

	I, L, J, T, Z, S, O;

}
