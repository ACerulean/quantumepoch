package quantumepoch.game.tetris;

import quantumepoch.graphics.Color;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * Represents a single cell of a tetris grid.
 */
public class Cell
{

	/** the constant size of all cells */
	public static final int SIZE = 24;

	/** the grid location of this cell */
	private CellLocation location;
	/** stores wheteher this cell is storing a tetris block */
	private boolean isTetrisBlock;
	/** this is the color of the current cell */
	private Color color = Color.FUCHSIA;
	/** the current texture of the classic cell */
	private TextureRegion texture;

	public Cell(CellLocation location, boolean tetrisBlock, Color color, TextureRegion texture)
	{
		this.location = location;
		this.isTetrisBlock = tetrisBlock;
		this.color = color;
		this.texture = texture;
	}

	/**
	 * @return the location of this tetris grid cell.
	 */
	public CellLocation getLocation()
	{
		return location;
	}

	/**
	 * @return the color of this tetris block
	 */
	public Color getColor()
	{
		return color;
	}

	/**
	 * @return the texture of this cell
	 */
	public TextureRegion getTexture()
	{
		return texture;
	}

	/**
	 * Sets the location of this tetris grid cell.
	 * 
	 * @param location
	 *            The new location of this cell.
	 */
	public void setLocation(CellLocation location)
	{
		this.location = location;
	}

	/**
	 * @return wether this tetris cell is a tetris block
	 */
	public boolean isTetrisBlock()
	{
		return isTetrisBlock;
	}

	/**
	 * Sets whether this tetris grid cell stores a tetris block.
	 * 
	 * @param tetrisBlock
	 *            Whether this tetris grid cell stores a tetris block.
	 */
	public void setTetrisBlock(boolean tetrisBlock)
	{
		this.isTetrisBlock = tetrisBlock;
	}

	/**
	 * Sets the color of the cell.
	 * 
	 * @param color
	 */
	public void setColor(Color color)
	{
		this.color = color;
	}

	/**
	 * Sets the texture of the cell
	 * 
	 * @param texture
	 */
	public void setTexture(TextureRegion texture)
	{
		this.texture = texture;
	}

}
