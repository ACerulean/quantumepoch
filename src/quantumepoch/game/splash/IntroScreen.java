package quantumepoch.game.splash;

import quantumepoch.game.Textures;

/**
 * The intro splash screen of the game.
 */
public class IntroScreen extends FadeScreen
{

	/**
	 * the constructor to get the intro screen textures
	 */
	public IntroScreen()
	{
		super(Textures.LOGO.getTexture());
	}
}
