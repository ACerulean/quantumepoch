package quantumepoch.game.splash;

import quantumepoch.game.Textures;

/**
 * The intro splash screen of the game.
 */
public class CTMScreen extends FadeScreen
{

	/**
	 * the menu for in game saving and quitting to bring up, constructor
	 */
	public CTMScreen()
	{
		super(Textures.CTM_LOGO.getTexture());
		scale = 3;
	}
}
