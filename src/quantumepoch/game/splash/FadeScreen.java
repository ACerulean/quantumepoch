package quantumepoch.game.splash;

import quantumepoch.core.Core;
import quantumepoch.core.SplashScreen;
import quantumepoch.game.Game;
import quantumepoch.game.Textures;
import quantumepoch.graphics.BlendFunction;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.texture.Texture;
import quantumepoch.input.Keyboard;
import quantumepoch.utils.Delay;

/**
 * An abstraction over a splash screen that fades over time.
 */
public class FadeScreen extends SplashScreen
{
	/**
	 * The amount of time to delay until the logo starts to fade.
	 */
	public static final long delayedFade = 0;

	private Texture texture;
	protected float scale = 1;

	/**
	 * Creates a new intro screen
	 */
	public FadeScreen(Texture texture)
	{
		super(new Delay(10000));
		this.texture = texture;
	}

	/**
	 * Updates the intro screen splash screen
	 */
	@Override
	public void update()
	{
		if (Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_RETURN))
		{
			Core.getCore().skipSplash();
		}
	}

	/** Renders the intro screen splash screen */
	@Override
	public void render()
	{
		Game.getInstance().getSpriteBatch().begin();
		Game.getInstance().getShapeBatch().begin();

		Core.getGraphics().setBlendingEnabled(true);
		Core.getGraphics().setBlendFunction(BlendFunction.ALPHA_BLEND);

		float alpha = 1;
		if (getDuration().remaining() >= delayedFade)
		{
			alpha = ((getDuration().remaining() - delayedFade) / (float) (getDuration().getDelay() - delayedFade));
		}

		Game.getInstance().getSpriteBatch().setTint(new Color(alpha, alpha, alpha));
		Game.getInstance().getSpriteBatch().render(Textures.SETTINGS_BG.getTexture(), 0, 0);
		Game.getInstance().getSpriteBatch().render(texture, texture.getWidth() / 2, texture.getHeight() / 2, (Core.getWindow().getWidth() - texture.getWidth()) / 2, (Core.getWindow().getHeight() - texture.getHeight()) / 2, 0, scale, scale);

		Game.getInstance().getSpriteBatch().end();
		Game.getInstance().getShapeBatch().end();
	}

}
