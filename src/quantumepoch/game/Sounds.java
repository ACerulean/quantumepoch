package quantumepoch.game;

import quantumepoch.audio.WaveAudio;

/**
 * Stores sound resources for the game
 */
public enum Sounds
{

	/** the space time theme */
	SPACE_TIME_THEME("music/Weird_space_time.wav"),
	BACK_ALLY_BEATS("music/Back_ally_beats.wav"),
	BENEATH_THE_SEWERS("music/Beneath_the_sewers.wav"),
	TETRIS_THEME("music/tetris_theme.wav"), 
	BOSS_THEME("music/A_boss_appeared.wav"),
	CHARGED("audio/charged.wav"),
	FIREWORKS("audio/fireworks.wav"),
	WIZARD_BALL("audio/WizardShoot.wav"),
	ARCHER_ARROW("audio/ShootArrow.wav"),
	KNIGHT_SWORD("audio/Sword.wav"),
	ENEMY_SHOOT("audio/enemyShoot.wav"),
	LINE_CLEARED("audio/lineCleared.wav");

	/** the wave audio track of this sound resource */
	private WaveAudio track;

	/**
	 * Creates a new sound with the specified path
	 * 
	 * @param path
	 *            The path of the sound resource
	 */
	Sounds(String path)
	{
		// if (IOUtils.isJar())
		// {
		// try
		// {
		//
		// BufferedInputStream bis = new BufferedInputStream(getClass().getResourceAsStream(path));
		// track = new WaveAudio(bis);
		// } catch (UnsupportedAudioFileException e)
		// {
		// e.printStackTrace();
		// } catch (IOException e)
		// {
		// e.printStackTrace();
		// }
		// } else
		// {
		// }
		this.track = WaveAudio.loadWaveAudio(path);
	}

	/**
	 * @return The WaveAudio of this sound resource
	 */
	public WaveAudio getTrack()
	{
		return track;
	}

}
