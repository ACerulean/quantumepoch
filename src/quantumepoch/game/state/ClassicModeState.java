package quantumepoch.game.state;

import org.lwjgl.input.Keyboard;

import quantumepoch.audio.AudioBuffer;
import quantumepoch.audio.AudioSource;
import quantumepoch.audio.WaveAudio;
import quantumepoch.core.Core;
import quantumepoch.core.state.GameState;
import quantumepoch.game.Game;
import quantumepoch.game.Sounds;
import quantumepoch.game.Textures;
import quantumepoch.game.Timer;
import quantumepoch.game.classic.ClassicGrid;
import quantumepoch.game.gui.MenuWindow;
import quantumepoch.game.profile.Profile;
import quantumepoch.game.tetris.Cell;
import quantumepoch.game.tetris.StandardControl;
import quantumepoch.game.tetris.TetrisGrid;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.gui.Component;

/**
 * The class for the state where you can play classic tetris.
 */
public class ClassicModeState extends GameState
{

	/** The classic tetris grid */
	private TetrisGrid grid;
	/** The timer that shows the total time for the game */
	private Timer timer;
	/** The amount of lines cleared */
	private int lastLineClearCount;
	/** The player's score for the game */
	private int score;
	/** The game's level/difficulty */
	private int level;
	/** The source of the games audio */
	private AudioSource source;
	/** If the game is paused */
	private boolean paused;
	/** The menu window used for saving and quitting */
	private MenuWindow menuWindow;

	private StandardControl stdControl;

	/**
	 * Creates a new classic mode state
	 * 
	 * @param id
	 *            The state ID
	 */
	public ClassicModeState(int id)
	{
		super(id);
		stdControl = new StandardControl();
	}

	/**
	 * @return the tetris grid
	 */
	public TetrisGrid getTetrisGrid()
	{
		return grid;
	}

	/**
	 * @return the game timer
	 */
	public Timer getTimer()
	{
		return timer;
	}

	/**
	 * @return the player's score
	 */
	public int getScore()
	{
		return score;
	}

	/**
	 * @return the game's level
	 */
	public int getLevel()
	{
		return level;
	}

	/**
	 * @return the amount of lines cleared
	 */
	public int getLinesCleared()
	{
		return lastLineClearCount;
	}

	/**
	 * Sets the game timer
	 * 
	 * @param timer
	 *            The new timer
	 */
	public void setTimer(Timer timer)
	{
		this.timer = timer;
	}

	/**
	 * Sets the player's score
	 * 
	 * @param score
	 *            The new score
	 */
	public void setScore(int score)
	{
		this.score = score;
	}

	/**
	 * Sets the current level
	 * 
	 * @param level
	 *            The new level
	 */
	public void setLevel(int level)
	{
		this.level = level;
	}

	/**
	 * Sets the amount of lines cleared by the player
	 * 
	 * @param linesCleared
	 *            The amount of lines cleared
	 */
	public void setLinesCleared(int linesCleared)
	{
		this.lastLineClearCount = linesCleared;
	}

	/**
	 * Initializes the tetris, grid, and the game song. Then it plays and loops the song.
	 */
	@Override
	public void init()
	{
		score = 0;
		grid = new ClassicGrid(10, 22);
		timer = new Timer(1);
		lastLineClearCount = 0;

		WaveAudio file = Sounds.TETRIS_THEME.getTrack();
		source = new AudioSource(new AudioBuffer(file.getFormat(), file.getData(), file.getSampleRate()));
		source.play();
		source.setLooping(true);
	}

	/**
	 * override method to resize the window
	 */
	@Override
	public void resized()
	{

	}

	/**
	 * the override method to update the game textures and sprites of the game state
	 */
	@Override
	public void update()
	{

		if (!paused)
		{
			stdControl.update(grid);
			grid.update();
			timer.update();
			level = grid.getLinesCleared() / 10;
			if (grid.hasOverflown())
				Game.getInstance().getGameStateManager().setCurrentState(Game.MAIN_MENU_STATE.getId());
		}

		// pause game and bring up menu window, or close menu window and unpause
		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_ESCAPE))
		{
			if (paused)
			{
				paused = false;
				menuWindow = null;
			} else
			{
				// open menu window
				paused = true;
				menuWindow = new MenuWindow(200, 200, 200, 300);
				Component.setFocused(menuWindow);
			}
		}

		if (paused)
		{
			menuWindow.update();

			if (menuWindow.getBackToGameButton().isPressed())
			{
				paused = false;
				menuWindow = null;
			} else if (menuWindow.getSaveButton().isPressed())
			{
				paused = false;
				// save
				Profile profile = Game.getInstance().getCurrentProfile();
				profile.getPlayerFile().saveClassicMode(this);
			} else if (menuWindow.getQuitButton().isPressed())
			{
				paused = false;
				Game.getInstance().getGameStateManager().setCurrentState(Game.MAIN_MENU_STATE.getId());
				// save and exit
			}
		}

		if (grid.getLinesCleared() - lastLineClearCount > 0)
		{
			int difference = grid.getLinesCleared() - lastLineClearCount;

			if (difference == 1)
			{
				score += 40 * (level + 1);
			} else if (difference == 2)
			{
				score += 100 * (level + 1);
			} else if (difference == 3)
			{
				score += 300 * (level + 1);
			} else if (difference == 4)
			{
				score += 1200 * (level + 1);
			}

			lastLineClearCount = grid.getLinesCleared();
		}
	}

	/**
	 * override method to render all the sprites and textures in the world
	 */
	@Override
	public void render()
	{
		int halfScreenWidth = Core.getWindow().getWidth() / 2;
		int halfWidth = (grid.getWidth() * Cell.SIZE) / 2;

		Game.getInstance().getSpriteBatch().begin();
		Game.getInstance().getShapeBatch().begin();
		Game.getInstance().getSpriteBatch().setTint(Color.WHITE);
		Game.getInstance().getSpriteBatch().render(Textures.CLASSIC_MODE_BG.getTexture(), 0, 0);
		grid.render(Game.getInstance().getSpriteBatch(), Game.getInstance().getShapeBatch(), halfScreenWidth - halfWidth, 10);
		Game.getInstance().getShapeBatch().end();

		Game.getInstance().getShapeBatch().begin();
		if (paused)
		{
			menuWindow.render(Game.getInstance().getShapeBatch(), Game.getInstance().getSpriteBatch(), Game.getInstance().getFontRenderer());
		}
		Game.getInstance().getShapeBatch().end();

		Game.getInstance().getSpriteBatch().end();

		Game.getInstance().getFontRenderer().render(Game.CALIBRI_30, "Time: " + timer, 10, 10);
		Game.getInstance().getFontRenderer().render(Game.CALIBRI_30, "Lines Cleared: " + grid.getLinesCleared(), 10, 40);
		Game.getInstance().getFontRenderer().render(Game.CALIBRI_30, "Level: " + (level + 1), 10, 70);
		Game.getInstance().getFontRenderer().render(Game.CALIBRI_30, "Score: " + score, 10, 100);

	}

	/**
	 * override method to quit the game
	 */
	@Override
	public void dispose()
	{
		source.stop();
	}

}
