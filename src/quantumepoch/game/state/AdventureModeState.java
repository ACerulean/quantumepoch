package quantumepoch.game.state;

import quantumepoch.audio.AudioSource;
import quantumepoch.core.state.GameState;
import quantumepoch.game.CharacterType;
import quantumepoch.game.Game;
import quantumepoch.game.Textures;
import quantumepoch.game.adventure.Worlds;
import quantumepoch.game.world.World;
import quantumepoch.game.world.entity.mob.Player;
import quantumepoch.graphics.texture.TextureRegion;

public class AdventureModeState extends GameState
{

	/*
	 * Plains Forest Underground cave Desert Ocean Space Other worldly environment City Sky Tundra Tetris kingdom(final boss) Each world will have multiple enemy fights.
	 */

	/** the worlds loaded for the current profile */
	private Worlds playerWorlds;
	/** the current world of the profile */
	private World currentWorld;
	/** the index of the current world */
	private int currentWorldIndex;

	/**
	 * the constructor for the adventure mode to get the id of the state
	 * 
	 * @param id
	 *            the id of the game state
	 */
	public AdventureModeState(int id)
	{
		super(id);
	}

	/**
	 * override method to initialize the adventure mode state
	 */
	@Override
	public void init()
	{

		if (Game.getInstance().getCurrentProfile().getCharacterType() == null)
		{
			Game.getInstance().getGameStateManager().setCurrentState(Game.CHARACTER_SELECT_STATE.getId());
		} else
		{
			playerWorlds = new Worlds(Game.getInstance().getCurrentProfile());
			currentWorldIndex = 0;

			setCurrentWorld(playerWorlds.getWorld(currentWorldIndex));
		}
	}

	/**
	 * method to set the current world of the adventure mode player
	 * 
	 * @param world
	 *            the world to start in
	 */
	public void setCurrentWorld(World world)
	{
		currentWorld = world;
		if (currentWorld.findPlayer() == null)
			currentWorld.getMobs().add(new Player(currentWorld, currentWorld.getSpawn().getX(), currentWorld.getSpawn().getY(), new TextureRegion(Textures.PLAYER.getTexture())));
	}

	/**
	 * gets the world the player has unlocked
	 * 
	 * @return returns the unlocked worlds
	 */
	public Worlds getWorlds()
	{
		return playerWorlds;
	}

	/**
	 * gets the current world the player is in
	 * 
	 * @return returns the world the player is currently in
	 */
	public World getCurrentWorld()
	{
		return currentWorld;
	}

	/**
	 * override method to resize window
	 */
	@Override
	public void resized()
	{

	}

	/**
	 * override method to update the screen and textures
	 */
	@Override
	public void update()
	{
		currentWorld.update();
	}

	/**
	 * override render method to render in the game textures and sprites
	 */
	@Override
	public void render()
	{
		Game.getInstance().getShapeBatch().begin();
		Game.getInstance().getSpriteBatch().begin();
		currentWorld.render();
		Game.getInstance().getSpriteBatch().end();
		Game.getInstance().getShapeBatch().end();
	}

	/**
	 * override method to quit the game.
	 */
	@Override
	public void dispose()
	{
		if (playerWorlds != null)
			Game.getInstance().getCurrentProfile().getPlayerFile().saveAdventureMode(Game.ADVENTURE_MODE_STATE);
		AudioSource.stopAll();
	}

}
