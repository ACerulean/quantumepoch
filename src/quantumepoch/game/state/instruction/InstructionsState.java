package quantumepoch.game.state.instruction;

import java.awt.Font;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;

import quantumepoch.core.Core;
import quantumepoch.core.state.GameState;
import quantumepoch.game.Game;
import quantumepoch.game.Textures;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.font.FontType;
import quantumepoch.graphics.font.TrueTypeFont;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * Handles updating and rendering for the instructions game state
 */
public class InstructionsState extends GameState
{

	/** the list of instructions pages */
	private List<InstructionPage> pages;
	/** the current instructions page */
	private int current;
	/** the font to render text in */
	public TrueTypeFont font;

	private TextureRegion bg = new TextureRegion(Textures.SETTINGS_BG.getTexture(), 0, 9 * 70, 16 * 70, 9 * 70);

	public InstructionsState(int id)
	{
		super(id);
		pages = new ArrayList<InstructionPage>();
		current = 0;
	}

	@Override
	public void init()
	{
		font = new TrueTypeFont(new FontType("Arial"), 23, true);

		BufferedReader instructions = new BufferedReader(new InputStreamReader(ClassLoader.getSystemResourceAsStream("instructions/instructions.txt")));
		Game.getInstance().getDebugger().setDisplay(false);
		String line = "";
		String currentHeader = "";
		// parser for custom instructions file
		try
		{
			while ((line = instructions.readLine()) != null)
			{
				if (line.contains("#header"))
				{
					line = line.replace("#header", "");
					currentHeader = line;
				} else if (!line.equals("") && !line.matches("[\t\n]"))
				{
					if (line.contains("#ss"))
					{
						int start = line.indexOf("#ss=\"") + 5;
						String ss = line.substring(start, line.indexOf("\"", start));
						pages.add(new InstructionPage(currentHeader, line.substring(line.indexOf("<") + 1, line.indexOf(">")), "instructions/screenShots/" + ss));
					} else
					{
						pages.add(new InstructionPage(currentHeader, line.substring(line.indexOf("<") + 1, line.indexOf(">")), null));
					}
				}
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		font.loadFont();
	}

	@Override
	public void resized()
	{

	}

	@Override
	public void update()
	{
		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_LEFT))
		{
			if (current > 0)
				--current;
		} else if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_RIGHT))
		{
			if (current + 1 < pages.size())
				++current;
			else
				current = 0;
		}

		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_ESCAPE))
		{
			Game.getInstance().getGameStateManager().setCurrentState(Game.MAIN_MENU_STATE.getId());
		}
	}

	@Override
	public void render()
	{
		// render the settings background
		Game.getInstance().getSpriteBatch().begin();
		Game.getInstance().getSpriteBatch().render(bg, 0, 0);
		Game.getInstance().getSpriteBatch().end();

		Game.getInstance().getFontRenderer().setMode(1);
		Game.getInstance().getSpriteBatch().setTint(Color.WHITE);
		Game.getInstance().getSpriteBatch().begin();
		pages.get(current).render(Game.getInstance().getFontRenderer(), font, Game.getInstance().getSpriteBatch());
		Game.getInstance().getSpriteBatch().end();
		String navigation = "Use the left and right arrow keys to naviagate instructions";
		Game.getInstance().getFontRenderer().render(font, navigation, (Core.getWindow().getWidth() - Game.CS_15.getStringWidth(navigation)) / 2, 75);
		Game.getInstance().getFontRenderer().setMode(0);
	}

	@Override
	public void dispose()
	{
		Game.getInstance().getDebugger().setDisplay(true);
	}

}
