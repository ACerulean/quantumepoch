package quantumepoch.game.state.instruction;

import quantumepoch.core.Core;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.batch.SpriteBatch;
import quantumepoch.graphics.font.FontRenderer;
import quantumepoch.graphics.font.TrueTypeFont;
import quantumepoch.graphics.texture.Texture;

/**
 * A single page of instructions during the instruction state.
 */
public class InstructionPage
{

	/** the header string of this instruction */
	private String header;
	/** the text of this instruction */
	private String text;
	/** the screenshot for this instruction */
	private Texture screenShot;

	/**
	 * Creates a new instruction page
	 * 
	 * @param header
	 *            The header of this instruction
	 * @param text
	 *            The text of this instruction
	 * @param ss
	 *            The screen shot for this instruction
	 */
	public InstructionPage(String header, String text, String ss)
	{
		this.header = header;
		this.text = text;
		if (ss != null)
			this.screenShot = new Texture(ss).load();
	}

	public String getHeader()
	{
		return header;
	}

	public void setHeader(String header)
	{
		this.header = header;
	}

	public String getText()
	{
		return text;
	}

	public void setText(String text)
	{
		this.text = text;
	}

	/**
	 * Renders this instruction page to the screen
	 * 
	 * @param fr
	 *            The font renderer to render text
	 * @param font
	 *            The font to render with
	 * @param batch
	 *            The batch to render the screen shot
	 */
	public void render(FontRenderer fr, TrueTypeFont font, SpriteBatch batch)
	{
		batch.setTint(Color.WHITE);
		if (screenShot != null)
			batch.render(screenShot, 0, 0, 200, 100, 0, 0.4f, 0.4f);
		batch.reset();
		fr.setColor(Color.RED);
		fr.render(font, header, (Core.getWindow().getWidth() - font.getStringWidth(header)) / 2, Core.getWindow().getHeight() - 40, true);
		fr.render(font, text, 10, Core.getWindow().getHeight() - 200, true);

	}
}
