package quantumepoch.game.state;

import quantumepoch.core.Core;
import quantumepoch.core.state.GameState;
import quantumepoch.game.Game;
import quantumepoch.game.Textures;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.gui.ImageTextButton;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * Updates and renders events when the player is on the main menu.
 */
public class MainMenuState extends GameState
{

	/** an instance of an adventure mode button */
	private ImageTextButton adventureModeButton;
	/** an instance of a classic mode button */
	private ImageTextButton classicModeButton;
	/** an instance of a multiplayer button */
	private ImageTextButton multiplayerButton;
	/** an instance of an achievement button */
	private ImageTextButton achievementsButton;
	/** an instance of an instructions button */
	private ImageTextButton instructionsButton;
	/** an instance of an options button */
	private ImageTextButton optionsButton;
	/** an instance of a quit button */
	private ImageTextButton quitButton;
	/** and instance of a credits button */
	private ImageTextButton creditsButton;

	/**
	 * Creates a new main menu state
	 * 
	 * @param id
	 *            The id of the state
	 */
	public MainMenuState(int id)
	{
		super(id);
	}

	/**
	 * override method to initialize the main menu state and create it in the game
	 */
	@Override
	public void init()
	{
		int spacing = 48 + 10;

		TextureRegion but = new TextureRegion(Textures.BUTTON.getTexture(), 0, 0, 12 * 24, 12 * 4);

		adventureModeButton = new ImageTextButton(null, 100, 475 - 0 * spacing, but, but, but, "Adventure Mode", Game.CALIBRI_30, Color.BLUE, Game.getInstance().getBlip());
		classicModeButton = new ImageTextButton(null, 100, 475 - 1 * spacing, but, but, but, "Classic Mode", Game.CALIBRI_30, Color.BLUE, Game.getInstance().getBlip());
		multiplayerButton = new ImageTextButton(null, 100, 475 - 2 * spacing, but, but, but, "Multiplayer", Game.CALIBRI_30, Color.BLUE, Game.getInstance().getBlip());
		achievementsButton = new ImageTextButton(null, 100, 475 - 3 * spacing, but, but, but, "Achievements", Game.CALIBRI_30, Color.BLUE, Game.getInstance().getBlip());
		instructionsButton = new ImageTextButton(null, 100, 475 - 4 * spacing, but, but, but, "Game Instructions", Game.CALIBRI_30, Color.BLUE, Game.getInstance().getBlip());
		creditsButton = new ImageTextButton(null, 100, 475 - 5 * spacing, but, but, but, "Credits", Game.CALIBRI_30, Color.BLUE, Game.getInstance().getBlip());
		optionsButton = new ImageTextButton(null, 100, 475 - 6 * spacing, but, but, but, "Options", Game.CALIBRI_30, Color.BLUE, Game.getInstance().getBlip());
		quitButton = new ImageTextButton(null, 100, 475 - 7 * spacing, but, but, but, "Quit", Game.CALIBRI_30, Color.BLUE, Game.getInstance().getBlip());
	}

	/**
	 * the override method to resize the window
	 */
	@Override
	public void resized()
	{

	}

	/**
	 * the override method to update the the game textures and sprites
	 */
	@Override
	public void update()
	{
		adventureModeButton.update();
		classicModeButton.update();
		multiplayerButton.update();
		achievementsButton.update();
		instructionsButton.update();
		creditsButton.update();
		optionsButton.update();
		quitButton.update();

		if (adventureModeButton.wasPressed())
		{
			Game.getInstance().getGameStateManager().setCurrentState(Game.PRE_ADVENTURE_MODE_STATE.getId());
		} else if (instructionsButton.wasPressed())
		{
			Game.getInstance().getGameStateManager().setCurrentState(Game.INSTRUCTIONS_STATE.getId());
		} else if (classicModeButton.wasPressed())
		{
			Game.getInstance().getGameStateManager().setCurrentState(Game.PRE_CLASSIC_MODE_STATE.getId());
		} else if (multiplayerButton.wasPressed())
		{
			Game.getInstance().getGameStateManager().setCurrentState(Game.MULTIPLAYER_STATE.getId());
		} else if (creditsButton.wasPressed())
		{
			Game.getInstance().getGameStateManager().setCurrentState(Game.CREDITS_STATE.getId());
		} else if (optionsButton.wasPressed())
		{
			Game.getInstance().getGameStateManager().setCurrentState(Game.OPTIONS_STATE.getId());
		} else if (quitButton.wasPressed())
		{
			Core.getCore().stop();
		}
	}

	/**
	 * the override method to render the game textures and sprites
	 */
	@Override
	public void render()
	{
		Game.getInstance().getSpriteBatch().begin();
		Game.getInstance().getShapeBatch().begin();

		// Core.getGraphics().setBlendingEnabled(false);
		adventureModeButton.render(Game.getInstance().getSpriteBatch(), Game.getInstance().getFontRenderer());
		classicModeButton.render(Game.getInstance().getSpriteBatch(), Game.getInstance().getFontRenderer());
		multiplayerButton.render(Game.getInstance().getSpriteBatch(), Game.getInstance().getFontRenderer());
		achievementsButton.render(Game.getInstance().getSpriteBatch(), Game.getInstance().getFontRenderer());
		instructionsButton.render(Game.getInstance().getSpriteBatch(), Game.getInstance().getFontRenderer());
		creditsButton.render(Game.getInstance().getSpriteBatch(), Game.getInstance().getFontRenderer());
		optionsButton.render(Game.getInstance().getSpriteBatch(), Game.getInstance().getFontRenderer());
		quitButton.render(Game.getInstance().getSpriteBatch(), Game.getInstance().getFontRenderer());

		Game.getInstance().getShapeBatch().end();
		Game.getInstance().getSpriteBatch().end();

	}

	/**
	 * override method to quit the game state
	 */
	@Override
	public void dispose()
	{

	}

}
