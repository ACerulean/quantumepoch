package quantumepoch.game.state;

import org.lwjgl.input.Keyboard;

import quantumepoch.core.Core;
import quantumepoch.core.state.GameState;
import quantumepoch.game.CharacterType;
import quantumepoch.game.Game;
import quantumepoch.game.Textures;
import quantumepoch.game.gui.CharacterButton;
import quantumepoch.graphics.BlendFunction;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.animation.TextureAnimation;

/**
 * the character selection state to select between the 3 different playable characters of the adventure game mode
 *
 */
public class CharacterSelectState extends GameState
{

	/**
	 * character instance variables for the different characters
	 */
	private CharacterButton knightButton;
	private CharacterButton wizardButton;
	private CharacterButton archerButton;

	private TextureAnimation knightAnim = new TextureAnimation(Textures.PLAYER.getTexture(), 48, 48, 0, 0, 48 * 5, 48 * 1, 100, new int[] { 0, 1, 2, 1, 0, 3, 4, 3 }).start();
	private TextureAnimation wizardAnim = new TextureAnimation(Textures.PLAYER.getTexture(), 48, 48, 0, 48, 48 * 5, 48 * 1, 100, new int[] { 0, 1, 2, 1, 0, 3, 4, 3 }).start();
	private TextureAnimation archerAnim = new TextureAnimation(Textures.PLAYER.getTexture(), 48, 48, 0, 48 * 2, 48 * 5, 48 * 1, 100, new int[] { 0, 1, 2, 1, 0, 3, 4, 3 }).start();

	private String text = "Choose a character";

	/**
	 * constructor to set the character selection screen and gets the id for the state
	 * 
	 * @param id
	 *            the id of the selection screen state
	 */
	public CharacterSelectState(int id)
	{
		super(id);
	}

	/**
	 * override method to initialize the character selection state
	 */
	@Override
	public void init()
	{

		int startX = Core.getWindow().getWidth() / 3 - 200 / 2;

		knightButton = new CharacterButton(startX + 2 * (200), 200, knightAnim.getCurrentFrame().getTexture(), knightAnim.getCurrentFrame().getTexture(), knightAnim.getCurrentFrame().getTexture(), 200, 200);
		wizardButton = new CharacterButton(startX + 1 * (200), 200, wizardAnim.getCurrentFrame().getTexture(), wizardAnim.getCurrentFrame().getTexture(), wizardAnim.getCurrentFrame().getTexture(), 200, 200);
		archerButton = new CharacterButton(startX + 0 * (200), 200, archerAnim.getCurrentFrame().getTexture(), archerAnim.getCurrentFrame().getTexture(), archerAnim.getCurrentFrame().getTexture(), 200, 200);
	}

	/**
	 * override method to resize the window
	 */
	@Override
	public void resized()
	{

	}

	/**
	 * override method to update the game when character is selected or moused over
	 */
	@Override
	public void update()
	{
		knightButton.update();
		wizardButton.update();
		archerButton.update();

		knightAnim.update();
		wizardAnim.update();
		archerAnim.update();

		knightButton.setHoveredTexture(knightAnim.getCurrentFrame().getTexture());
		wizardButton.setHoveredTexture(wizardAnim.getCurrentFrame().getTexture());
		archerButton.setHoveredTexture(archerAnim.getCurrentFrame().getTexture());

		boolean pressed = false;
		if (knightButton.wasPressed())
		{
			Game.getInstance().getCurrentProfile().setCharacterType(CharacterType.KNIGHT);
			pressed = true;
		} else if (wizardButton.wasPressed())
		{
			Game.getInstance().getCurrentProfile().setCharacterType(CharacterType.WIZARD);
			pressed = true;
		} else if (archerButton.wasPressed())
		{
			Game.getInstance().getCurrentProfile().setCharacterType(CharacterType.ARCHER);
			pressed = true;
		}

		if (pressed)
		{
			Game.getInstance().getGameStateManager().setCurrentState(Game.ADVENTURE_MODE_STATE.getId());
			Game.getInstance().getCurrentProfile().getPlayerFile().saveCharacterType();
		}
		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_ESCAPE))
			Game.getInstance().getGameStateManager().setCurrentState(Game.MAIN_MENU_STATE.getId());
	}

	/**
	 * override method to render the different textures, sprites, and backgrounds
	 */
	@Override
	public void render()
	{
		Game.getInstance().getShapeBatch().begin();
		Game.getInstance().getSpriteBatch().setTint(Color.WHITE);
		Core.getGraphics().setBlendFunction(BlendFunction.ALPHA_BLEND);
		Core.getGraphics().setBlendingEnabled(true);
		Game.getInstance().getSpriteBatch().reset();
		knightButton.render(Game.getInstance().getSpriteBatch(), Game.getInstance().getShapeBatch());
		wizardButton.render(Game.getInstance().getSpriteBatch(), Game.getInstance().getShapeBatch());
		archerButton.render(Game.getInstance().getSpriteBatch(), Game.getInstance().getShapeBatch());
		Game.getInstance().getSpriteBatch().end();
		Game.getInstance().getShapeBatch().end();

		int x = (Core.getWindow().getWidth() - Game.CALIBRI_20.getStringWidth(text)) / 2;
		Game.getInstance().getFontRenderer().setColor(Color.WHITE);
		Game.getInstance().getFontRenderer().render(Game.CALIBRI_20, text, x, Core.getWindow().getHeight() - 200);

	}

	/**
	 * override method to quit out of the state
	 */
	@Override
	public void dispose()
	{

	}

}
