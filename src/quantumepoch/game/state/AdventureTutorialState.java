package quantumepoch.game.state;

import quantumepoch.core.state.GameState;
import quantumepoch.game.CharacterType;
import quantumepoch.game.Game;
import quantumepoch.game.Textures;
import quantumepoch.game.world.World;
import quantumepoch.game.world.WorldSave;
import quantumepoch.game.world.entity.mob.Player;

public class AdventureTutorialState extends GameState
{

	private World tutorialWorld;

	public AdventureTutorialState(int id)
	{
		super(id);
	}

	@Override
	public void init()
	{
		Game.getInstance().getCurrentProfile().setCharacterType(CharacterType.WIZARD);
		tutorialWorld = new WorldSave("worlds/tutorialWorld").createWorld(false, true);
		tutorialWorld.getMobs().add(new Player(tutorialWorld, tutorialWorld.getSpawn().getX(), tutorialWorld.getSpawn().getY(), Textures.PLAYER.getTexture().toTextureRegion()));
	}

	@Override
	public void resized()
	{

	}

	@Override
	public void update()
	{
		tutorialWorld.update();
	}

	@Override
	public void render()
	{
		tutorialWorld.render();
	}

	@Override
	public void dispose()
	{
		Game.getInstance().getCurrentProfile().setCharacterType(null);
	}

}
