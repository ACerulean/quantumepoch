package quantumepoch.game.state;

import quantumepoch.core.Core;
import quantumepoch.core.state.GameState;
import quantumepoch.game.Game;
import quantumepoch.game.Textures;
import quantumepoch.game.Timer;
import quantumepoch.game.classic.ClassicGrid;
import quantumepoch.game.tetris.Cell;
import quantumepoch.game.tetris.MultiplayerControl;
import quantumepoch.game.tetris.TetrisGrid;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.texture.TextureRegion;
import quantumepoch.input.Keyboard;
import quantumepoch.utils.Delay;

public class MultiplayerState extends GameState
{

	/** the player 1 grid */
	private TetrisGrid grid1;
	/** the player 2 grid */
	private TetrisGrid grid2;
	/** the game length timer */
	private Timer timer;
	/** message for end game */
	private String message;
	/** delay until the game returns to the main menu */
	private Delay endDelay;

	private TextureRegion bg;

	private String quitText = "Press shift + 1 to quit the game";
	private MultiplayerControl stdControl;

	/**
	 * Creates a new multiplayer state.
	 * 
	 * @param id
	 *            The id of the state
	 */
	public MultiplayerState(int id)
	{
		super(id);
	}

	/**
	 * override method to initialize the multiplayer state in the game
	 */
	@Override
	public void init()
	{
		timer = new Timer(0, 2, 0, -1);
		message = "";
		initPlayerOne(0);
		initPlayerTwo(0);
		bg = new TextureRegion(Textures.SETTINGS_BG.getTexture(), 0, Core.getWindow().getHeight(), Core.getWindow().getWidth(), Core.getWindow().getHeight());
		stdControl = new MultiplayerControl();
	}

	/**
	 * method to initialize the player one scores and attributes
	 * @param score the score of player one
	 */
	private void initPlayerOne(int score)
	{
		grid1 = new ClassicGrid(10, 22);
		grid1.setNextBufferOffset(24);
		grid1.setLinesCleared(score);
	}

	private void initPlayerTwo(int score)
	{
		grid2 = new ClassicGrid(10, 22);
		// move over the rendering of the next buffer offset to the other side
		// of the grid
		grid2.setNextBufferOffset(24 * 6 + grid2.getWidth() * Cell.SIZE);
		grid2.setLinesCleared(score);
	}

	@Override
	public void resized()
	{

	}

	@Override
	public void update()
	{

		if (message.equals(""))
		{
			grid1.update();
			grid2.update();

			stdControl.update(grid1, grid2);

			if (grid1.hasOverflown())
				initPlayerOne(grid1.getLinesCleared() - 5);
			if (grid2.hasOverflown())
				initPlayerTwo(grid2.getLinesCleared() - 5);

			if (timer.getSeconds() <= 0 && timer.getMinutes() <= 0 && timer.getHours() <= 0)
			{
				if (grid1.getLinesCleared() > grid2.getLinesCleared())
				{
					message = "Player 1 has won!";
				} else if (grid2.getLinesCleared() > grid1.getLinesCleared())
				{
					message = "Player 2 has won!";
				} else
				{
					message = "The game has tied!";
				}
				message += ", returning to main menu";
			}
			timer.update();
			endDelay = new Delay(5000);
		}
		if (endDelay != null && endDelay.isReady())
			Game.getInstance().getGameStateManager().setCurrentState(Game.MAIN_MENU_STATE.getId());
		if ((Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_LSHIFT) || Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_RSHIFT)) && Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_1))
			Game.getInstance().getGameStateManager().setCurrentState(Game.MAIN_MENU_STATE.getId());

	}

	@Override
	public void render()
	{
		Game.getInstance().getSpriteBatch().begin();
		Game.getInstance().getShapeBatch().begin();

		Game.getInstance().getSpriteBatch().setTint(Color.WHITE);
		Game.getInstance().getSpriteBatch().render(bg, 0, 0);

		Game.getInstance().getSpriteBatch().reset();

		if (message != null)
		{
			int tw = Game.CALIBRI_30.getStringWidth(message);
			Game.getInstance().getFontRenderer().render(Game.CALIBRI_30, message, (Core.getWindow().getWidth() - tw) / 2, 600);
		}
		Game.getInstance().getFontRenderer().render(Game.CALIBRI_30, "Time: " + timer, 10, 10);
		Game.getInstance().getFontRenderer().render(Game.CALIBRI_30, "Score 1: " + grid1.getLinesCleared(), 10, 70);
		Game.getInstance().getFontRenderer().render(Game.CALIBRI_30, "Score 2: " + grid2.getLinesCleared(), 10, 40);

		int textWidth = Game.CALIBRI_30.getStringWidth(quitText);
		Game.getInstance().getFontRenderer().render(Game.CALIBRI_30, quitText, (Core.getWindow().getWidth() - textWidth) / 2, 2);

		grid1.render(Game.getInstance().getSpriteBatch(), Game.getInstance().getShapeBatch(), 300, 60);
		grid2.render(Game.getInstance().getSpriteBatch(), Game.getInstance().getShapeBatch(), Core.getWindow().getWidth() - Cell.SIZE * grid2.getWidth() - 300, 60);

		Game.getInstance().getSpriteBatch().end();
		Game.getInstance().getShapeBatch().end();
	}

	@Override
	public void dispose()
	{

	}

}
