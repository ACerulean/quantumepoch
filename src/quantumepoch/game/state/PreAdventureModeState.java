package quantumepoch.game.state;

import org.lwjgl.input.Keyboard;

import quantumepoch.core.Core;
import quantumepoch.core.state.GameState;
import quantumepoch.game.CharacterType;
import quantumepoch.game.Game;
import quantumepoch.game.RenderUtils;
import quantumepoch.game.Textures;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.animation.TextureAnimation;
import quantumepoch.graphics.gui.ImageTextButton;
import quantumepoch.graphics.texture.TextureRegion;

public class PreAdventureModeState extends GameState
{

	private ImageTextButton playGameButton;
	private TextureAnimation anim = null;

	public PreAdventureModeState(int id)
	{
		super(id);
	}

	@Override
	public void init()
	{
		TextureRegion button = new TextureRegion(Textures.BUTTON.getTexture(), 0, 0, 12 * 24, 12 * 4);
		int x = (int) (Core.getWindow().getWidth() - button.getWidth()) / 2;
		playGameButton = new ImageTextButton(null, x, 200, button, button, button, "Play Game", Game.CALIBRI_30, Color.BLUE, Game.getInstance().getBlip());

		CharacterType type = Game.getInstance().getCurrentProfile().getCharacterType();
		if (type == CharacterType.WIZARD)
		{
			anim = new TextureAnimation(Textures.PLAYER.getTexture(), 48, 48, 0, 48, 48 * 5, 48 * 1, 100, new int[] { 0, 1, 2, 1, 0, 3, 4, 3 }).start();
		} else if (type == CharacterType.KNIGHT)
		{
			anim = new TextureAnimation(Textures.PLAYER.getTexture(), 48, 48, 0, 0, 48 * 5, 48 * 1, 100, new int[] { 0, 1, 2, 1, 0, 3, 4, 3 }).start();
		} else if (type == CharacterType.ARCHER)
		{
			anim = new TextureAnimation(Textures.PLAYER.getTexture(), 48, 48, 0, 48 * 2, 48 * 5, 48 * 1, 100, new int[] { 0, 1, 2, 1, 0, 3, 4, 3 }).start();
		}
	}

	@Override
	public void resized()
	{

	}

	@Override
	public void update()
	{
		playGameButton.update();
		if (anim != null)
			anim.update();

		if (playGameButton.isPressed())
			Game.getInstance().getGameStateManager().setCurrentState(Game.ADVENTURE_MODE_STATE.getId());

		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_ESCAPE))
			Game.getInstance().getGameStateManager().setCurrentState(Game.MAIN_MENU_STATE.getId());
	}

	@Override
	public void render()
	{
		Game.getInstance().getSpriteBatch().begin();
		Game.getInstance().getShapeBatch().begin();
		String text = "No details to display because you have not started a game on this account yet.";
		boolean newGame = true;
		if (anim != null)
		{
			int x = (int) (Core.getWindow().getWidth() - anim.getCurrentFrame().getTexture().getWidth() * 5) / 2;
			Game.getInstance().getSpriteBatch().setTint(Color.WHITE);
			Game.getInstance().getSpriteBatch().render(anim.getCurrentFrame().getTexture(), 0, 0, x, 300, 0, 5, 5);
			Game.getInstance().getSpriteBatch().reset();
			RenderUtils.renderHealthBar(x, 290, anim.getCurrentFrame().getTexture().getWidth() * 5, 10, Game.getInstance().getCurrentProfile().getExperiance() / (float) Game.getInstance().getCurrentProfile().getRequiredExperiance(), Color.RED, Color.GREEN);
			text = Game.getInstance().getCurrentProfile().getCharacterType() + " level " + Game.getInstance().getCurrentProfile().getLevel();
			newGame = false;
		}
		int midx = (Core.getWindow().getWidth() - Game.CALIBRI_30.getStringWidth(text)) / 2;
		Game.getInstance().getFontRenderer().render(Game.CALIBRI_30, text, midx, 250);
		if (newGame)
			playGameButton.setText("Start New");
		else
			playGameButton.setText("Play");
		playGameButton.render(Game.getInstance().getSpriteBatch(), Game.getInstance().getFontRenderer());
		Game.getInstance().getSpriteBatch().end();
		Game.getInstance().getShapeBatch().end();
	}

	@Override
	public void dispose()
	{

	}

}
