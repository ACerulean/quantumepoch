package quantumepoch.game.state;

import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;

import quantumepoch.core.Core;
import quantumepoch.core.state.GameState;
import quantumepoch.game.Game;
import quantumepoch.graphics.font.FontType;
import quantumepoch.graphics.font.TrueTypeFont;

public class CreditsState extends GameState
{

	private TrueTypeFont normalFont = new TrueTypeFont(new FontType("Cambria"), 18, true);
	private TrueTypeFont headerFont = new TrueTypeFont(new FontType("Cambria"), 24, true);

	private List<String> credits;
	private float scroll;

	public CreditsState(int id)
	{
		super(id);
		credits = new ArrayList<String>();
		credits.add("Quantum Epoch, a game made by High School students for the BPA SET national competition");
		credits.add("");
		credits.add("#hTeam:");
		credits.add("Ethan Wikoff");
		credits.add("Joey Leavell");
		credits.add("Sam Speece");
		credits.add("Stuart Helm");
		credits.add("");
		credits.add("#hGame design");
		credits.add("Ethan Wikoff");
		credits.add("Joey Leavell");
		credits.add("Sam Speece");
		credits.add("Stuart Helm");
		credits.add("");
		credits.add("#hEngine development");
		credits.add("Joey Leavell");
		credits.add("");
		credits.add("#hGame Code Development");
		credits.add("Ethan Wikoff");
		credits.add("Joey Leavell");
		credits.add("Sam Speece");
		credits.add("");
		credits.add("#hLevel Design");
		credits.add("Ethan Wikoff");
		credits.add("");
		credits.add("#hAudio and Music");
		credits.add("Stuart Helm");
		credits.add("");
		credits.add("#hSprites and Textures");
		credits.add("Stuart Helm");
		credits.add("");
		credits.add("#hGUI Elements");
		credits.add("Stuart Helm");
		credits.add("");
		credits.add("#hBeta Testers");
		credits.add("Ethan Wikoff");
		credits.add("Joey Leavell");
		credits.add("Sam Speece");
		credits.add("Stuart Helm");
		credits.add("");
		credits.add("All students participate in CSSA, Computer Science Students Association.");
	}

	@Override
	public void init()
	{
		Game.getInstance().getDebugger().setDisplay(false);
		normalFont.loadFont();
		headerFont.loadFont();
	}

	@Override
	public void resized()
	{

	}

	@Override
	public void update()
	{
		scroll += 0.5f;
		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_ESCAPE))
			Game.getInstance().getGameStateManager().setCurrentState(Game.MAIN_MENU_STATE.getId());
	}

	@Override
	public void render()
	{
		int currentY = Core.getWindow().getHeight() - 100;

		for (String x : credits)
		{
			if (x.contains("#h"))
			{
				String newString = x.replace("#h", "");
				int cx = (Core.getWindow().getWidth() - headerFont.getStringWidth(newString)) / 2;
				Game.getInstance().getFontRenderer().render(headerFont, newString, cx, currentY + scroll);
				currentY -= headerFont.getCharacterHeight();
			} else
			{
				int cx = (Core.getWindow().getWidth() - normalFont.getStringWidth(x)) / 2;
				Game.getInstance().getFontRenderer().render(normalFont, x, cx, currentY + scroll);
				currentY -= normalFont.getCharacterHeight();
			}
			if (credits.indexOf(x) == credits.size() - 1 && scroll + currentY > Core.getWindow().getHeight())
				Game.getInstance().getGameStateManager().setCurrentState(Game.MAIN_MENU_STATE.getId());
		}
	}

	@Override
	public void dispose()
	{
		Game.getInstance().getDebugger().setDisplay(true);
	}

}
