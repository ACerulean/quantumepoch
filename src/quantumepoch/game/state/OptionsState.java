package quantumepoch.game.state;

import org.lwjgl.input.Keyboard;

import quantumepoch.core.Core;
import quantumepoch.core.state.GameState;
import quantumepoch.game.Game;
import quantumepoch.game.Main;
import quantumepoch.game.Textures;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.gui.BasicSlider;
import quantumepoch.graphics.texture.TextureRegion;

public class OptionsState extends GameState
{

	/** the gap between options sliders */
	public static final int SLIDER_GAP = 30;

	/** the slider for master volume setting */
	private BasicSlider masterVolume;
	/** the slider for fps */
	private BasicSlider fpsSlider;
	/** the slider for graphics */
	private BasicSlider graphicsSlider;

	private TextureRegion bg = new TextureRegion(Textures.SETTINGS_BG.getTexture(), 0, 9 * 70, 16 * 70, 9 * 70), bg2 = new TextureRegion(Textures.SETTINGS_BG.getTexture(), 0, 0, 16 * 70, 9 * 70);

	private float r = (float) (Math.random() * 100) + 50, g = (float) (Math.random() * 100) + 50, b = (float) (Math.random() * 100) + 50;
	private float dr, dg, db;
	private Color color = new Color(0);

	/**
	 * Creates a new options state.
	 * 
	 * @param id
	 *            The id of the state
	 */
	public OptionsState(int id)
	{
		super(id);
	}

	@Override
	public void init()
	{
		masterVolume = new BasicSlider(200, 200, Core.getWindow().getHeight() - BasicSlider.SLIDER_PIECE_HEIGHT * 1 - SLIDER_GAP * 1 - 200, new Color(0, 0.0f, 0.0f), Color.RED);
		masterVolume.setNormalizedSliderPosition(Game.getInstance().getCurrentProfile().getProfileOptions().getMasterVolume());

		fpsSlider = new BasicSlider(200, 200, Core.getWindow().getHeight() - BasicSlider.SLIDER_PIECE_HEIGHT * 2 - SLIDER_GAP * 2 - 200, Color.RED, Color.RED);
		fpsSlider.setNormalizedSliderPosition((Core.getCore().getForegroundFps() - 50) / 200f);

		graphicsSlider = new BasicSlider(200, 200, Core.getWindow().getHeight() - BasicSlider.SLIDER_PIECE_HEIGHT * 3 - SLIDER_GAP * 3 - 200, Color.RED, Color.RED);
		graphicsSlider.setNormalizedSliderPosition(Game.getInstance().getCurrentProfile().getProfileOptions().getGraphicsSetting() * 0.33f);

		dr = (float) (Math.random());
		dg = (float) (Math.random());
		db = (float) (Math.random());
	}

	@Override
	public void resized()
	{

	}

	public String getGraphicsSettingString()
	{
		String graphicsSetting = "";
		if (graphicsSlider.getNormalizedSliderPosition() < 0.33f)
		{
			graphicsSetting = "Low";
		} else if (graphicsSlider.getNormalizedSliderPosition() < 0.66f)
		{
			graphicsSetting = "Medium";
		} else
		{
			graphicsSetting = "High";
		}
		return graphicsSetting;
	}

	public int getGraphicsSettingInt()
	{
		int graphicsSetting = 0;
		if (graphicsSlider.getNormalizedSliderPosition() < 0.33f)
		{
			graphicsSetting = 0;
		} else if (graphicsSlider.getNormalizedSliderPosition() < 0.66f)
		{
			graphicsSetting = 1;
		} else
		{
			graphicsSetting = 2;
		}
		return graphicsSetting;
	}

	@Override
	public void update()
	{
		// set volume line color dependent on volume
		masterVolume.getLineColor().setRed(masterVolume.getNormalizedSliderPosition() + 0.3f);
		masterVolume.update();

		fpsSlider.update();

		graphicsSlider.update();

		if (r <= 50 || r >= 250)
			dr *= -1;
		if (g <= 50 || g >= 250)
			dg *= -1;
		if (b <= 50 || b >= 250)
			db *= -1;

		r += dr;
		g += dg;
		b += db;
		color.setRed(r / 255f);
		color.setGreen(g / 255f);
		color.setBlue(b / 255f);

		Game.getInstance().getCurrentProfile().getProfileOptions().setGraphicsSetting(getGraphicsSettingInt());
		Game.getInstance().getCurrentProfile().getProfileOptions().setMasterVolume(masterVolume.getNormalizedSliderPosition());
		Main.getCore().setForegroundFps((int) (fpsSlider.getNormalizedSliderPosition() * 200 + 50)); // between
																										// 30
																										// and
																										// 230

		if (Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_ESCAPE))
		{
			Game.getInstance().getGameStateManager().setCurrentState(Game.MAIN_MENU_STATE.getId());
		}
	}

	@Override
	public void render()
	{
		Game.getInstance().getSpriteBatch().begin();
		Game.getInstance().getSpriteBatch().setTint(color);
		Game.getInstance().getSpriteBatch().render(bg, 0, 0);
		Game.getInstance().getSpriteBatch().setTint(Color.WHITE);
		Game.getInstance().getSpriteBatch().render(bg2, 0, 0);
		Game.getInstance().getSpriteBatch().end();

		Game.getInstance().getShapeBatch().begin();
		// render master volume
		Game.getInstance().getFontRenderer().setColor(Color.WHITE);
		Game.getInstance().getFontRenderer().render(Game.CALIBRI_30, "Master Volume", 200, Core.getWindow().getHeight() - BasicSlider.SLIDER_PIECE_HEIGHT * 1 - SLIDER_GAP * 1 - 180);
		masterVolume.render(Game.getInstance().getShapeBatch());
		Game.getInstance().getFontRenderer().render(Game.CALIBRI_30, "FPS: " + Core.getCore().getForegroundFps(), 200, Core.getWindow().getHeight() - BasicSlider.SLIDER_PIECE_HEIGHT * 2 - SLIDER_GAP * 2 - 180);
		fpsSlider.render(Game.getInstance().getShapeBatch());
		Game.getInstance().getFontRenderer().render(Game.CALIBRI_30, "Graphics: " + getGraphicsSettingString(), 200, Core.getWindow().getHeight() - BasicSlider.SLIDER_PIECE_HEIGHT * 3 - SLIDER_GAP * 3 - 180);
		graphicsSlider.render(Game.getInstance().getShapeBatch());
		Game.getInstance().getShapeBatch().end();
	}

	@Override
	public void dispose()
	{

	}

}
