package quantumepoch.game.state;

import java.io.File;
import java.io.IOException;

import quantumepoch.core.Core;
import quantumepoch.core.state.GameState;
import quantumepoch.game.Game;
import quantumepoch.game.Textures;
import quantumepoch.game.gui.CreateProfileWindow;
import quantumepoch.game.profile.Profile;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.gui.Component;
import quantumepoch.graphics.gui.ImageTextButton;
import quantumepoch.graphics.texture.TextureRegion;
import quantumepoch.utils.IOUtils;

public class ProfileSelectState extends GameState
{

	/** the maximum amount of profiles */
	public static final int MAX_PROFILES = 5;
	/** the root path for player save files */
	public static final String PLAYER_DATA_PATH = IOUtils.getRootParentPath() + "\\playerdata";

	/** the directory of player files */
	private File playerDataDirectory;
	/** an array of profile buttons */
	private ImageTextButton[] profileButtons;
	/** the button for deleting an existing profile */
	private ImageTextButton deleteProfile;
	/** the create profile window when in create mode */
	private CreateProfileWindow profileWindow;
	/** the current profile selected */
	private int profileSelected;
	/** This allows you to delete a profile. */
	private boolean deleteMode;

	private Profile[] profiles;

	/**
	 * Creates a new profile selection state
	 * 
	 * @param id
	 *            The id of the game state
	 */
	public ProfileSelectState(int id)
	{
		super(id);
	}

	@Override
	public void init()
	{
		profileButtons = new ImageTextButton[MAX_PROFILES];
		profiles = new Profile[MAX_PROFILES];

		int halfWidth = Core.getWindow().getWidth() / 2;
		int profileButtonGap = 10;
		int currentY = 500;

		TextureRegion but = new TextureRegion(Textures.BUTTON.getTexture(), 0, 0, 12 * 24, 12 * 4);

		for (int i = 0; i < profileButtons.length; i++)
		{
			profileButtons[i] = new ImageTextButton(null, halfWidth - (int) but.getWidth() / 2, currentY, but, but, but, "Empty", Game.CALIBRI_30, Color.BLUE, Game.getInstance().getBlip());
			// profileButtons[i] = new BasicButton(null, halfWidth -
			// BUTTON_WIDTH / 2, currentY, "Empty", Game.CALIBRI_30, Color.RED,
			// BUTTON_WIDTH, BUTTON_HEIGHT);
			currentY -= (profileButtonGap + but.getHeight());
		}

		// createProfile = new ImageTextButton(null, halfWidth - (int)
		// but.getWidth() / 2, 200, but, but, but, "Create Profile",
		// Game.CALIBRI_30, Color.BLUE);
		deleteProfile = new ImageTextButton(null, halfWidth - (int) but.getWidth() / 2, 142, but, but, but, "Delete Profile", Game.CALIBRI_30, Color.BLUE, Game.getInstance().getBlip());
		// selectProfile = new ImageTextButton(null, halfWidth - (int)
		// but.getWidth() / 2, 84, but, but, but, "Select Profile",
		// Game.CALIBRI_30, Color.BLUE);

		profileSelected = -1;

		playerDataDirectory = new File(PLAYER_DATA_PATH);
		if (!playerDataDirectory.exists())
		{
			playerDataDirectory.mkdir();
		}

		File[] profileList = playerDataDirectory.listFiles();

		for (int i = 0; i < profileList.length; i++)
		{
			// add a profile

			// a huge loading block will be here soon

			String name = profileList[i].getName().substring(0, profileList[i].getName().indexOf("."));
			profiles[i] = new Profile(name, profileList[i].getAbsolutePath(), 0, 0, true);
			profileButtons[i].setText(name);
		}

	}

	public void deleteCurrentProfile()
	{
		// delete player save file
		if (profiles[profileSelected] != null)
		{
			new File(profiles[profileSelected].getSavePath()).delete();
			profiles[profileSelected] = null;
			profileButtons[profileSelected].setText("Empty");
			profileSelected = -1;
			deleteMode = false;
		}
	}

	public void enterCurrentProfile()
	{
		// enter a save file from the player
		Game.getInstance().setCurrentProfile(profiles[profileSelected]);
		Game.getInstance().getGameStateManager().setCurrentState(Game.MAIN_MENU_STATE.getId());
	}

	public void createCurrentProfile()
	{
		// create a new profile.
		profileWindow = new CreateProfileWindow();
	}

	@Override
	public void resized()
	{

	}

	@Override
	public void update()
	{
		for (ImageTextButton profileButton : profileButtons)
		{
			profileButton.update();
		}

		deleteProfile.update();

		if (profileWindow != null)
			Component.setFocused(profileWindow);
		if (Core.getInput().getMouse().wasButtonPressed(0))
		{
			if (deleteProfile.isPressed())
			{
				deleteMode = !deleteMode;
			} else
				for (int i = 0; i < profileButtons.length; i++)
				{
					if (profileButtons[i].isPressed())
					{
						profileSelected = i;
						if (deleteMode)
							deleteCurrentProfile();
						else if (profiles[profileSelected] == null)
						{
							createCurrentProfile();
						} else
							enterCurrentProfile();

						break;
					}
				}
		}
		// check if the create profile is selected

		if (profileWindow != null)
		{
			profileWindow.update();

			if (profileWindow.getCreateButton().isPressed())
			{

				// a new profile has beenm requested to be created

				String name = profileWindow.getNameField().getText();

				profiles[profileSelected] = new Profile(name, PLAYER_DATA_PATH + "/" + name + ".player", 0, 0, false);
				profileButtons[profileSelected].setText(profiles[profileSelected].getName());

				// create the file
				try
				{
					new File(profiles[profileSelected].getSavePath()).createNewFile();
				} catch (IOException e)
				{
					e.printStackTrace();
				}

				Component.setFocused(null);
				profileWindow = null;

				// save data to memory
			} else if (profileWindow.getCancelButton().isPressed())
			{
				Component.setFocused(null);
				profileWindow = null;
			}
		}

	}

	@Override
	public void render()
	{
		Game.getInstance().getSpriteBatch().begin();
		Game.getInstance().getSpriteBatch().setTint(Color.WHITE);
		for (ImageTextButton profileButton : profileButtons)
		{
			if (deleteMode)
			{
				profileButton.setTint(Color.RED);
			} else
			{

				profileButton.setTint(Color.BLUE);

			}

			profileButton.render(Game.getInstance().getSpriteBatch(), Game.getInstance().getFontRenderer());
		}
		deleteProfile.render(Game.getInstance().getSpriteBatch(), Game.getInstance().getFontRenderer());

		if (profileWindow != null)
		{
			profileWindow.render(Game.getInstance().getShapeBatch(), Game.getInstance().getSpriteBatch(), Game.getInstance().getFontRenderer());
		}

		Game.getInstance().getSpriteBatch().end();
	}

	@Override
	public void dispose()
	{

	}

}
