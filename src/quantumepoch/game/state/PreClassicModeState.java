package quantumepoch.game.state;

import java.util.List;

import org.lwjgl.input.Keyboard;

import quantumepoch.core.Core;
import quantumepoch.core.state.GameState;
import quantumepoch.game.Game;
import quantumepoch.game.Textures;
import quantumepoch.game.classic.ClassicCell;
import quantumepoch.game.classic.ClassicGrid;
import quantumepoch.game.classic.ClassicTetromino;
import quantumepoch.game.tetris.CellLocation;
import quantumepoch.game.tetris.TetrisGrid;
import quantumepoch.game.tetris.Tetromino;
import quantumepoch.game.tetris.TetrominoType;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.gui.ImageTextButton;
import quantumepoch.graphics.texture.TextureRegion;
import quantumepoch.utils.xml.XMLDocument;
import quantumepoch.utils.xml.XMLElement;

public class PreClassicModeState extends GameState
{

	/** an instance of a button for loading a saved game */
	private ImageTextButton loadSavedButton;
	/** an instance of a button for starting a new game */
	private ImageTextButton newGameButton;

	/**
	 * Creates a new pre classic mode state.
	 * 
	 * @param id
	 *            The id of the state
	 */
	public PreClassicModeState(int id)
	{
		super(id);
	}

	private void loadClassicWorld()
	{
		XMLDocument doc = Game.getInstance().getCurrentProfile().getPlayerFile().getXmlDoc();

		XMLElement root = doc.getRootElement().getChildByName("classicMode");
		if (root != null)
		{

			Game.getInstance().getGameStateManager().setCurrentState(Game.CLASSIC_MODE_STATE.getId());

			TetrisGrid tgrid = Game.CLASSIC_MODE_STATE.getTetrisGrid();

			String time = root.getChildByName("time").getValue();
			int score = Integer.parseInt(root.getChildByName("score").getValue());
			int level = Integer.parseInt(root.getChildByName("level").getValue());
			int linesCleared = Integer.parseInt(root.getChildByName("linesCleared").getValue());

			Game.CLASSIC_MODE_STATE.getTimer().setSeconds(Integer.parseInt(time.split(":")[2]));
			Game.CLASSIC_MODE_STATE.getTimer().setMinutes(Integer.parseInt(time.split(":")[1]));
			Game.CLASSIC_MODE_STATE.getTimer().setHours(Integer.parseInt(time.split(":")[0]));
			Game.CLASSIC_MODE_STATE.setScore(score);
			Game.CLASSIC_MODE_STATE.setLevel(level);
			Game.CLASSIC_MODE_STATE.setLinesCleared(linesCleared);
			tgrid.setLinesCleared(linesCleared);

			XMLElement fallingTetromino = root.getChildByName("fallingTetromino");
			String type = fallingTetromino.getAttribs().get(0).getValue();

			XMLElement pivot = fallingTetromino.getChildByName("pivot");
			CellLocation loc = new CellLocation(Integer.parseInt(pivot.getAttribs().get(0).getValue()), Integer.parseInt(pivot.getAttribs().get(1).getValue()));

			CellLocation[] locations = new CellLocation[4];
			List<XMLElement> children = fallingTetromino.getChildrenByName("blockdata");
			for (int i = 0; i < 4; i++)
			{
				XMLElement elem = children.get(i);
				locations[i] = new CellLocation(Integer.parseInt(elem.getAttribs().get(0).getValue()), Integer.parseInt(elem.getAttribs().get(1).getValue()));
			}

			int colorIndex = 0;
			for (int i = 0; i < TetrominoType.values().length; i++)
			{
				if (TetrominoType.values()[i] == TetrominoType.valueOf(type))
				{
					colorIndex = i;
				}
			}

			Tetromino tet = new ClassicTetromino(Game.CLASSIC_MODE_STATE.getTetrisGrid(), loc, TetrominoType.valueOf(type), TetrisGrid.BLOCK_TEXTURE, ClassicGrid.TETROMINO_COLORS[colorIndex]);
			Game.CLASSIC_MODE_STATE.getTetrisGrid().setCurrentTetromino(tet);

			XMLElement grid = root.getChildByName("grid");

			int width = Integer.parseInt(grid.getAttribs().get(0).getValue());
			int height = Integer.parseInt(grid.getAttribs().get(1).getValue());

			List<XMLElement> cellElements = grid.getChildrenByName("cell");
			for (int x = 0; x < width; x++)
			{
				for (int y = 0; y < height; y++)
				{
					XMLElement elem = cellElements.get(x + (y * width));

					boolean empty = Boolean.parseBoolean(elem.getAttrib("empty").getValue());

					String[] values = elem.getAttrib("color").getValue().split(" ");
					Color color = new Color(Integer.parseInt(values[0]), Integer.parseInt(values[1]), Integer.parseInt(values[2]));

					Game.CLASSIC_MODE_STATE.getTetrisGrid().getCells()[x + (y * width)].setTetrisBlock(!empty);
					((ClassicCell) Game.CLASSIC_MODE_STATE.getTetrisGrid().getCells()[x + (y * width)]).setColor(color);

				}
			}

		}
	}

	@Override
	public void init()
	{
		int halfWidth = Core.getWindow().getWidth() / 2;
		int buttonWidth = 300;

		TextureRegion but = new TextureRegion(Textures.BUTTON.getTexture(), 0, 0, 12 * 24, 12 * 4);

		loadSavedButton = new ImageTextButton(null, halfWidth - buttonWidth / 2, 400, but, but, but, "Load Game", Game.CALIBRI_30, Color.BLUE, Game.getInstance().getBlip());// new
																																												// BasicButton(null,
																																												// halfWidth
																																												// -
																																												// buttonWidth
																																												// /
																																												// 2,
																																												// 400,
																																												// "Load Saved Game",
																																												// Game.CALIBRI_30,
																																												// BUTTON_COLOR,
																																												// buttonWidth,
																																												// 40,
																																												// Game.getInstance().getBlip());
		newGameButton = new ImageTextButton(null, halfWidth - buttonWidth / 2, 350, but, but, but, "New Game", Game.CALIBRI_30, Color.BLUE, Game.getInstance().getBlip());// new
																																											// BasicButton(null,
																																											// halfWidth
																																											// -
																																											// buttonWidth
																																											// /
																																											// 2,
																																											// 400,
																																											// "Load Saved Game",
																																											// Game.CALIBRI_30,
																																											// BUTTON_COLOR,
																																											// buttonWidth,
																																											// 40,
																																											// Game.getInstance().getBlip());
	}

	@Override
	public void resized()
	{

	}

	@Override
	public void update()
	{
		loadSavedButton.update();
		newGameButton.update();

		if (loadSavedButton.isPressed())
		{
			loadClassicWorld();
		} else if (newGameButton.isPressed())
		{
			Game.getInstance().getGameStateManager().setCurrentState(Game.CLASSIC_MODE_STATE.getId());
		}

		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_ESCAPE))
			Game.getInstance().getGameStateManager().setCurrentState(Game.MAIN_MENU_STATE.getId());

	}

	@Override
	public void render()
	{
		Game.getInstance().getSpriteBatch().begin();
		loadSavedButton.render(Game.getInstance().getSpriteBatch(), Game.getInstance().getFontRenderer());
		newGameButton.render(Game.getInstance().getSpriteBatch(), Game.getInstance().getFontRenderer());

		Game.getInstance().getSpriteBatch().end();
	}

	@Override
	public void dispose()
	{

	}

}
