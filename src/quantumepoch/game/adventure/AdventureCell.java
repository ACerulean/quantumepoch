//package quantumepoch.game.adventure;
//
//import quantumepoch.game.tetris.Cell;
//import quantumepoch.game.tetris.CellLocation;
//import quantumepoch.graphics.Color;
//import quantumepoch.graphics.texture.TextureRegion;
//
///**
// * A single adventure mode cell that inherits from the Cell to add additional functionality for adventure mode.
// */
//public class AdventureCell extends Cell
//{
//
//	/** the resulting command of this cell when a line is cleared */
//	private CommandType commandType;
//
//	/**
//	 * Creates a new adventure cell.
//	 * 
//	 * @param location
//	 *            The grid location of the cell
//	 * @param tetrisBlock
//	 *            Whether this cell is a tetris block.
//	 * @param color
//	 *            The color of this adventure cell
//	 * @param texture
//	 *            The texture of this cell
//	 * @param commandType
//	 *            The commandType of this cell
//	 */
//	public AdventureCell(CellLocation location, boolean tetrisBlock, Color color, TextureRegion texture, CommandType commandType)
//	{
//		super(location, tetrisBlock, color, texture);
//		this.commandType = commandType;
//	}
//
//	/**
//	 * @return the command type of this cell
//	 */
//	public CommandType getCommandType()
//	{
//		return commandType;
//	}
//
//	/**
//	 * Sets the command type of this cell.
//	 * 
//	 * @param commandType
//	 *            The new command type of this cell.
//	 */
//	public void setCommandType(CommandType commandType)
//	{
//		this.commandType = commandType;
//	}
//}
