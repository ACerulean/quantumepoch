package quantumepoch.game.adventure;

/**
 * Stores all possible command types for tetrominos.
 */
public enum CommandType
{

	HEALTH_POTION, MAIN_ATTACK_CHARGE;

}
