package quantumepoch.game.adventure;

import quantumepoch.game.classic.ClassicCell;

/**
 * An interface which defines callback methods for classes wanting to listen for tetris grid events.
 *
 */
public interface GridListener
{

	/**
	 * Invoked when a row has been dropped on a grid.
	 * 
	 * @param source
	 *            The source grid
	 * @param cell
	 *            The array of cells that was dropped
	 */
	public void rowDropped(AdventureGrid source, ClassicCell[] cell);

}
