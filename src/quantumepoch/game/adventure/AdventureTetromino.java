//package quantumepoch.game.adventure;
//
//import quantumepoch.game.tetris.CellLocation;
//import quantumepoch.game.tetris.TetrisGrid;
//import quantumepoch.game.tetris.Tetromino;
//import quantumepoch.game.tetris.TetrominoType;
//import quantumepoch.graphics.Color;
//
///**
// * An implementation of an adventure tetromino for adventure mode.
// */
//public class AdventureTetromino extends Tetromino
//{
//	/** the command type of this tetromino */
//	private CommandType commandType;
//
//	/**
//	 * Creates a new adventure tetromino.
//	 * 
//	 * @param grid
//	 *            The grid to which this tetromino belongs
//	 * @param start
//	 *            The start pivot of this tetromino
//	 * @param type
//	 *            The type of this tetromino
//	 * @param commandType
//	 *            The command type of this tetromino
//	 */
//	public AdventureTetromino(TetrisGrid grid, CellLocation start, TetrominoType type, CommandType commandType)
//	{
//		super(grid, start, type, Color.RED, TetrisGrid.BLOCK_TEXTURE);
//
//		if (commandType == CommandType.HEALTH_POTION)
//		{
//			setColor(Color.RED);
//		} else if (commandType == CommandType.MAIN_ATTACK_CHARGE)
//		{
//			setColor(Color.BLUE);
//		}
//
//		this.commandType = commandType;
//	}
//
//	/**
//	 * @return the command type of this tetromino
//	 */
//	public CommandType getCommandType()
//	{
//		return commandType;
//	}
//
//	/**
//	 * Sets the commnad type of this tetromino
//	 * 
//	 * @param commandType
//	 *            The new command type
//	 */
//	public void setCommandType(CommandType commandType)
//	{
//		this.commandType = commandType;
//	}
//}
