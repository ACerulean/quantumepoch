package quantumepoch.game.adventure;

import java.util.ArrayList;
import java.util.List;

import quantumepoch.game.classic.ClassicCell;
import quantumepoch.game.classic.ClassicGrid;
import quantumepoch.game.classic.ClassicTetromino;
import quantumepoch.game.tetris.Cell;
import quantumepoch.game.tetris.CellLocation;
import quantumepoch.game.tetris.TetrisGrid;
import quantumepoch.game.tetris.Tetromino;
import quantumepoch.game.tetris.TetrominoType;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.batch.ShapeBatch;
import quantumepoch.graphics.batch.SpriteBatch;

/**
 * An implementation of an adventure tetris grid, which extends the normal tetris grid to add adventure mdoe functionality.
 */
public class AdventureGrid extends TetrisGrid
{

	/** the possitble colors of a tetromino */
	public static final Color[] TETROMINO_COLORS = new Color[] { Color.RED, Color.BLUE, Color.GREEN, Color.INDIGO, Color.CYAN, Color.CORNFLOWER_BLUE, Color.GOLD };

	/** the grid listeners when a line is cleared in adventure mode */
	private List<GridListener> gridListeners;

	/**
	 * Creates a new adventure grid
	 * 
	 * @param width
	 *            The width of the grid
	 * @param height
	 *            The height of the grid
	 */
	public AdventureGrid(int width, int height, boolean renderNextBuffer)
	{
		super(width, height, renderNextBuffer);
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				cells[x + (y * width)] = new ClassicCell(new CellLocation(x, y), false, Color.WHITE, BLOCK_TEXTURE);
			}
		}

		gridListeners = new ArrayList<GridListener>();
	}

	/**
	 * Adds a grid listener. This handles events that could happen during tetris gameplay.
	 * 
	 * @param listener
	 *            The listener to add
	 */
	public void addGridListener(GridListener listener)
	{
		gridListeners.add(listener);
	}

	/**
	 * Saves the current tetromino to the grid.
	 */
	@Override
	public void saveTetrominoToGrid()
	{
		for (CellLocation location : currentTetromino.getBlockLocations())
		{
			cells[location.getX() + (location.getY() * width)].setTetrisBlock(true);
			((ClassicCell) getCellAt(location.getX(), location.getY())).setColor(((ClassicTetromino) currentTetromino).getColor());
		}
	}

	/**
	 * Generates a new tetromino
	 */
	@Override
	public Tetromino generateTetromino()
	{
		int index = rand.nextInt(TetrominoType.values().length);

		TetrominoType gen = TetrominoType.values()[index];
		while (isTetrominoOnQueue(gen))
		{
			index = (index + 1) % TetrominoType.values().length;
			gen = TetrominoType.values()[index];
		}

		return new ClassicTetromino(this, new CellLocation(width / 2, height - 2), gen, ClassicGrid.BLOCK_TEXTURE, ClassicGrid.TETROMINO_COLORS[index]);
	}

	@Override
	public void dropRows(int linesToDrop, int minComplete)
	{
		for (int i = 0; i < linesToDrop; i++)
		{
			final ClassicCell[] cellsDropped = new ClassicCell[width];
			for (int y = minComplete + 1; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					if (getCellAt(x, y).isTetrisBlock())
					{
						ClassicCell current = (ClassicCell) getCellAt(x, y);
						ClassicCell under = (ClassicCell) getCellAt(x, y - 1);
						under.setTetrisBlock(true);
						under.setColor(current.getColor());
						current.setTetrisBlock(false);
						cellsDropped[x] = current;
					}
				}
			}

			for (GridListener listener : gridListeners)
			{
				listener.rowDropped(this, cellsDropped);
			}
		}
	}

	@Override
	public void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch, int xPosition, int yPosition)
	{
		super.render(spriteBatch, shapeBatch, xPosition, yPosition);
		spriteBatch.setTint(Color.WHITE);

		if (getCurrentTetromino() != null)

			for (CellLocation loc : getCurrentFall())
			{
				spriteBatch.render(((ClassicTetromino) currentTetromino).getTexture(), xPosition + loc.getX() * Cell.SIZE, yPosition + loc.getY() * Cell.SIZE);
			}

		if (getCurrentTetromino() != null)
		{
			spriteBatch.setTint(((ClassicTetromino) currentTetromino).getColor());
			for (CellLocation location : currentTetromino.getBlockLocations())
			{
				spriteBatch.render(((ClassicTetromino) currentTetromino).getTexture(), xPosition + location.getX() * Cell.SIZE, yPosition + location.getY() * Cell.SIZE);
			}
		}

		for (Cell cell : cells)
		{
			if (cell.isTetrisBlock())
			{
				spriteBatch.setTint(((ClassicCell) cell).getColor());
				spriteBatch.render(((ClassicCell) cell).getTexture(), xPosition + cell.getLocation().getX() * Cell.SIZE, yPosition + cell.getLocation().getY() * Cell.SIZE);
			}
		}
	}
}
