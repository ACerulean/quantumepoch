package quantumepoch.game.adventure;

import java.util.List;

import quantumepoch.game.Game;
import quantumepoch.game.Textures;
import quantumepoch.game.profile.Profile;
import quantumepoch.game.world.World;
import quantumepoch.game.world.WorldSave;
import quantumepoch.game.world.entity.mob.Slime;
import quantumepoch.game.world.entity.mob.SlimeBoss;
import quantumepoch.game.world.entity.mob.Square;
import quantumepoch.game.world.entity.mob.Stump;
import quantumepoch.utils.xml.XMLDocument;
import quantumepoch.utils.xml.XMLElement;

/**
 * Stores a collection of worlds for a specific profile during runtime and is responsible for loading them all.
 */
public class Worlds
{

	/** the loaded worlds */
	private World[] loadedWorlds;
	/** the profile of these worlds */
	private Profile profile;
	/** whether the worlds have finished loading */
	private boolean loaded;

	/**
	 * Creates a world loader.
	 * 
	 * @param prof
	 *            The profile of these worlds
	 */
	public Worlds(Profile prof)
	{
		this.profile = prof;
		run();
	}

	/**
	 * @return the profile of these worlds
	 */
	public Profile getProfile()
	{
		return profile;
	}

	/**
	 * @param name
	 *            The name of the world to search for
	 * @return The loaded world, or null if non existant
	 */
	public World getWorld(String name)
	{
		for (World world : loadedWorlds)
			if (world.getName().equals(name))
				return world;
		return null;
	}

	public void loadWorld(String path, boolean next)
	{
		World[] arr = new World[loadedWorlds.length + 1];
		for (int i = 0; i < loadedWorlds.length; i++)
			arr[i] = loadedWorlds[i];
		arr[arr.length - 1] = new WorldSave(path).createWorld(next, true);
		loadedWorlds = arr;
	}

	/**
	 * @return whether the worlds have loaded
	 */
	public boolean isLoaded()
	{
		return loaded;
	}

	/**
	 * @param index
	 *            The index of a loaded world
	 * @return the loaded world
	 */
	public World getWorld(int index)
	{
		return loadedWorlds[index];
	}

	public World[] getLoadedWorlds()
	{
		return loadedWorlds;
	}

	/**
	 * Attempts to load all worlds specified by the profile save file.
	 */
	public void run()
	{
		XMLDocument file = profile.getPlayerFile().getXmlDoc();
		XMLElement adventureModeNode = file.getRootElement().getChildByName("adventureMode");
		int worldCount = 0;
		for (XMLElement element : adventureModeNode.getChildren())
			if (element.getName().equals("world"))
				worldCount++;

		if (adventureModeNode.getChildByName("level") != null)
		{
			Game.getInstance().getCurrentProfile().setLevel(Integer.parseInt(adventureModeNode.getChildByName("level").getValue()));
			Game.getInstance().getCurrentProfile().setExperiance(Integer.parseInt(adventureModeNode.getChildByName("exp").getValue()));
		}

		if (worldCount == 0)
		{
			loadedWorlds = new World[1];
			loadedWorlds[0] = new WorldSave("/worlds/tutorialWorld").createWorld(false, true);
		} else
		{
			loadedWorlds = new World[worldCount];

			Game.getInstance().getCurrentProfile().setWorld(worldCount - 1);

			int currentWorld = 0;
			for (XMLElement element : adventureModeNode.getChildren())
			{
				if (element.getName().equals("world"))
				{
					String worldName = element.getChildByName("name").getValue();
					String worldPath = "/worlds/" + worldName;

					List<XMLElement> entities = element.getChildrenByName("entity");

					if (worldCount == 1 && entities.size() == 0)
						loadedWorlds[currentWorld] = new WorldSave(worldPath).createWorld(currentWorld < worldCount - 1, true);
					else
						loadedWorlds[currentWorld] = new WorldSave(worldPath).createWorld(currentWorld < worldCount - 1, false);

					for (XMLElement entityElement : entities)
					{
						String type = entityElement.getAttrib("type").getValue();
						float x = Float.parseFloat(entityElement.getAttrib("x").getValue()), y = Float.parseFloat(entityElement.getAttrib("y").getValue());
						int level = Integer.parseInt(entityElement.getAttrib("level").getValue());
						if (type.equals(Slime.class.getName()))
						{
							loadedWorlds[currentWorld].getMobs().add(new Slime(loadedWorlds[currentWorld], x, y, Textures.ENEMIES.getTexture().toTextureRegion(), level));
						} else if (type.equals(Square.class.getName()))
						{
							loadedWorlds[currentWorld].getMobs().add(new Square(loadedWorlds[currentWorld], x, y, Textures.ENEMIES.getTexture().toTextureRegion(), level));
						} else if (type.equals(Stump.class.getName()))
						{
							loadedWorlds[currentWorld].getMobs().add(new Stump(loadedWorlds[currentWorld], x, y, Textures.ENEMIES.getTexture().toTextureRegion(), level));
						}
					}

					XMLElement boss = element.getChildByName("boss");
					if (boss != null)
					{
						String type = boss.getAttrib("type").getValue();
						float x = Float.parseFloat(boss.getAttrib("x").getValue()), y = Float.parseFloat(boss.getAttrib("y").getValue());
						int level = Integer.parseInt(boss.getAttrib("level").getValue());
						if (type.equals(SlimeBoss.class.getName()))
							loadedWorlds[currentWorld].setWorldBoss(new SlimeBoss(loadedWorlds[currentWorld], x, y, Textures.ENEMY.getTexture().toTextureRegion(), level));
					}

					currentWorld++;
				}
			}
		}
		loaded = true;
	}
}
