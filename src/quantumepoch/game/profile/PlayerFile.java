package quantumepoch.game.profile;

import java.io.File;
import java.io.IOException;
import java.util.List;

import quantumepoch.core.Core;
import quantumepoch.game.Game;
import quantumepoch.game.classic.ClassicCell;
import quantumepoch.game.state.AdventureModeState;
import quantumepoch.game.state.ClassicModeState;
import quantumepoch.game.tetris.Cell;
import quantumepoch.game.tetris.CellLocation;
import quantumepoch.game.world.entity.mob.Enemy;
import quantumepoch.game.world.entity.mob.MobileEntity;
import quantumepoch.game.world.entity.mob.Player;
import quantumepoch.game.world.entity.mob.WorldNavigator;
import quantumepoch.utils.xml.XMLAttrib;
import quantumepoch.utils.xml.XMLDocument;
import quantumepoch.utils.xml.XMLElement;
import quantumepoch.utils.xml.XMLHeader;

/**
 * Represents a player save file
 */
public class PlayerFile
{

	/** the xml doc of this player save profile */
	private XMLDocument xmlDoc;
	/** the profile to save in this player file */
	private Profile profile;

	/**
	 * Creates a new player profile with the specified arguments
	 * 
	 * @param file
	 *            The file to save this player file to.
	 * @param profile
	 *            The profile that will be stored in this player file.
	 * @param existing
	 *            Whether the player file already exists
	 */
	public PlayerFile(File file, Profile profile, boolean existing)
	{
		xmlDoc = new XMLDocument(file);

		this.profile = profile;

		if (!existing)
		{
			if (!file.exists())
			{
				try
				{
					file.createNewFile();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}

			initialize();
		}

		xmlDoc.load();

	}

	/**
	 * @return the xml doc of this player file
	 */
	public XMLDocument getXmlDoc()
	{
		return xmlDoc;
	}

	/**
	 * @return the profile to save in this player file
	 */
	public Profile getProfile()
	{
		return profile;
	}

	/**
	 * Sets the XMLDoc used by this player file.
	 * 
	 * @param xmlDoc
	 *            The new xml document
	 */
	public void setXmlDoc(XMLDocument xmlDoc)
	{
		this.xmlDoc = xmlDoc;
	}

	/**
	 * Sets the profile to save in this player file.
	 * 
	 * @param profile
	 *            The new profile to save
	 */
	public void setProfile(Profile profile)
	{
		this.profile = profile;
	}

	/**
	 * Initializes this player file with new data. This is used if this is a new player file.
	 */
	private void initialize()
	{
		xmlDoc.setXmlHeader(new XMLHeader("1.0", "UTF-8"));

		XMLElement root = new XMLElement("player", null);
		xmlDoc.setRootElement(root);
		xmlDoc.getRootElement().addAttrib(new XMLAttrib("name", profile.getName()));

		XMLElement adventureModeRoot = new XMLElement("adventureMode", null);
		xmlDoc.getRootElement().addChild(adventureModeRoot);

		// adventureModeRoot.addChild(new XMLElement("characterType", "null"));
		//
		// XMLElement world = new XMLElement("world", null);
		// adventureModeRoot.addChild(world);
		// world.addChild(new XMLElement("name", "plainsWorld"));

		xmlDoc.save();
	}

	/**
	 * Saves the profile's character type to memory
	 */
	public void saveCharacterType()
	{
		XMLElement charType = xmlDoc.getRootElement().getChildByName("adventureMode").getChildByName("characterType");
		if (charType == null)
			charType = new XMLElement("charType", null);
		if (xmlDoc.getRootElement().getChildByName("adventureMode").getChildByName("characterType") == null)
			xmlDoc.getRootElement().getChildByName("adventureMode").addChild(charType);
		charType.setValue(profile.getCharacterType().toString());
		xmlDoc.save();
	}

	/**
	 * Saves the classic mode state to memory
	 * 
	 * @param classicMode
	 *            The ClassicModeState to save to the stored profile
	 */
	public void saveClassicMode(ClassicModeState classicMode)
	{
		xmlDoc.getRootElement().removeChild(xmlDoc.getRootElement().getChildByName("classicMode"));

		XMLElement classicModeRoot = new XMLElement("classicMode", null);
		xmlDoc.getRootElement().addChild(classicModeRoot);

		// add time element
		classicModeRoot.addChild(new XMLElement("time", classicMode.getTimer().toString()));
		// add score element
		classicModeRoot.addChild(new XMLElement("score", "" + classicMode.getScore()));
		// add level element
		classicModeRoot.addChild(new XMLElement("level", "" + classicMode.getLevel()));
		// add lines cleared element
		classicModeRoot.addChild(new XMLElement("linesCleared", "" + classicMode.getLinesCleared()));

		// add falling tetromino
		XMLElement fallingTetromino = new XMLElement("fallingTetromino", null);
		fallingTetromino.addAttrib(new XMLAttrib("type", classicMode.getTetrisGrid().getCurrentTetromino().getType().toString()));
		XMLElement pivotPoint = new XMLElement("pivot", null);
		pivotPoint.addAttrib(new XMLAttrib("x", classicMode.getTetrisGrid().getCurrentTetromino().getPivot().getX() + ""));
		pivotPoint.addAttrib(new XMLAttrib("y", classicMode.getTetrisGrid().getCurrentTetromino().getPivot().getY() + ""));
		fallingTetromino.addChild(pivotPoint);
		classicModeRoot.addChild(fallingTetromino);
		for (CellLocation location : classicMode.getTetrisGrid().getCurrentTetromino().getBlockLocations())
		{
			XMLElement blockDataElement = new XMLElement("blockdata", null);
			blockDataElement.addAttrib(new XMLAttrib("x", location.getX() + ""));
			blockDataElement.addAttrib(new XMLAttrib("y", location.getY() + ""));
			fallingTetromino.addChild(blockDataElement);
		}

		XMLElement grid = new XMLElement("grid", null);

		int width = classicMode.getTetrisGrid().getWidth();
		int height = classicMode.getTetrisGrid().getHeight();

		grid.addAttrib(new XMLAttrib("width", "" + width));
		grid.addAttrib(new XMLAttrib("height", "" + height));

		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				Cell cell = classicMode.getTetrisGrid().getCells()[x + (y * width)];
				XMLElement cellElement = new XMLElement("cell", null);
				cellElement.addAttrib(new XMLAttrib("color", ((ClassicCell) cell).getColor().toString()));
				cellElement.addAttrib(new XMLAttrib("x", "" + x));
				cellElement.addAttrib(new XMLAttrib("y", "" + y));
				cellElement.addAttrib(new XMLAttrib("empty", "" + (cell.isTetrisBlock() == false)));
				grid.addChild(cellElement);
			}
		}

		classicModeRoot.addChild(grid);

		xmlDoc.save();
	}

	/**
	 * Saves a current adventure mode state to memory
	 * 
	 * @param state
	 *            The AdventureModeState to save to memory
	 */
	public void saveAdventureMode(AdventureModeState state)
	{
		Core.getLog().info("Saving adventure mode...");
		List<XMLElement> worlds = xmlDoc.getRootElement().getChildByName("adventureMode").getChildren();
		worlds.clear();

		int index = 0;

		XMLElement charType = new XMLElement("characterType", Game.getInstance().getCurrentProfile().getCharacterType().toString());
		XMLElement level = new XMLElement("level", "" + Game.getInstance().getCurrentProfile().getLevel());
		XMLElement exp = new XMLElement("exp", "" + Game.getInstance().getCurrentProfile().getExperiance());
		worlds.add(charType);
		worlds.add(level);
		worlds.add(exp);

		for (int i = 0; i <= Game.getInstance().getCurrentProfile().getWorld(); i++)
		{
			XMLElement worldElement = new XMLElement("world", null);
			worldElement.addChild(new XMLElement("name", WorldNavigator.WORLD_FILE_NAMES[index++]));
			worlds.add(worldElement);

			for (MobileEntity mob : state.getWorlds().getWorld(i).getMobs())
			{
				if (!(mob instanceof Player))
				{
					Enemy e = (Enemy) mob;
					XMLElement entityElement = new XMLElement("entity", null);
					entityElement.addAttrib(new XMLAttrib("type", e.getClass().getName()));
					entityElement.addAttrib(new XMLAttrib("level", String.valueOf(e.getLevel())));
					entityElement.addAttrib(new XMLAttrib("x", String.valueOf(mob.getX())));
					entityElement.addAttrib(new XMLAttrib("y", String.valueOf(mob.getY())));
					worldElement.addChild(entityElement);
				}
			}

			if (state.getWorlds().getWorld(i).getWorldBoss() != null)
			{
				XMLElement bossElement = new XMLElement("boss", null);
				bossElement.addAttrib(new XMLAttrib("type", state.getWorlds().getWorld(i).getWorldBoss().getClass().getName()));
				bossElement.addAttrib(new XMLAttrib("level", String.valueOf(state.getWorlds().getWorld(i).getWorldBoss().getLevel())));
				bossElement.addAttrib(new XMLAttrib("x", String.valueOf(state.getWorlds().getWorld(i).getWorldBoss().getX())));
				bossElement.addAttrib(new XMLAttrib("y", String.valueOf(state.getWorlds().getWorld(i).getWorldBoss().getY())));
				worldElement.addChild(bossElement);
			}
		}
		xmlDoc.save();
	}
}
