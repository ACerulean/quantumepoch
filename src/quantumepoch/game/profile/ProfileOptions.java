package quantumepoch.game.profile;

/**
 * Data structure for storing options associated with a profile.
 */
public class ProfileOptions
{

	/** Audio Settings: master volume */
	private float masterVolume;

	/** Video/Display Settings: brightness */
	private float brightness;

	/* Graphics quality setting* */
	private int graphicsSetting;

	/**
	 * Core Settings: FPS limit
	 */
	private int fpsCap;

	/**
	 * Creates new profile options
	 */
	public ProfileOptions()
	{
		// defaults
		masterVolume = 0.2f;
		brightness = 0.2f;
		fpsCap = 60;
		graphicsSetting = 2;
	}

	/**
	 * @return the master volume
	 */
	public float getMasterVolume()
	{
		return masterVolume;
	}

	/**
	 * Sets the master volume
	 * 
	 * @param masterVolume
	 *            The new volume
	 */
	public void setMasterVolume(float masterVolume)
	{
		this.masterVolume = masterVolume;
	}
	
	/**
	 * @return the graphics quality setting
	 */
	public int getGraphicsSetting()
	{
		return graphicsSetting;
	}

	/**
	 * Sets the new graphics setting
	 * 
	 * @param gfx
	 *            The new graphics quality setting
	 */
	public void setGraphicsSetting(int gfx)
	{
		graphicsSetting = gfx;
	}

	/**
	 * @return the games brightness
	 */
	public float getBrightness()
	{
		return brightness;
	}

	/**
	 * Sets the brightness of the screen/game
	 * 
	 * @param brightness
	 *            The new brightness
	 */
	public void setBrightness(float brightness)
	{
		this.brightness = brightness;
	}

	/**
	 * @return the FPS limit/cap
	 */
	public int getFpsCap()
	{
		return fpsCap;
	}

	/**
	 * Sets the amount of frames your game can go up to
	 * 
	 * @param fpsCap
	 *            The new FPS limit/cap
	 */
	public void setFpsCap(int fpsCap)
	{
		this.fpsCap = fpsCap;
	}
}
