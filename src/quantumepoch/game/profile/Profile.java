package quantumepoch.game.profile;

import java.io.File;

import quantumepoch.game.CharacterType;

/**
 * Represents an in game profile. To save this profile, this class uses XML documents via the PlayerFile class
 */
public class Profile
{

	/** the name of this profile */
	private String name;
	/** the player file this profile will be saved to */
	private PlayerFile playerFile;
	/** the current world this profile is on */
	private int world;
	/** the type of character this profile has selected for adventure mode */
	private CharacterType characterType;
	/** the level of this profile */
	private int level;
	/** the experience of this profile */
	private int exp;
	/** the required exp for the next level */
	private int reqExp;

	private ProfileOptions options;

	/**
	 * Creates a new profile
	 */
	public Profile()
	{
		this(null, null, 0, 0, false);
	}

	/**
	 * Create a new profile with the specified arguments
	 * 
	 * @param name
	 *            The name of the profile
	 * @param savePath
	 *            The save path of this profile
	 * @param worldIndex
	 *            The index of the world that this profile is on
	 * @param level
	 *            The level of this player
	 * @param existing
	 *            Whether this profile already exists
	 */
	public Profile(String name, String savePath, int worldIndex, int level, boolean existing)
	{
		this.name = name;
		this.playerFile = new PlayerFile(new File(savePath), this, existing);
		this.world = worldIndex;
		this.level = level;
		options = new ProfileOptions();

		setLevel(0);
		// set character type
		if (playerFile.getXmlDoc().getRootElement().getChildByName("adventureMode").getChildByName("characterType") != null)
			for (CharacterType type : CharacterType.values())
				if (type.toString().equalsIgnoreCase(playerFile.getXmlDoc().getRootElement().getChildByName("adventureMode").getChildByName("characterType").getValue()))
					setCharacterType(type);
		if (playerFile.getXmlDoc().getRootElement().getChildByName("adventureMode").getChildByName("level") != null)
			setLevel(Integer.parseInt(playerFile.getXmlDoc().getRootElement().getChildByName("adventureMode").getChildByName("level").getValue()));
		if (playerFile.getXmlDoc().getRootElement().getChildByName("adventureMode").getChildByName("exp") != null)
			setExperiance(Integer.parseInt(playerFile.getXmlDoc().getRootElement().getChildByName("adventureMode").getChildByName("exp").getValue()));

	}

	/**
	 * @return the name of this profile
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @return the player file save of this profile
	 */
	public PlayerFile getPlayerFile()
	{
		return playerFile;
	}

	public ProfileOptions getProfileOptions()
	{
		return options;
	}

	/**
	 * @return the save path of this profile
	 */
	public String getSavePath()
	{
		return playerFile.getXmlDoc().getFile().getAbsolutePath();
	}

	/**
	 * @return the world index of this profile
	 */
	public int getWorld()
	{
		return world;
	}

	/**
	 * @return the character type of this profile
	 */
	public CharacterType getCharacterType()
	{
		return characterType;
	}

	/**
	 * @return the level of this profile
	 */
	public int getLevel()
	{
		return level;
	}

	/**
	 * @return the experiance of this profile
	 */
	public int getExperiance()
	{
		return exp;
	}

	/**
	 * @return the required exp for the next level
	 */
	public float getRequiredExperiance()
	{
		return reqExp;
	}

	/**
	 * Sets the name of this profile.
	 * 
	 * @param name
	 *            The new name of this profile
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * Sets the player file
	 * 
	 * @param playerFile
	 *            The new PlayerFile
	 */
	public void setPlayerFile(PlayerFile playerFile)
	{
		this.playerFile = playerFile;
	}

	/**
	 * Sets the current world of this profile
	 * 
	 * @param world
	 *            The current world index
	 */
	public void setWorld(int world)
	{
		this.world = world;
	}

	/**
	 * Sets the character type of this profile
	 * 
	 * @param characterType
	 *            The new character type of this profile
	 */
	public void setCharacterType(CharacterType characterType)
	{
		this.characterType = characterType;
	}

	/**
	 * Sets the level of this profile
	 * 
	 * @param level
	 *            The new level of this profile
	 */
	public void setLevel(int level)
	{
		this.level = level;
		this.reqExp = 10 * level + 10;
	}

	/**
	 * Sets the experience of this profile and updates the level if experience exceeds.
	 * 
	 * @param experiance
	 *            The new experience of this profile.
	 */
	public void setExperiance(int experiance)
	{
		this.exp = experiance;

		while (exp >= reqExp)
		{
			this.exp -= reqExp;
			setLevel(level + 1);
		}
	}

}
