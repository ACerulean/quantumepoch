package quantumepoch.game;

import quantumepoch.utils.Delay;

/**
 * Utility class provided to time tetris games.
 */
public class Timer
{

	/** hours count */
	private int hours;
	/** minutes count */
	private int minutes;
	/** seconds count */
	private int seconds;
	/** the delay until we increment the time */
	private Delay countUp;
	/** the increment amount */
	private int increment;

	/**
	 * Creates a new timer
	 * 
	 * @param increment
	 *            The amount to increment by
	 */
	public Timer(int increment)
	{
		this(0, 0, 0, increment);
	}

	/**
	 * Creates a new timer
	 * 
	 * @param startSeconds
	 *            The seconds to start at
	 * @param startMinutes
	 *            The minutes to start at
	 * @param startHours
	 *            The hours to start at
	 * @param increment
	 *            The increment amount
	 */
	public Timer(int startSeconds, int startMinutes, int startHours, int increment)
	{
		this.seconds = startSeconds;
		this.minutes = startMinutes;
		this.hours = startHours;
		this.countUp = new Delay(1000);
		this.increment = increment;
	}

	/**
	 * @return elapsed hours
	 */
	public int getHours()
	{
		return hours;
	}

	/**
	 * @return elapsed minutes
	 */
	public int getMinutes()
	{
		return minutes;
	}

	/**
	 * @return Elapsed seconds
	 */
	public int getSeconds()
	{
		return seconds;
	}

	/**
	 * Sets the amount of hours
	 * 
	 * @param hours
	 *            The new amount of hours
	 */
	public void setHours(int hours)
	{
		this.hours = hours;
	}

	/**
	 * Sets the amount of minutesD
	 * 
	 * @param minutes
	 *            The new amount of minutes
	 */
	public void setMinutes(int minutes)
	{
		this.minutes = minutes;
	}

	/**
	 * Sets the amount of seconds
	 * 
	 * @param seconds
	 *            The new amount of seconds
	 */
	public void setSeconds(int seconds)
	{
		this.seconds = seconds;
	}

	/**
	 * Updates this timer
	 */
	public void update()
	{
		if (countUp.isReady())
		{
			seconds += increment;
		}

		if (seconds >= 60 || seconds < 0)
		{
			minutes += increment > 0 ? (1) : (-1);
			seconds = increment > 0 ? 0 : 59;
		}

		if (minutes >= 60)
		{
			hours += increment > 0 ? 1 : -1;
			minutes = increment > 0 ? 0 : 60;
		}
	}

	
	private String fixTime(int time)
	{
		if(time < 10) return "0" + time;
		return ""+time;
	}
	
	/**
	 * @return a string representation of this timer
	 */
	@Override
	public String toString()
	{
		return fixTime(hours) + ":" + fixTime(minutes) + ":" + fixTime(seconds);
	}

}
