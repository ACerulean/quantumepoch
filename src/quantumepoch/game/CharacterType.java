package quantumepoch.game;

/**
 * Stores the possible types of characters for adventure mode.
 */
public enum CharacterType
{

	/** the knight character */
	KNIGHT,
	/** the wizard character */
	WIZARD, 
	/** the archer character */
	ARCHER;

}
