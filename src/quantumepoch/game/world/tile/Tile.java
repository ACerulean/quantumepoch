package quantumepoch.game.world.tile;

import java.util.ArrayList;
import java.util.List;

import quantumepoch.game.Game;
import quantumepoch.game.Textures;
import quantumepoch.game.world.World;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * A tile is a solid, rectangular, axis oriented and static piece of geometry that is used for decor and collision boxes throughout worlds.
 */
public class Tile
{

	public static final TextureRegion GRASS = new TextureRegion(Textures.TILES.getTexture(), 1 * 32, 7 * 32, Tile.SIZE, Tile.SIZE);
	public static final TextureRegion STONE = new TextureRegion(Textures.TILES.getTexture(), 5 * 32, 7 * 32, Tile.SIZE, Tile.SIZE);
	public static final TextureRegion STONE_DARK = new TextureRegion(Textures.TILES.getTexture(), 6 * 32, 7 * 32, Tile.SIZE, Tile.SIZE);
	public static final TextureRegion SAND = new TextureRegion(Textures.TILES.getTexture(), 3 * 32, 7 * 32, Tile.SIZE, Tile.SIZE);
	public static final TextureRegion WOOD = new TextureRegion(Textures.TILES.getTexture(), 2 * 32, 6 * 32, Tile.SIZE, Tile.SIZE);
	public static final TextureRegion SNOW = new TextureRegion(Textures.TILES.getTexture(), 1 * 32, 6 * 32, Tile.SIZE, Tile.SIZE);
	public static final TextureRegion CLOUD = new TextureRegion(Textures.TILES.getTexture(), 0 * 32, 6 * 32, Tile.SIZE, Tile.SIZE);
	public static final TextureRegion METAL = new TextureRegion(Textures.TILES.getTexture(), 0 * 32, 5 * 32, Tile.SIZE, Tile.SIZE);
	public static final TextureRegion METAL_WALL = new TextureRegion(Textures.TILES.getTexture(), 1 * 32, 5 * 32, Tile.SIZE, Tile.SIZE);
	public static final TextureRegion SANDSTONE = new TextureRegion(Textures.TILES.getTexture(), 4 * 32, 7 * 32, Tile.SIZE, Tile.SIZE);

	/** the global size in width and height of tiles, in pixels */
	public static final int SIZE = 32;
	/** the texture of the tile */
	private TextureRegion texture;
	/** the color of the tile */
	private Color tint;
	/** whether this tile is solid; a solid tile cannot be walked through */
	private boolean solid;
	/** the world this tile is contained within */
	private World world;

	/**
	 * Creates a new Tile
	 * 
	 * @param world
	 * @param tint
	 */
	public Tile(World world, Color tint)
	{
		this(world, null, tint);
	}

	/**
	 * Creates a new Tile
	 * 
	 * @param world
	 * @param texture
	 */
	public Tile(World world, TextureRegion texture)
	{
		this(world, texture, Color.WHITE);
	}

	/**
	 * Creates a new Tile
	 * 
	 * @param world
	 * @param texture
	 * @param solid
	 *            whether this tile is solid; a solid tile cannot be walked through
	 */
	public Tile(World world, TextureRegion texture, boolean solid)
	{
		this(world, texture, Color.WHITE);
		this.solid = solid;
	}

	/**
	 * Creates a new tile
	 * 
	 * @param world
	 * @param texture
	 * @param tint
	 */
	public Tile(World world, TextureRegion texture, Color tint)
	{
		this.texture = texture;
		this.tint = tint;
		this.world = world;
	}

	public TextureRegion getTexture()
	{
		return texture;
	}

	public Color getTint()
	{
		return tint;
	}

	public boolean isSolid()
	{
		return solid;
	}

	public World getWorld()
	{
		return world;
	}

	public void setTint(Color tint)
	{
		this.tint = tint;
	}

	public void setTexture(TextureRegion texture)
	{
		this.texture = texture;
	}

	/**
	 * Sets whether this tile is solid, which determines if it can be walked through.
	 * 
	 * @param solid
	 *            Whether this tile is solid
	 */
	public void setSolid(boolean solid)
	{
		this.solid = solid;
	}

	/**
	 * Draws this tile at the specified [x,y] position
	 * 
	 * @param x
	 * @param y
	 */
	public void render(int x, int y)
	{
		if (texture == null)
		{
			Game.getInstance().getShapeBatch().setColor(tint);
			Game.getInstance().getShapeBatch().renderFilledRect(x, y, SIZE, SIZE);
		} else
		{
			Game.getInstance().getSpriteBatch().setTint(tint);
			Game.getInstance().getSpriteBatch().render(texture, x, y);
		}
	}

	/**
	 * @param world
	 * @param x
	 * @param y
	 * @return The tile that is most abundent around this tile
	 */
	public static Tile mostAbundentAdjacentTile(World world, int x, int y)
	{
		List<Tile> tiles = new ArrayList<Tile>();

		for (int i = -1; i <= 1; i++)
		{
			for (int j = -1; j <= 1; j++)
			{
				if (!(j == 0 && i == 0) && (x + i) >= 0 && (y + j) >= 0 && (x + i) < world.getWidth() && (y + j) < world.getHeight())
				{
					int xx = (x + i) * Tile.SIZE;
					int yy = (y + j) * Tile.SIZE;
					tiles.add(world.getTileAt(xx, yy));
				}
			}
		}

		List<Tile> iterated = new ArrayList<Tile>();
		Tile abun = null;
		int abunCount = 0;
		for (int i = 0; i < tiles.size(); i++)
		{
			if (iterated.contains(tiles.get(i)))
				continue;

			int count = 0;
			for (Tile t : tiles)
				if (t != null)
					if (tiles.get(i) != null && t.getClass().getName().equals(tiles.get(i).getClass().getName()))
						count++;

			if (count > abunCount)
			{
				abun = tiles.get(i);
				abunCount = count;
			}

			iterated.add(tiles.get(i));
		}

		return abun;
	}
}
