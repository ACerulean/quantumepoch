package quantumepoch.game.world.tile;

import quantumepoch.game.world.World;

public class TileSand extends Tile
{

	public TileSand(World world, boolean solid)
	{
		super(world, SAND, solid);
	}

}
