package quantumepoch.game.world.tile;

import quantumepoch.game.world.World;

public class TileMetalWall extends Tile
{

	public TileMetalWall(World world)
	{
		super(world, METAL_WALL);
		setSolid(true);
	}

}
