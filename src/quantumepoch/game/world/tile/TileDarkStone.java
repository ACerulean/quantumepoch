package quantumepoch.game.world.tile;

import quantumepoch.game.world.World;

public class TileDarkStone extends Tile
{

	public TileDarkStone(World world)
	{
		super(world, STONE_DARK);
	}

}
