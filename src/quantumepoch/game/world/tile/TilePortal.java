package quantumepoch.game.world.tile;

import quantumepoch.game.Game;
import quantumepoch.game.Textures;
import quantumepoch.game.world.World;
import quantumepoch.game.world.entity.Entity;
import quantumepoch.game.world.entity.particle.ParticleEmitter;
import quantumepoch.graphics.animation.TextureAnimation;
import quantumepoch.utils.Delay;
import quantumepoch.utils.math.Vector2f;

public class TilePortal extends AnimatedTile
{

	public static final TextureAnimation PORTAL = new TextureAnimation(Textures.PORTAL.getTexture(), 32, 32, 0, 0, 32 * 5, 32, 50, new int[] { 0, 1, 2, 3, 4, 3, 2, 1 });

	private Vector2f tp;
	private ParticleEmitter emitter;

	public TilePortal(World world, Vector2f tp, float x, float y)
	{
		super(world, PORTAL);
		this.tp = tp;
		emitter = new ParticleEmitter(world, new Vector2f(x, y), 3, 1, new Delay(500));
	}

	public void tp(Entity e)
	{
		e.setX(tp.getX());
		e.setY(tp.getY());
	}

	public Vector2f getTp()
	{
		return tp;
	}

	public void setTp(Vector2f tp)
	{
		this.tp = tp;
	}

	@Override
	public void update()
	{
		super.update();
		emitter.update();
	}

	@Override
	public void render(int x, int y)
	{
		Tile abun = mostAbundentAdjacentTile(getWorld(), (x - getWorld().getTranslateX()) / Tile.SIZE, (y - getWorld().getTranslateY()) / Tile.SIZE);
		Game.getInstance().getSpriteBatch().render(abun.getTexture(), x, y);
		super.render(x, y);
	}

}
