package quantumepoch.game.world.tile;

import quantumepoch.game.world.World;

public class TileSnow extends Tile
{

	public TileSnow(World world)
	{
		super(world, SNOW);
	}

}
