package quantumepoch.game.world.tile;

import quantumepoch.game.world.World;
import quantumepoch.graphics.animation.TextureAnimation;

/**
 * A tile that can be animated with a texture animation
 */
public class AnimatedTile extends Tile
{

	/** the animation of this tile */
	private TextureAnimation anim;

	/**
	 * Createas a new animated tile
	 * 
	 * @param world
	 * @param anim
	 */
	public AnimatedTile(World world, TextureAnimation anim)
	{
		super(world, anim.getCurrentFrame().getTexture());
		this.anim = anim;
		anim.start();
	}

	/**
	 * Updates this tile's animation
	 */
	public void update()
	{
		anim.update();
		setTexture(anim.getCurrentFrame().getTexture());
	}

}
