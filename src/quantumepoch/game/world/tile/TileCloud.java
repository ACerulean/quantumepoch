package quantumepoch.game.world.tile;

import quantumepoch.game.world.World;

public class TileCloud extends Tile
{

	public TileCloud(World world)
	{
		super(world, CLOUD);
	}

}
