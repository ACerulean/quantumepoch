package quantumepoch.game.world.tile;

import quantumepoch.game.Textures;
import quantumepoch.game.world.World;
import quantumepoch.graphics.animation.TextureAnimation;

public class TileLava extends AnimatedTile
{

	private static TextureAnimation staticAnim = new TextureAnimation(Textures.WATER_LAVA.getTexture(), 32, 32, 0, 32, 32 * 3, 32, 500, new int[] { 0, 1, 2, 2, 1 });

	public TileLava(World world)
	{
		super(world, staticAnim);
	}

}
