package quantumepoch.game.world.tile;

import quantumepoch.game.world.World;

public class TileMetal extends Tile
{

	public TileMetal(World world)
	{
		super(world, METAL);
	}

}
