package quantumepoch.game.world.tile;

import quantumepoch.game.world.World;

public class TileGrass extends Tile
{

	public TileGrass(World world, boolean solid)
	{
		super(world, GRASS, solid);
	}

}
