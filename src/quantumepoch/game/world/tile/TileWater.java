package quantumepoch.game.world.tile;

import quantumepoch.game.Textures;
import quantumepoch.game.world.World;
import quantumepoch.graphics.animation.TextureAnimation;

public class TileWater extends AnimatedTile
{

	private static TextureAnimation staticAnim = new TextureAnimation(Textures.WATER_LAVA.getTexture(), 32, 32, 0, 0, 32 * 4, 32, 500, new int[] { 0, 1, 2, 3, 2, 1 });

	public TileWater(World world)
	{
		super(world, staticAnim);
		setSolid(false);
	}

}
