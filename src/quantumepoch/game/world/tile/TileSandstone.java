package quantumepoch.game.world.tile;

import quantumepoch.game.world.World;

public class TileSandstone extends Tile
{

	public TileSandstone(World world)
	{
		super(world, SANDSTONE);
		setSolid(true);
	}

}
