package quantumepoch.game.world.tile;

import quantumepoch.game.world.World;

public class TileWood extends Tile
{

	public TileWood(World world)
	{
		super(world, WOOD, false);
	}

}
