package quantumepoch.game.world.tile;

import quantumepoch.game.world.World;

public class TileStone extends Tile
{

	public TileStone(World world)
	{
		super(world, STONE);
		setSolid(true);
	}

}
