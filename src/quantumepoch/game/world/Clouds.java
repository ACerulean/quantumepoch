package quantumepoch.game.world;


import quantumepoch.game.Textures;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * enumeration of cloud textures and dimensions
 *
 */
public enum Clouds
{

	CLOUD_1(new TextureRegion(Textures.CLOUDS.getTexture(), 0, 0, 128, 64)),
	CLOUD_2(new TextureRegion(Textures.CLOUDS.getTexture(), 0, 64, 128, 64)),
	CLOUD_3(new TextureRegion(Textures.CLOUDS.getTexture(), 128, 0, 128, 64)),
	CLOUD_4(new TextureRegion(Textures.CLOUDS.getTexture(), 128, 64, 128, 64)),
	CLOUD_5(new TextureRegion(Textures.CLOUDS.getTexture(), 128*2, 0, 128, 64)),
	CLOUD_6(new TextureRegion(Textures.CLOUDS.getTexture(), 128*2, 64, 128, 64));

	private TextureRegion region;

	/**
	 * constructor for cloud enum
	 * 
	 * @param r region of clouds
	 */
	Clouds(TextureRegion r)
	{
		region = r;
	}

	/**
	 * gets the texture for the clouds region
	 * 
	 * @return returns texture region for cloud
	 */
	public TextureRegion getTexture()
	{
		return region;
	}

}
