package quantumepoch.game.world;

/**
 * Represents a world with a specific index in an enum array. This is used to store the worlds in the order they should occur in game play.
 */
public enum IndexedWorld
{

	/** The plains world */
	PLAINS_WORLD("Plains World"), 
	/** The forest world */
	FOREST_WORLD("Forest World"),
	/** The cave world */
	CAVE_WORLD("Cave World"),
	/** The desert world */
	DESERT_WORLD("Desert World"),
	/** The ocean world */
	OCEAN_WORLD("Ocean World"),
	/** The tundra world */
	TUNDRA_WORLD("Tundra World"),
	/** The sky world */
	SKY_WORLD("Sky World"),
	/** The space world */
	SPACE_WORLD("Space World");

	/** the name of the world */
	private String name;

	/**
	 * Creates a new indexed world with the specified name
	 * @param name The name of the world
	 */
	IndexedWorld(String name)
	{
		this.name = name;
	}

	/**
	 * @return the game of this world
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param index The index of the world to retrieve.
	 * @return The world with the specified index
	 */
	public static IndexedWorld getWorld(int index)
	{
		return values()[index];
	}

	/**
	 * Gets a world index with the specified name.
	 * @param name The name of the world to find the index of
	 * @return the index of the world with the specified name, or null if not found
	 */
	public static int getWorldIndex(String name)
	{
		for (int i = 0; i < values().length; i++)
			if (values()[i].getName().equalsIgnoreCase(name))
				return i;
		return -1;
	}
}
