package quantumepoch.game.world;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import quantumepoch.core.Core;
import quantumepoch.game.Game;
import quantumepoch.game.Textures;
import quantumepoch.game.world.entity.imment.Cactus;
import quantumepoch.game.world.entity.imment.Tree;
import quantumepoch.game.world.entity.mob.Slime;
import quantumepoch.game.world.entity.mob.SlimeBoss;
import quantumepoch.game.world.entity.mob.Square;
import quantumepoch.game.world.entity.mob.Stump;
import quantumepoch.game.world.tile.Tile;
import quantumepoch.game.world.tile.TileCloud;
import quantumepoch.game.world.tile.TileDarkStone;
import quantumepoch.game.world.tile.TileGrass;
import quantumepoch.game.world.tile.TileLava;
import quantumepoch.game.world.tile.TileMetal;
import quantumepoch.game.world.tile.TileMetalWall;
import quantumepoch.game.world.tile.TilePortal;
import quantumepoch.game.world.tile.TileSand;
import quantumepoch.game.world.tile.TileSandstone;
import quantumepoch.game.world.tile.TileSnow;
import quantumepoch.game.world.tile.TileStone;
import quantumepoch.game.world.tile.TileWater;
import quantumepoch.game.world.tile.TileWood;
import quantumepoch.game.world.tutorial.TutorialWorld;
import quantumepoch.graphics.Bitmap;
import quantumepoch.graphics.texture.TextureRegion;
import quantumepoch.utils.math.Vector2f;

/**
 * Class for saving the world data and all the objects and entities spawned in world, along with player and player progress.
 *
 */
public class WorldSave
{

	private String root;

	/**
	 * constructor for world save that sets path for save
	 * 
	 * @param dirPath
	 *            path for save
	 */
	public WorldSave(String dirPath)
	{
		root = dirPath;
	}

	/**
	 * gets the root for world save
	 * 
	 * @return returns root
	 */
	public String getRoot()
	{
		return root;
	}

	/**
	 * creates the world save of all the entities and bitmaps and player data for future saves and loads
	 * 
	 * @return returns save of world as object
	 */
	public World createWorld(boolean next, boolean generateEnemies)
	{
		World result = null;

		try
		{
			Properties meta = new Properties();
			Core.getLog().info("Loading world from image at " + root);
			meta.load(WorldSave.class.getResourceAsStream(root + "/meta.dat"));
			Bitmap raster = new Bitmap(root + "/tiles.png");
			Bitmap entities = new Bitmap(root + "/EntityMap.png");

			Vector2f spawn = null;
			if (meta.getProperty("spawn") != null)
			{
				spawn = new Vector2f(Float.parseFloat(meta.getProperty("spawn").split(",")[0]) * Tile.SIZE, (raster.getHeight() * Tile.SIZE) - (Float.parseFloat(meta.getProperty("spawn").split(",")[1]) * Tile.SIZE));
			}

			if (meta.getProperty("name").equalsIgnoreCase("Tutorial World"))
			{
				result = new TutorialWorld(meta.getProperty("name"), spawn, raster, Integer.parseInt(meta.getProperty("width")), Integer.parseInt(meta.getProperty("height")), next);
			} else
			{
				result = new World(meta.getProperty("name"), spawn, raster, Integer.parseInt(meta.getProperty("width")), Integer.parseInt(meta.getProperty("height")), next);
			}

			int worldWidth = result.getWidth();
			int worldHeight = result.getHeight();

			for (int x = 0; x < worldWidth; x++)
			{
				for (int y = 0; y < worldHeight; y++)
				{
					int color = raster.getPixel(x, y) | 0xFF000000;
					Tile t = null;

					if (color == 0xFF00FF00)
						t = new TileGrass(result, false);

					if (color == 0xFF0000FF)
						t = new TileWater(result);

					if (color == 0xFFFFFF00)
						t = new TileSand(result, false);

					if (color == 0xFF808080)
						t = new TileStone(result);

					if (color == 0xFFFF8040)
						t = new TileWood(result);

					if (color == 0xFF8000FF)
					{
						// parse portal
						String key = "tp," + x + "," + (result.getHeight() - y - 1);
						String val = meta.getProperty(key);

						int xx = 0;
						int yy = 0;
						if (val != null)
						{
							xx = Integer.parseInt(val.split(",")[0]);
							yy = result.getHeight() - Integer.parseInt(val.split(",")[1]) - 1;
						}

						t = new TilePortal(result, new Vector2f(xx * Tile.SIZE, yy * Tile.SIZE), x * Tile.SIZE + 16, y * Tile.SIZE + 16);
					}

					if (color == 0xFFFF8000)
						t = new TileLava(result);

					if (color == 0xFF404040)
						t = new TileDarkStone(result);

					if (color == 0xFF008000)
					{
						t = new TileGrass(result, false);
						result.getImmobileEntities().add(new Tree(result, x * Tile.SIZE, y * Tile.SIZE));
					}

					if (color == 0xFF408000)
					{
						t = new TileSand(result, false);
						result.getImmobileEntities().add(new Cactus(result, x * Tile.SIZE, y * Tile.SIZE));
					}

					if (color == 0xFFFFFFFF)
						t = new TileSnow(result);

					if (color == 0xFFFEFEFE)
						t = new TileCloud(result);

					if (color == 0xFFA0A0A0)
						t = new TileMetal(result);

					if (color == 0xFFC0C0C0)
						t = new TileMetalWall(result);

					if (color == 0xFFC8C880)
						t = new TileSandstone(result);

					// other tile checks go here

					if (t != null)
					{
						result.getTiles()[x + y * worldWidth] = t;
					}
				}
			}

			if (generateEnemies)
				for (int x = 0; x < worldWidth; x++)
				{
					for (int y = 0; y < worldHeight; y++)
					{
						int color = entities.getPixel(x, y) | 0xFF000000;
						if (color == 0xFFFF0000)
						{
							int level = (int) (Math.random() * 6);
							int type = (int) (Math.random() * 3);
							if (type == 0)
								result.getMobs().add(new Square(result, x * Tile.SIZE, y * Tile.SIZE, new TextureRegion(Textures.ENEMIES.getTexture()), level));
							else if (type == 1)
								result.getMobs().add(new Slime(result, x * Tile.SIZE, y * Tile.SIZE, new TextureRegion(Textures.ENEMIES.getTexture()), level));
							else if (type == 2)
								result.getMobs().add(new Stump(result, x * Tile.SIZE, y * Tile.SIZE, new TextureRegion(Textures.ENEMIES.getTexture()), level));
						} else if (color == 0xFF800000)
						{
							int level = 8;
							result.setWorldBoss(new SlimeBoss(result, x * Tile.SIZE, y * Tile.SIZE, new TextureRegion(Textures.ENEMIES.getTexture(), 0, 0, 32, 32), level));
						}
					}
				}
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}

		return result;
	}
}
