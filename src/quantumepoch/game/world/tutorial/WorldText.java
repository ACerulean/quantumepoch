package quantumepoch.game.world.tutorial;

import quantumepoch.game.Game;
import quantumepoch.graphics.font.TrueTypeFont;
import quantumepoch.utils.math.Vector2f;

public class WorldText
{

	private String text;
	private Vector2f position;

	public WorldText(String text, Vector2f position)
	{
		this.text = text;
		this.position = position;
	}

	public String getText()
	{
		return text;
	}

	public void setText(String text)
	{
		this.text = text;
	}

	public Vector2f getPosition()
	{
		return position;
	}

	public void setPosition(Vector2f position)
	{
		this.position = position;
	}

	public void render(TrueTypeFont font, int tx, int ty)
	{
		String[] lines = text.split("\n");
		int curY = (int) position.getY();
		for (String x : lines)
		{
			Game.getInstance().getFontRenderer().render(font, x, position.getX() + tx, curY + ty - font.getCharacterHeight());
			curY -= font.getCharacterHeight();
		}
	}

}
