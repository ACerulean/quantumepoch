package quantumepoch.game.world.tutorial;

import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import quantumepoch.game.Game;
import quantumepoch.game.world.World;
import quantumepoch.game.world.tile.Tile;
import quantumepoch.graphics.Bitmap;
import quantumepoch.graphics.font.FontType;
import quantumepoch.graphics.font.TrueTypeFont;
import quantumepoch.utils.math.Vector2f;

public class TutorialWorld extends World
{

	private TrueTypeFont font;

	private List<WorldText> worldText;

	public TutorialWorld(String name, Vector2f spawn, Bitmap minimap, int width, int height, boolean next)
	{
		super(name, spawn, minimap, width, height, next);
		worldText = new ArrayList<WorldText>();

		worldText.add(new WorldText("Home World", new Vector2f(2 * Tile.SIZE, -10 * Tile.SIZE)));
		worldText.add(new WorldText("Basic Controls:", new Vector2f(2 * Tile.SIZE, -17 * Tile.SIZE)));
		worldText.add(new WorldText("Use the standard WASD keys to move \nup, down left and right.", new Vector2f(2 * Tile.SIZE, -18 * Tile.SIZE)));
		worldText.add(new WorldText("Hold down space to sprint", new Vector2f(2 * Tile.SIZE, -20 * Tile.SIZE)));
		worldText.add(new WorldText("The tetris universe has been turned dark by \nevil forces and it is your job to stop it. You \nwill face many challenges ahead. This world \nis intended to start your journey to restore the \nuniverse. Follow the path and you will gain the \ninformation you need to succeed.", new Vector2f(13 * Tile.SIZE, -27 * Tile.SIZE)));
		worldText
				.add(new WorldText(
						"There are multiple worlds you will need to \nbeat in order to progress. If you look to the \nright of the screen, you will see multiple world \nimages. These are the worlds you must go \nthrough in order to restore light to this universe. \nAny world that is not darked out you may travel \nto by left clicking on it. The next world is \nalready unlocked for you. Try clicking on it \nnow, and then come back. There is also a \nconvieniant minimap in the bottom \n left, showing your position.",
						new Vector2f(46 * Tile.SIZE, -29 * Tile.SIZE)));
		worldText.add(new WorldText("You must progress through worlds by \nbeating all enemies in the world. The closest \nenemy to your position can be found by going\ntowards the arrow pointing away from your \nplayer. To engage an enemy, simply approach \nit until you are in the circle of influence, then \npress enter. Once all enemies have been \ndefeated, the next world opens up. For more \ninformation on battling, read the game manual.", new Vector2f(91 * Tile.SIZE, -25 * Tile.SIZE)));
		font = new TrueTypeFont(new FontType("Arial"), 20, true);
		font.loadFont();
	}

	@Override
	public void render()
	{
		renderTimeDarkness();
		Game.getInstance().getSpriteBatch().reset();
		renderTiles();
		Game.getInstance().getSpriteBatch().reset();
		for (WorldText text : worldText)
		{
			text.render(font, getTranslateX(), (getHeight() * Tile.SIZE) + getTranslateY());
		}
		renderEntities();
		renderPauseMenu();
		renderMiniMap();
		findPlayer().renderInterface();
	}

}
