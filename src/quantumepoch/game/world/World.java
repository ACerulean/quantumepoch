package quantumepoch.game.world;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;

import quantumepoch.audio.AudioBuffer;
import quantumepoch.audio.AudioSource;
import quantumepoch.core.Core;
import quantumepoch.game.Game;
import quantumepoch.game.Sounds;
import quantumepoch.game.Textures;
import quantumepoch.game.gui.MenuWindow;
import quantumepoch.game.tetrisbattle.BattleStats;
import quantumepoch.game.tetrisbattle.TetrisBattle;
import quantumepoch.game.world.entity.Entity;
import quantumepoch.game.world.entity.imment.ImmobileEntity;
import quantumepoch.game.world.entity.mob.Boss;
import quantumepoch.game.world.entity.mob.MobileEntity;
import quantumepoch.game.world.entity.mob.Player;
import quantumepoch.game.world.entity.mob.Slime;
import quantumepoch.game.world.entity.mob.WorldNavigator;
import quantumepoch.game.world.entity.particle.FireworkParticle;
import quantumepoch.game.world.entity.particle.Particle;
import quantumepoch.game.world.entity.particle.TextParticle;
import quantumepoch.game.world.entity.projectile.Projectile;
import quantumepoch.game.world.tile.AnimatedTile;
import quantumepoch.game.world.tile.Tile;
import quantumepoch.graphics.Bitmap;
import quantumepoch.graphics.BlendFunction;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.texture.Texture;
import quantumepoch.graphics.texture.TextureRegion;
import quantumepoch.physics.BoundingBox2D;
import quantumepoch.utils.Delay;
import quantumepoch.utils.math.Vector2f;

/**
 * Main class for world containing all the different generations for the different aspects of the world, including things like mobs, dimensions, spawn, projectiles and etc. Also contains mostly return and get commands for the parts of the world.
 */
public class World
{

	// world data
	private String name;
	private int width;
	private int height;
	private int translateX;
	private int translateY;
	private Tile[] tiles;
	private TetrisBattle tetrisBattle;
	private BattleStats battleStats;
	private List<MobileEntity> mobs;
	private List<Projectile> projectiles;
	private List<Particle> particles;
	private List<ImmobileEntity> immobileEntities;
	private Texture miniMap;
	private Vector2f spawn;

	private MenuWindow menuWindow;
	private boolean paused;

	// fireworks
	private Delay normalFireworkDelay = new Delay(500);
	private final int normalMaxFireworks = 3;
	private final int winningMaxFireworks = 10;
	private int normalFireworkCount = -1;
	private int winningFireworkCount = -1;

	// ambient lighting(time goes from 0 - 1)
	private float time = 0.2f;
	private boolean timeEnabled;

	// loading
	private boolean loadedNext;

	// world audio
	private static AudioSource fireworksSource, bossTheme, theme1, theme2, theme3;

	private TextureRegion mapBorderOverlay;

	/** the boss of this world */
	private Boss worldBoss;
	private boolean bossDeafeated;

	private boolean fade;
	private float creditsFade = 1.0f;

	/**
	 * constructor for only name, and dimensions.
	 * 
	 * @param name
	 *            name of world
	 * @param width
	 *            width of world
	 * @param height
	 *            height of the world
	 */
	public World(String name, int width, int height, boolean next)
	{
		this(name, new Vector2f(0, 0), null, width, height, next);
	}

	/**
	 * constructor for world for dimensions, setting spawn, and setting minimap
	 * 
	 * @param name
	 *            name of world
	 * @param spawn
	 *            spawn point for world to be set
	 * @param minimap
	 *            minimap of world taken from bitmap
	 * @param width
	 *            width of world
	 * @param height
	 *            height of world
	 */
	public World(String name, Vector2f spawn, Bitmap minimap, int width, int height, boolean next)
	{
		name = name.replaceAll("\"", "");
		this.name = name;
		this.width = width;
		this.height = height;
		tiles = new Tile[width * height];
		mobs = new ArrayList<MobileEntity>();
		projectiles = new ArrayList<Projectile>();
		particles = new ArrayList<Particle>();
		immobileEntities = new ArrayList<ImmobileEntity>();

		loadedNext = next;

		battleStats = new BattleStats();

		if (minimap != null)
		{
			// special circumstances for certain worlds
			if (name.equals("Ocean World"))
			{
				minimap = new Bitmap(minimap).replaceColorWithTransparency(0xFF0000FF);
			} else if (name.equals("Cave World"))
			{

			} else if (name.equals("Space World"))
			{
				minimap = new Bitmap(minimap).replaceColorWithTransparency(0xFF000000);
			}

			// switch this to per world
			timeEnabled = true;

			this.miniMap = new Texture(minimap).load();
		}

		this.spawn = spawn;

		fireworksSource = new AudioSource(new AudioBuffer(Sounds.FIREWORKS.getTrack().getFormat(), Sounds.FIREWORKS.getTrack().getData(), Sounds.FIREWORKS.getTrack().getSampleRate()));
		if (bossTheme == null)
			bossTheme = new AudioSource(new AudioBuffer(Sounds.BOSS_THEME.getTrack().getFormat(), Sounds.BOSS_THEME.getTrack().getData(), Sounds.BOSS_THEME.getTrack().getSampleRate()));
		if (theme1 == null)
			theme1 = new AudioSource(new AudioBuffer(Sounds.BACK_ALLY_BEATS.getTrack().getFormat(), Sounds.BACK_ALLY_BEATS.getTrack().getData(), Sounds.BACK_ALLY_BEATS.getTrack().getSampleRate()));
		if (theme2 == null)
			theme2 = new AudioSource(new AudioBuffer(Sounds.BENEATH_THE_SEWERS.getTrack().getFormat(), Sounds.BENEATH_THE_SEWERS.getTrack().getData(), Sounds.BENEATH_THE_SEWERS.getTrack().getSampleRate()));
		if (theme3 == null)
			theme3 = new AudioSource(new AudioBuffer(Sounds.SPACE_TIME_THEME.getTrack().getFormat(), Sounds.SPACE_TIME_THEME.getTrack().getData(), Sounds.SPACE_TIME_THEME.getTrack().getSampleRate()));

		playTheme();

		mapBorderOverlay = new TextureRegion(Textures.MINIMAP_BORDER.getTexture(), 0, 0, 256, 256);
	}

	public void playTheme()
	{
		AudioSource.stopAll();
		if (worldBoss != null && mobs.contains(worldBoss))
			bossTheme.play();
		else
		{
			if (Game.getInstance().getCurrentProfile().getWorld() <= 3)
			{
				theme1.play();
				theme1.setLooping(true);
			} else if (Game.getInstance().getCurrentProfile().getWorld() <= 5)
			{
				theme2.play();
				theme2.setLooping(true);
			} else
			{
				theme3.play();
				theme3.setLooping(true);
			}
		}
	}

	/**
	 * gets the name of the world
	 * 
	 * @return returns name of world
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * gets width of world
	 * 
	 * @return returns width of world
	 */
	public int getWidth()
	{
		return width;
	}

	/**
	 * gets the height of the world
	 * 
	 * @return returns the height
	 */
	public int getHeight()
	{
		return height;
	}

	/**
	 * gets the x translation of the world
	 * 
	 * @return returns x translation
	 */
	public int getTranslateX()
	{
		return translateX;
	}

	/**
	 * gets the y translation of the world
	 * 
	 * @return returns y translation
	 */
	public int getTranslateY()
	{
		return translateY;
	}

	/**
	 * gets the spawn of the world
	 * 
	 * @return returns spawn
	 */
	public Vector2f getSpawn()
	{
		return spawn;
	}

	/**
	 * @return the boss of this world
	 */
	public Boss getWorldBoss()
	{
		return worldBoss;
	}

	/**
	 * @param worldBoss
	 *            the boss of this world
	 */
	public void setWorldBoss(Boss worldBoss)
	{
		this.worldBoss = worldBoss;
	}

	/**
	 * sets the spawn of the world
	 * 
	 * @param spawn
	 *            spawn of world
	 */
	public void setSpawn(Vector2f spawn)
	{
		this.spawn = spawn;
	}

	/**
	 * gets the tile array for world
	 * 
	 * @return returns tile array
	 */
	public Tile[] getTiles()
	{
		return tiles;
	}

	/**
	 * gets the battle for the adventure world
	 * 
	 * @return returns battle object
	 */
	public TetrisBattle getBattle()
	{
		return tetrisBattle;
	}

	/**
	 * gets battle stats for the tetris battle
	 * 
	 * @return returns battle stats
	 */
	public BattleStats getBattleStats()
	{
		return battleStats;
	}

	/**
	 * gets the tile at a certain location of the world
	 * 
	 * @param x
	 *            the x of world
	 * @param y
	 *            the y of world
	 * @return returns the tile at location
	 */
	public Tile getTileAt(float x, float y)
	{
		if (x / Tile.SIZE > 0 && y / Tile.SIZE > 0)
			return tiles[(int) (x / Tile.SIZE) + ((int) (y / Tile.SIZE) * width)];
		else
			return null;
	}

	/**
	 * gets enemies in the world
	 * 
	 * @return returns list of mobs
	 */
	public List<MobileEntity> getMobs()
	{
		return mobs;
	}

	/**
	 * gets the projectiles of the world
	 * 
	 * @return returns list of projectiles
	 */
	public List<Projectile> getProjectiles()
	{
		return projectiles;
	}

	/**
	 * gets the particles in the world
	 * 
	 * @return returns list of particles
	 */
	public List<Particle> getParticles()
	{
		return particles;
	}

	/**
	 * gets all non-moving objects in world
	 * 
	 * @return returns list of IE's
	 */
	public List<ImmobileEntity> getImmobileEntities()
	{
		return immobileEntities;
	}

	/**
	 * sets name of the world
	 * 
	 * @param name
	 *            name of world
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * sets the x translation of the world
	 * 
	 * @param translateX
	 *            x translation to move
	 */
	public void setTranslateX(int translateX)
	{
		this.translateX = translateX;
	}

	/**
	 * sets the y translation of the world
	 * 
	 * @param translateY
	 *            y translation to move
	 */
	public void setTranslateY(int translateY)
	{
		this.translateY = translateY;
	}

	/**
	 * translates the x amount of tiles
	 * 
	 * @param magX
	 *            amount to translate in x direction
	 */
	public void translateX(int magX)
	{
		translate(magX, 0);
	}

	/**
	 * translates the y amount of tiles
	 * 
	 * @param magY
	 *            amount to translate in y direction
	 */
	public void translateY(int magY)
	{
		translate(0, magY);
	}

	/**
	 * translates the total amount in both directions
	 * 
	 * @param magX
	 *            x translation amount
	 * @param magY
	 *            y translation amount
	 */
	public void translate(int magX, int magY)
	{
		this.translateX += magX;
		this.translateY += magY;
	}

	/**
	 * sets the battles in the world to encounter
	 * 
	 * @param battle
	 *            battle object to set in world
	 */
	public void setBattle(TetrisBattle battle)
	{
		this.tetrisBattle = battle;
	}

	/**
	 * sets the camera on the player on spawn
	 * 
	 * @param e
	 *            object/entity to set camera on
	 */
	public void centerCameraOnEntity(Entity e)
	{
		translateX = (int) (-e.getX() + (Core.getWindow().getWidth() - e.getWidth()) / 2);
		translateY = (int) (-e.getY() + (Core.getWindow().getHeight() - e.getHeight()) / 2);
	}

	/** Spawns a random amount of slimes in random places around the map. */
	public void spawnRandSlimes()
	{
		for (int i = 0; i < 0; i++)
		{
			float randX = (float) Math.random() * getWidth() * Tile.SIZE;
			float randY = (float) Math.random() * getWidth() * Tile.SIZE;

			getMobs().add(new Slime(this, randX, randY, new TextureRegion(Textures.ENEMY.getTexture()), 0));
		}
	}

	/**
	 * Updates the game consistently with each movement or change, makes changes to mobs and entities movement and sets all aspects of world.
	 */
	public void update()
	{

		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_ESCAPE))
		{
			paused = !paused;

			if (paused)
			{
				menuWindow = new MenuWindow(200, 100, 200, 300);
			} else
			{
				menuWindow = null;
			}
		}

		if (!paused)
		{
			if (fade)
			{
				creditsFade -= 0.002f;
			}
			if (creditsFade <= 0.1f)
				Game.getInstance().getGameStateManager().setCurrentState(Game.CREDITS_STATE.getId());
			if (battleStats.isBattleOver())
			{

				particles.clear();

				// /////////////////////////////////
				// HANDLE WHAT HAPPENS WHEN THE PLAYER LOSES AND REMOVE THE
				// PORTAL TO NEXT WORLD IF PLAYER WON WORLD
				// ////////////////////////////////

				int levelBefore = Game.getInstance().getCurrentProfile().getLevel();

				if (battleStats.hasPlayerWon())
				{
					normalFireworkCount = normalMaxFireworks;

					mobs.remove(tetrisBattle.getOpponent().getEnemy());
					TextParticle t = new TextParticle(this, findPlayer().getX(), findPlayer().getY(), 1, 1, Color.GREEN, 5, 500, "+" + (battleStats.getExpEarned() + tetrisBattle.getOpponent().getEnemy().getExp()) + " exp");
					particles.add(t);

					if (tetrisBattle.getOpponent().getEnemy() instanceof Boss)
					{
						bossDeafeated = true;
						worldBoss = null;
						winningFireworkCount = 20;
						if (Game.getInstance().getCurrentProfile().getWorld() == 8)
						{
							fade = true;
							winningFireworkCount = 30;
							normalFireworkDelay = new Delay(200);
							TextParticle t2 = new TextParticle(this, findPlayer().getX(), findPlayer().getY(), 1, 1, Color.GREEN, 5, 500, "Light Restored!");
							particles.add(t2);
						}
					}
					Game.getInstance().getCurrentProfile().setExperiance(Game.getInstance().getCurrentProfile().getExperiance() + tetrisBattle.getOpponent().getEnemy().getExp() + battleStats.getExpEarned());
				}

				if (Game.getInstance().getCurrentProfile().getLevel() != levelBefore)
				{
					// level up
					TextParticle t = new TextParticle(this, findPlayer().getX(), findPlayer().getY(), 1, 1, Color.GREEN, 5, 500, "Level up!");
					particles.add(t);
					if (Game.getInstance().getCurrentProfile().getLevel() == 2)
					{
						TextParticle t2 = new TextParticle(this, findPlayer().getX(), findPlayer().getY(), 1, 1, Color.GREEN, 5, 500, "Debuff ability unlocked!");
						particles.add(t2);
					} else if (Game.getInstance().getCurrentProfile().getLevel() == 6)
					{
						TextParticle t2 = new TextParticle(this, findPlayer().getX(), findPlayer().getY(), 1, 1, Color.GREEN, 5, 500, "Stun ability unlocked!");
						particles.add(t2);
					} else if (Game.getInstance().getCurrentProfile().getLevel() == 12)
					{
						TextParticle t2 = new TextParticle(this, findPlayer().getX(), findPlayer().getY(), 1, 1, Color.GREEN, 5, 500, "Power ability unlocked!");
						particles.add(t2);
					}
				}

				tetrisBattle = null;
				battleStats = new BattleStats();
				playTheme();
			}
			if (!mobs.contains(worldBoss) && worldBoss != null && mobs.size() < 2)
			{
				AudioSource.stopAll();
				bossTheme.play();
				bossTheme.setLooping(true);
				mobs.add(worldBoss);
				TextParticle t2 = new TextParticle(this, findPlayer().getX(), findPlayer().getY(), 1, 1, Color.GREEN, 5, 500, "World boss spawned!");
				particles.add(t2);
			}
			if (((worldBoss == null && !loadedNext) || (bossDeafeated && !loadedNext)) && Game.getInstance().getCurrentProfile().getWorld() < 8)
			{
				loadedNext = true;
				winningFireworkCount = winningMaxFireworks;
				TextParticle t2 = new TextParticle(this, findPlayer().getX(), findPlayer().getY(), 1, 1, Color.GREEN, 5, 500, "Next world unlocked!");
				particles.add(t2);
				if (Game.getInstance().getCurrentProfile().getWorld() < 8)
				{
					// only player remains
					Game.getInstance().getCurrentProfile().setWorld(Game.getInstance().getCurrentProfile().getWorld() + 1);
					int index = Game.getInstance().getCurrentProfile().getWorld();
					Game.ADVENTURE_MODE_STATE.getWorlds().loadWorld("/worlds/" + WorldNavigator.WORLD_FILE_NAMES[index], false);
				}
			}

			// update time
			time = 0.2f + Game.getInstance().getCurrentProfile().getWorld() * 0.1f;

			if (tetrisBattle == null)
			{

				int startX = -translateX / Tile.SIZE;
				int startY = -translateY / Tile.SIZE;
				int w = Core.getWindow().getWidth() / Tile.SIZE + 1;
				int h = Core.getWindow().getHeight() / Tile.SIZE + 1;

				if (startX < 0)
					startX = 0;

				if (startY < 0)
					startY = 0;

				for (int x = startX; x <= startX + w && x < width; x++)
				{
					for (int y = startY; y <= startY + h && y < height; y++)
					{
						if (tiles[x + (y * width)] != null)
						{
							if (tiles[x + (y * width)] instanceof AnimatedTile)
							{
								((AnimatedTile) tiles[x + (y * width)]).update();
							}
						}
					}
				}

				for (int i = 0; i < mobs.size(); i++)
				{
					mobs.get(i).update();
				}

				for (int i = 0; i < projectiles.size(); i++)
				{
					projectiles.get(i).update();
				}
				for (int i = 0; i < particles.size(); i++)
				{
					particles.get(i).update();
				}
				for (int i = 0; i < immobileEntities.size(); i++)
				{
					immobileEntities.get(i).update();
				}

				Game.getInstance().getLightRenderer().getOccluders().clear();

				if (normalFireworkCount != -1 && normalFireworkDelay.isReady())
				{
					normalFireworkCount--;
					spawnFireworks();
				}
				if (winningFireworkCount != -1 && normalFireworkDelay.isReady())
				{
					spawnFireworks();
					winningFireworkCount--;
				}

			} else
			{
				tetrisBattle.update();
			}

		} else
		{
			menuWindow.update();

			if (menuWindow.getBackToGameButton().wasPressed())
			{
				menuWindow = null;
				paused = false;
			} else if (menuWindow.getSaveButton().wasPressed())
			{
				// save
				Game.getInstance().getCurrentProfile().getPlayerFile().saveAdventureMode(Game.ADVENTURE_MODE_STATE);
			} else if (menuWindow.getQuitButton().wasPressed())
			{
				// save and go to main menu
				// Game.getInstance().getCurrentProfile().getPlayerFile().saveAdventureMode(Game.ADVENTURE_MODE_STATE);
				// save
				Game.getInstance().getCurrentProfile().getPlayerFile().saveAdventureMode(Game.ADVENTURE_MODE_STATE);
				if (getBattle() != null)
					getBattle().getAudio().stop();
				Game.getInstance().getGameStateManager().setCurrentState(Game.MAIN_MENU_STATE.getId());
			}
		}

	}

	/**
	 * Spawns fireworks and plays firework sound
	 */
	private void spawnFireworks()
	{
		final float fireworkParticles = 30;

		Player p = findPlayer();
		float rx = p.getX() + p.getWidth() / 2 + (float) (((Math.random() - 0.5f) * 2) * Core.getWindow().getWidth() / 2);
		float ry = p.getY() + p.getHeight() / 2 + (float) (((Math.random() - 0.5f) * 2) * Core.getWindow().getHeight() / 2);

		for (int i = 0; i < fireworkParticles; i++)
		{
			particles.add(new FireworkParticle(this, rx, ry, 10, 400, 3));
		}
		fireworksSource.play();
	}

	/**
	 * Day/night cycle.
	 */
	public void renderTimeDarkness()
	{
		if (timeEnabled)
			Game.getInstance().getSpriteBatch().getAmbient().setColor(new Color(time, time, time, 1.0f));
	}

	/**
	 * Renders the world's tiles.
	 */
	public void renderTiles()
	{
		// tile rendering system. renders only tiles that are visible
		int startX = -translateX / Tile.SIZE;
		int startY = -translateY / Tile.SIZE;
		int w = Core.getWindow().getWidth() / Tile.SIZE + 1;
		int h = Core.getWindow().getHeight() / Tile.SIZE + 1;

		if (startX < 0)
			startX = 0;

		if (startY < 0)
			startY = 0;

		Core.getGraphics().setBlendingEnabled(true);
		Core.getGraphics().setBlendFunction(BlendFunction.ALPHA_BLEND);
		Game.getInstance().getLightRenderer().getOccluders().clear();
		for (int x = startX; x <= startX + w && x < width; x++)
		{
			for (int y = startY; y <= startY + h && y < height; y++)
			{
				if (tiles[x + (y * width)] != null)
				{
					if (tiles[x + y * width].isSolid())
						Game.getInstance().getLightRenderer().getOccluders().add(new BoundingBox2D(new Vector2f(x * Tile.SIZE + translateX, y * Tile.SIZE + translateY), Tile.SIZE, Tile.SIZE));
					tiles[x + (y * width)].render(x * Tile.SIZE + translateX, y * Tile.SIZE + translateY);
				}
			}
		}
	}

	/**
	 * Renders all of the entities in this world, includes projectiles, particles, immobs, and mobs.
	 */
	public void renderEntities()
	{
		for (Projectile proj : projectiles)
		{
			proj.render();
		}
		for (Particle particle : particles)
		{
			particle.render();
		}
		for (ImmobileEntity imment : immobileEntities)
		{
			imment.render();
		}
		for (MobileEntity mob : mobs)
		{
			mob.render();
		}
	}

	/**
	 * Renders the world minimap in the bottom left corner.
	 */
	public void renderMiniMap()
	{
		if (miniMap != null)
		{
			final float scale = 0.8f, scale2 = 0.82f;

			Game.getInstance().getSpriteBatch().reset();
			Game.getInstance().getShapeBatch().reset();

			Core.getGraphics().setBlendingEnabled(true);
			Core.getGraphics().setBlendFunction(BlendFunction.ALPHA_BLEND);

			Game.getInstance().getSpriteBatch().setTint(Color.WHITE);
			Game.getInstance().getSpriteBatch().getAmbient().setColor(Color.WHITE);
			Game.getInstance().getSpriteBatch().render(miniMap, 0, 0, 0, scale, scale);

			Game.getInstance().getShapeBatch().setColor(Color.LAVENDER);

			Player p = null;
			for (MobileEntity m : mobs)
			{
				if (m instanceof Player)
				{
					p = (Player) m;
				}
			}
			Game.getInstance().getSpriteBatch().reset();
			Game.getInstance().getShapeBatch().renderCircle((p.getX() / Tile.SIZE) * scale, (p.getY() / Tile.SIZE) * scale, 5);
			Game.getInstance().getSpriteBatch().render(mapBorderOverlay, 0, 0, 0, 0, 0, scale2, scale2);
		}
	}

	/**
	 * Renders the pause menu when the player pauses the game.
	 */
	public void renderPauseMenu()
	{
		if (paused)
			menuWindow.render(Game.getInstance().getShapeBatch(), Game.getInstance().getSpriteBatch(), Game.getInstance().getFontRenderer());
	}

	/**
	 * Renders the translations and bitmap dimensions of world
	 */
	public void render()
	{
		if (fade)
		{
			time = creditsFade;
		}
		Game.getInstance().getSpriteBatch().reset();

		if (tetrisBattle == null)
		{
			renderTimeDarkness();
			Game.getInstance().getSpriteBatch().reset();

			renderTiles();
			for (Projectile proj : projectiles)
			{
				proj.render();
			}
			for (Particle particle : particles)
			{
				particle.render();
			}
			for (ImmobileEntity imment : immobileEntities)
			{
				imment.render();
			}
			renderMiniMap();
			for (MobileEntity mob : mobs)
			{
				mob.render();
			}

			findPlayer().renderInterface();

		} else
		{
			tetrisBattle.render();
		}

		renderPauseMenu();

	}

	/**
	 * Sets the time for the world and player.
	 * 
	 * @param time
	 *            THe time of this world.
	 */
	public void setTime(float time)
	{
		this.time = time;
	}

	/**
	 * Finds player camera and in relation to other entities.
	 * 
	 * @return player location as object
	 */
	public Player findPlayer()
	{
		for (MobileEntity mob : mobs)
			if (mob instanceof Player)
				return (Player) mob;
		return null;
	}
}
