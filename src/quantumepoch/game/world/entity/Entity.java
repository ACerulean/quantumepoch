package quantumepoch.game.world.entity;

import java.util.ArrayList;
import java.util.List;

import quantumepoch.game.Game;
import quantumepoch.game.world.World;
import quantumepoch.game.world.tile.Tile;
import quantumepoch.game.world.tile.TileWater;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.texture.TextureRegion;
import quantumepoch.physics.BoundingBox2D;
import quantumepoch.utils.math.Vector2f;

/**
 * An abstraction over an entity in some world. An entity can be defined as a
 * bounding box with a texture. This class also provides functionality for
 * simple physics simulations for realistic movement.
 */
public abstract class Entity
{

	/** the world that this entity occupies */
	private World world;
	/** the acceleration of this entity */
	private Vector2f acceleration;
	/** the velocity of this entity */
	private Vector2f velocity;
	/** the box that bounds this entity */
	private BoundingBox2D boundingBox;
	/** the texture of this entity, always a region for efficientcy */
	private TextureRegion texture;
	/** the color tintint of this entity's texture */
	private Color tint;
	/** the amount this entity should be rotated by */
	private float rotation;
	/** the scale factor of this entity's texture */
	private float scale = 1;
	/**
	 * the damp factor of an entity's acceleration and velocity, this is what
	 * stops an entity from moving over time
	 */
	private final float dampEpsilon = 0.01f;

	/**
	 * Creates a new entity.
	 * 
	 * @param world
	 *            The world this entity should occupy.
	 * @param x
	 *            The x-coordinate in the world.
	 * @param y
	 *            The y-coordinate in the world.
	 * @param width
	 *            The width of this entity.
	 * @param height
	 *            The height of this entity.
	 * @param tint
	 *            The color of this entity.
	 */
	public Entity(World world, float x, float y, float width, float height, Color tint)
	{
		this(world, x, y, width, height, null, tint);
	}

	/**
	 * Creates a new entity.
	 * 
	 * @param world
	 *            The world this entity should occupy.
	 * @param x
	 *            The x-coordinate in the world.
	 * @param y
	 *            The y-coordinate in the world.
	 * @param texture
	 *            The texture of this entity.
	 */
	public Entity(World world, float x, float y, TextureRegion texture)
	{
		this(world, x, y, texture, Color.WHITE);
	}

	/**
	 * 
	 * @param world
	 *            The world this entity should occupy.
	 * @param x
	 *            The x-coordinate in the world.
	 * @param y
	 *            The y-coordinate in the world.
	 * @param width
	 *            The width of this entity.
	 * @param height
	 *            The height of this entity.
	 */
	public Entity(World world, float x, float y, TextureRegion texture, Color tint)
	{
		this(world, x, y, texture.getWidth(), texture.getHeight(), texture, tint);
	}

	/**
	 * Creates a new entity.
	 * 
	 * @param world
	 *            The world this entity should occupy.
	 * @param x
	 *            The x-coordinate in the world.
	 * @param y
	 *            The y-coordinate in the world.
	 * @param width
	 *            The width of this entity.
	 * @param height
	 *            The height of this entity.
	 * @param width
	 *            The width of this entity.
	 * @param height
	 *            The height of this entity.
	 */
	public Entity(World world, float x, float y, float width, float height, TextureRegion texture, Color tint)
	{
		this.world = world;
		this.boundingBox = new BoundingBox2D(new Vector2f(x, y), width, height);
		this.tint = tint;
		this.texture = texture;
		acceleration = new Vector2f();
		velocity = new Vector2f();
	}

	/**
	 * @return the world that the entity occupies
	 */
	public World getWorld()
	{
		return world;
	}

	/**
	 * @return the bounding box that the entity is bounded by
	 */
	public BoundingBox2D getBoundingBox()
	{
		return boundingBox;
	}

	/**
	 * @return the acceleration of this entity in pixels per update squared.
	 */
	public Vector2f getAcceleration()
	{
		return acceleration;
	}

	/**
	 * @return the velocity of this entity in pixels per second
	 */
	public Vector2f getVelocity()
	{
		return velocity;
	}

	/**
	 * @return the rotation of this entity
	 */
	public float getRotation()
	{
		return rotation;
	}

	/**
	 * @return the scale factor of this entity
	 */
	public float getScale()
	{
		return scale;
	}

	/**
	 * @return the x position of this entity in the world
	 */
	public float getX()
	{
		return boundingBox.getPosition().getX();
	}

	/**
	 * @return the y position of this entity in the world
	 */
	public float getY()
	{
		return boundingBox.getPosition().getY();
	}

	/**
	 * @return the width of the entity
	 */
	public float getWidth()
	{
		return boundingBox.getWidth();
	}

	/**
	 * @return the height of the entity
	 */
	public float getHeight()
	{
		return boundingBox.getHeight();
	}

	/**
	 * @return the color of the entity
	 */
	public Color getTint()
	{
		return tint;
	}

	/**
	 * @return the texture of the entity
	 */
	public TextureRegion getTexture()
	{
		return texture;
	}

	/**
	 * Calculates the distance from this entity to another relative to their
	 * centers.
	 * 
	 * @param other
	 *            The other entity.
	 * @return The distance between this entity and the other entity
	 */
	public float distance(Entity other)
	{
		float dx = Math.abs((getX() + getWidth() / 2) - (other.getX() + other.getWidth() / 2));
		float dy = Math.abs((getY() + getHeight() / 2) - (other.getY() + (other.getHeight() / 2)));
		return (float) Math.sqrt(dx * dx + dy * dy);
	}

	/**
	 * Sets the bounds of this entity.
	 * 
	 * @param boundingBox
	 *            The new bounds.
	 */
	public void setBoundingBox(BoundingBox2D boundingBox)
	{
		this.boundingBox = boundingBox;
	}

	/**
	 * Sets the x position of this entity.
	 * 
	 * @param x
	 *            The new x.
	 */
	public void setX(float x)
	{
		boundingBox.getPosition().setX(x);
	}

	/**
	 * Sets the y position of this entity.
	 * 
	 * @param y
	 *            The new y.
	 */
	public void setY(float y)
	{
		boundingBox.getPosition().setY(y);
	}

	/**
	 * Sets the rotation of this entity.
	 * 
	 * @param rotation
	 *            The new rotation.
	 */
	public void setRotation(float rotation)
	{
		this.rotation = rotation;
	}

	/**
	 * Sets the width of this entity.
	 * 
	 * @param width
	 *            The new width.
	 */
	public void setWidth(float width)
	{
		boundingBox.setWidth(width);
	}

	/**
	 * Sets the height of this entity.
	 * 
	 * @param height
	 *            The new height.
	 */
	public void setHeight(float height)
	{
		boundingBox.setHeight(height);
	}

	/**
	 * Sets the color of this entity.
	 * 
	 * @param tint
	 *            The new color.
	 */
	public void setTint(Color tint)
	{
		this.tint = tint;
	}

	/**
	 * Sets the scale factor of this entity.
	 * 
	 * @param scale
	 *            The new scale factor.
	 */
	public void setScale(float scale)
	{
		this.scale = scale;
	}

	/**
	 * Sets the texture of this texture.
	 * 
	 * @param texture
	 *            The new texture.
	 */
	public void setTexture(TextureRegion texture)
	{
		this.texture = texture;
		setWidth(texture.getWidth());
		setHeight(texture.getHeight());
	}

	/**
	 * Calculates a list of tiles in which this entity is currently colliding
	 * with.
	 * 
	 * @return A list of tiles that this entity is colliding with
	 */
	public List<Tile> getCollidingTiles()
	{
		return getCollidingTiles(0, 0);
	}

	/**
	 * @return if this current entity is in a water tile
	 */
	public boolean isInWater()
	{
		for (Tile t : getCollidingTiles())
		{
			if (t instanceof TileWater)
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Calculates a list of tiles in which this entity is colliding with.
	 * 
	 * @param dx
	 *            The translation of the entity x position before calculation.
	 * @param dy
	 *            * The translation of the entity y position before calculation.
	 * 
	 * @return A list of tiles in which this entity is colliding with
	 */
	public List<Tile> getCollidingTiles(float dx, float dy)
	{
		float newX = getX() + dx;
		float newY = getY() + dy;
		List<Tile> result = new ArrayList<Tile>();
		for (float x = newX; x <= newX + getWidth(); x += getWidth() / Tile.SIZE)
		{
			for (float y = newY; y <= newY + getHeight(); y += getHeight() / Tile.SIZE)
			{
				Tile t = getWorld().getTileAt(x, y);
				if (!result.contains(t))
				{
					result.add(t);
				}
			}
		}
		return result;
	}

	/**
	 * @param dx
	 *            The translation on the x axis before calculating the result.
	 * @return Whether this entity can move
	 */
	public boolean canMoveX(float dx)
	{
		float newX = getX() + dx;

		// loop through each vertex
		boolean canMove = true;
		for (float x = newX; x <= newX + getWidth(); x += getWidth() / 4)
		{
			for (float y = getY(); y <= getY() + getHeight(); y += getHeight() / 4)
			{
				Tile t = getWorld().getTileAt(x, y);

				if (t != null && getWorld().getTileAt(x, y).isSolid())
				{
					canMove = false;
				}
			}
		}
		return canMove;
	}

	/**
	 * @param dy
	 *            The translation on the y axis before calculating the result.
	 * @return Whether this entity can move
	 */
	public boolean canMoveY(float dy)
	{
		float newY = getY() + dy;

		// loop through each vertex
		boolean canMove = true;
		for (float x = getX(); x <= getX() + getWidth(); x += getWidth() / 4)
		{
			for (float y = newY; y <= newY + getHeight(); y += getHeight() / 4)
			{
				Tile t = getWorld().getTileAt(x, y);

				if (t != null && getWorld().getTileAt(x, y).isSolid())
				{
					canMove = false;
				}
			}
		}
		return canMove;
	}

	/**
	 * Damps the acceleration on the x axis by dx.
	 * 
	 * @param dx
	 *            The damp factor.
	 */
	public void dampAccelerationX(float dx)
	{
		acceleration.setX(acceleration.getX() * dx);

		if (Math.abs(acceleration.getX()) <= dampEpsilon)
			resetAccelerationX();
	}

	/**
	 * Damps the acceleration on the y axis by dy.
	 * 
	 * @param dy
	 *            The damp factor.
	 */
	public void dampAccelerationY(float dy)
	{
		acceleration.setY(acceleration.getY() * dy);

		if (Math.abs(acceleration.getY()) <= dampEpsilon)
			resetAccelerationY();
	}

	/**
	 * Damps the acceleration on both axes by dx and dy.
	 * 
	 * @param dx
	 *            The x damp factor.
	 * @param dy
	 *            The y damp factor.
	 */
	public void dampAcceleration(float dx, float dy)
	{
		dampAccelerationX(dx);
		dampAccelerationY(dy);
	}

	/**
	 * Resets the acceleration on the x axis to 0.
	 */
	public void resetAccelerationX()
	{
		acceleration.setX(0);
	}

	/**
	 * Resets the acceleration on the x and y axis to 0.
	 */
	public void resetAccelerationY()
	{
		acceleration.setY(0);
	}

	/**
	 * Resets the acceleration on the x nd y axis to 0.
	 */
	public void resetAcceleration()
	{
		resetAccelerationX();
		resetAccelerationY();
	}

	/**
	 * Damps the velocity on the y axis by dy.
	 * 
	 * @param dy
	 *            The damp factor
	 */
	public void dampVelocityY(float dy)
	{
		velocity.setY(velocity.getY() * dy);

		if (Math.abs(velocity.getY()) <= dampEpsilon)
			resetVelocityY();
	}

	/**
	 * Damps the acceleration on the x axis by dx.
	 * 
	 * @param dx
	 *            The damp factor.
	 */
	public void dampVelocityX(float dx)
	{
		velocity.setX(velocity.getX() * dx);

		if (Math.abs(velocity.getX()) <= dampEpsilon)
			resetVelocityX();
	}

	/**
	 * Damps the velocity on both axes by dx and dy.
	 * 
	 * @param dx
	 *            The x damp factor.
	 * @param dy
	 *            The y damp factor.
	 */
	public void dampVelocity(float dx, float dy)
	{
		dampVelocityX(dx);
		dampVelocityY(dy);
	}

	/**
	 * Resets the velocity on the x axis to 0.
	 */
	public void resetVelocityX()
	{
		velocity.setX(0);
	}

	/**
	 * Resets the velocity on the y axis to 0.
	 */
	public void resetVelocityY()
	{
		velocity.setY(0);
	}

	/**
	 * Resets the acceleration on the x and y axis to 0.
	 */
	public void resetVelocity()
	{
		resetVelocityX();
		resetVelocityY();
	}

	/**
	 * Updates the position of this entity based on the current velocity and
	 * acceleration.
	 */
	public void updateMovement()
	{
		velocity.set(velocity.add(acceleration));
		getBoundingBox().getPosition().set(getBoundingBox().getPosition().add(velocity));
	}

	/** Updates the current state of this entity */
	public abstract void update();

	/** Renders this entity to the screen */
	public void render()
	{
		int tx = 0;
		int ty = 0;
		if (world.getBattle() == null)
		{
			tx = world.getTranslateX();
			ty = world.getTranslateY();
		}
		if (texture == null)
		{
			Game.getInstance().getShapeBatch().setColor(tint);
			Game.getInstance().getShapeBatch().renderFilledRect(getX() + tx, getY() + ty, getWidth(), getHeight());
		} else
		{
			Game.getInstance().getSpriteBatch().setTint(tint);
			Game.getInstance().getSpriteBatch().render(texture, getWidth() / 2, getHeight() / 2, getX() + tx, getY() + ty, rotation, scale, scale);
		}
	}

}
