package quantumepoch.game.world.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Stores a list of nodes for a moving object to travel through in order
 */
public class NodeTraveller
{

	/** the list of nodes of this node traveller */
	private List<NodePoint> points;
	/** the current point index */
	private int currentPoint;
	/** whether this node traveller has reacher its destination */
	private boolean complete;

	/**
	 * Creates a new node traveller.
	 */
	public NodeTraveller()
	{
		points = new ArrayList<NodePoint>();
	}

	/**
	 * @return the list of node points to travel to
	 */
	public List<NodePoint> getPoints()
	{
		return points;
	}

	/**
	 * @return whether the moving object has reached its destination
	 */
	public boolean isComplete()
	{
		return complete;
	}

	/**
	 * Updates the entity to travel through the list of node points
	 * 
	 * @param e
	 *            The entity to update
	 */
	public void update(Entity e)
	{
		if (!complete)
		{
			NodePoint p = points.get(currentPoint);

			final float epsilon = 0.01f;

			float dx = e.getX() - p.getPosition().getX();
			float dy = e.getY() - p.getPosition().getY();

			if ((Math.abs(dx) <= epsilon || -Math.abs(dx) >= epsilon) && (Math.abs(dy) <= epsilon || -Math.abs(dy) >= epsilon))
			{
				if (currentPoint >= points.size())
					complete = true;
				else
					currentPoint++;
			} else
			{
				float nx = e.getX();
				float ny = e.getY();

				if (Math.abs(dx) <= p.getSpeed())
				{
					nx = p.getPosition().getX();
				} else
				{
					nx += -Math.signum(dx) * p.getSpeed();
				}

				if (Math.abs(dy) <= p.getSpeed())
				{
					ny = p.getPosition().getY();
				} else
				{
					ny += -Math.signum(dy) * p.getSpeed();
				}

				e.getBoundingBox().getPosition().set(nx, ny);
			}
		}
	}
}
