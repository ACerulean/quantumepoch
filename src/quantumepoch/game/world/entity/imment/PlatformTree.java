package quantumepoch.game.world.entity.imment;

import quantumepoch.game.Textures;
import quantumepoch.game.world.World;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * Class for the platform tree immobile entity that appears in the background of
 * a tetris battle against an enemy
 */
public class PlatformTree extends ImmobileEntity
{

	/** The texture for a small platform tree */
	public static final TextureRegion TREE_SMALL = new TextureRegion(Textures.TREES.getTexture(), 0, 1, 46, 72);
	/** The texture for a medium platform tree */
	public static final TextureRegion TREE_MEDIUM = new TextureRegion(Textures.TREES.getTexture(), 46, 1, 46, 200);
	/** The texture for a tall platform tree */
	public static final TextureRegion TREE_TALL = new TextureRegion(Textures.TREES.getTexture(), 46 * 2, 1, 55, 250);

	/**
	 * Creates a new platform tree entity
	 * 
	 * @param world
	 *            The world this entity will be loaded in to
	 * @param x
	 *            The x-coordinate in the world
	 * @param y
	 *            The y-coordinate in the world
	 * @param size
	 *            The size of the platform tree; 1 = small tree, 2 = medium
	 *            tree, and 3 = tall tree
	 */
	public PlatformTree(World world, float x, float y, int size)
	{
		super(world, x, y, TREE_SMALL);

		if (size == 1)
		{
			setTexture(TREE_SMALL);
		} else if (size == 2)
		{
			setTexture(TREE_MEDIUM);
		} else if (size == 3)
		{
			setTexture(TREE_TALL);
		} else
		{
			setTexture(TREE_TALL);
		}
	}

	/** Renders the tree to the screen */
	@Override
	public void render()
	{
		super.render();
		/*
		 * Game.getInstance().getSpriteBatch().setTint(getTint());
		 * Game.getInstance().getSpriteBatch().render(getTexture(), 0, 0, getX()
		 * + getWorld().getTranslateX(), getY() + getWorld().getTranslateY(), 0,
		 * 2, 2);
		 */
	}

}
