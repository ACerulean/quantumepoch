package quantumepoch.game.world.entity.imment;

import quantumepoch.game.Textures;
import quantumepoch.game.world.World;
import quantumepoch.graphics.texture.TextureRegion;

/** Class that holds that texture and value for a cactus */
public class Cactus extends ImmobileEntity
{

	/** The cactus texture */
	public static final TextureRegion texture = new TextureRegion(Textures.TILES.getTexture(), 1 * 32, 2 * 32, 32, 32);

	/**
	 * Creates a new cactus entity
	 * 
	 * @param world
	 *            What world this cactus will be loaded on to
	 * @param x
	 *            The x-coordinate on the world
	 * @param y
	 *            The y-coordinate on the world
	 */
	public Cactus(World world, float x, float y)
	{
		super(world, x, y, texture);
		setScale(1.3f);
		float rand = (float)Math.random() * 2;
		setRotation(rand);
	}
}
