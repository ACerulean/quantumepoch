package quantumepoch.game.world.entity.imment;

import quantumepoch.game.world.Clouds;
import quantumepoch.game.world.World;
import quantumepoch.game.world.tile.Tile;

/** Class that holds that texture and value for a cloud */
public class CloudEntity extends ImmobileEntity
{
	/** The speed that cloud travels across the screen */
	private float speed;

	/**
	 * Creates a new cloud entity
	 * 
	 * @param world
	 *            The world this cloud will be loaded on to
	 * @param x
	 *            The x-coordinate on the world
	 * @param y
	 *            The y-coordinate on the world
	 */
	public CloudEntity(World world, float x, float y)
	{
		super(world, x, y, Clouds.values()[(int) (Math.random() * Clouds.values().length)].getTexture());
		speed = (float) (Math.random() * 5) + 2;
	}

	/** Updates where on the screen the cloud is located based on the speed */
	@Override
	public void update()
	{
		super.update();
		setX(getX() + speed);

		if (getX() + getWidth() >= getWorld().getWidth() * Tile.SIZE)
			getWorld().getImmobileEntities().remove(this);
	}

}
