package quantumepoch.game.world.entity.imment;

import quantumepoch.game.Textures;
import quantumepoch.game.world.World;
import quantumepoch.game.world.tile.Tile;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * Class for the tree immobile entity that appears in the different worlds
 */
public class Tree extends ImmobileEntity
{
	/** Texture for the tree */
	public static final TextureRegion texture = new TextureRegion(Textures.TILES.getTexture(), 1 * 32, 3 * 32, 32, 32);

	private Tile mostAbundent;

	/**
	 * Creates a new tree entity
	 * 
	 * @param world
	 *            The world this tree will be loaded on
	 * @param x
	 *            The x-coordinate in the world
	 * @param y
	 *            The y-coordinate in the world
	 */
	public Tree(World world, float x, float y)
	{
		super(world, x, y, texture);
		setScale(1.7f);
		float rand = (float) Math.random() * 2;
		setRotation(rand);
	}

	@Override
	public void update()
	{
		super.update();
		if (mostAbundent == null)
			mostAbundent = Tile.mostAbundentAdjacentTile(getWorld(), (int) (getX() / Tile.SIZE), (int) (getY() / Tile.SIZE));
	}

	/**
	 * Renders the tree on the world and renders the cell below it based the
	 * most abundant textures of the cells that surround it
	 */
	@Override
	public void render()
	{
		if (mostAbundent != null)
			mostAbundent.render((int) getX() + getWorld().getTranslateX(), (int) getY() + getWorld().getTranslateY());
		super.render();
	}
}
