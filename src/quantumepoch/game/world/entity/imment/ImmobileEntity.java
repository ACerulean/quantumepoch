package quantumepoch.game.world.entity.imment;

import quantumepoch.game.world.World;
import quantumepoch.game.world.entity.Entity;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.texture.TextureRegion;

/** Abstract class for entities that cannot be moved through the use of input */
public abstract class ImmobileEntity extends Entity
{

	/**
	 * Creates a new immobile entity
	 * 
	 * @param world
	 *            The world this immobile entity is being load into
	 * @param x
	 *            The x-coordinate on the world
	 * @param y
	 *            The y-coordinate on the world
	 * @param width
	 *            The width of this entity
	 * @param height
	 *            The height of this entity
	 * @param tint
	 *            The tint of the texture of this entity
	 */
	public ImmobileEntity(World world, float x, float y, float width, float height, Color tint)
	{
		super(world, x, y, width, height, tint);
	}

	/**
	 * Creates a new immobile entity
	 * 
	 * @param world
	 *            The world this immobile entity is being loaded into
	 * @param x
	 *            The X-coordinate on the world
	 * @param y
	 *            The Y-coordinate on the world
	 * @param width
	 *            The width of this entity
	 * @param height
	 *            The height of this entity
	 * @param texture
	 *            The texture for this entity
	 * @param tint
	 *            The tint of the texture of this entity
	 */
	public ImmobileEntity(World world, float x, float y, float width, float height, TextureRegion texture, Color tint)
	{
		super(world, x, y, width, height, texture, tint);
	}

	/**
	 * Creates a new immobile entity
	 * 
	 * @param world
	 *            The world this immobile entity is being load into
	 * @param x
	 *            The X-coordinate on the world
	 * @param y
	 *            The Y-coordinate on the world
	 * @param texture
	 *            The texture for this entity
	 * @param tint
	 *            The tint of the texture of this entity
	 */
	public ImmobileEntity(World world, float x, float y, TextureRegion texture, Color tint)
	{
		super(world, x, y, texture, tint);
	}

	/**
	 * Creates a new immobile entity
	 * 
	 * @param world
	 *            The world this immobile entity is being load into
	 * @param x
	 *            The X-coordinate on the world
	 * @param y
	 *            The Y-coordinate on the world
	 * @param texture
	 *            The texture for this entity
	 */
	public ImmobileEntity(World world, float x, float y, TextureRegion texture)
	{
		super(world, x, y, texture);
	}

	@Override
	public void update()
	{

	}

}
