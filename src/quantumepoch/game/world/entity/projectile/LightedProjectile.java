package quantumepoch.game.world.entity.projectile;

import quantumepoch.game.Game;
import quantumepoch.game.Textures;
import quantumepoch.game.world.World;
import quantumepoch.game.world.entity.Entity;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.lighting.PointLight;
import quantumepoch.utils.math.Vector3f;

public class LightedProjectile extends Projectile
{

	private PointLight light;

	public LightedProjectile(World world, float x, float y, float speed, float angle, float range, float damage, String type, Entity firing, Color lightColor)
	{
		super(world, x, y, Textures.PROJECTILES.getTexture().toTextureRegion(), speed, angle, range, damage, type, firing);
		light = new PointLight(new Vector3f(x, y, 0f), lightColor, 1, 1, 0, 0.001f);
		setWidth(1);
		setHeight(1);
	}

	@Override
	public void render()
	{
		Game.getInstance().getLightRenderer().renderPointLight(light);
	}
	
	@Override
	public void update()
	{
		super.update();
		light.getPosition().set(getX(), getY(), 0);
	}
}
