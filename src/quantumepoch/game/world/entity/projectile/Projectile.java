package quantumepoch.game.world.entity.projectile;

import java.util.List;

import quantumepoch.game.inventory.Ability;
import quantumepoch.game.inventory.BlindingLight;
import quantumepoch.game.inventory.FireAttack;
import quantumepoch.game.inventory.IceArrow;
import quantumepoch.game.inventory.LightningStrike;
import quantumepoch.game.inventory.SwordStun;
import quantumepoch.game.world.World;
import quantumepoch.game.world.entity.Entity;
import quantumepoch.game.world.entity.mob.Enemy;
import quantumepoch.game.world.entity.mob.MobileEntity;
import quantumepoch.game.world.entity.mob.battle.BattleEnemy;
import quantumepoch.game.world.entity.mob.battle.BattlePlayer;
import quantumepoch.graphics.texture.TextureRegion;
import quantumepoch.utils.math.Vector2f;

/**
 * An entity that follows a linear path of motion and can inflict damage.
 */
public abstract class Projectile extends Entity
{

	/** the starting point of this projectile */
	private Vector2f start;
	/** the length in pixels that this projectile can travel */
	private float range;
	/** the damage this projectile will inflict on impact */
	protected float damage;
	/** the entity, if any, firing this projectile */
	protected Entity firing;
	/** The type of attack: normal, fire, and ice */
	protected String type;

	/**
	 * Creates a new projectile
	 * 
	 * @param world
	 * @param x
	 * @param y
	 * @param texture
	 * @param speed
	 * @param angle
	 *            The angle this projectile will face when fired. Zero degrees
	 *            faces right.
	 * @param range
	 * @param damage
	 * @param firing
	 */
	public Projectile(World world, float x, float y, TextureRegion texture, float speed, float angle, float range, float damage, String type, Entity firing)
	{
		super(world, x, y, texture);

		getVelocity().set(new Vector2f((float) (Math.cos(angle) * speed), (float) (Math.sin(angle)) * speed));

		start = new Vector2f(x, y);

		this.range = range;

		this.damage = damage;
		this.type = type;
		this.firing = firing;
	}

	/**
	 * @return the damage inflicted by this projectile
	 */
	public float getDamage()
	{
		return damage;
	}

	/**
	 * @return the entity firing, or null if none.
	 */
	public Entity getFiring()
	{
		return firing;
	}

	public String getType()
	{
		return type;
	}

	@Override
	public void update()
	{
		super.updateMovement();

		List<MobileEntity> mobs = null;
		if (getWorld().getBattle() != null)
		{
			mobs = getWorld().getBattle().getMobs();
		} else
		{
			mobs = getWorld().getMobs();
		}

		for (MobileEntity mob : mobs)
		{
			if ((mob instanceof BattleEnemy || mob instanceof Enemy || mob instanceof BattlePlayer) && getBoundingBox().intersects(mob.getBoundingBox()) && mob != firing)
			{
				if (mob instanceof BattleEnemy)
				{
					if (this.getType().equals("Fire"))
					{
						FireAttack fire = ((FireAttack) getWorld().getBattle().getPlayer().getAbilities()[1]);
						if (((BattleEnemy) mob).isFrozen())
						{
							((BattleEnemy) mob).thaw();
							((BattleEnemy) mob).decreaseHealth(damage);
						} else
						{
							((BattleEnemy) mob).decreaseHealth(damage);
							((BattleEnemy) mob).burn(fire.getBurnDamage(), fire.getBurnTime());
						}
					} else if (this.getType().equals("Disable"))
					{
						Ability ability = getWorld().getBattle().getPlayer().getAbility();
						if (ability.getName().equals("Sword Stun"))
						{
							((BattleEnemy) mob).decreaseHealth(damage);
							((BattleEnemy) mob).disable(((SwordStun) ability).getStunTime());
						} else if (ability.getName().equals("Lightning Strike"))
						{
							((BattleEnemy) mob).decreaseHealth(damage);
							((BattleEnemy) mob).disable(((LightningStrike) ability).getStunTime());
						} else
							((BattleEnemy) mob).disable(((BlindingLight) ability).getStunTime());
					} else if (this.getType().equals("Ice"))
					{
						if (getWorld().getBattle().getPlayer().getAbilities()[2].getName().equals("Ice Arrow"))
						{
							((BattleEnemy) mob).decreaseHealth(damage);
							if (!((BattleEnemy) mob).isBurning())
								((BattleEnemy) mob).freeze(((IceArrow) getWorld().getBattle().getPlayer().getAbilities()[2]).getFreezeTime());
						}
					} else
						((BattleEnemy) mob).decreaseHealth(damage);
				} else if (mob instanceof Enemy)
				{
					((Enemy) mob).decreaseHealth(damage);
				} else if (mob instanceof BattlePlayer)
				{
					((BattlePlayer) mob).decreaseHealth(damage);
				}
				if (getWorld().getBattle() == null)
					getWorld().getProjectiles().remove(this);
				else
					getWorld().getBattle().getProjectiles().remove(this);
			}

		}

		if (start.subtract(getBoundingBox().getPosition()).length() >= range)
		{
			getWorld().getProjectiles().remove(this);
		}
	}

}
