package quantumepoch.game.world.entity.projectile;

import quantumepoch.core.Core;
import quantumepoch.game.Textures;
import quantumepoch.game.world.World;
import quantumepoch.game.world.entity.Entity;
import quantumepoch.graphics.texture.TextureRegion;

public class SwordSlash extends Projectile
{

	public SwordSlash(World world, float x, float y, float damage, Entity firing)
	{
		super(world, x, y, new TextureRegion(Textures.PROJECTILES.getTexture(), 16 * 2, 16 * 1, 15, 18), 8, 0, 200, damage, "Normal", firing);
		setScale(3);
	}

	public SwordSlash(World world, float x, float y, float damage, String type, Entity firing)
	{
		super(world, x, y, new TextureRegion(Textures.PROJECTILES.getTexture(), 16 * 2, 16 * 1, 15, 18), 8, 0, 200, damage, type, firing);
		setScale(2f);
	}

	@Override
	public void update()
	{
		super.update();
		if (!canMoveX(getVelocity().getX()) || !canMoveY(getVelocity().getY()))
		{
			getWorld().getProjectiles().remove(this);
		}
	}
}
