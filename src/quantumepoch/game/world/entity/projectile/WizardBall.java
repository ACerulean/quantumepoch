package quantumepoch.game.world.entity.projectile;

import quantumepoch.core.Core;
import quantumepoch.game.Textures;
import quantumepoch.game.world.World;
import quantumepoch.game.world.entity.Entity;
import quantumepoch.game.world.entity.particle.ParticleEmitter;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.texture.TextureRegion;
import quantumepoch.utils.Delay;
import quantumepoch.utils.math.Vector2f;

/**
 * A blue ball constantly releasing particles
 */
public class WizardBall extends Projectile
{

	/** the texture of the wizard ball */
	public static final TextureRegion region = new TextureRegion(Textures.PROJECTILES.getTexture(), 1, 1, 13, 13);
	/** the particle emitter for wizard particles */
	private ParticleEmitter emitter;

	/**
	 * Creates a new wizard ball
	 * 
	 * @param world
	 * @param x
	 * @param y
	 * @param firing
	 */
	public WizardBall(World world, float x, float y, float damage, Entity firing)
	{
		super(world, x + 30, y, region, 10, (float) Math.atan2(Core.getInput().getMouse().getY() - Core.getWindow().getHeight() / 2, Core.getInput().getMouse().getX() - Core.getWindow().getWidth() / 2), 500, damage, "Normal", firing);
		setTint(Color.WHITE);
		emitter = new ParticleEmitter(world, new Vector2f(x, y), 5, 0, new Delay(50));
	}

	/**
	 * Creates a new wizard ball
	 * 
	 * @param world
	 * @param x
	 * @param y
	 * @param angle
	 * @param firing
	 */
	public WizardBall(World world, float x, float y, float angle, float damage, Entity firing)
	{
		super(world, x + 30, y, region, 10, angle, 500, damage, "Normal", firing);
		setTint(Color.WHITE);
		emitter = new ParticleEmitter(world, new Vector2f(x, y), 5, 0, new Delay(50));
	}

	/**
	 * Creates a new wizard ball with tint
	 * 
	 * @param world
	 * @param x
	 * @param y
	 * @param angle
	 * @param firing
	 * @param tint
	 */
	public WizardBall(World world, float x, float y, float angle, float damage, Entity firing, Color tint)
	{
		super(world, x + 30, y, region, 10, angle, 500, damage, "Normal", firing);
		super.damage = damage;
		emitter = new ParticleEmitter(world, new Vector2f(x, y), 5, 0, new Delay(50));
		setTint(tint);
	}

	/**
	 * Creates a new fire wizard ball
	 * 
	 * @param world
	 * @param x
	 * @param y
	 * @param angle
	 * @param firing
	 * @param tint
	 */
	public WizardBall(World world, float x, float y, float angle, float damage, String type, Entity firing)
	{
		super(world, x + 30, y, region, 10, angle, 500, damage, type, firing);
		super.damage = damage;
		emitter = new ParticleEmitter(world, new Vector2f(x, y), 5, 0, new Delay(50));
		setTint(Color.WHITE);
	}

	/**
	 * Creates a new fire wizard ball with tint
	 * 
	 * @param world
	 * @param x
	 * @param y
	 * @param angle
	 * @param firing
	 * @param tint
	 */
	public WizardBall(World world, float x, float y, float angle, float damage, String type, Entity firing, Color tint)
	{
		super(world, x + 30, y, region, 10, angle, 500, damage, type, firing);
		super.damage = damage;
		emitter = new ParticleEmitter(world, new Vector2f(x, y), 5, 0, new Delay(50));
		setTint(tint);
	}

	@Override
	public void update()
	{

		emitter.setPos(new Vector2f(getX(), getY()));
		emitter.update();

		super.update();

		if (!getWorld().getProjectiles().contains(this))
		{
			for (int i = 0; i < 25; i++)
			{
				getWorld().getParticles().add(emitter.makeParticle());
			}
		}

		if (!canMoveX(getVelocity().getX()) || !canMoveY(getVelocity().getY()))
		{
			getWorld().getProjectiles().remove(this);

			for (int i = 0; i < 25; i++)
			{
				getWorld().getParticles().add(emitter.makeParticle());
			}
		}
	}
}
