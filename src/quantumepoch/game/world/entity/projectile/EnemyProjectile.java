package quantumepoch.game.world.entity.projectile;

import quantumepoch.game.Textures;
import quantumepoch.game.world.World;
import quantumepoch.game.world.entity.Entity;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.texture.TextureRegion;

public class EnemyProjectile extends Projectile
{

	public EnemyProjectile(World world, float x, float y, float angle, float damage, Entity firing)
	{
		super(world, x, y, new TextureRegion(Textures.PROJECTILES.getTexture(), 0, 0, 16, 16), 10, angle, 500, damage, "Normal", firing);
		setTint(Color.RED);
	}

}
