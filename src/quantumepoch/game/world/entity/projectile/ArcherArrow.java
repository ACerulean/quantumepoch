package quantumepoch.game.world.entity.projectile;

import quantumepoch.core.Core;
import quantumepoch.game.Textures;
import quantumepoch.game.world.World;
import quantumepoch.game.world.entity.Entity;
import quantumepoch.graphics.texture.TextureRegion;

public class ArcherArrow extends Projectile
{

	/**
	 * Creates a new archer arrow projectile
	 * 
	 * @param world
	 *            The world the arrow is being created in
	 * @param x
	 *            The x-coordinate in the world
	 * @param y
	 *            The y-coordinate in the world
	 * @param texture
	 *            The texture of the arrow
	 * @param damage
	 *            The damage done on impact by the arrow
	 * @param firing
	 *            The entity firing the arrow
	 */
	public ArcherArrow(World world, float x, float y, TextureRegion texture, float damage, Entity firing)
	{
		super(world, x, y, texture, 8, (float) Math.atan2(Core.getInput().getMouse().getY() - Core.getWindow().getHeight() / 2, Core.getInput().getMouse().getX() - Core.getWindow().getWidth() / 2), 200, damage, "Normal", firing);
	}

	/**
	 * Creates a new archer arrow projectile
	 * 
	 * @param world
	 *            The world the arrow is being created in
	 * @param x
	 *            The x-coordinate in the world
	 * @param y
	 *            The y-coordinate in the world
	 * @param texture
	 *            The texture of the arrow
	 * @param damage
	 *            The damage done on impact by the arrow
	 * @param firing
	 *            The entity firing the arrow
	 * @param theta
	 *            The angle the arrow will be pointed
	 */
	public ArcherArrow(World world, float x, float y, TextureRegion texture, float damage, Entity firing, float theta)
	{
		super(world, x, y, texture, 8, theta, 200, damage, "Normal", firing);
		setRotation(theta - (float) Math.toRadians(90));
	}

	/**
	 * Creates a new archer arrow projectile
	 * 
	 * @param world
	 *            The world this arrow is being created in
	 * @param x
	 *            The x-coordinate in the world
	 * @param y
	 *            The y-coordinate in the world
	 * @param texture
	 *            The texture for this arrow
	 * @param type
	 *            The ability type this arrow is being used for
	 * @param damage
	 *            The damage done on impact by this arrow
	 * @param firing
	 *            The entity firing this arrow
	 * @param theta
	 *            The angle this projectile will face when fired. Zero degrees
	 *            faces right.
	 */
	public ArcherArrow(World world, float x, float y, TextureRegion texture, String type, float damage, Entity firing, float theta)
	{
		super(world, x, y, texture, 8, theta, 200, damage, type, firing);
		setRotation(theta - (float) Math.toRadians(90));
		setScale(2f);
	}

	@Override
	public void update()
	{
		super.update();
		if (!canMoveX(getVelocity().getX()) || !canMoveY(getVelocity().getY()))
		{
			getWorld().getProjectiles().remove(this);
		}
	}

}
