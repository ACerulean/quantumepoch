package quantumepoch.game.world.entity.mob;

import quantumepoch.game.Textures;
import quantumepoch.game.world.World;
import quantumepoch.game.world.entity.mob.battle.BattlePlayer;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.animation.TextureAnimation;

/** Class of a potion that can move across the screen */
public class MovingPotion extends MobileEntity
{
	/** The texture animation of this potion */
	private TextureAnimation anim;

	public MovingPotion(World world, float x, float y, BattlePlayer player)
	{
		super(world, x, y, 16, 16, Color.RED, 10);
		anim = new TextureAnimation(Textures.POTIONS.getTexture(), 16, 16, 150, new int[] { 0, 1, 2, 3 });
		anim.start();

		setScale(5f);
	}

	/** Animates the texture */
	@Override
	public void update()
	{
		anim.update();
		setTexture(anim.getCurrentFrame().getTexture());
	}

}
