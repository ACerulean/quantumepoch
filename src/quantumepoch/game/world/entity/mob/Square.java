package quantumepoch.game.world.entity.mob;

import quantumepoch.game.Textures;
import quantumepoch.game.world.World;
import quantumepoch.graphics.animation.TextureAnimation;
import quantumepoch.graphics.texture.TextureRegion;

/** Class for the basic enemy square. Can come in 6 colors and levels. */
public class Square extends Enemy
{

	/**
	 * Creates a new square.
	 * 
	 * @param world
	 *            The world this square is located in
	 * @param x
	 *            The x-coordinate in the world
	 * @param y
	 *            The y-coordinate in the world
	 * @param texture
	 *            The square entity's texture
	 * @param level
	 *            The square difficulty level
	 */
	public Square(World world, float x, float y, TextureRegion texture, int level)
	{
		super(world, x, y, texture, level * 5 + 5, level);
		anim = new TextureAnimation(Textures.ENEMIES.getTexture(), 48, 48, 0, 48 * 2, 48 * 2, 48 * 1, 150, new int[] { 0, 1, 2, 1 });
		anim.start();
		name = "Block";
	}
}
