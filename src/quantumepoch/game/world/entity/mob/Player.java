package quantumepoch.game.world.entity.mob;

import java.io.IOException;

import org.lwjgl.input.Keyboard;

import quantumepoch.core.Core;
import quantumepoch.game.CharacterType;
import quantumepoch.game.Game;
import quantumepoch.game.Textures;
import quantumepoch.game.tetrisbattle.TetrisBattle;
import quantumepoch.game.world.World;
import quantumepoch.game.world.entity.mob.battle.BattleEnemy;
import quantumepoch.game.world.entity.mob.battle.BattlePlayer;
import quantumepoch.game.world.tile.Tile;
import quantumepoch.game.world.tile.TilePortal;
import quantumepoch.graphics.Bitmap;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.animation.TextureAnimation;
import quantumepoch.graphics.gui.Component;
import quantumepoch.graphics.lighting.PointLight;
import quantumepoch.graphics.texture.TextureRegion;
import quantumepoch.utils.math.Vector3f;

/**
 * The class for the player entity that you will be able to control the movements of with your keyboard and mouse. The player will be able to level up when the experience fills up and will include a world navigator on its HUD that will allow the player to move around to others worlds. If the player entity is in the proximity of another mod than the player will be able to battle it.
 */
public class Player extends MobileEntity
{
	/** Width of the player's experience bar */
	public static final int EXP_BAR_WIDTH = 300;
	/** Height of the player's experience bar */
	public static final int EXP_BAR_HEIGHT = 10;
	/** Range at which the player will be in the proximity of another entity */
	public static final int PROXIMITY = 150;

	/** Mobile entity that is within the proximity of the player */
	private MobileEntity closeMob;

	private MobileEntity closestMob;

	/** How fast the player can move across the world */
	private float speed = 1;

	/** Texture animation for the player entity */
	private TextureAnimation anim;
	/** Width of the experience bar */
	private WorldNavigator navigator;

	private PointLight playerLight;

	/**
	 * Creates a new player
	 * 
	 * @param world
	 *            The world this player is located in
	 * @param x
	 *            The x-coordinate in the world
	 * @param y
	 *            The y-coordinate in the world
	 * @param texture
	 *            The player entity's texture
	 */
	public Player(World world, float x, float y, TextureRegion texture)
	{
		super(world, x, y, texture, 10 + (int) ( Game.getInstance().getCurrentProfile().getLevel() * 3));

		CharacterType type = Game.getInstance().getCurrentProfile().getCharacterType();

		setWidth(48);
		setHeight(48);

		if (type == CharacterType.KNIGHT)
		{
			anim = new TextureAnimation(Textures.PLAYER.getTexture(), 48, 48, 0, 0, 48 * 5, 48 * 1, 100, new int[] { 0, 1, 2, 1, 0, 3, 4, 3 });
		} else if (type == CharacterType.WIZARD)
		{
			anim = new TextureAnimation(Textures.PLAYER.getTexture(), 48, 48, 0, 48, 48 * 5, 48 * 1, 100, new int[] { 0, 1, 2, 1, 0, 3, 4, 3 });
		} else if (type == CharacterType.ARCHER)
		{
			anim = new TextureAnimation(Textures.PLAYER.getTexture(), 48, 48, 0, 48 * 2, 48 * 5, 48 * 1, 100, new int[] { 0, 1, 2, 1, 0, 3, 4, 3 });
		}

		if (anim != null)
			anim.start();

		// File base = new File("/worlds");//
		// IOUtils.getInternalResourceAsFile("/worlds");
		Bitmap[] bitmaps = new Bitmap[9];

		try
		{
			bitmaps[0] = new Bitmap(Player.class.getResourceAsStream("/worlds/tutorialWorld/tiles.png"));
			bitmaps[1] = new Bitmap(Player.class.getResourceAsStream("/worlds/plainsWorld/tiles.png"));
			bitmaps[2] = new Bitmap(Player.class.getResourceAsStream("/worlds/forestWorld/tiles.png"));
			bitmaps[3] = new Bitmap(Player.class.getResourceAsStream("/worlds/caveWorld/tiles.png"));
			bitmaps[4] = new Bitmap(Player.class.getResourceAsStream("/worlds/desertWorld/tiles.png"));
			bitmaps[5] = new Bitmap(Player.class.getResourceAsStream("/worlds/oceanWorld/tiles.png"));
			bitmaps[6] = new Bitmap(Player.class.getResourceAsStream("/worlds/tundraWorld/tiles.png"));
			bitmaps[7] = new Bitmap(Player.class.getResourceAsStream("/worlds/skyWorld/tiles.png"));
			bitmaps[8] = new Bitmap(Player.class.getResourceAsStream("/worlds/spaceWorld/tiles.png"));
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		/*
		 * if (IOUtils.isJar()) { bitmaps[0] = new Bitmap(Player.class.getResourceAsStream("/worlds/plainsWorld/tiles.png")); bitmaps[0] = new Bitmap(Player.class.getResourceAsStream("/worlds/plainsWorld/tiles.png")); bitmaps[0] = new Bitmap(Player.class.getResourceAsStream("/worlds/plainsWorld/tiles.png")); bitmaps[0] = new Bitmap(Player.class.getResourceAsStream("/worlds/plainsWorld/tiles.png")); bitmaps[0] = new Bitmap(Player.class.getResourceAsStream("/worlds/plainsWorld/tiles.png"));
		 * bitmaps[0] = new Bitmap(Player.class.getResourceAsStream("/worlds/plainsWorld/tiles.png")); bitmaps[0] = new Bitmap(Player.class.getResourceAsStream("/worlds/plainsWorld/tiles.png")); bitmaps[0] = new Bitmap(Player.class.getResourceAsStream("/worlds/plainsWorld/tiles.png")); } else { File base = IOUtils.getInternalResourceAsFile("/worlds"); int counter = 0; for (File file : base.listFiles()) { for (File f : file.listFiles()) if (f.getName().contains(".png")) bitmaps[counter] = new
		 * Bitmap(IOUtils.loadInteralImage("worlds/" + file.getName() + "/" + f.getName()), true); counter++; } }
		 */

		navigator = new WorldNavigator(bitmaps);

		playerLight = new PointLight(new Vector3f(Core.getWindow().getWidth() / 2, Core.getWindow().getHeight() / 2, 0), Color.WHITE, 1, 7f, 0, 0.0005f);
	}

	/**
	 * @return the player texture animation
	 */
	public TextureAnimation getAnimation()
	{
		return anim;
	}

	/**
	 * @return returns the mob that is considered to be in this player's circle of influence
	 */
	public MobileEntity getInProximityMob()
	{
		return closeMob;
	}

	/**
	 * Update the player's location in a world. If you press the space key you can sprint and if you walk on water you are slowed down. Also the player is restricted from walking on certain textures, such as stone, trees, cactuses and rocks.
	 */
	@Override
	public void update()
	{

		updateMovement();

		setTexture(anim.getCurrentFrame().getTexture());

		setRotation((float) -Math.atan2(Core.getInput().getMouse().getY() - (Core.getWindow().getHeight() / 2), Core.getInput().getMouse().getX() - (Core.getWindow().getWidth() / 2)) - (float) Math.toRadians(90));

		if (isInWater())
		{
			if (Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_SPACE))
			{
				speed = 1;
			} else
			{
				speed = 0.2f;
			}
		} else
		{
			if (Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_SPACE))
			{
				speed = 10;
			} else
			{
				speed = 1;
			}
		}

		if (getWorld().getBattle() != null)
		{
			if (Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_DOWN))
			{
				getWorld().getBattle().getPlayerTetrisGrid().setFallSpeed(50);
			} else
			{
				getWorld().getBattle().getPlayerTetrisGrid().setFallSpeed(500);
			}
		} else
		{
			boolean moveX = false;
			boolean moveY = false;
			if (Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_W) && canMoveY(getVelocity().getY()))
			{
				getAcceleration().setY(0.9f);
				moveY = true;
			}
			if (Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_S) && canMoveY(getVelocity().getY()))
			{
				getAcceleration().setY(-0.9f);
				moveY = true;
			}
			if (Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_A) && canMoveX(-getVelocity().getX()))
			{
				getAcceleration().setX(-0.9f);
				moveX = true;
			}
			if (Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_D) && canMoveX(getVelocity().getX()))
			{
				getAcceleration().setX(0.9f);
				moveX = true;
			}

			if (!moveX)
			{
				dampAccelerationX(0.1f);
				if (getAcceleration().getX() == 0)
					dampVelocityX(0.1f);
			}

			if (!moveY)
			{
				dampAccelerationY(0.1f);
				if (getAcceleration().getY() == 0)
					dampVelocityY(0.1f);
			}

			if (!canMoveX(2 * getVelocity().getX() + getAcceleration().getX()))
			{
				resetAccelerationX();
				resetVelocityX();
			}

			if (!canMoveY(2 * getVelocity().getY() + getAcceleration().getY()))
			{
				resetAccelerationY();
				resetVelocityY();
			}

			if (Math.abs(getVelocity().getX()) >= speed)
				getVelocity().setX(Math.signum(getVelocity().getX()) * speed);

			if (Math.abs(getVelocity().getY()) >= speed)
				getVelocity().setY(Math.signum(getVelocity().getY()) * speed);

			if (getVelocity().getX() == 0 && getVelocity().getY() == 0)
			{
				anim.setCurrentFrame(0);
			} else
			{
				anim.update();
			}

			getWorld().centerCameraOnEntity(this);

			if (!getWorld().getMobs().contains(closestMob))
				closestMob = null;
			if (!getWorld().getMobs().contains(closeMob))
				closeMob = null;

			for (MobileEntity mob : getWorld().getMobs())
			{
				if (mob instanceof Enemy)
				{
					if (distance(mob) < PROXIMITY)
					{
						closeMob = mob;
						if (Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_RETURN))
						{
							// initiate battle
							TetrisBattle battle = new TetrisBattle(getWorld());

							battle.init(new BattlePlayer(getWorld(), this, battle), BattleEnemy.createBattleEnemy(getWorld(), (Enemy) closeMob, battle));

							getWorld().setBattle(battle);
						}
						break;
					} else
					{
						closeMob = null;
					}
				}
			}

			for (MobileEntity mob : getWorld().getMobs())
				if (mob != getWorld().findPlayer() && (closestMob == null || distance(mob) < distance(closestMob)))
					closestMob = mob;
		}

		for (float x = getX(); x <= getX() + getWidth(); x += getWidth())
		{
			for (float y = getY(); y <= getY() + getHeight(); y += getHeight())
			{
				Tile t = getWorld().getTileAt(x, y);

				if (t instanceof TilePortal)
				{
					((TilePortal) t).tp(this);
				}
			}
		}

		Component.setFocused(null);
		navigator.update();

	}

	/**
	 * Renders the player texture and animation, player name, experience bar, and the world navigator to the screen.
	 */
	@Override
	public void render()
	{
		super.render();

	}

	public void renderInterface()
	{
		// render other interface elements
		String name = Game.getInstance().getCurrentProfile().getName();
		Game.getInstance().getFontRenderer().render(Game.CS_15, name, getX() + getWidth() / 2 - Game.CS_15.getStringWidth(name) / 2 + getWorld().getTranslateX(), getY() + 42 + getWorld().getTranslateY());

		// coordinates
		if (Game.getInstance().getDebugger().isDisplayed())
		{
			int cx = Core.getWindow().getWidth() - 300, cy = Core.getWindow().getHeight() - 50;
			Game.getInstance().getFontRenderer().render(Game.CALIBRI_30, "X: " + String.format("%.2f", getX() / Tile.SIZE) + " Y: " + String.format("%.2f", getY() / Tile.SIZE), cx, cy);
		}

		String expText = "Level: " + Game.getInstance().getCurrentProfile().getLevel();
		float amount = Game.getInstance().getCurrentProfile().getExperiance();
		int expX = Core.getWindow().getWidth() / 2 - EXP_BAR_WIDTH / 2;
		int expY = 20;
		int expTextX = Core.getWindow().getWidth() / 2 - Game.CALIBRI_30.getStringWidth(expText) / 2;
		Game.getInstance().getShapeBatch().setColor(Color.WHITE);
		Game.getInstance().getShapeBatch().renderRect(expX, expY, EXP_BAR_WIDTH, EXP_BAR_HEIGHT, 0, 0, 0);
		Game.getInstance().getShapeBatch().setColor(Color.GREEN);
		Game.getInstance().getShapeBatch().renderFilledRect(expX, expY, (amount * EXP_BAR_WIDTH) / Game.getInstance().getCurrentProfile().getRequiredExperiance(), EXP_BAR_HEIGHT);

		Game.getInstance().getFontRenderer().render(Game.CALIBRI_30, expText, expTextX, expY + 10);

		if (closeMob != null)
		{
			Game.getInstance().getShapeBatch().setColor(Color.GRAY);
			Game.getInstance().getShapeBatch().renderCircle(closeMob.getX() + closeMob.getWidth() / 2 + getWorld().getTranslateX(), closeMob.getY() + closeMob.getHeight() / 2 + getWorld().getTranslateY(), PROXIMITY);
		}

		if (closestMob != null && closeMob == null)
		{
			float mx = getX() + getWidth() / 2, my = getY() + getHeight() / 2;
			float dx = closestMob.getX() + closestMob.getWidth() / 2 - mx, dy = closestMob.getY() + closestMob.getHeight() / 2 - my;
			double angle = Math.atan2(dy, dx);

			float sx = Core.getWindow().getWidth() / 2, sy = Core.getWindow().getHeight() / 2;
			float endX = sx + 95 * (float) Math.cos(angle), endY = sy + 95 * (float) Math.sin(angle);
			Game.getInstance().getShapeBatch().setColor(Color.ORANGE);
			Game.getInstance().getShapeBatch().renderLine(sx + 45 * (float) Math.cos(angle), sy + 45 * (float) Math.sin(angle), endX, endY);
			Game.getInstance().getShapeBatch().renderLine(endX, endY, endX + 20 * (float) Math.cos(angle - Math.toRadians(90 + 30)), endY + 20 * (float) Math.sin(angle - Math.toRadians(90 + 30)));
			Game.getInstance().getShapeBatch().renderLine(endX, endY, endX + 20 * (float) Math.cos(angle + Math.toRadians(90 + 30)), endY + 20 * (float) Math.sin(angle + Math.toRadians(90 + 30)));
		}

		if (Game.getInstance().getCurrentProfile().getProfileOptions().getGraphicsSetting() < 2)
		{
			Game.getInstance().getLightRenderer().renderPointLight(playerLight, false);
		} else
		{
			Game.getInstance().getLightRenderer().renderPointLight(playerLight, true);
		}

		navigator.render();
	}
}
