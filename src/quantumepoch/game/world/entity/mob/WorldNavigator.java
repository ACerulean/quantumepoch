package quantumepoch.game.world.entity.mob;

import quantumepoch.core.Core;

import quantumepoch.game.Game;
import quantumepoch.game.world.World;
import quantumepoch.game.world.entity.particle.FireworkParticle;
import quantumepoch.graphics.Bitmap;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.gui.ImageTextButton;
import quantumepoch.graphics.texture.Texture;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * Hud element at the right side of the screen that will allow the player to
 * telepot between worlds when all of the enemies in a world have been
 * eliminated.
 */
public class WorldNavigator
{
	/** The world file names. */
	public static final String[] WORLD_FILE_NAMES = { "tutorialWorld", "plainsWorld", "forestWorld", "caveWorld", "desertWorld", "oceanWorld", "tundraWorld", "skyWorld", "spaceWorld" };
	/** The buttons that when clicked will telepot the player. */
	private ImageTextButton[] worldButtons;

	/** The size of the world navigator. */
	private static final int SIZE = 256;
	/** The size of the padding. */
	private static final int PADDING = 10;
	/** How much the world nav will be scaled up by. */
	private static final float SCALE = 0.2f;

	/**
	 * Creates a new world navigator.
	 * 
	 * @param worlds
	 *            The list of world bitmap files.
	 */
	public WorldNavigator(Bitmap[] worlds)
	{
		worldButtons = new ImageTextButton[worlds.length];

		// load textures
		TextureRegion[] textures = new TextureRegion[worlds.length];
		for (int i = 0; i < worlds.length; i++)
			textures[i] = new TextureRegion(new Texture(worlds[i]).load());

		int x = Core.getWindow().getWidth() - (int) (SIZE * SCALE) - PADDING;
		int i = 0;
		for (int y = 10; y <= worlds.length * (SIZE * SCALE + PADDING); y += (SIZE * SCALE + PADDING))
		{
			worldButtons[i] = new ImageTextButton(null, x, y, textures[i], textures[i], textures[i], "", Game.CALIBRI_20, Color.WHITE, Game.getInstance().getBlip());
			worldButtons[i].setScaleX(SCALE);
			worldButtons[i].setScaleY(SCALE);
			i++;
		}
	}

	/**
	 * Teleports the player when the player clicks on the world nav and shoots
	 * fireowrkds when they do so.
	 */
	public void update()
	{
		for (int i = 0; i < worldButtons.length; i++)
		{
			worldButtons[i].update();
			if (worldButtons[i].wasPressed())
				if (i < Game.ADVENTURE_MODE_STATE.getWorlds().getLoadedWorlds().length && Game.ADVENTURE_MODE_STATE.getWorlds().getWorld(i) != null)
				{
					Game.ADVENTURE_MODE_STATE.setCurrentWorld(Game.ADVENTURE_MODE_STATE.getWorlds().getWorld(i));

					World world = Game.ADVENTURE_MODE_STATE.getCurrentWorld();
					for (int j = 0; j < 50; j++)
						world.getParticles().add(new FireworkParticle(world, world.findPlayer().getX() + world.findPlayer().getWidth() / 2, world.findPlayer().getY() + world.findPlayer().getHeight() / 2, 20, 120, 2));
				}
		}
	}

	/**
	 * Renders the world nav to the screen.
	 */
	public void render()
	{
		for (int i = 0; i < worldButtons.length; i++)
		{
			if (i > Game.getInstance().getCurrentProfile().getWorld())
				worldButtons[i].setTint(new Color(0x11, 0x11, 0x11));
			else
				worldButtons[i].setTint(Color.WHITE);
			worldButtons[i].render(Game.getInstance().getSpriteBatch(), Game.getInstance().getFontRenderer());
		}
	}
}
