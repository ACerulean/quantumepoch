package quantumepoch.game.world.entity.mob;

import quantumepoch.game.world.World;
import quantumepoch.graphics.texture.TextureRegion;

/** An abstract class for bosses. */
public abstract class Boss extends Enemy
{
	/**
	 * Creates a new giant slime boss.
	 * 
	 * @param world
	 *            The world this slime is located in.
	 * @param x
	 *            The x-coordinate in the world.
	 * @param y
	 *            The y-coordinate in the world.
	 * @param texture
	 *            The slime entity's texture.
	 * @param level
	 *            The slime difficulty level.
	 */
	public Boss(World world, float x, float y, TextureRegion texture, int level)
	{
		super(world, x, y, texture, level, level);
	}
}
