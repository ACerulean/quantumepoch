package quantumepoch.game.world.entity.mob;

import quantumepoch.game.Textures;
import quantumepoch.game.world.World;
import quantumepoch.graphics.animation.TextureAnimation;
import quantumepoch.graphics.texture.TextureRegion;

/** A boss that is a giant purple slime at level 8. */
public class SlimeBoss extends Boss
{
	/**
	 * Creates a new giant slime boss.
	 * 
	 * @param world
	 *            The world this slime is located in.
	 * @param x
	 *            The x-coordinate in the world.
	 * @param y
	 *            The y-coordinate in the world.
	 * @param texture
	 *            The slime entity's texture.
	 * @param level
	 *            The slime difficulty level.
	 */
	public SlimeBoss(World world, float x, float y, TextureRegion texture, int level)
	{
		super(world, x, y, texture, level);
		anim = new TextureAnimation(Textures.ENEMIES.getTexture(), 48, 48, 0, 48 * 4, 48 * 2, 48 * 1, 150, new int[] { 0, 1, 2, 1 });
		setScale(5f);
		anim.start();
		name = "Slime Boss";
		textOffset = 70;
	}
}
