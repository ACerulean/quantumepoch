package quantumepoch.game.world.entity.mob;

import quantumepoch.game.world.World;
import quantumepoch.game.world.entity.Entity;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.texture.TextureRegion;
import quantumepoch.utils.Delay;

/**
 * Abstract class for movable entities
 */
public abstract class MobileEntity extends Entity
{
	/** The amount of health the entity can have */
	private float maxHealth;
	/** The amount of health the entity does have */
	private float health;
	/** Default tint of mob */
	private Color defaultTint = Color.WHITE;
	/** If the player is burning or not */
	private boolean isBurning;
	/** How often the effects of being burned or frozen take place */
	private Delay delay;
	/** How many seconds the mob is burned or frozen */
	protected int sec = -1;
	/** Amount of damage taken when burned */
	protected float burnMag;
	/** If the mob is frozen of not */
	private boolean isFrozen;
	/** How often the effects of being blinded take place */
	private Delay dDelay;
	/** How many seconds the mob is blinded */
	protected int secDisabled = -1;
	/** If the mob can see to attack */
	private boolean isDisabled;

	/**
	 * Creates a new enemy entity
	 * 
	 * @param world
	 *            The world this entity will be loaded into
	 * @param x
	 *            The x-coordinate of the world
	 * @param y
	 *            The y-coordinate of the world
	 * @param width
	 *            The width of this entity
	 * @param height
	 *            The height of this entity
	 * @param tint
	 *            The tint of the texture of this entity
	 * @param maxHealth
	 *            The total amount of health the entity may have
	 */
	public MobileEntity(World world, float x, float y, float width, float height, Color tint, float maxHealth)
	{
		super(world, x, y, width, height, tint);
		this.maxHealth = maxHealth;
		health = maxHealth;
		delay = new Delay(1000);
		defaultTint = tint;
	}

	/**
	 * Creates a new enemy entity
	 * 
	 * @param world
	 *            The world this entity will be loaded into
	 * @param x
	 *            The x-coordinate of the world
	 * @param y
	 *            The y-coordinate of the world
	 * @param width
	 *            The width of this entity
	 * @param height
	 *            The height of this entity
	 * @param texture
	 *            The texture for this entity
	 * @param tint
	 *            The tint of the texture of this entity
	 * @param maxHealth
	 *            The total amount of health the entity may have
	 */
	public MobileEntity(World world, float x, float y, float width, float height, TextureRegion texture, Color tint, float maxHealth)
	{
		super(world, x, y, width, height, texture, tint);
		this.maxHealth = maxHealth;
		health = maxHealth;
		delay = new Delay(1000);
		defaultTint = tint;
	}

	/**
	 * Creates a new enemy entity
	 * 
	 * @param world
	 *            The world this entity will be loaded into
	 * @param x
	 *            The x-coordinate of the world
	 * @param y
	 *            The y-coordinate of the world
	 * @param texture
	 *            The texture for this entity
	 * @param tint
	 *            The tint of the texture of this entity
	 * @param maxHealth
	 *            The total amount of health the entity may have
	 */
	public MobileEntity(World world, float x, float y, TextureRegion texture, Color tint, float maxHealth)
	{
		super(world, x, y, texture, tint);
		this.maxHealth = maxHealth;
		health = maxHealth;
		delay = new Delay(1000);
		defaultTint = tint;
	}

	/**
	 * Creates a new enemy entity
	 * 
	 * @param world
	 *            The world this entity will be loaded into
	 * @param x
	 *            The x-coordinate of the world
	 * @param y
	 *            The y-coordinate of the world
	 * @param texture
	 *            The texture for this entity
	 * @param maxHealth
	 *            The total amount of health the entity may have
	 */
	public MobileEntity(World world, float x, float y, TextureRegion texture, float maxHealth)
	{
		super(world, x, y, texture);
		this.maxHealth = maxHealth;
		health = maxHealth;
		delay = new Delay(1000);
	}

	/**
	 * Check whether the entity is capable of moving in a certain direction
	 * 
	 * @param dx
	 *            The x-coordinate of where the entity is trying to move
	 * @param dy
	 *            The y-coordinate of where the entity is trying to move
	 * @return true if the entity can move in the given direction, and false
	 *         otherwise
	 */
	public boolean canMove(float dx, float dy)
	{
		return canMoveX(dx) && canMoveY(dy);
	}
	
	public float getMaxHealth()
	{
		return maxHealth;
	}
	
	public void setMaxHealth(float maxHealth)
	{
		this.maxHealth = maxHealth;
	}

	/**
	 * @return the health of the entity
	 */
	public float getHealth()
	{
		return health;
	}

	/**
	 * Sets the health of this entity
	 * 
	 * @param health
	 *            The new health
	 */
	public void setHealth(float health)
	{
		this.health = health;
	}

	/**
	 * Decreases the health of this entity by a certain magnitude
	 * 
	 * @param mag
	 *            The magnitude at which the health will decrease by
	 */
	public void decreaseHealth(float mag)
	{
		health -= mag;
	}

	/**
	 * @return the default tint of this mob
	 */
	public Color getDefaultTint()
	{
		return defaultTint;
	}

	public void setDefaultTint(Color tint)
	{
		defaultTint = tint;
	}

	/**
	 * @return if this mob is burning
	 */
	public boolean isBurning()
	{
		return isBurning;
	}

	/**
	 * @return if this mob is frozen
	 */
	public boolean isFrozen()
	{
		return isFrozen;
	}

	/**
	 * @return if this mob is blinded
	 */
	public boolean isDisabled()
	{
		return isDisabled;
	}

	public void thaw()
	{
		sec = -1;
		isFrozen = false;
	}

	public void unblind()
	{
		sec = -1;
		isDisabled = false;
	}

	/**
	 * Burns this mob for the amount of time set
	 * 
	 * @param mag
	 *            How much burn damage is taken each second.
	 * @param time
	 *            How long this mob is burned
	 */
	public void burn(float mag, int burnTime)
	{
		isBurning = true;
		if (sec == -1)
		{
			sec = burnTime;
			delay = new Delay(1000);
			burnMag = mag;
		}
		if (sec == 0 || health - burnMag <= 1)
		{
			sec = -1;
			isBurning = false;
		}
		if (delay.isReady())
		{
			decreaseHealth(mag);
			sec--;
		}
	}

	/**
	 * Freezes this mob for the amount of time set
	 * 
	 * @param freezeTime
	 *            The time frozen
	 */
	public void freeze(int freezeTime)
	{
		isFrozen = true;
		if (sec == -1)
		{
			sec = freezeTime;
			delay = new Delay(1000);
		}
		if (sec == 0)
		{
			thaw();
		}
		if (delay.isReady())
		{
			sec--;
		}
	}

	/**
	 * Blinds this mob so that they cannot attack for a certain set of time
	 * 
	 * @param blindTime
	 *            The time blinded
	 */
	public void disable(int dTime)
	{
		isDisabled = true;
		if (secDisabled == -1)
		{
			secDisabled = dTime;
			dDelay = new Delay(1000);
		}
		if (sec == 0)
		{
			unblind();
		}
		if (dDelay.isReady())
		{
			secDisabled--;
		}
	}
}
