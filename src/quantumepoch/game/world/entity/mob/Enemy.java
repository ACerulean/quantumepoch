package quantumepoch.game.world.entity.mob;

import quantumepoch.game.Game;
import quantumepoch.game.inventory.Ability;
import quantumepoch.game.world.World;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.animation.TextureAnimation;
import quantumepoch.graphics.lighting.PointLight;
import quantumepoch.graphics.texture.TextureRegion;
import quantumepoch.utils.math.Vector3f;

/**
 * Abstract class for movable enemy entities that appear around a world that you can battle by pressing enter
 */
public abstract class Enemy extends MobileEntity
{
	/** The different colors of a slime, ordered based difficulty */
	public static final Color[] LEVEL_COLORS = { Color.LIME_GREEN, Color.GOLD, Color.RED, Color.FUCHSIA, Color.BLUE, Color.PURPLE, Color.PURPLE, Color.PURPLE, Color.PURPLE, Color.PURPLE, Color.PURPLE, Color.PURPLE, Color.PURPLE };
	/** Difficulty level of the enemy */
	protected int level;
	/** Texture animation of the enemy */
	protected TextureAnimation anim;
	/** The light of the enemy */
	protected PointLight light;
	/** the name of this enemy */
	protected String name;
	/** The ability the entity has */
	protected Ability abil;

	/** the amount of exp given on kill */
	private int exp;

	/** the text offset rendering for this enemy */
	protected int textOffset = 50;

	/**
	 * Creates a new enemy entity
	 * 
	 * @param world
	 *            The world this enemy will be loaded into
	 * @param x
	 *            The x-coordinate of the world
	 * @param y
	 *            The y-coordinate of the world
	 * @param width
	 *            The width of this enemy
	 * @param height
	 *            The height of this enemy
	 * @param tint
	 *            The tint of the texture of this enemy
	 */
	public Enemy(World world, float x, float y, float width, float height, Color tint, int exp, int level)
	{
		super(world, x, y, width, height, tint, 10 + (int) (level * 5));
		setExp(exp);
		setLevel(level);
		initLight(x, y);
		if (level >= LEVEL_COLORS.length)
			setDefaultTint(Color.WHITE);
		else
			setDefaultTint(LEVEL_COLORS[level]);
	}

	/**
	 * Creates a new enemy entity
	 * 
	 * @param world
	 *            The world this enemy entity will be loaded into
	 * @param x
	 *            The x-coordinate of the world
	 * @param y
	 *            The y-coordinate of the world
	 * @param width
	 *            The width of this enemy entity
	 * @param height
	 *            The height of this enemy entity
	 * @param texture
	 *            The texture for this enemy entity
	 * @param tint
	 *            The tint of the texture of this enemy entity
	 */
	public Enemy(World world, float x, float y, float width, float height, TextureRegion texture, Color tint, int exp, int level)
	{
		super(world, x, y, width, height, texture, tint, 10 + (int) (level * 5));
		setExp(exp);
		setLevel(level);
		initLight(x, y);

		if (level >= LEVEL_COLORS.length)
			setDefaultTint(Color.WHITE);
		else
			setDefaultTint(LEVEL_COLORS[level]);
	}

	/**
	 * Creates a new enemy entity
	 * 
	 * @param world
	 *            The world this enemy entity will be loaded into
	 * @param x
	 *            The x-coordinate of the world
	 * @param y
	 *            The y-coordinate of the world
	 * @param texture
	 *            The texture for this enemy entity
	 * @param tint
	 *            The tint of the texture of this enemy entity
	 */
	public Enemy(World world, float x, float y, TextureRegion texture, Color tint, int exp, int level)
	{
		super(world, x, y, texture, tint, 10 + (int) (level * 5));
		setExp(exp);
		setLevel(level);
		initLight(x, y);

		if (level >= LEVEL_COLORS.length)
			setDefaultTint(Color.WHITE);
		else
			setDefaultTint(LEVEL_COLORS[level]);
	}

	/**
	 * Creates a new enemy entity
	 * 
	 * @param world
	 *            The world this enemy entity will be loaded into
	 * @param x
	 *            The x-coordinate of the world
	 * @param y
	 *            The y-coordinate of the world
	 * @param texture
	 *            The texture for this enemy entity
	 */
	public Enemy(World world, float x, float y, TextureRegion texture, int exp, int level)
	{
		super(world, x, y, texture, 10 + (int) (level * 2.5));
		setExp(exp);
		setLevel(level);
		initLight(x, y);
		if (level >= LEVEL_COLORS.length)
			setDefaultTint(Color.WHITE);
		else
			setDefaultTint(LEVEL_COLORS[level]);
	}

	public void initLight(float x, float y)
	{
		Color c = Color.WHITE;
		if (level < LEVEL_COLORS.length)
			c = LEVEL_COLORS[level];
		light = new PointLight(new Vector3f(x, y, 0), c, 1, 5, 0, 0.0005f);
	}

	/**
	 * @return the amount of experience given by defeating this enemy
	 */
	public int getExp()
	{
		return exp;
	}

	/**
	 * Sets how much experience this enemy gives.
	 * 
	 * @param exp
	 *            The new experience amount.
	 */
	public void setExp(int exp)
	{
		this.exp = exp;
	}

	/**
	 * @return the level of this enemy
	 */
	public int getLevel()
	{
		return level;
	}

	/**
	 * Sets the difficulty level of this enemy.
	 * 
	 * @param level
	 *            The new level.
	 */
	public void setLevel(int level)
	{
		if (level >= LEVEL_COLORS.length)
			setTint(Color.WHITE);
		else
			setTint(LEVEL_COLORS[level]);
		this.level = level;
	}

	/**
	 * @return the ability that this enemy has
	 */
	public Ability getAbility()
	{
		return abil;
	}

	/**
	 * @return the texture animation of this enemy
	 */
	public TextureAnimation getAnimation()
	{
		return anim;
	}

	@Override
	public void update()
	{
		if (anim != null)
		{
			anim.update();
			setTexture(anim.getCurrentFrame().getTexture());
		}
		light.getPosition().setX(getX() + getWorld().getTranslateX() + getWidth() / 2);
		light.getPosition().setY(getY() + getWorld().getTranslateY() + getHeight() / 2);

		if (this instanceof Boss)
			setLevel(8 + 2 * (Game.getInstance().getCurrentProfile().getWorld() - 2));
	}

	@Override
	public void render()
	{
		super.render();

		float x = (getWidth() - Game.CALIBRI_20.getStringWidth(name + " level " + level)) / 2;
		float dist = getWorld().findPlayer().distance(this);

		if (getWorld().findPlayer().getInProximityMob() == this)
			Game.getInstance().getFontRenderer().render(Game.CALIBRI_20, name + " level " + level, getX() + x + getWorld().getTranslateX(), getY() + getWorld().getTranslateY() + textOffset);
		if (dist < 1000f)
		{
			if (Game.getInstance().getCurrentProfile().getProfileOptions().getGraphicsSetting() == 1)
			{
				Game.getInstance().getLightRenderer().renderPointLight(light, false);
			} else if (Game.getInstance().getCurrentProfile().getProfileOptions().getGraphicsSetting() == 2)
			{
				Game.getInstance().getLightRenderer().renderPointLight(light, true);
			}
		}
	}
}
