package quantumepoch.game.world.entity.mob.battle;

import quantumepoch.audio.AudioBuffer;
import quantumepoch.audio.AudioSource;
import quantumepoch.game.Sounds;
import quantumepoch.game.tetrisbattle.TetrisBattle;
import quantumepoch.game.world.World;
import quantumepoch.game.world.entity.mob.Enemy;
import quantumepoch.game.world.entity.projectile.EnemyProjectile;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.animation.TextureAnimation;
import quantumepoch.utils.Delay;

/**
 * The battel enemy class that includes basic enemies such as slimes, stumps,
 * and squares/blocks.
 */
public class BasicBattleEnemy extends BattleEnemy
{
	/** The delay between each attack made my this enemy. */
	private Delay nextMove;
	/** The difficulty to defeat this enemy. */
	private int diff;
	/** The texture animation for this enemy. */
	private TextureAnimation animation;
	/** The audio source for the enemy sounds. */
	private AudioSource shootSource;

	// private Delay moveDelay;
	// private int rotTry;
	// private int dir = 1;

	/**
	 * Creates a new basic battle enemy.
	 * 
	 * @param world
	 *            The world this basic battle enemy is located in.
	 * @param x
	 *            The x-coordinate in the world.
	 * @param y
	 *            The y-coordinate in the world.
	 * @param enemy
	 *            The enemy entity.
	 * @param battle
	 *            The battle this entity is apart of.
	 * @param difficulty
	 *            The difficulty it is to defeat this enemy.
	 * @param tint
	 *            The color tint of this enemy.
	 * @param anim
	 *            The texture animation of this enemy.
	 */
	public BasicBattleEnemy(World world, float x, float y, Enemy enemy, TetrisBattle battle, int difficulty, Color tint, TextureAnimation anim)
	{
		super(world, x, y, enemy, enemy.getTexture(), battle, enemy.getHealth());
		diff = difficulty;
		setDefaultTint(tint);
		nextDelay();
		this.animation = anim;
		animation.stop();
		shootSource = new AudioSource(new AudioBuffer(Sounds.ENEMY_SHOOT.getTrack().getFormat(), Sounds.ENEMY_SHOOT.getTrack().getData(), Sounds.ENEMY_SHOOT.getTrack().getSampleRate()));
	}

	/**
	 * Creates the next delay in this enemies attacks.
	 */
	public void nextDelay()
	{
		nextMove = new Delay( 13000 + (int) (Math.random() * 5000) - diff);
	}

	@Override
	public void update()
	{
		super.update();
		setTint(getDefaultTint());
		animation.update();
		setTexture(animation.getCurrentFrame().getTexture());
		if (animation.isStarted() && animation.getCurrentFrameIndex() == 0)
			animation.stop();
		if (nextMove.isReady() && (!isFrozen() || !isDisabled()))
		{
			shootSource.play();
			nextDelay();
			getWorld().getBattle().getProjectiles().add(new EnemyProjectile(getWorld(), getX() + getWidth() / 2, getY() + getHeight() / 2, (float) (Math.PI), getEnemy().getLevel() / 2 + 1, this));
			animation.start();
		} else if (isFrozen())
		{
			setTint(Color.LIGHT_BLUE);
		}
	}
}
