package quantumepoch.game.world.entity.mob.battle;

import quantumepoch.core.Core;
import quantumepoch.game.Game;
import quantumepoch.game.RenderUtils;
import quantumepoch.game.inventory.Ability;
import quantumepoch.game.tetris.Cell;
import quantumepoch.game.tetrisbattle.TetrisBattle;
import quantumepoch.game.world.World;
import quantumepoch.game.world.entity.mob.Enemy;
import quantumepoch.game.world.entity.mob.MobileEntity;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * An abstract class used to create enemies that the player will battle.
 */
public abstract class BattleEnemy extends MobileEntity
{
	/** The tetris battle this entity is in. */
	protected TetrisBattle battle;
	/** The enemy of this battle enemy. */
	private Enemy enemy;
	/** This battle enemy's attack. */
	private Ability ability;

	/**
	 * Creates a new battle enemy.
	 * 
	 * @param world
	 *            The world this basic battle enemy is located in.
	 * @param x
	 *            The x-coordinate in the world.
	 * @param y
	 *            The y-coordinate in the world.
	 * @param enemy
	 *            The enemy entity.
	 * @param texture
	 *            The texture region for this entity.
	 * @param battle
	 *            The tetris battle this entity is apart of.
	 * @param maxHealth
	 *            The max health of this entity.
	 */
	public BattleEnemy(World world, float x, float y, Enemy enemy, TextureRegion texture, TetrisBattle battle, float maxHealth)
	{
		super(world, x, y, texture, enemy.getHealth());
		this.battle = battle;
		this.enemy = enemy;

		setTint(enemy.getTint());
		setTexture(enemy.getTexture());
	}

	/**
	 * @return the enemy entity this battle entity is for
	 */
	public Enemy getEnemy()
	{
		return enemy;
	}

	/**
	 * @return the current ability being used by this battle enemy
	 */
	public Ability getAbility()
	{
		return ability;
	}

	@Override
	public void update()
	{
		if (sec > -1 && isBurning())
			burn(burnMag, 1);
		else if (sec > -1 && isFrozen())
			freeze(1);
	}

	@Override
	public void render()
	{
		super.render();
		final int w = (int) (2 * getWidth()), h = 10;
		RenderUtils.renderHealthBar(getX() + (getWidth() - w) / 2, getY() + getHeight() + 28, w, h, getHealth() / getMaxHealth(), Color.RED, Color.GREEN);
	}

	public static BattleEnemy createBattleEnemy(World world, Enemy enemy, TetrisBattle battle)
	{
		final float x = (Core.getWindow().getWidth() - Cell.SIZE * battle.getPlayerTetrisGrid().getWidth() - 300) + ((Cell.SIZE * battle.getPlayerTetrisGrid().getWidth() - 48) / 2);
		final float y = 30 + battle.getPlayerTetrisGrid().getHeight() * Cell.SIZE;
		return new BasicBattleEnemy(world, x, y, enemy, battle, enemy.getLevel() * 200, enemy.getTint(), enemy.getAnimation());
	}
}
