package quantumepoch.game.world.entity.mob.battle;

import org.lwjgl.input.Keyboard;

import quantumepoch.core.Core;
import quantumepoch.game.CharacterType;
import quantumepoch.game.Game;
import quantumepoch.game.RenderUtils;
import quantumepoch.game.Textures;
import quantumepoch.game.gui.AbilityButton;
import quantumepoch.game.inventory.Ability;
import quantumepoch.game.inventory.BlindingLight;
import quantumepoch.game.inventory.FireArrow;
import quantumepoch.game.inventory.FireBlast;
import quantumepoch.game.inventory.FireSlash;
import quantumepoch.game.inventory.GroundStrike;
import quantumepoch.game.inventory.IceArrow;
import quantumepoch.game.inventory.LightningStrike;
import quantumepoch.game.inventory.PlasmaBall;
import quantumepoch.game.inventory.PowerShot;
import quantumepoch.game.inventory.ShootArrow;
import quantumepoch.game.inventory.Slash;
import quantumepoch.game.inventory.SwordStun;
import quantumepoch.game.tetris.Cell;
import quantumepoch.game.tetrisbattle.TetrisBattle;
import quantumepoch.game.world.World;
import quantumepoch.game.world.entity.mob.MobileEntity;
import quantumepoch.game.world.entity.mob.Player;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.animation.TextureAnimation;
import quantumepoch.graphics.texture.TextureRegion;

/** The player entity during a tetris battle. */
public class BattlePlayer extends MobileEntity
{
	/** Array of Ability objects. */
	private Ability[] abilities;
	/** Array of AbilityButton objects. */
	private AbilityButton[] buttons;
	/** The index of the current player ability. */
	private int ability;
	/** The player entity. */
	private Player player;
	/** The texture animation for the player. */
	private TextureAnimation anim;
	
	private int level;
	

	/**
	 * Creates a new battle player.
	 * 
	 * @param world
	 *            The world this battle player is located in.
	 * @param player
	 *            The player entity of this battle player,
	 * @param battle
	 *            The tetris battle this battle player is apart of.
	 */
	public BattlePlayer(World world, Player player, TetrisBattle battle)
	{
		super(world, 300 + ((battle.getPlayerTetrisGrid().getWidth() * Cell.SIZE) / 2) - player.getTexture().getWidth() / 2, 30 + battle.getPlayerTetrisGrid().getHeight() * Cell.SIZE, player.getTexture(),  10 + (Game.getInstance().getCurrentProfile().getLevel() * 5.7f));

		CharacterType type = Game.getInstance().getCurrentProfile().getCharacterType();

		abilities = new Ability[4];
		if (type == CharacterType.KNIGHT)
		{
			abilities[0] = new Slash("Slash", new TextureRegion(Textures.GUI_ABILITIES.getTexture(), 0 * 32, 2 * 32, 32, 32), 0, this, 6f + level * 2);
			abilities[1] = new FireSlash("Fire Slash", new TextureRegion(Textures.GUI_ABILITIES.getTexture(), 1 * 32, 2 * 32, 32, 32), 0, this, 6f + level * 2, .5f * (level * 3), 5 * level);
			abilities[2] = new SwordStun("Sword Stun", new TextureRegion(Textures.GUI_ABILITIES.getTexture(), 2 * 32, 2 * 32, 32, 32), 0, this, 3f + level * 2, 3 + level);
			abilities[3] = new GroundStrike("Ground Strike", new TextureRegion(Textures.GUI_ABILITIES.getTexture(), 3 * 32, 2 * 32, 32, 32), 0, this, 12f  + level * 3);
			anim = new TextureAnimation(Textures.PLAYER_PLATFORM.getTexture(), 48, 72, 0, 0, 48 * 2, 72, 100, new int[] { 2, 1, 0, 1 }).stop();
		} else if (type == CharacterType.WIZARD)
		{
			abilities[0] = new PlasmaBall("Plasma Ball", new TextureRegion(Textures.GUI_ABILITIES.getTexture(), 2 * 32, 32, 32, 32), 0, this, 5f + level * 2);
			abilities[1] = new FireBlast("Fire Blast", new TextureRegion(Textures.GUI_ABILITIES.getTexture(), 1 * 32, 32, 32, 32), 0, this, 0f + level * 3, 1.5f * (level * 3), 10 * level);
			abilities[2] = new BlindingLight("Blinding Light", new TextureRegion(Textures.GUI_ABILITIES.getTexture(), 3 * 32, 32, 32, 32), 0, this, 7 + (level / 3));
			abilities[3] = new LightningStrike("Lightning Strike", new TextureRegion(Textures.GUI_ABILITIES.getTexture(), 0 * 32, 32, 32, 32), 0, this, 8f  + level * 3, 3 + (level / 3));
			anim = new TextureAnimation(Textures.PLAYER_PLATFORM.getTexture(), 48, 72, 0, 72, 48 * 2, 72, 100, new int[] { 2, 1, 0, 1 }).stop();
		} else if (type == CharacterType.ARCHER)
		{
			abilities[0] = new ShootArrow("Shoot Arrow", new TextureRegion(Textures.PROJECTILES.getTexture(), 2 * 16, 0, 16, 16), 0, this, 4f + level * 3);
			abilities[1] = new FireArrow("Fire Arrow", new TextureRegion(Textures.PROJECTILES.getTexture(), 3 * 16, 0, 16, 16), 0, this, 5f + level, .5f * (level * 3), 10 * level);
			abilities[2] = new IceArrow("Ice Arrow", new TextureRegion(Textures.PROJECTILES.getTexture(), 4 * 16, 0, 16, 16), 0, this, 4f + level * 2, 5 + level / 3);
			abilities[3] = new PowerShot("Power Shot", new TextureRegion(Textures.PROJECTILES.getTexture(), 5 * 16, 0, 16, 16), 0, this, 10f + level * 3);
			for (Ability a : abilities)
			{
				a.setScale(2);
				a.setRotation((float) Math.toRadians(180));
			}
			anim = new TextureAnimation(Textures.PLAYER_PLATFORM.getTexture(), 48, 72, 0, 72 * 2, 48 * 2, 72, 100, new int[] { 2, 1, 0, 1 }).stop();
		}
		ability = 0;

		buttons = new AbilityButton[4];
		for (int i = 0; i < 4; i++)
			buttons[i] = new AbilityButton(null, 10 + i * 75, 200, abilities[i]);
		this.player = player;
		level = Game.getInstance().getCurrentProfile().getLevel();
	}

	/**
	 * @return the player entity
	 */
	public Player getPlayer()
	{
		return player;
	}

	/**
	 * @return array of abilities the battle player has for the character type.
	 */
	public Ability[] getAbilities()
	{
		return abilities;
	}

	/**
	 * @return the current ability
	 */
	public Ability getAbility()
	{
		return abilities[ability];
	}

	/**
	 * Plays the player animation, and allows you to select and use your
	 * abilities.
	 */
	@Override
	public void update()
	{

		if (anim != null)
		{
			anim.update();
			setTexture(anim.getCurrentFrame().getTexture());

			if (anim.isStarted() && anim.getCurrentFrameIndex() == 0)
				anim.stop();
		}

		for (int i = 0; i < 4; i++)
			if (buttons[i] != null)
				buttons[i].update();

		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_RETURN) && (getAbility().getChargePercent() >= 1 || (getAbility().equals(abilities[0]) && getAbility().getChargePercent() > 0)))
		{
			if (anim != null)
				anim.start();
			getAbility().use();
			getAbility().setChargePercent(0);
		}

		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_1))
		{
			ability = 0;
		}

		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_2))
		{
			if (level >= 2)
				ability = 1;
		}

		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_3))
		{
			if (level >= 6)
				ability = 2;
		}

		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_4))
		{
			if (level >= 12)
				ability = 3;
		}
	}

	/**
	 * Lowers the amount of color tint.
	 */
	private void scaleDownTint()
	{
		Game.getInstance().getSpriteBatch().setTint(Game.getInstance().getSpriteBatch().getTint().scale(0.4f));
		Game.getInstance().getShapeBatch().setColor(Game.getInstance().getShapeBatch().getColor().scale(0.4f));
	}

	/**
	 * Sets the tint the battle player would be normally.
	 */
	private void normalTint()
	{
		Game.getInstance().getSpriteBatch().setTint(Color.WHITE);
		Game.getInstance().getShapeBatch().setColor(Color.WHITE);
	}

	/** Renders the ability buttons, charge bar, and health bar. */
	@Override
	public void render()
	{
		super.render();
		buttons[0].render();
		if (level >= 2)
			normalTint();
		else
			scaleDownTint();
		buttons[1].render();
		if (level >= 6)
			normalTint();
		else
			scaleDownTint();
		buttons[2].render();
		if (level >= 12)
			normalTint();
		else
			scaleDownTint();
		buttons[3].render();
		normalTint();
		Game.getInstance().getShapeBatch().setColor(Color.WHITE);
		Game.getInstance().getShapeBatch().renderRect(buttons[ability].getScreenX() - 3, buttons[ability].getScreenY() - 10, 38, 350, 0, 0, 0);
		RenderUtils.renderHealthBar(this, getHealth() / getMaxHealth(), Color.RED, Color.GREEN);
	}

}
