package quantumepoch.game.world.entity.mob;

import quantumepoch.game.Textures;
import quantumepoch.game.world.World;
import quantumepoch.graphics.animation.TextureAnimation;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * Class for the slime; which is the most basic and common of the enemies. The
 * slime can come in six colors which indicate how hard the slime will be able
 * to defeat.
 */
public class Slime extends Enemy
{

	/**
	 * Creates a new slime
	 * 
	 * @param world
	 *            The world this slime is located in
	 * @param x
	 *            The x-coordinate in the world
	 * @param y
	 *            The y-coordinate in the world
	 * @param texture
	 *            The slime entity's texture
	 * @param level
	 *            The slime difficulty level
	 */
	public Slime(World world, float x, float y, TextureRegion texture, int level)
	{
		super(world, x, y, texture, level * 5 + 5, level);
		anim = new TextureAnimation(Textures.ENEMIES.getTexture(), 48, 48, 0, 48 * 4, 48 * 2, 48 * 1, 150, new int[] { 0, 1, 2, 1 });
		anim.start();
		name = "Slime";
	}
}
