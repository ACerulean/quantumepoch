package quantumepoch.game.world.entity;

import quantumepoch.utils.math.Vector2f;

/**
 * Represents a point for a moving object to move to.
 */
public class NodePoint
{

	/** the position of this node */
	private Vector2f position;
	/** the speed to move to this node */
	private float speed;

	/** Creates a new node point */
	public NodePoint()
	{
		position = new Vector2f();
		speed = 0;
	}

	/** Creates a new node point with the specified position and speed */
	public NodePoint(Vector2f pos, float speed)
	{
		this.position = pos;
		this.speed = speed;
	}

	/**
	 * @return the position of this node point
	 */
	public Vector2f getPosition()
	{
		return position;
	}

	/**
	 * Sets the position of this node point
	 * 
	 * @param position
	 *            The new position of this node point
	 */
	public void setPosition(Vector2f position)
	{
		this.position = position;
	}

	/**
	 * @return the speed to travel to this node point
	 */
	public float getSpeed()
	{
		return speed;
	}

	/**
	 * Sets the speed required to travel to this node point
	 * 
	 * @param speed
	 *            The new speed to travel to this node point
	 */
	public void setSpeed(float speed)
	{
		this.speed = speed;
	}

}
