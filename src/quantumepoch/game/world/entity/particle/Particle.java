package quantumepoch.game.world.entity.particle;

import quantumepoch.game.world.World;
import quantumepoch.game.world.entity.Entity;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.texture.TextureRegion;
import quantumepoch.utils.math.Vector2f;

/**
 * A single particle in a world
 */
public abstract class Particle extends Entity
{

	/** the life of this particle in ticks */
	private int life;
	/** the amount of ticks that this particle has lived */
	private int ticksLived;
	/** whetehr or not this particle is affected by gravity */
	private boolean gravityAffected;

	protected Vector2f gravity = new Vector2f(0, -0.2f);

	/**
	 * Creates a new particle
	 * 
	 * @param world
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param tint
	 * @param ms
	 *            The max speed of the particle
	 * @param life
	 *            The max life of the particle
	 */
	public Particle(World world, float x, float y, float width, float height, Color tint, float ms, int life)
	{
		super(world, x, y, width, height, tint);
		randomize(ms, life);
	}

	public Vector2f getGravity()
	{
		return gravity;
	}

	public void setGravity(Vector2f gravity)
	{
		this.gravity = gravity;
	}

	/**
	 * Creates a new particle
	 * 
	 * @param world
	 * @param x
	 * @param y
	 * @param texture
	 * @param ms
	 *            The max speed of the particle
	 * @param life
	 *            The max life of the particle
	 */
	public Particle(World world, float x, float y, TextureRegion texture, float ms, int life)
	{
		super(world, x, y, texture);
		randomize(ms, life);
	}

	/**
	 * @return whether this particle is affected by gravity
	 */
	public boolean isGravityAffected()
	{
		return gravityAffected;
	}

	/**
	 * Sets whether this particle iis affected by gravity
	 * 
	 * @param ga
	 *            Whether this particle is affected by gravity
	 */
	public void setGravityAffected(boolean ga)
	{
		this.gravityAffected = ga;
	}

	/**
	 * Gives the particle a random velocity
	 * 
	 * @param ms
	 *            The max speed of this particle
	 * @param life
	 *            The max life of this particle
	 */
	private void randomize(float ms, int life)
	{
		float rx = (float) (Math.random() * ms - ms / 2);
		float ry = (float) (Math.random() * ms - ms / 2);

		getVelocity().set(new Vector2f(rx, ry));
		this.life = life;
		this.ticksLived = 0;

		if (getWorld().getBattle() != null)
		{
			setGravityAffected(true);
			this.life *= 10;
		}
	}

	@Override
	public void update()
	{
		ticksLived++;

		updateMovement();

		if (gravityAffected)
		{
			getAcceleration().setY(gravity.getY());
		}

		if (ticksLived >= life)
		{
			getWorld().getParticles().remove(this);
			if (getWorld().getBattle() != null)
			{
				getWorld().getBattle().getParticles().remove(this);
			}
		}
	}
}
