package quantumepoch.game.world.entity.particle;

import quantumepoch.game.world.World;
import quantumepoch.utils.Delay;
import quantumepoch.utils.math.Vector2f;

/**
 * Emits a certain amount of particles every time frame.
 */
public class ParticleEmitter
{

	/** the world to emit particles in */
	private World world;
	/** the position of the emitter */
	private Vector2f pos;
	/** the count of particles each release */
	private int count;
	/** the integer type of particle */
	private int type;
	/** the delay until particles are released */
	private Delay delay;

	/**
	 * Creates a new particle emitter
	 * 
	 * @param world
	 * @param pos
	 * @param count
	 * @param type
	 *            The type of particle to release
	 * @param delay
	 *            The delay until new particles are released
	 */
	public ParticleEmitter(World world, Vector2f pos, int count, int type, Delay delay)
	{
		this.world = world;
		this.pos = pos;
		this.count = count;
		this.type = type;
		this.delay = delay;
	}

	/**
	 * @return the world of this particle emitter
	 */
	public World getWorld()
	{
		return world;
	}

	/**
	 * @return the position of this particle emitter
	 */
	public Vector2f getPos()
	{
		return pos;
	}

	/**
	 * Sets the position of this particle emitter
	 * 
	 * @param pos
	 *            The new position of this particle emitter
	 */
	public void setPos(Vector2f pos)
	{
		this.pos = pos;
	}

	/**
	 * @return the amount of particles released each time frame of this emitter
	 */
	public int getCount()
	{
		return count;
	}

	/**
	 * Sets the amount of particles released each time frame
	 * 
	 * @param count
	 *            The amount of particles released each time frame
	 */
	public void setCount(int count)
	{
		this.count = count;
	}

	/**
	 * @return the type of particle released
	 */
	public int getType()
	{
		return type;
	}

	/**
	 * Sets the type of particle released.
	 * 
	 * @param type
	 *            The type of particle released
	 */
	public void setType(int type)
	{
		this.type = type;
	}

	/**
	 * @return The delay until a new particle is released
	 */
	public Delay getDelay()
	{
		return delay;
	}

	/**
	 * Sets the delay, the time it takes to release another particle.
	 * 
	 * @param delay
	 *            The new delay
	 */
	public void setDelay(Delay delay)
	{
		this.delay = delay;
	}

	/**
	 * Creates a new particle and places it into the world
	 * 
	 * @return The particle created
	 */
	public Particle makeParticle()
	{
		Particle p = null;
		if (type == 0)
		{
			p = new WizardParticle(world, pos.getX(), pos.getY(), 10);
		} else if (type == 1)
		{
			p = new TetrisParticle(world, pos.getX(), pos.getY(), 2, 120);
		}
		return p;
	}

	/**
	 * Updates this emitter, releasing particles when possible.
	 */
	public void update()
	{
		if (delay.isReady())
		{
			for (int i = 0; i < count; i++)
			{
				Particle p = makeParticle();
				if (p != null)
				{
					if (world.getBattle() != null)
						world.getBattle().getParticles().add(p);
					else
						world.getParticles().add(p);
				}
			}
		}
	}

}
