package quantumepoch.game.world.entity.particle;

import quantumepoch.game.Game;
import quantumepoch.game.world.World;
import quantumepoch.graphics.Color;
import quantumepoch.utils.math.Vector2f;

public class TextParticle extends Particle
{

	private String text;

	public TextParticle(World world, float x, float y, float width, float height, Color tint, float ms, int life, String text)
	{
		super(world, x, y, width, height, tint, ms, life);
		this.text = text;
		setGravity(new Vector2f(0, -0.03f));
	}

	@Override
	public void update()
	{
		super.update();
	}

	@Override
	public void render()
	{
		Game.getInstance().getSpriteBatch().reset();
		Game.getInstance().getShapeBatch().reset();
		Game.getInstance().getFontRenderer().setColor(getTint());
		Game.getInstance().getFontRenderer().render(Game.CALIBRI_20, text, getX() + getWorld().getTranslateX(), getY() + getWorld().getTranslateY());
		Game.getInstance().getFontRenderer().setColor(Color.WHITE);
	}

}
