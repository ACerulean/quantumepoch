package quantumepoch.game.world.entity.particle;

import quantumepoch.game.Textures;
import quantumepoch.game.world.World;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * A circular particle used in wizard balls.
 */
public class WizardParticle extends Particle
{

	/** the texture of the particle */
	public static final TextureRegion region = new TextureRegion(Textures.PROJECTILES.getTexture(), 21, 5, 5, 5);

	/**
	 * Creates a new wizard particle.
	 * 
	 * @param world
	 * @param x
	 * @param y
	 * @param life
	 */
	public WizardParticle(World world, float x, float y, int life)
	{
		super(world, x, y, region, 5, life);
		setTint(Color.CORNFLOWER_BLUE);
	}

	@Override
	public void update()
	{
		super.update();
//		if (!canMoveX(getVelocity().getX()))
//		{
//			getVelocity().setX(-getVelocity().getX());
//		}
//
//		if (!canMoveY(getVelocity().getY()))
//		{
//			getVelocity().setY(-getVelocity().getY());
//		}
	}

}
