package quantumepoch.game.world.entity.particle;

import quantumepoch.game.world.World;
import quantumepoch.graphics.Color;

/**
 * A randomly colored square particle.
 */
public class FireworkParticle extends Particle
{

	/**
	 * Creates a new firework particle
	 * 
	 * @param world
	 * @param x
	 * @param y
	 * @param ms
	 * @param life
	 * @param s
	 */
	public FireworkParticle(World world, float x, float y, float ms, int life, float s)
	{
		super(world, x, y, s, s, new Color((int) (Math.random() * 0xFFFFFF)), ms, life);
		setGravityAffected(true);
	}

}
