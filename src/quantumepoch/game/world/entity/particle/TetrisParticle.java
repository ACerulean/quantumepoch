package quantumepoch.game.world.entity.particle;

import quantumepoch.game.TetrisStarfield;
import quantumepoch.game.world.World;

/**
 * A particle shaped as a tetris piece
 */
public class TetrisParticle extends Particle
{

	/** the speed of rotation */
	private float rotSpeed = 0;

	/**
	 * Creates a new tetris particle
	 * 
	 * @param world
	 * @param x
	 * @param y
	 * @param ms
	 * @param life
	 */
	public TetrisParticle(World world, float x, float y, float ms, int life)
	{
		super(world, x, y, TetrisStarfield.PIECES[0], ms, life);
		int i = (int) (Math.random() * 6);
		setTexture(TetrisStarfield.PIECES[i]);
		setTint(TetrisStarfield.COLORS[i]);
		setScale(0.2f);
		setWidth(getTexture().getWidth() * getScale());
		setHeight(getTexture().getHeight() * getScale());

		rotSpeed = (float) Math.random() / 10f * 0.5f - (1 / 10f);
	}

	@Override
	public void update()
	{
		super.update();
		 setRotation(getRotation() + rotSpeed);
		/*
		 * if (!canMoveX(getVelocity().getX())) { getVelocity().setX(-getVelocity().getX()); }
		 * 
		 * if (!canMoveY(getVelocity().getY())) { getVelocity().setY(-getVelocity().getY()); }
		 */
	}
}
