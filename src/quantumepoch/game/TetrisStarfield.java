package quantumepoch.game;

import java.util.ArrayList;
import java.util.List;

import quantumepoch.core.Core;
import quantumepoch.graphics.BlendFunction;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.batch.SpriteBatch;
import quantumepoch.graphics.lighting.PointLight;
import quantumepoch.graphics.texture.TextureRegion;
import quantumepoch.utils.math.Vector3f;

/**
 * A cool background tetris starfield effect
 */
public class TetrisStarfield
{

	/** the texture pieces of the starfield */
	public static final TextureRegion[] PIECES = { new TextureRegion(Textures.MENU_TETRIS_PIECES.getTexture(), 0, 0, 24 * 4, 24), new TextureRegion(Textures.MENU_TETRIS_PIECES.getTexture(), 0, 24 * 5, 24 * 2, 24 * 3), new TextureRegion(Textures.MENU_TETRIS_PIECES.getTexture(), 0, 24 * 5, 24 * 2, 24 * 3), new TextureRegion(Textures.MENU_TETRIS_PIECES.getTexture(), 0, 24 * 2, 24 * 3, 24 * 2), new TextureRegion(Textures.MENU_TETRIS_PIECES.getTexture(), 24 * 4, 24 * 4, 24 * 3, 24 * 2),
			new TextureRegion(Textures.MENU_TETRIS_PIECES.getTexture(), 24 * 4, 24 * 7, 24 * 3, 24 * 2), new TextureRegion(Textures.MENU_TETRIS_PIECES.getTexture(), 24 * 2, 24 * 4, 24 * 2, 24 * 2) };
	/** the colors of the pieces */
	public static final Color[] COLORS = new Color[] { Color.RED, Color.BLUE, Color.GREEN, Color.INDIGO, Color.CYAN, Color.CORNFLOWER_BLUE, Color.GOLD };

	/** the starfield pieces */
	private List<StarfieldPiece> pieces;
	/** the stars represented as lights */
	private List<PointLight> stars;

	/**
	 * Creates a new tetris starfield
	 */
	public TetrisStarfield()
	{
		pieces = new ArrayList<StarfieldPiece>();

		for (int i = 0; i < 200; i++)
			addPiece();

		stars = new ArrayList<PointLight>();

		for (int i = 0; i < 5; i++)
			addStar();
	}

	/**
	 * @return the starfield pieces
	 */
	public List<StarfieldPiece> getPieces()
	{
		return pieces;
	}

	/**
	 * @return the stars
	 */
	public List<PointLight> getStars()
	{
		return stars;
	}

	/**
	 * Adds a new random piece to this starfield
	 */
	public void addPiece()
	{
		int rand = (int) (Math.random() * PIECES.length);
		float rx = (float) (Math.random() * 2 - 1);
		float ry = (float) (Math.random() * 2 - 1);
		float rz = (float) (Math.random() + 0.9f);

		pieces.add(new StarfieldPiece(new Vector3f(rx, ry, rz), PIECES[rand], COLORS[rand], 0, (float) (Math.random() * 2 - 1) / 20f));

	}

	/**
	 * Adds a new random star to this starfield
	 */
	public void addStar()
	{
		float rx = (float) (Math.random() * 2 - 1);
		float ry = (float) (Math.random() * 2 - 1);
		float rz = (float) (Math.random() + 1f);

		stars.add(new PointLight(new Vector3f(rx, ry, rz), new Color((int) (Math.random() * 0xFFFFFF)), 1, 0, 0.1f, 0));
	}

	public int getGraphicsSetting()
	{
		int gfx = 2;
		if (Game.getInstance().getCurrentProfile() != null)
			gfx = Game.getInstance().getCurrentProfile().getProfileOptions().getGraphicsSetting();
		return gfx;
	}

	/**
	 * Updates this starfield
	 */
	public void update()
	{

		for (StarfieldPiece piece : pieces)
		{
			piece.getPos().setZ(piece.getPos().getZ() - 0.005f);
			piece.update();
		}

		int gfx = getGraphicsSetting();
		if (gfx > 0)
			for (PointLight star : stars)
			{
				star.getPosition().setZ(star.getPosition().getZ() - 0.005f);
			}
	}

	/**
	 * Renders this starfield
	 * 
	 * @param batch
	 *            The SpriteBatch to render textures with
	 */
	public void render(SpriteBatch batch)
	{

		if (!batch.hasBegan())
			batch.begin();
		else
			batch.reset();

		batch.setTint(Color.WHITE);
		batch.render(Textures.SPACE_BG.getTexture(), 0, 0, 0, 0, 0, Core.getWindow().getWidth() / (float) Textures.SPACE_BG.getTexture().getWidth(), Core.getWindow().getHeight() / (float) Textures.SPACE_BG.getTexture().getHeight());

		Core.getGraphics().setBlendingEnabled(true);
		Core.getGraphics().setBlendFunction(BlendFunction.ALPHA_BLEND);
		final float hw = Core.getWindow().getWidth() / 2;
		final float hh = Core.getWindow().getHeight() / 2;

		List<StarfieldPiece> rem = new ArrayList<StarfieldPiece>();
		List<StarfieldPiece> renderedPieces = new ArrayList<StarfieldPiece>(pieces);
		for (int i = 0; i < pieces.size(); i++)
		{
			int far = 0;

			for (int j = 0; j < renderedPieces.size(); j++)
			{
				if (renderedPieces.get(j).getPos().getZ() > renderedPieces.get(far).getPos().getZ())
				{
					far = j;
				}
			}

			StarfieldPiece piece = renderedPieces.get(far);
			renderedPieces.remove(far);

			float z = piece.getPos().getZ();

			float halfTanFOV = (float) Math.tan(180 / 2f);

			if (z <= 0)
			{
				rem.add(piece);
			} else
			{
				float x = (((piece.getPos().getX()) / (z * halfTanFOV)) * hw + hw);
				float y = (((piece.getPos().getY()) / (z * halfTanFOV)) * hh + hh);

				batch.setTint(piece.getColor());

				float scale = z > 1 ? 0 : 1 - z;
				batch.render(piece.getTexture(), piece.getTexture().getWidth() / 2, piece.getTexture().getHeight(), x, y, piece.getRot(), scale, scale);
			}

		}

		for (StarfieldPiece piece : rem)
		{
			pieces.remove(piece);
			addPiece();
		}

		batch.end();
		if (getGraphicsSetting() > 0)
		{
			Core.getGraphics().setBlendingEnabled(true);
			Core.getGraphics().setBlendFunction(BlendFunction.ADDITIVE);
			List<PointLight> remStars = new ArrayList<PointLight>();
			List<PointLight> renderedStars = new ArrayList<PointLight>(stars);
			for (int i = 0; i < stars.size(); i++)
			{
				int far = 0;

				for (int j = 0; j < renderedStars.size(); j++)
				{
					if (renderedStars.get(j).getPosition().getZ() > renderedStars.get(far).getPosition().getZ())
					{
						far = j;
					}
				}

				PointLight star = renderedStars.get(far);
				renderedStars.remove(far);

				float z = star.getPosition().getZ();

				float halfTanFOV = (float) Math.tan(180 / 2f);

				if (z <= 0)
				{
					remStars.add(star);
				} else
				{
					float x = (((star.getPosition().getX()) / (z * halfTanFOV)) * hw + hw);
					float y = (((star.getPosition().getY()) / (z * halfTanFOV)) * hh + hh);

					star.setConstAtten(0.1f);
					star.setLinearAtten(z);
					Game.getInstance().getLightRenderer().renderPointLight(new PointLight(new Vector3f(x, y, 1), star.getColor(), 1, star.getConstAtten(), star.getLinearAtten(), star.getQuadAtten()), true);
				}

			}

			for (PointLight light : remStars)
			{
				stars.remove(light);
				addStar();
			}
		}
		Core.getGraphics().setBlendingEnabled(true);
		Core.getGraphics().setBlendFunction(BlendFunction.ALPHA_BLEND);

	}
}
