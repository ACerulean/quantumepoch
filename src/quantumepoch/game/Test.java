package quantumepoch.game;

import java.util.ArrayList;
import java.util.List;

import quantumepoch.core.Application;
import quantumepoch.core.Core;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.batch.SpriteBatch;
import quantumepoch.graphics.lighting.Light2DRenderer;
import quantumepoch.graphics.lighting.PointLight;
import quantumepoch.graphics.texture.Texture;
import quantumepoch.graphics.texture.TextureRegion;
import quantumepoch.physics.BoundingBox2D;
import quantumepoch.utils.math.Vector2f;
import quantumepoch.utils.math.Vector3f;

public class Test implements Application
{

	public static final Texture SHEET = new Texture("textures/sprites/Ground_Tiles.png");

	private List<PointLight> lights;
	private Light2DRenderer renderer;
	private TextureRegion grass;
	private SpriteBatch spriteBatch;

	public static void main(String[] args)
	{
		new Core(null, new Test(), true, "Test", 800, 600, false, 60, 60, 60, 60);
	}

	@Override
	public void init()
	{
		spriteBatch = new SpriteBatch();
		lights = new ArrayList<PointLight>();
		lights.add(new PointLight(new Vector3f(100, 100, 0), Color.WHITE, 0.5f, 1.99f, 0.0f, 0.0001f));
		renderer = new Light2DRenderer();
		Core.getInput().getMouse().setGrabbed(true);

		SHEET.load();
		grass = new TextureRegion(SHEET, 1 * 32, 7 * 32, 32, 32);
		spriteBatch.getAmbient().setColor(spriteBatch.getAmbient().getColor().scale(0.3f));

	}

	@Override
	public void resized()
	{

	}

	@Override
	public void update()
	{
		int x = Core.getInput().getMouse().getX();
		int y = Core.getInput().getMouse().getY();
		lights.get(0).getPosition().set(x, y, 0);

		if (Core.getInput().getMouse().wasButtonPressed(0))
		{
			renderer.getOccluders().add(new BoundingBox2D(new Vector2f(x - 10, y - 10), 20, 20));
		}
		if (Core.getInput().getMouse().wasButtonPressed(1))
		{
			Color rand = new Color((int) (Math.random() * 0xFFFFFF));
			lights.add(new PointLight(new Vector3f(x, y, 0), rand, 0.5f, 1f, 0.0f, 0.0001f));
		}

		int scrollAmount = Core.getInput().getMouse().getDWheel();
		float mask = Math.abs((lights.get(0).getConstAtten() + scrollAmount * 0.001f) % 20);
		lights.get(0).setConstAtten(mask);
	}

	@Override
	public void render()
	{
		spriteBatch.begin();
		for (int x = 0; x < 32; x++)
			for (int y = 0; y < 32; y++)
				spriteBatch.render(grass, x * 32, y * 32);
		spriteBatch.end();

		for (PointLight light : lights)
			renderer.renderPointLight(light, true);
		// renderer.renderPointLight(light2, true);
	}

	@Override
	public void dispose()
	{

	}

}
