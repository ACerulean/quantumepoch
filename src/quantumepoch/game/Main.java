package quantumepoch.game;

import java.util.ArrayList;

import quantumepoch.core.Core;
import quantumepoch.core.SplashScreen;
import quantumepoch.game.splash.CTMScreen;
import quantumepoch.game.splash.IntroScreen;

/**
 * Launches the QuantumEpoch game.
 */
public class Main
{

	/** the title of the game window */
	public static final String TITLE = "Quantum Epoch";
	/** the width of the game window */
	public static final int WIDTH = 16 * 70;
	/** the height of the game window */
	public static final int HEIGHT = 9 * 70;
	/** whether the window is resizable */
	public static final boolean RESIZABLE = false;
	/** the maximum frames per second */
	public static final int FPS_CAP = 60;
	/** the maximum updates per second */
	public static final int UPS_CAP = 60;

	/** the singleton reference, stored here for user friendliness */
	private static Core core = null;

	/**
	 * The main entry point of the game
	 * 
	 * @param args
	 *            Command line arguments, shouldn't be any in this case
	 */

	public static void main(String[] args)
	{

		Game game = new Game();

		ArrayList<SplashScreen> splashScreens = new ArrayList<SplashScreen>();
		splashScreens.add(new CTMScreen());
		splashScreens.add(new IntroScreen());
		
		System.out.printf("%10d", 9);

		core = new Core(splashScreens, game, false, TITLE, WIDTH, HEIGHT, RESIZABLE, FPS_CAP, UPS_CAP, FPS_CAP, UPS_CAP);
		core.start();
	}

	/**
	 * @return the singleton instance of core
	 */
	public static Core getCore()
	{
		return core;
	}

}
