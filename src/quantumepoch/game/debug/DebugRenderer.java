package quantumepoch.game.debug;

import java.awt.Font;

import org.lwjgl.input.Keyboard;

import quantumepoch.core.Core;
import quantumepoch.game.Game;
import quantumepoch.graphics.font.FontType;
import quantumepoch.graphics.font.TrueTypeFont;

/**
 * A class which provides useful tools for drawing debug info to the screen.
 */
public class DebugRenderer
{

	/** the debug info to render */
	private String[] debugInfo;
	/** the amount of verticle spacing between entries */
	private int verticleSpacing;
	/** whether or not to display the debug info */
	private boolean display;
	/** the font to render the debug information in */
	private TrueTypeFont font;

	/**
	 * Creates a new debug renderer
	 */
	public DebugRenderer()
	{
		debugInfo = null;

		font = new TrueTypeFont(FontType.CALIBRI, 50, true);
		font.loadFont();

		verticleSpacing = 50;
		display = true;
	}

	/**
	 * @return the verticle spacing between debug entries
	 */
	public int getVerticleSpacing()
	{
		return verticleSpacing;
	}

	/**
	 * @return the debug entries to render
	 */
	public String[] getDebugInfo()
	{
		return debugInfo;
	}

	/**
	 * @return whether or not to render the debug information
	 */
	public boolean isDisplayed()
	{
		return display;
	}

	/**
	 * Sets the vericle spacing of the deug information.
	 * 
	 * @param verticleSpacing
	 *            The new verticle spacing
	 */
	public void setVerticleSpacing(int verticleSpacing)
	{
		this.verticleSpacing = verticleSpacing;
	}

	/**
	 * Set whether to display the debug information
	 * 
	 * @param display
	 *            Whether or not to display the debug info
	 */
	public void setDisplay(boolean display)
	{
		this.display = display;
	}

	/**
	 * Updates the debug information
	 * 
	 * @param debugInfo
	 *            The new debug information
	 */
	public void update(String[] debugInfo)
	{
		this.debugInfo = debugInfo;

		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_F3))
		{
			display = !display;
		}
	}

	/**
	 * Renders the debug information to the top left of the screen
	 */
	public void render()
	{
		if (display && debugInfo != null)
		{
			int currentY = Core.getWindow().getHeight() - verticleSpacing;
			int currentX = 10;

			for (String x : debugInfo)
			{
				Game.getInstance().getFontRenderer().render(font, x, currentX, currentY);
				currentY -= verticleSpacing;
			}
		}

	}
}
