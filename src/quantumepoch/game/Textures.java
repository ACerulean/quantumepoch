package quantumepoch.game;

import quantumepoch.graphics.Bitmap;
import quantumepoch.graphics.texture.Texture;

/**
 * Stores the texture resources for this game
 */
public enum Textures
{

	TILES(new Bitmap("textures/sprites/Ground_Tiles.png").replaceMagentaWithTransparancy()),
	PLAYER(new Bitmap("textures/sprites/Player_Over.png").flipY().replaceMagentaWithTransparancy()),
	ENEMY(new Bitmap(32, 32).fill(0xFFFF0000)),
	TREES(new Bitmap("textures/sprites/Trees.png").replaceMagentaWithTransparancy()),
	LOGO(new Bitmap("textures/gui/main_menu/QE_Logo_Dev.png").replaceMagentaWithTransparancy()),
	CTM_LOGO(new Bitmap("textures/gui/main_menu/logo_ctm.png").replaceMagentaWithTransparancy()),
	SETTINGS_BG(new Bitmap("textures/gui/BG_Settings.png").replaceMagentaWithTransparancy()),
	USE_ABILITY(new Bitmap(8, 8).fill(0xFFFFFFFF)),
	MENU_TETRIS_PIECES(new Bitmap("textures/sprites/Menu_Tetris_Pieces.png").flipY().replaceMagentaWithTransparancy()),
	BUTTON(new Bitmap("textures/gui/Button.png").replaceMagentaWithTransparancy()),
	SPACE_BG("textures/gui/main_menu/Space_Bg.png"),
	PROJECTILES(new Bitmap("textures/sprites/Projectiles.png").flipY().replaceMagentaWithTransparancy()),
	CLASSIC_MODE_BG("textures/gui/CLm_Bg1.png"),
	WATER_LAVA(new Bitmap("textures/sprites/WaterLavaAnim.png").flipY()),
	PORTAL(new Bitmap("textures/sprites/Portal.png").replaceMagentaWithTransparancy()),
	ENEMIES(new Bitmap("textures/sprites/Enemy_Forest.png").replaceMagentaWithTransparancy()),
	POTIONS(new Bitmap("textures/sprites/itm_Potions.png").replaceMagentaWithTransparancy()),
	GUI_SKIN(new Bitmap("textures/gui/MenuComp.png").flipY().replaceMagentaWithTransparancy()),
	CLOUDS(new Bitmap("textures/sprites/Clouds.png").transparentize(25).replaceMagentaWithTransparancy()),
	GUI_ABILITIES(new Bitmap("textures/gui/gui_abilities.png").replaceMagentaWithTransparancy()),
	PLAYER_PLATFORM(new Bitmap("textures/sprites/pf_players_attack.png").replaceMagentaWithTransparancy().flipY().flipX()),
	CURSOR(new Bitmap("textures/gui/gui_cursor.png").replaceMagentaWithTransparancy()),
	MINIMAP_BORDER(new Bitmap("textures/gui/UI_MapBoarder.png").replaceMagentaWithTransparancy()),
	BATTLE_BG(new Bitmap("textures/gui/bg_battle.png"));

	/** The texture of a texture region */
	private Texture texture;

	/**
	 * Creates a new texture resource from a bitmap
	 * 
	 * @param bitmap
	 *            The bitmap of the texture
	 * */
	Textures(Bitmap bitmap)
	{
		texture = new Texture(bitmap);
	}

	/**
	 * Creates a new texture resource from path
	 * 
	 * @param path
	 *            The path of the texture
	 */
	Textures(String path)
	{
		texture = new Texture(path);
	}

	/**
	 * @return the texture of this resource
	 */
	public Texture getTexture()
	{
		return texture;
	}

	/**
	 * Loads all texture resources into OpenGL memory space.
	 */
	public static void loadAllTextures()
	{
		for (Textures texture : Textures.values())
		{
			texture.getTexture().load();
		}
	}

}
