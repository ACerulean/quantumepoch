package quantumepoch.game.gui;

import quantumepoch.game.Game;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.batch.ShapeBatch;
import quantumepoch.graphics.batch.SpriteBatch;
import quantumepoch.graphics.gui.ImageButton;
import quantumepoch.graphics.texture.TextureRegion;
import quantumepoch.physics.BoundingBox2D;

/**
 * A character select button used when selecting a character for the first time
 * in adventure mode.
 */
public class CharacterButton extends ImageButton
{

	/** the width of the button */
	private float width;
	/** the height of the button */
	private float height;

	/**
	 * Creates a new character button
	 */
	public CharacterButton()
	{
		super();
	}

	/**
	 * Creates a new character button
	 * 
	 * @param relativeX
	 *            The relative x of the character button
	 * @param relativeY
	 *            The relative y of the character button
	 * @param normalTexture
	 *            The texture used when rendering the button
	 * @param hoveredTexture
	 *            The texture used when hovering over a button
	 * @param pressedTexture
	 *            The texture used when the button is pressed
	 * @param boundingBox
	 *            The bounding box of a button
	 */
	public CharacterButton(int relativeX, int relativeY, TextureRegion normalTexture, TextureRegion hoveredTexture, TextureRegion pressedTexture, BoundingBox2D boundingBox)
	{
		super(null, relativeX, relativeY, normalTexture, hoveredTexture, pressedTexture, boundingBox, Game.getInstance().getBlip());
	}

	/**
	 * Creates a new character button
	 * 
	 * @param relativeX
	 *            The relative x of the character button
	 * @param relativeY
	 *            The relative y of the character button
	 * @param normalTexture
	 *            The texture used when rendering the button
	 * @param hoveredTexture
	 *            The texture used when hovering over a button
	 * @param pressedTexture
	 *            The texture used when the button is pressed
	 * @param boundingBox
	 *            The bounding box of a button
	 * @param width
	 *            The width of the button
	 * @param height
	 *            The height of the button
	 */
	public CharacterButton(int relativeX, int relativeY, TextureRegion normalTexture, TextureRegion hoveredTexture, TextureRegion pressedTexture, float width, float height)
	{
		super(null, relativeX, relativeY, normalTexture, hoveredTexture, pressedTexture, Game.getInstance().getBlip());
		this.width = width;
		this.height = height;

		getBoundingBox().setWidth(width);
		getBoundingBox().setHeight(height);
	}

	/**
	 * Renders this character button to the screen.
	 * 
	 * @param spriteBatch
	 *            The sprite batch used to render this button to the screen
	 * @param shapeBatch
	 *            The shape batch used to render this button to the screen
	 */
	public void render(SpriteBatch spriteBatch, ShapeBatch shapeBatch)
	{
		TextureRegion toRender = null;
		if (!isHovering())
		{
			toRender = getNormalTexture();
		} else if (isHovering() && !isPressed())
		{
			toRender = getHoveredTexture();
		} else if (isPressed())
		{
			toRender = getPressedTexture();
		}
		spriteBatch.render(toRender, 0, 0, getScreenX(), getScreenY(), 0, width / getNormalTexture().getWidth(), height / getNormalTexture().getHeight());
		spriteBatch.reset();
		if (isHovering())
		{
			shapeBatch.setColor(Color.BLUE);
			shapeBatch.renderRect(getScreenX(), getScreenY(), width, height, 0, 0, 0);
		}
	}

}
