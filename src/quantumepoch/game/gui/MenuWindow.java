package quantumepoch.game.gui;

import quantumepoch.game.Game;
import quantumepoch.game.Textures;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.batch.ShapeBatch;
import quantumepoch.graphics.batch.SpriteBatch;
import quantumepoch.graphics.font.FontRenderer;
import quantumepoch.graphics.gui.BasicWindow;
import quantumepoch.graphics.gui.ImageTextButton;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * A menu window used in game for returning to the game, saving and quiting.
 */
public class MenuWindow extends BasicWindow
{

	/** return to game button */
	private ImageTextButton backToGameButton;
	/** save button */
	private ImageTextButton saveButton;
	/** quit button */
	private ImageTextButton quitButton;

	/**
	 * Creates a new menu window
	 * 
	 * @param x
	 *            The x position of the menu window
	 * @param y
	 *            The y position of the meun window
	 * @param width
	 *            The width of the menu window
	 * @param height
	 */
	public MenuWindow(int x, int y, int width, int height)
	{
		super(Color.GRAY, Color.WHITE, x, y, width, height, new TextureRegion(Textures.GUI_SKIN.getTexture(), 0, 0, 47, 47), 1, 1);
		scaleX = width / bg.getWidth();
		scaleY = height / bg.getHeight();
		TextureRegion but = new TextureRegion(Textures.BUTTON.getTexture(), 288, 0, 133, 36);
		backToGameButton = new ImageTextButton(this, 34, 90, but, but, but, "Return", Game.CALIBRI_30, Color.BLUE, Game.getInstance().getBlip());
		saveButton = new ImageTextButton(this, 34, 50, but, but, but, "Save", Game.CALIBRI_30, Color.BLUE, Game.getInstance().getBlip());
		quitButton = new ImageTextButton(this, 34, 10, but, but, but, "Quit", Game.CALIBRI_30, Color.BLUE, Game.getInstance().getBlip());
	}

	/**
	 * the image of the button to get back to main game
	 * 
	 * @return returns the button texture
	 */
	public ImageTextButton getBackToGameButton()
	{
		return backToGameButton;
	}

	/**
	 * the image of the button to save the game
	 * 
	 * @return returns the button texture
	 */
	public ImageTextButton getSaveButton()
	{
		return saveButton;
	}

	/**
	 * the image of the button to quit the game
	 * 
	 * @return returns the button texture
	 */
	public ImageTextButton getQuitButton()
	{
		return quitButton;
	}

	/**
	 * override method that updates the button textures of which buttons to display
	 */
	@Override
	public void update()
	{
		super.update();
		backToGameButton.update();
		saveButton.update();
		quitButton.update();
	}

	/**
	 * the render method that renders the buttons into the game menus and world
	 * 
	 * @param shapeBatch
	 *            the shape file of the buttons to pull from
	 * @param spriteBatch
	 *            the sprite file of the buttons to pull from
	 * @param fr
	 *            the font to render the buttons in
	 */
	public void render(ShapeBatch shapeBatch, SpriteBatch spriteBatch, FontRenderer fr)
	{
		super.render(spriteBatch, shapeBatch);
		shapeBatch.reset();

		backToGameButton.render(spriteBatch, fr);
		saveButton.render(spriteBatch, fr);
		quitButton.render(spriteBatch, fr);
	}

}
