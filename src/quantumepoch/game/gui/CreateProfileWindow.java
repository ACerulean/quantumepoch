package quantumepoch.game.gui;

import quantumepoch.core.Core;
import quantumepoch.game.Game;
import quantumepoch.game.Textures;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.batch.ShapeBatch;
import quantumepoch.graphics.batch.SpriteBatch;
import quantumepoch.graphics.font.FontRenderer;
import quantumepoch.graphics.gui.BasicTextField;
import quantumepoch.graphics.gui.BasicWindow;
import quantumepoch.graphics.gui.Component;
import quantumepoch.graphics.gui.ImageTextButton;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * Window used when creating a new profile.
 */
public class CreateProfileWindow extends BasicWindow
{

	/** the name field */
	private BasicTextField nameField;
	/** the create button */
	private ImageTextButton createButton;
	/** the cancel button */
	private ImageTextButton cancelButton;

	/**
	 * Creates the standard profile window.
	 */
	public CreateProfileWindow()
	{
		this((Core.getWindow().getWidth() - 100) / 2 - 50, (Core.getWindow().getHeight() - 100) / 2);
	}

	/**
	 * Creates the standard profile window at given position.
	 * 
	 * @param x
	 *            The x pos of this window
	 * @param y
	 *            The y pos of this window
	 */
	public CreateProfileWindow(int x, int y)
	{
		this(x, y, 200, 200);
	}

	/**
	 * Creates a new create profile window.
	 * 
	 * @param x
	 *            The x pos of this window
	 * @param y
	 *            The y pos of this window
	 * @param width
	 *            The width of this window
	 * @param height
	 *            The height of this window
	 */
	public CreateProfileWindow(int x, int y, int width, int height)
	{
		super(Color.GRAY, Color.WHITE, x, y, width, height, new TextureRegion(Textures.GUI_SKIN.getTexture(), 0, 0, 47, 47), 1, 1);

		scaleX = width / bg.getWidth();
		scaleY = height / bg.getHeight();

		TextureRegion but = new TextureRegion(Textures.BUTTON.getTexture(), 288, 0, 133, 36);
		cancelButton = new ImageTextButton(this, 10, 10, but, but, but, "Cancel", Game.CS_15, Color.WHITE, Game.getInstance().getBlip());
		createButton = new ImageTextButton(this, 10, 50, but, but, but, "Create", Game.CS_15, Color.WHITE, Game.getInstance().getBlip());
		cancelButton.setTint(Color.BLUE);
		createButton.setTint(Color.BLUE);
		nameField = new BasicTextField(this, 10, 90, width - 20, 30, "", Game.CS_15, Color.RED, Color.WHITE, new TextureRegion(Textures.GUI_SKIN.getTexture(), 0, 54, 47, 19), 1, 1);
		nameField.setScaleX(nameField.getWidth() / nameField.getBg().getWidth());
		nameField.setScaleY(nameField.getHeight() / nameField.getBg().getHeight());
		cancelButton.setScaleX(nameField.getWidth() / cancelButton.getNormalTexture().getWidth());
		createButton.setScaleX(nameField.getWidth() / createButton.getNormalTexture().getWidth());
	}

	/**
	 * @return the name field
	 */
	public BasicTextField getNameField()
	{
		return nameField;
	}

	/**
	 * @return the create button
	 */
	public ImageTextButton getCreateButton()
	{
		return createButton;
	}

	/**
	 * @return the cancel button
	 */
	public ImageTextButton getCancelButton()
	{
		return cancelButton;
	}

	/**
	 * Updates this create profile window.
	 */
	@Override
	public void update()
	{
		super.update();
		cancelButton.update();
		createButton.update();
		nameField.update();
	}

	/**
	 * Renders this window to the screen
	 * 
	 * @param shapeBatch
	 * @param spriteBatch
	 * @param renderer
	 */
	public void render(ShapeBatch shapeBatch, SpriteBatch spriteBatch, FontRenderer renderer)
	{
		super.render(spriteBatch, shapeBatch);
		spriteBatch.setTint(Color.WHITE);
		cancelButton.render(spriteBatch, renderer);
		createButton.render(spriteBatch, renderer);
		nameField.render(spriteBatch, renderer);
		renderer.setColor(Color.CORNFLOWER_BLUE);
		renderer.render(nameField.getFont(), "Name:", nameField.getScreenX() + (getWidth() - nameField.getFont().getStringWidth("Name:")) / 2, nameField.getScreenY() + 28);
	}
}
