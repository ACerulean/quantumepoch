package quantumepoch.game.gui;

import quantumepoch.game.inventory.Item;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.gui.BasicButton;
import quantumepoch.graphics.gui.Component;

/**
 * the stack that shows the next couple of tetris blocks that will be created
 *
 */
public class ItemStackButton extends BasicButton
{

	/**
	 * the constructor to get the next couple of blocks to be created
	 * @param parent the parent block
	 * @param x the x position of block to pull
	 * @param y the y position of block to pull
	 * @param item the actual block to pull
	 * @param color the color of the block
	 */
	public ItemStackButton(Component parent, int x, int y, Item item, Color color)
	{
		super(parent, x, y, item.getName(), Color.RED, 50);
	}

}
