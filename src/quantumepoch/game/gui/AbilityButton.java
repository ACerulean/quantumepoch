package quantumepoch.game.gui;

import quantumepoch.game.Game;
import quantumepoch.game.inventory.Ability;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.gui.Component;
import quantumepoch.graphics.texture.TextureRegion;

/**
 * An ability button for adventure mode to use a certain ability
 */
public class AbilityButton extends Component
{

	/** the ability this button uses */
	private Ability ability;
	/** the height of the start of the adventure button */
	private final int h = 200;
	/** the gap between the adventure button components */
	private final int gap = 10;

	/**
	 * Creates a new ability button
	 * 
	 * @param parent
	 *            The parent component
	 * @param x
	 *            The x ops
	 * @param y
	 *            The y pos
	 * @param abil
	 *            The ability this button uses
	 */
	public AbilityButton(Component parent, int x, int y, Ability abil)
	{
		super(parent, x, y);
		this.ability = abil;
	}

	/**
	 * @return the ability this button uses
	 */
	public Ability getAbility()
	{
		return ability;
	}

	/**
	 * Sets the ability of this button
	 * 
	 * @param ability
	 *            The new ability of this button
	 */
	public void setAbility(Ability ability)
	{
		this.ability = ability;
	}

	/**
	 * Updates the state of this adventure mode button
	 */
	@Override
	public void update()
	{
	}

	/**
	 * Renders this ability button to the screen
	 */
	public void render()
	{

		TextureRegion t = ability.getAbilityTexture();

		Game.getInstance().getShapeBatch().setColor(Color.GREEN);
		Game.getInstance().getShapeBatch().renderFilledRect(getScreenX() + (t.getWidth() - 4) / 2, getScreenY(), 8, (ability.getChargePercent() * h));

		Game.getInstance().getShapeBatch().setColor(Color.WHITE);
		Game.getInstance().getShapeBatch().renderRect(getScreenX() + (t.getHeight() - 4) / 2, getScreenY(), 8, h, 0, 0, 0);

		Game.getInstance().getSpriteBatch().render(ability.getAbilityTexture(), ability.getAbilityTexture().getWidth() / 2, ability.getAbilityTexture().getHeight() / 2, getScreenX(), getScreenY() + h + gap, ability.getRotation(), ability.getScale(), ability.getScale());
	}
}
