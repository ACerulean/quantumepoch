package quantumepoch.game;

import java.awt.Font;
import java.nio.IntBuffer;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Cursor;
import org.lwjgl.input.Keyboard;

import quantumepoch.audio.AudioBuffer;
import quantumepoch.audio.AudioSource;
import quantumepoch.audio.WaveAudio;
import quantumepoch.core.Application;
import quantumepoch.core.Core;
import quantumepoch.core.state.GameState;
import quantumepoch.core.state.GameStateManager;
import quantumepoch.game.debug.DebugRenderer;
import quantumepoch.game.profile.Profile;
import quantumepoch.game.state.AdventureModeState;
import quantumepoch.game.state.AdventureTutorialState;
import quantumepoch.game.state.CharacterSelectState;
import quantumepoch.game.state.ClassicModeState;
import quantumepoch.game.state.CreditsState;
import quantumepoch.game.state.MainMenuState;
import quantumepoch.game.state.MultiplayerState;
import quantumepoch.game.state.OptionsState;
import quantumepoch.game.state.PreAdventureModeState;
import quantumepoch.game.state.PreClassicModeState;
import quantumepoch.game.state.ProfileSelectState;
import quantumepoch.game.state.instruction.InstructionsState;
import quantumepoch.graphics.batch.ShapeBatch;
import quantumepoch.graphics.batch.SpriteBatch;
import quantumepoch.graphics.font.FontRenderer;
import quantumepoch.graphics.font.FontType;
import quantumepoch.graphics.font.TrueTypeFont;
import quantumepoch.graphics.lighting.Light2DRenderer;
import quantumepoch.utils.Delay;
import quantumepoch.utils.IOUtils;
import quantumepoch.window.LWJGLWindow;

/**
 * The main Game class of quantum epoch. In short, this class is an abstraction over all of the different game states which can be easily swapped in other states.
 */
public class Game implements Application
{

	// INDEX THE WORLDS IN THE META FILE

	/** the global singleton of the profile select state */
	public static final ProfileSelectState PROFILE_SELECT_STATE = new ProfileSelectState(0);
	/** the global singleton of the main menu state */
	public static final MainMenuState MAIN_MENU_STATE = new MainMenuState(1);
	/** the global singleton of the pre classic mode state */
	public static final PreClassicModeState PRE_CLASSIC_MODE_STATE = new PreClassicModeState(2);
	/** the global singleton of the classic mode state */
	public static final ClassicModeState CLASSIC_MODE_STATE = new ClassicModeState(3);
	/** the global singleton of the instructions state */
	public static final InstructionsState INSTRUCTIONS_STATE = new InstructionsState(4);
	/** the global singleton of the adventure mode state */
	public static final AdventureModeState ADVENTURE_MODE_STATE = new AdventureModeState(5);
	public static final PreAdventureModeState PRE_ADVENTURE_MODE_STATE = new PreAdventureModeState(6);
	public static final AdventureTutorialState ADVENTURE_TUTORIAL_STATE = new AdventureTutorialState(7);
	/** the global singleton of the character select state */
	public static final CharacterSelectState CHARACTER_SELECT_STATE = new CharacterSelectState(8);
	/** the global singleton of the multiplayer state */
	public static final MultiplayerState MULTIPLAYER_STATE = new MultiplayerState(9);
	/** the global singleton of the options state */
	public static final OptionsState OPTIONS_STATE = new OptionsState(10);
	/** the global singleton of the credits state */
	public static final CreditsState CREDITS_STATE = new CreditsState(11);

	/** the global singleton an instance of a calibbri size 30 font */
	public static final TrueTypeFont CALIBRI_30 = new TrueTypeFont(FontType.CALIBRI, 30, true);
	/** the global singleton of a calibri size 20 font */
	public static final TrueTypeFont CALIBRI_20 = new TrueTypeFont(FontType.CALIBRI, 20, true);
	/** the global singleton of a comic sans size 15 font */
	public static final TrueTypeFont CS_15 = new TrueTypeFont(FontType.COMIC_SANS, 20, true);

	/** The singleton instance of this game class */
	private static Game instance = null;

	/** the main game state manager */
	private GameStateManager gameStateManager;
	/** a debug renderer used to render fps and ups to the screen */
	private DebugRenderer dbg;
	/** the global sprite batch that any game state can use to render sprites */
	private SpriteBatch spriteBatch;
	/** the global shape batch that any game state can use to render shapes. */
	private ShapeBatch shapeBatch;
	/** the global font renderer any game state can use to render font strings */
	private FontRenderer fontRenderer;
	/** the global light renderer any game state can use to render lights */
	private Light2DRenderer lightRenderer;
	/** thc current working profile that was selected at game start */
	private Profile currentProfile;
	/** the default blip noise a button hover makes */
	private AudioSource blip;
	/** delay used to output debug information to standard out */
	private Delay del = new Delay(1000);
	/**
	 * an array of strings holding debug information to be rendered to the screen
	 */
	private String[] dbgInfo;
	/**
	 * an instance of a tetris starfield object to selectively render in the background of different game states
	 */
	private TetrisStarfield starField;

	/**
	 * Creates a new game instance. NOTE: this class is a singleton, so only one instance may be created. This is typically done by the Main class
	 */
	public Game()
	{
		if (instance == null)
		{
			instance = this;
		} else
		{
			throw new IllegalStateException("Can not create more than one instance of game");
		}

	}

	/**
	 * @return the debug renderer
	 */
	public DebugRenderer getDebugger()
	{
		return dbg;
	}

	/**
	 * @return the singleton instance of this class
	 */
	public static Game getInstance()
	{
		return instance;
	}

	/**
	 * @return the default blip sound for game buttons
	 */
	public AudioSource getBlip()
	{
		return blip;
	}

	/**
	 * @return the global game state manager
	 */
	public GameStateManager getGameStateManager()
	{
		return gameStateManager;
	}

	/**
	 * @return the global sprite batch
	 */
	public SpriteBatch getSpriteBatch()
	{
		return spriteBatch;
	}

	/**
	 * @return the global shape batch
	 */
	public ShapeBatch getShapeBatch()
	{
		return shapeBatch;
	}

	/**
	 * @return the global font renderer
	 */
	public FontRenderer getFontRenderer()
	{
		return fontRenderer;
	}

	/**
	 * @return the global light renderer
	 */
	public Light2DRenderer getLightRenderer()
	{
		return lightRenderer;
	}

	/**
	 * @return the current working profile
	 */
	public Profile getCurrentProfile()
	{
		return currentProfile;
	}

	/**
	 * Sets the current working profile.
	 * 
	 * @param profile
	 *            The profile in which to switch to.
	 */
	public void setCurrentProfile(Profile profile)
	{
		this.currentProfile = profile;
	}

	/**
	 * Initializes and loads all resources used by quantum epoch such as textures and sounds.
	 */
	@Override
	public void init()
	{
		CALIBRI_30.loadFont();
		CALIBRI_20.loadFont();
		CS_15.loadFont();

		Textures.loadAllTextures();

		WaveAudio audio = WaveAudio.loadWaveAudio("audio/blip.wav");
		blip = new AudioSource(new AudioBuffer(audio.getFormat(), audio.getData(), audio.getSampleRate()));

		gameStateManager = new GameStateManager();
		gameStateManager.addGameState(MULTIPLAYER_STATE);
		gameStateManager.addGameState(OPTIONS_STATE);
		gameStateManager.addGameState(INSTRUCTIONS_STATE);
		gameStateManager.addGameState(ADVENTURE_MODE_STATE);
		gameStateManager.addGameState(CHARACTER_SELECT_STATE);
		gameStateManager.addGameState(PROFILE_SELECT_STATE);
		gameStateManager.addGameState(MAIN_MENU_STATE);
		gameStateManager.addGameState(PRE_CLASSIC_MODE_STATE);
		gameStateManager.addGameState(CLASSIC_MODE_STATE);
		gameStateManager.addGameState(PRE_ADVENTURE_MODE_STATE);
		gameStateManager.addGameState(ADVENTURE_TUTORIAL_STATE);
		gameStateManager.addGameState(CREDITS_STATE);

		/*
		 * if (Main.judgeMode) { gameStateManager.setCurrentState(MAIN_MENU_STATE.getId()); String name = "Judge"; Profile judgeProfile = null;
		 * 
		 * File[] files = new File("playerdata").listFiles(); boolean exists = false; for (File f : files) if (f.getName().contains("Judge")) exists = true; if (!exists) { judgeProfile = new Profile(name, "playerdata" + File.separator + name + ".player", 0, 0, false); } else { judgeProfile = new Profile(name, "playerdata" + File.separator + name + ".player", 0, 0, true); } currentProfile = judgeProfile; } else { }
		 */
		gameStateManager.setCurrentState(0);

		dbg = new DebugRenderer();

		spriteBatch = new SpriteBatch();
		shapeBatch = new ShapeBatch();
		fontRenderer = new FontRenderer();
		lightRenderer = new Light2DRenderer();

		dbgInfo = new String[] { "FPS: " + Core.getCore().getFps(), "UPS: " + Core.getCore().getUps() };

		starField = new TetrisStarfield();

		IntBuffer cursorBuffer = IOUtils.createIntBuffer(16 * 16);
		for (int i = 0; i < 16 * 16; i++)
		{
			cursorBuffer.put(Textures.CURSOR.getTexture().getBitmap().getData()[i]);
		}
		cursorBuffer.flip();
		try
		{
			Cursor c = new Cursor(16, 16, 0, 15, 1, cursorBuffer, null);
			org.lwjgl.input.Mouse.setNativeCursor(c);
		} catch (LWJGLException e)
		{
			e.printStackTrace();
		}

	}

	/**
	 * Handles resize events
	 */
	@Override
	public void resized()
	{
		gameStateManager.getCurrentState().resized();
		((LWJGLWindow) Core.getWindow()).setViewport(0, 0, Core.getWindow().getWidth(), Core.getWindow().getHeight());
	}

	/**
	 * Updates the game. This task is delegated to the current loaded game state of the game state manager.
	 */
	@Override
	public void update()
	{
		gameStateManager.getCurrentState().update();

		lightRenderer.getOccluders().clear();

		dbgInfo[0] = "FPS: " + Core.getCore().getFps();
		dbgInfo[1] = "UPS: " + Core.getCore().getUps();
		dbg.update(dbgInfo);
		dbg.setDisplay(false);

		if (del.isReady())
		{
			// memory usage
			// Runtime.getRuntime().freeMemory()) / (1024 * 1024));
		}

		starField.update();

		if (currentProfile != null)
		{
			// update game options based on current profile
			AudioSource.updateAudioSources(currentProfile.getProfileOptions().getMasterVolume());
		}

		if ((Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_LSHIFT) || Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_RSHIFT)) && Core.getInput().getKeyboard().isKeyDown(Keyboard.KEY_ESCAPE))
		{
			Main.getCore().stop();
		}
	}

	/**
	 * Renders the game. This task is delegated to the current loaded game state of the game state manager.
	 */
	@Override
	public void render()
	{
		Light2DRenderer.count = 0;
		GameState s = gameStateManager.getCurrentState();

		if (s == CHARACTER_SELECT_STATE || s == PROFILE_SELECT_STATE || s == PRE_CLASSIC_MODE_STATE || s == MAIN_MENU_STATE || s == PRE_ADVENTURE_MODE_STATE || s == CREDITS_STATE)
		{
			starField.render(spriteBatch);
		}

		gameStateManager.getCurrentState().render();
		dbg.render();

		System.out.println(Light2DRenderer.count);

	}

	/**
	 * Disposes this game and all resources. The current profile is also saved before exiting.
	 */
	@Override
	public void dispose()
	{
		gameStateManager.getCurrentState().dispose();

		// save the profile
		if (currentProfile != null)
		{
			currentProfile.getPlayerFile().getXmlDoc().save();
		}
	}

}
