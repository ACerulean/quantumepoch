package quantumepoch.game;

import org.lwjgl.input.Keyboard;

import quantumepoch.core.Application;
import quantumepoch.core.Core;
import quantumepoch.graphics.Color;
import quantumepoch.graphics.batch.ShapeBatch;
import quantumepoch.utils.Delay;


public class Challenge implements Application
{

	// draggable rect left click
	// when mouse enters, color changes
	// left click change color
	// mode for dragging and size changing

	float x, y, w = 200, h = 200;
	ShapeBatch sb;
	Color c = Color.WHITE;
	Delay d = new Delay(500);
	boolean dd = false, d2 = false;
	boolean mode = true;

	@Override
	public void init()
	{
		sb = new ShapeBatch();
	}

	@Override
	public void resized()
	{

	}

	@Override
	public void update()
	{
		if (Core.getInput().getKeyboard().wasKeyPressed(Keyboard.KEY_M) && !d2)
			mode = !mode;

		int x1 = Core.getInput().getMouse().getX(), y1 = Core.getInput().getMouse().getY();
		if (Core.getInput().getMouse().isButtonDown(0) && x1 >= x && x1 < x + w && y1 >= y && y1 < y + h && !d2)
		{
			d2 = true;
			Core.getInput().getMouse().resetDX();
			Core.getInput().getMouse().resetDY();
		}
		if (d2)
		{
			float dx = Core.getInput().getMouse().getDX(), dy = Core.getInput().getMouse().getDY();
			if (Core.getInput().getMouse().wasButtonPressed(0))
			{
				c = new Color((int) (Math.random() * 0xFFFFFF));
				dd = true;
			}
			if (mode)
			{
				x += dx;
				y += dy;
			} else
			{
				w += dx;
				h += dy;
			}
			if (!Core.getInput().getMouse().isButtonDown(0))
				d2 = false;
		} else if (dd)
			c = Color.WHITE;

		Core.getWindow().setTitle("" + Core.getCore().getFps());
	}

	@Override
	public void render()
	{
		sb.begin();
		sb.setColor(c);
		sb.renderFilledRect(x, y, w, h);
		sb.setColor(Color.BLACK);
		sb.renderFilledCircle(200, 200, 50);

		sb.setColor(new Color(0xFFDFC4));

		sb.renderFilledCircle(200, 200, 48);

		sb.end();
	}

	@Override
	public void dispose()
	{
		// TODO Auto-generated method stub
	}

	public static void main(String[] args)
	{
		Challenge game = new Challenge();
		Core core = new Core(null, game, false, "Game", 16 * 60, 9 * 60, false, -1, 60, -1, 60);
		core.start();
	}

}
