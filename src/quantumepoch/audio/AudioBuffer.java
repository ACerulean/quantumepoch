package quantumepoch.audio;

import static org.lwjgl.openal.AL10.alBufferData;
import static org.lwjgl.openal.AL10.alDeleteBuffers;
import static org.lwjgl.openal.AL10.alGenBuffers;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

/**
 * A buffer for raw PCM audio data to be stored.
 */
public class AudioBuffer
{

	/**
	 * The OpenAL handle to this audio buffer. Used for referencing this audio
	 * buffer.
	 */
	private int handle;

	/**
	 * Creates an audio buffer with no inital data.
	 */
	public AudioBuffer()
	{
		genHandle();
	}

	/**
	 * Creates an audio buffer with the specified format, data and frequency.
	 * 
	 * @param format
	 * @param data
	 * @param freq
	 */
	public AudioBuffer(int format, ByteBuffer data, int freq)
	{
		genHandle();
		sendAudioData(format, data, freq);
	}

	/**
	 * Creates an audio buffer with the specified format, data and frequency.
	 * 
	 * @param format
	 * @param data
	 * @param freq
	 */
	public AudioBuffer(int format, ShortBuffer data, int freq)
	{
		genHandle();
		sendAudioData(format, data, freq);
	}

	/**
	 * Creates an audio buffer with the specified format, data and frequency.
	 * 
	 * @param format
	 * @param data
	 * @param freq
	 */
	public AudioBuffer(int format, IntBuffer data, int freq)
	{
		genHandle();
		sendAudioData(format, data, freq);
	}

	/**
	 * Generates an OpenAL handle for this audio buffer.
	 */
	private void genHandle()
	{
		handle = alGenBuffers();
	}

	/**
	 * @return The handle OpenAL uses to reference this audio buffer.
	 */
	public int getHandle()
	{
		return handle;
	}

	/**
	 * Sends the specified format, data in byte format and frequency audio data
	 * in to the audio buffer.
	 * 
	 * @param format
	 * @param data
	 * @param freq
	 */
	public void sendAudioData(int format, ByteBuffer data, int freq)
	{
		alBufferData(handle, format, data, freq);
	}

	/**
	 * Sends the specified format, data in short format and frequency audio data
	 * in to the audio buffer.
	 * 
	 * @param format
	 * @param data
	 * @param freq
	 */
	public void sendAudioData(int format, ShortBuffer data, int freq)
	{
		alBufferData(handle, format, data, freq);
	}

	/**
	 * Sends the specified format, data in integer format and frequency audio
	 * data in to the audio buffer.
	 * 
	 * @param format
	 * @param data
	 * @param freq
	 */
	public void sendAudioData(int format, IntBuffer data, int freq)
	{
		alBufferData(handle, format, data, freq);
	}

	/**
	 * Disposes of this audio buffer's resources.
	 */
	public void dispose()
	{
		alDeleteBuffers(handle);
	}

}
