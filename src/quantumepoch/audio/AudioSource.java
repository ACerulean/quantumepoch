package quantumepoch.audio;

import static org.lwjgl.openal.AL10.AL_BUFFER;
import static org.lwjgl.openal.AL10.AL_FALSE;
import static org.lwjgl.openal.AL10.AL_GAIN;
import static org.lwjgl.openal.AL10.AL_LOOPING;
import static org.lwjgl.openal.AL10.AL_PITCH;
import static org.lwjgl.openal.AL10.AL_POSITION;
import static org.lwjgl.openal.AL10.AL_TRUE;
import static org.lwjgl.openal.AL10.AL_VELOCITY;
import static org.lwjgl.openal.AL10.alDeleteSources;
import static org.lwjgl.openal.AL10.alGenSources;
import static org.lwjgl.openal.AL10.alSource3f;
import static org.lwjgl.openal.AL10.alSourcePause;
import static org.lwjgl.openal.AL10.alSourcePlay;
import static org.lwjgl.openal.AL10.alSourceStop;
import static org.lwjgl.openal.AL10.alSourcef;
import static org.lwjgl.openal.AL10.alSourcei;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a real world source of audio. In the real world, an audio source
 * would have a sound, position, velocity, gain and pitch. In this case, sound
 * is represented as raw PCM data stored in an audio buffer. This class has
 * methods for playing, stopping and looping the audio.
 */
public class AudioSource
{

	private static List<AudioSource> audioSources = new ArrayList<AudioSource>();

	public static void updateAudioSources(float masterVolume)
	{
		for (AudioSource source : audioSources)
		{
			source.setGain(masterVolume);
		}
	}
	
	public static void stopAll()
	{
		for (AudioSource source : audioSources)
		{
			source.stop();
		}
	}

	/**
	 * The OpenAL handle for this audio souce. This is used to referece the
	 * audio source.
	 */
	private int handle;

	/**
	 * Creates an empty audio source with no audio data.
	 */
	public AudioSource()
	{
		this(null);
	}

	/**
	 * Creates an audio source with data from the specified audio buffer.
	 * 
	 * @param data
	 */
	public AudioSource(AudioBuffer data)
	{
		handle = alGenSources();

		if (data != null)
		{
			loadBufferToSource(data);
		}
		audioSources.add(this);
		setGain(0.2f);
	}

	/**
	 * @return The OpenAL handle for this audio souce
	 */
	public int getHandle()
	{
		return handle;
	}

	/**
	 * Loads the specified audio buffer's data to be played by this source.
	 * 
	 * @param data
	 */
	public void loadBufferToSource(AudioBuffer data)
	{
		alSourcei(handle, AL_BUFFER, data.getHandle());
	}

	/**
	 * Sets the (x, y, z) position of this audio source.
	 * 
	 * @param x
	 *            position
	 * @param y
	 *            position
	 * @param z
	 *            position
	 */
	public void setPosition(float x, float y, float z)
	{
		alSource3f(handle, AL_POSITION, x, y, z);
	}

	/**
	 * Sets the (x, y, z) velocity of this audio source.
	 * 
	 * @param x
	 *            position
	 * @param y
	 *            position
	 * @param z
	 *            position
	 */
	public void setVelocity(float x, float y, float z)
	{
		alSource3f(handle, AL_VELOCITY, x, y, z);
	}

	/**
	 * Sets the gain of this audio source.
	 * 
	 * @param gain
	 */
	public void setGain(float gain)
	{
		alSourcef(handle, AL_GAIN, gain);
	}

	/**
	 * Sets the pitch of this audio source.
	 * 
	 * @param pitch
	 */
	public void setPitch(float pitch)
	{
		alSourcef(handle, AL_PITCH, pitch);
	}

	/**
	 * Stops this audio source from playing.
	 */
	public void stop()
	{
		alSourceStop(handle);
	}

	/**
	 * Pauses this audio source.
	 */
	public void pause()
	{
		alSourcePause(handle);
	}

	/**
	 * Plays this audio source.
	 */
	public void play()
	{
		alSourcePlay(handle);
	}

	/**
	 * Loops this audio source if true, else does not loop.
	 * 
	 * @param loop
	 *            count
	 */
	public void setLooping(boolean loop)
	{
		if (loop)
		{
			alSourcei(handle, AL_LOOPING, AL_TRUE);
		} else
		{
			alSourcei(handle, AL_LOOPING, AL_FALSE);
		}
	}

	/**
	 * Disposes of this audio source's resources.
	 */
	public void dispose()
	{
		alDeleteSources(handle);
	}

}
