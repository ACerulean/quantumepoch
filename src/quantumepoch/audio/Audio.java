package quantumepoch.audio;

import static org.lwjgl.openal.AL10.AL_NO_ERROR;
import static org.lwjgl.openal.AL10.AL_ORIENTATION;
import static org.lwjgl.openal.AL10.AL_POSITION;
import static org.lwjgl.openal.AL10.AL_VELOCITY;
import static org.lwjgl.openal.AL10.alGetError;
import static org.lwjgl.openal.AL10.alListener3f;

import org.lwjgl.LWJGLException;
import org.lwjgl.openal.AL;

import quantumepoch.core.Core;

/**
 * Main class for handling audio. Interfaces with OpenAL to define listener
 * properties.
 */
public class Audio
{

	/**
	 * Creates an instance of audio. This class is a singleton; the only
	 * instance should be defined inside of Core.
	 */
	public Audio()
	{
		if (Core.getAudio() != null)
		{
			throw new IllegalStateException("Can not make more than one instance of Audio");
		}
	}

	/**
	 * Returns the next integer error that OpenAL has thrown.
	 * 
	 * @return An integer representing the next OpenAL error.
	 */
	public int getALError()
	{
		return alGetError();
	}

	/**
	 * Sets the position of the listener in the virtual 3D world given a (x, y,
	 * z) coordinate.
	 * 
	 * @param x
	 *            position
	 * @param y
	 *            position
	 * @param z
	 *            position
	 */
	public void setListenerPosition(float x, float y, float z)
	{
		alListener3f(AL_POSITION, x, y, z);
	}

	/**
	 * Sets the velocity of the listener in the virtual 3D world given a (x, y,
	 * z) vector.
	 * 
	 * @param x
	 *            position
	 * @param y
	 *            position
	 * @param z
	 *            position
	 */
	public void setListenerVelocity(float x, float y, float z)
	{
		alListener3f(AL_VELOCITY, x, y, z);
	}

	/**
	 * Sets the orientation of the listener in the virtual 3D world given a (x,
	 * y, z) orientation.
	 * 
	 * @param x
	 *            rotation
	 * @param y
	 *            rotation
	 * @param z
	 *            rotation
	 */
	public void setListenerOrientation(float rotX, float rotY, float rotZ)
	{
		alListener3f(AL_ORIENTATION, rotX, rotY, rotZ);
	}

	/**
	 * Checks all of the OpenAL errors that have been throws and crashes with a
	 * list of all errors if any have been detected.
	 */
	public void checkALErrors()
	{
		int err = 0;
		boolean berr = false;

		while ((err = getALError()) != AL_NO_ERROR)
		{
			berr = true;
			Core.getLog().fatal("OpenAL error: " + err);
		}

		if (berr)
		{
			// crash report
			System.exit(1);
		}

	}

	/**
	 * Initializes OpenAL. Should only be called from the core engine.
	 */
	public void create()
	{
		try
		{
			AL.create();
		} catch (LWJGLException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Disposes of any audio resources. Should only be called from the core engine.
	 */
	public void dispose()
	{
		AL.destroy();
	}

}
