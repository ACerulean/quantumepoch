package quantumepoch.audio;

import static org.lwjgl.openal.AL10.AL_FORMAT_MONO16;
import static org.lwjgl.openal.AL10.AL_FORMAT_MONO8;
import static org.lwjgl.openal.AL10.AL_FORMAT_STEREO16;
import static org.lwjgl.openal.AL10.AL_FORMAT_STEREO8;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import quantumepoch.utils.IOUtils;

/**
 * Represents a WAV audio file containing raw PCM data.
 */
public class WaveAudio
{

	/**
	 * The raw PCM data of this WAV file.
	 */
	private ByteBuffer data;

	/**
	 * The OpenAL format of this WAV file.
	 */
	private int format;

	/**
	 * The sample rate of this audio track.
	 */
	private int sampleRate;

	/**
	 * Constructs a new audio track from the specified wav file path.
	 * 
	 * @param path
	 * @throws FileNotFoundException
	 * @throws UnsupportedAudioFileException
	 * @throws IOException
	 */
	public WaveAudio(String path) throws FileNotFoundException, UnsupportedAudioFileException, IOException
	{
		this(new BufferedInputStream(IOUtils.getInternalResourceAsStream(path)));
	}

	/**
	 * Constructs a new audio track from the specified wav file.
	 * 
	 * @param file
	 * @throws FileNotFoundException
	 * @throws UnsupportedAudioFileException
	 * @throws IOException
	 */
	public WaveAudio(File file) throws FileNotFoundException, UnsupportedAudioFileException, IOException
	{
		this(new BufferedInputStream(new FileInputStream(file)));
	}

	/**
	 * Constructs a new audio track from the specified input stream.
	 * 
	 * @param stream
	 * @throws UnsupportedAudioFileException
	 * @throws IOException
	 */
	public WaveAudio(InputStream stream) throws UnsupportedAudioFileException, IOException
	{
		this(AudioSystem.getAudioInputStream(stream));
	}

	/**
	 * Constructs a new audio track from the specified audio stream.
	 * 
	 * @param stream
	 */
	public WaveAudio(AudioInputStream stream)
	{

		AudioFormat audioFormat = stream.getFormat();

		// load the openal format
		if (audioFormat.getChannels() == 1)
		{
			if (audioFormat.getSampleSizeInBits() == 8)
			{
				format = AL_FORMAT_MONO8;
			} else if (audioFormat.getSampleSizeInBits() == 16)
			{
				format = AL_FORMAT_MONO16;
			}
		} else if (audioFormat.getChannels() == 2)
		{
			if (audioFormat.getSampleSizeInBits() == 8)
			{
				format = AL_FORMAT_STEREO8;
			} else if (audioFormat.getSampleSizeInBits() == 16)
			{
				format = AL_FORMAT_STEREO16;
			}
		}

		sampleRate = (int) audioFormat.getSampleRate();

		// load wave data
		try
		{
			int size = stream.available();
			byte[] rawData = new byte[size];

			stream.read(rawData);

			data = IOUtils.byteArrayToBuffer(rawData);
		} catch (IOException e)
		{
			e.printStackTrace();
		}

	}

	/**
	 * @return The raw PCM data of this WAV file.
	 */
	public ByteBuffer getData()
	{
		return data;
	}

	/**
	 * @return The OpenAL format of this WAV file.
	 */
	public int getFormat()
	{
		return format;
	}

	/**
	 * @return The sample rate of this WAV file.
	 */
	public int getSampleRate()
	{
		return sampleRate;
	}

	public static WaveAudio loadWaveAudio(String path)
	{
		try
		{
			return new WaveAudio(path);
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (UnsupportedAudioFileException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	public static WaveAudio loadWaveAudio(File file)
	{
		try
		{
			return new WaveAudio(file);
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (UnsupportedAudioFileException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	public static WaveAudio loadWaveAudio(InputStream stream)
	{
		try
		{
			return new WaveAudio(stream);
		} catch (UnsupportedAudioFileException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	public static WaveAudio loadWaveAudio(AudioInputStream stream)
	{
		return new WaveAudio(stream);
	}

}
