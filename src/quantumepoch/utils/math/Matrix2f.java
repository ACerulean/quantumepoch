package quantumepoch.utils.math;

/**
 * The 2nd matrix class
 *
 */
public class Matrix2f
{

	private float[][] mat;

	/**
	 * creates the new matrix
	 */
	public Matrix2f()
	{
		this.mat = new float[2][2];
	}

	/**
	 * fills the new matrix
	 * 
	 * @param mat
	 *            the matrix
	 */
	public Matrix2f(Matrix2f mat)
	{
		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 2; x++)
			{
				setElement(x, y, mat.getElement(x, y));
			}
		}
	}

	/**
	 * gets the elements in the matrix
	 * 
	 * @param x
	 *            the x value
	 * @param y
	 *            the y value
	 * @return returns the element in the matrix
	 */
	public float getElement(int x, int y)
	{
		if (x >= 0 && y >= 0 && x < 2 && y < 2)
		{
			return mat[y][x];
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	/**
	 * gets the row in the matrix
	 * 
	 * @param row
	 *            the row to find
	 * @return returns the row in the matrix
	 */
	public float[] getRow(int row)
	{
		if (row >= 0 && row < 2)
		{
			return mat[row];
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	/**
	 * gets the column in hte matrix
	 * 
	 * @param col
	 *            the column to find
	 * @return returns the column in the matrix
	 */
	public float[] getColumn(int col)
	{
		if (col >= 0 && col < 2)
		{
			float[] res = new float[2];
			for (int i = 0; i < 2; i++)
			{
				res[i] = mat[i][col];
			}
			return res;
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	/**
	 * sets the element in the matrix
	 * 
	 * @param x
	 *            the x value
	 * @param y
	 *            the y value
	 * @param value
	 *            the value to set
	 */
	public void setElement(int x, int y, float value)
	{
		if (x >= 0 && y >= 0 && x < 2 && y < 2)
		{
			mat[y][x] = value;
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to set element that was out of matrix bounds");
		}
	}

	/**
	 * sets the row of the matrix
	 * 
	 * @param row
	 *            the row to be set
	 * @param value
	 *            the value of the row to set
	 */
	public void setRow(int row, float[] value)
	{
		if (row >= 0 && row < 2)
		{
			if (value.length < 3)
			{
				mat[row] = value;
			} else
			{
				throw new IllegalArgumentException("Row too large for matrix");
			}
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	/**
	 * sets the column for the matrix
	 * 
	 * @param col
	 *            the column to be set
	 * @param value
	 *            the value to set
	 */
	public void setColumn(int col, float[] value)
	{
		if (col >= 0 && col < 2)
		{
			if (value.length < 3)
			{
				for (int i = 0; i < 2; i++)
				{
					mat[i][col] = value[i];
				}
			} else
			{
				throw new IllegalArgumentException("Column too large for matrix");
			}
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	/**
	 * sets the matrix
	 * 
	 * @param mat
	 *            the matrix
	 */
	public void set(Matrix2f mat)
	{
		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 2; x++)
			{
				setElement(x, y, mat.getElement(x, y));
			}
		}
	}

	/**
	 * adds to the matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @return returns the new matrix
	 */
	public Matrix2f add(Matrix2f mat)
	{
		Matrix2f res = new Matrix2f();
		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 2; x++)
			{
				res.setElement(x, y, getElement(x, y) + mat.getElement(x, y));
			}
		}
		return res;
	}

	/**
	 * subtracts from the matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @return returns the new matrix
	 */
	public Matrix2f subtract(Matrix2f mat)
	{
		Matrix2f res = new Matrix2f();
		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 2; x++)
			{
				res.setElement(x, y, getElement(x, y) - mat.getElement(x, y));
			}
		}
		return res;
	}

	/**
	 * multiplies the elements in the matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @return returns the new matrix
	 */
	public Matrix2f multiply(Matrix2f mat)
	{
		Matrix2f res = new Matrix2f();
		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 2; x++)
			{
				Vector2f row = new Vector2f(getRow(y));
				Vector2f col = new Vector2f(mat.getColumn(x));
				res.setElement(x, y, row.dotProduct(col));
			}
		}
		return res;
	}

	/**
	 * transposes the matrix
	 * 
	 * @return returns the new matrix
	 */
	public Matrix2f transpose()
	{
		Matrix2f res = new Matrix2f();
		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 2; x++)
			{
				res.setElement(y, x, getElement(x, y));
			}
		}
		return res;
	}

	/**
	 * scales the matrix
	 * 
	 * @param scaler
	 *            the scaler
	 */
	public void scale(float scaler)
	{
		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 2; x++)
			{
				setElement(x, y, getElement(x, y) * scaler);
			}
		}
	}

	/**
	 * sets the identity of the matrix
	 */
	public void setIdentity()
	{
		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 2; x++)
			{
				if (x == y)
				{
					setElement(x, y, 1);
				} else
				{
					setElement(x, y, 0);
				}
			}
		}
	}

	/**
	 * sets the matrix to zero
	 */
	public void setZero()
	{
		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 2; x++)
			{
				setElement(x, y, 0);
			}
		}
	}

}
