package quantumepoch.utils.math;

/**
 * the third matrix class
 *
 */
public class Matrix3f
{

	private float[][] mat;

	/**
	 * creates the new matrix
	 */
	public Matrix3f()
	{
		this.mat = new float[3][3];
	}

	/**
	 * fills the new matrix with elements
	 * 
	 * @param mat
	 *            the matrix
	 */
	public Matrix3f(Matrix3f mat)
	{
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				getElement(x, y, mat.getElement(x, y));
			}
		}
	}

	/**
	 * gets the elements in the matrix
	 * 
	 * @param x
	 *            the x value
	 * @param y
	 *            the y value
	 * @return returns the element searching for
	 */
	public float getElement(int x, int y)
	{
		if (x >= 0 && y >= 0 && x < 3 && y < 3)
		{
			return mat[y][x];
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	/**
	 * gets the row from the matrix
	 * 
	 * @param row
	 *            the row to search for
	 * @return returns the row
	 */
	public float[] getRow(int row)
	{
		if (row >= 0 && row < 3)
		{
			return mat[row];
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	/**
	 * gets the column from the matrix
	 * 
	 * @param col
	 *            the column to search for
	 * @return returns the column
	 */
	public float[] getColumn(int col)
	{
		if (col >= 0 && col < 3)
		{
			float[] res = new float[3];
			for (int i = 0; i < 3; i++)
			{
				res[i] = mat[i][col];
			}
			return res;
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	/**
	 * gets the element from the matrix
	 * 
	 * @param x
	 *            the x value
	 * @param y
	 *            the y value
	 * @param value
	 *            the value to search for
	 */
	public void getElement(int x, int y, float value)
	{
		if (x >= 0 && y >= 0 && x < 3 && y < 3)
		{
			mat[y][x] = value;
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to set element that was out of matrix bounds");
		}
	}

	/**
	 * sets the row for the matrix
	 * 
	 * @param row
	 *            the row to set
	 * @param value
	 *            the value to set
	 */
	public void setRow(int row, float[] value)
	{
		if (row >= 0 && row < 3)
		{
			if (value.length < 3)
			{
				mat[row] = value;
			} else
			{
				throw new IllegalArgumentException("Row too large for matrix");
			}
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	/**
	 * sets the column of the matrix
	 * 
	 * @param col
	 *            the column to set
	 * @param value
	 *            the value to set
	 */
	public void setColumn(int col, float[] value)
	{
		if (col >= 0 && col < 3)
		{
			if (value.length < 3)
			{
				for (int i = 0; i < 3; i++)
				{
					mat[i][col] = value[i];
				}
			} else
			{
				throw new IllegalArgumentException("Column too large for matrix");
			}
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	/**
	 * sets the matrix
	 * 
	 * @param mat
	 *            the matrix
	 */
	public void set(Matrix3f mat)
	{
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				getElement(x, y, mat.getElement(x, y));
			}
		}
	}

	/**
	 * adds to the matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @return returns the new matrix
	 */
	public Matrix3f add(Matrix3f mat)
	{
		Matrix3f res = new Matrix3f();
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				res.getElement(x, y, getElement(x, y) + mat.getElement(x, y));
			}
		}
		return res;
	}

	/**
	 * subtracts from the matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @return returns the new matrix
	 */
	public Matrix3f subtract(Matrix3f mat)
	{
		Matrix3f res = new Matrix3f();
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				res.getElement(x, y, getElement(x, y) - mat.getElement(x, y));
			}
		}
		return res;
	}

	/**
	 * multiplies to the matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @return returns the new matrix
	 */
	public Matrix3f multiply(Matrix3f mat)
	{
		Matrix3f res = new Matrix3f();
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				Vector3f row = new Vector3f(getRow(y));
				Vector3f col = new Vector3f(mat.getColumn(x));
				res.getElement(x, y, (float) row.dot(col));
			}
		}
		return res;
	}

	/**
	 * transposes the matrix
	 * 
	 * @return returns the new matrix
	 */
	public Matrix3f transpose()
	{
		Matrix3f res = new Matrix3f();
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				res.getElement(y, x, getElement(x, y));
			}
		}
		return res;
	}

	/**
	 * scales the matrix
	 * 
	 * @param scaler
	 *            the scaling value
	 */
	public void scale(float scaler)
	{
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				getElement(x, y, getElement(x, y) * scaler);
			}
		}
	}

	/**
	 * sets the identity of the matrix
	 */
	public void setIdentity()
	{
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				if (x == y)
				{
					getElement(x, y, 1);
				} else
				{
					getElement(x, y, 0);
				}
			}
		}
	}

	/**
	 * sets the matrix to zero
	 */
	public void setZero()
	{
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				getElement(x, y, 0);
			}
		}
	}

}
