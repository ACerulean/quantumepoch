package quantumepoch.utils.math;

/**
 * the third vector class that instantiates and fills the vector for the utils
 * class
 *
 */
public class Vector3f
{

	private float x;
	private float y;
	private float z;

	/**
	 * constructor the sets the values to zero
	 */
	public Vector3f()
	{
		this.x = 0;
		this.y = 0;
		this.z = 0;
	}

	/**
	 * constructor that sets the value to the params
	 * 
	 * @param x
	 *            the x value to set
	 * @param y
	 *            the y value to set
	 * @param z
	 *            the z value
	 */
	public Vector3f(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * constructor that sets all the value to the same value
	 * 
	 * @param v
	 *            the param value to set all values to
	 */
	public Vector3f(float v)
	{
		this.x = v;
		this.y = v;
		this.z = v;
	}

	/**
	 * constructor that sets the value of the vector to that of another
	 * 
	 * @param vec
	 *            the vector to copy
	 * @param z
	 *            the z value
	 */
	public Vector3f(Vector2f vec, float z)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = z;
	}

	/**
	 * constructor that sets the values to that of another vector
	 * 
	 * @param vec
	 *            the vector to copy
	 */
	public Vector3f(Vector3f vec)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = vec.getZ();
	}

	/**
	 * constructor that sets the values to the vector array
	 * 
	 * @param vec
	 *            the array to set values to
	 */
	public Vector3f(float[] vec)
	{
		if (vec.length == 2)
		{
			this.x = vec[0];
			this.y = vec[1];
		} else
		{
			throw new IllegalArgumentException("Array incorrect size for vector");
		}
	}

	/**
	 * gets the x value of the vector
	 * 
	 * @return returns the x value
	 */
	public float getX()
	{
		return x;
	}

	/**
	 * gets the y value of the vector
	 * 
	 * @return returns the y value
	 */
	public float getY()
	{
		return y;
	}

	/**
	 * gets the z value of the vector
	 * 
	 * @return returns the z value
	 */
	public float getZ()
	{
		return z;
	}

	/**
	 * sets the x value of the vector
	 * 
	 * @param x
	 *            the x value to set
	 */
	public void setX(float x)
	{
		this.x = x;
	}

	/**
	 * sets the y value of the vector
	 * 
	 * @param y
	 *            the y value to set
	 */
	public void setY(float y)
	{
		this.y = y;
	}

	/**
	 * sets the z value of the vector
	 * 
	 * @param z
	 *            the z value to set
	 */
	public void setZ(float z)
	{
		this.z = z;
	}

	/**
	 * sets all the value of the vector
	 * 
	 * @param x
	 *            the x value to set
	 * @param y
	 *            the y value to set
	 * @param z
	 *            the z value to set
	 */
	public void set(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * sets the values of the vector to the same value
	 * 
	 * @param v
	 *            the value to set all the values to
	 */
	public void set(float v)
	{
		this.x = v;
		this.y = v;
		this.z = v;
	}

	/**
	 * sets the values of the vector to another vector
	 * 
	 * @param vec
	 *            the vector to copy
	 * @param z
	 *            the z value to set
	 */
	public void set(Vector2f vec, float z)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = z;
	}

	/**
	 * sets the values of another vector to this vector
	 * 
	 * @param vec
	 *            the vector to copy the values of
	 */
	public void set(Vector3f vec)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = vec.getZ();
	}

	/**
	 * sets the values of the vector to the vector array
	 * 
	 * @param vec
	 */
	public void set(float[] vec)
	{
		if (vec.length == 3)
		{
			this.x = vec[0];
			this.y = vec[1];
			this.z = vec[2];
		} else
		{
			throw new IllegalArgumentException("Array incorrect size for vector");
		}
	}

	// vec operators

	/**
	 * adds to the vector
	 * 
	 * @param vec
	 *            the vector to add to
	 * @return returns the new vector
	 */
	public Vector3f add(Vector3f vec)
	{
		return new Vector3f(this.x + vec.getX(), this.y + vec.getY(), this.z + vec.getZ());
	}

	/**
	 * subtracts from the vector
	 * 
	 * @param vec
	 *            the vector to subtract to
	 * @return returns the new vector
	 */
	public Vector3f subtract(Vector3f vec)
	{
		return new Vector3f(this.x - vec.getX(), this.y - vec.getY(), this.z - vec.getZ());
	}

	/**
	 * multiplies to the vector
	 * 
	 * @param vec
	 *            the vector to multiply to
	 * @return returns the new vector
	 */
	public Vector3f multiply(Vector3f vec)
	{
		return new Vector3f(this.x * vec.getX(), this.y * vec.getY(), this.z * vec.getZ());
	}

	/**
	 * puts the vector to a power
	 * 
	 * @param power
	 *            the power to use the vector on
	 * @return returns the new vector
	 */
	public Vector3f pow(double power)
	{
		return new Vector3f((float) Math.pow(this.x, power), (float) Math.pow(this.y, power), (float) Math.pow(this.z, power));
	}

	/**
	 * gets the length of the vector
	 * 
	 * @return returns the length of the vector
	 */
	public double length()
	{
		return Math.sqrt(x * x + y * y + z * z);
	}

	/**
	 * gets the product of the vector
	 * 
	 * @param vec
	 *            the vector to use
	 * @return returns the product of the vectors values
	 */
	public double dot(Vector3f vec)
	{
		return this.x * vec.getX() + this.y * vec.getY() + this.z * vec.getZ();
	}

	public Vector3f cross(Vector3f vec)
	{
		float newX = y * vec.getZ() - z * vec.getY();
		float newY = z * vec.getX() - x * vec.getZ();
		float newZ = x * vec.getY() - y * vec.getX();
		return new Vector3f(newX, newY, newZ);
	}

	/**
	 * normalizes the vector values
	 */
	public void normalize()
	{
		double length = length();
		this.x /= length;
		this.y /= length;
		this.z /= length;
	}

	/**
	 * scales the vector
	 * 
	 * @param scaler
	 *            the scale to use on the vector values
	 */
	public void scale(float scaler)
	{
		this.x *= scaler;
		this.y *= scaler;
		this.z *= scaler;
	}

	/**
	 * inverts the x value
	 */
	public void invertX()
	{
		this.x *= -1;
	}

	/**
	 * inverts the y value
	 */
	public void invertY()
	{
		this.y *= -1;
	}

	/**
	 * inverts the z value
	 */
	public void invertZ()
	{
		this.z *= -1;
	}

	/**
	 * inverts the whole vector
	 */
	public void invert()
	{
		scale(-1);
	}

	/**
	 * asks if the values of the vector is equal to the other vector
	 * 
	 * @param other
	 *            the other vector to compare to
	 * @return returns a boolean based on comparison of vectors
	 */
	public boolean equals(Vector3f other)
	{
		return x == other.getX() && y == other.getY() && z == other.getZ();
	}

}
