package quantumepoch.utils.math;

/**
 * creates and fills vectors for the utils class
 *
 */
public class Vector2f
{

	private float x;
	private float y;

	/**
	 * empty constructor
	 */
	public Vector2f()
	{

	}

	/**
	 * constructor with value
	 * 
	 * @param value
	 *            the value for the vector
	 */
	public Vector2f(float value)
	{
		this.x = value;
		this.y = value;
	}

	/**
	 * constructor with both x and y value
	 * 
	 * @param x
	 *            the x value for the vector
	 * @param y
	 *            the y value for the vector
	 */

	public Vector2f(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	/**
	 * constructor with array of values
	 * 
	 * @param vec
	 *            the array of values for the vector
	 */
	public Vector2f(float[] vec)
	{
		if (vec.length == 2)
		{
			this.x = vec[0];
			this.y = vec[1];
		} else
		{
			throw new IllegalArgumentException("Array incorrect size for vector2f");
		}
	}

	/**
	 * the length of the vector
	 * 
	 * @return returns the length of the vector
	 */
	public float length()
	{
		return (float) Math.sqrt(x * x + y * y);
	}

	/**
	 * the length of the vector squared
	 * 
	 * @return returns the length of the vector squared
	 */
	public float lengthSquared()
	{
		return x * x + y * y;
	}

	/**
	 * adds to the vector
	 * 
	 * @param vec
	 *            the vector value to add
	 * @return returns the new vector
	 */
	public Vector2f add(Vector2f vec)
	{
		return new Vector2f(x + vec.getX(), y + vec.getY());
	}

	/**
	 * subtracts from the vector
	 * 
	 * @param vec
	 *            the value to subtract by
	 * @return returns the new vector
	 */
	public Vector2f subtract(Vector2f vec)
	{
		return new Vector2f(x - vec.getX(), y - vec.getY());
	}

	/**
	 * multiplies to the vector
	 * 
	 * @param scaler
	 *            the scaler to multiply by
	 * @return returns the new vector
	 */
	public Vector2f multiply(float scaler)
	{
		return new Vector2f(x * scaler, y * scaler);
	}

	/**
	 * multiplies to the vector
	 * 
	 * @param mat
	 *            the matrix
	 * @return returns the new matrix
	 */
	public Vector2f multiply(Matrix2f mat)
	{
		// implement later
		return null;
	}

	/**
	 * divides to the vector
	 * 
	 * @param value
	 *            the value to divide by
	 * @return returns the new vector
	 */
	public Vector2f divide(float value)
	{
		return new Vector2f(x / value, y / value);
	}

	/**
	 * makes the vector negative
	 * 
	 * @return returns the new vector
	 */
	public Vector2f negate()
	{
		return new Vector2f(-x, -y);
	}

	/**
	 * gets the distance of the vector
	 * 
	 * @param vector
	 *            the vector to get the distance
	 * @return returns the vector distance
	 */
	public float distance(Vector2f vector)
	{
		return subtract(vector).length();
	}

	/**
	 * gets the distance squared of the vector
	 * 
	 * @param other
	 *            the other vector
	 * @return returns the distance squared of the vector
	 */
	public float distanceSquared(Vector2f other)
	{
		return subtract(other).lengthSquared();
	}

	/**
	 * subtracts and multiplies the value of the vector
	 * 
	 * @param target
	 *            the vector to use
	 * @param l
	 *            the length to multiply by
	 * @return returns the new vector
	 */
	public Vector2f lerp(Vector2f target, float l)
	{
		return subtract(target).multiply(l);
	}

	/**
	 * sets and gets the angle of the vector
	 * 
	 * @param vector
	 *            the vector to use
	 * @return returns the angle
	 */
	public float angleBetween(Vector2f vector)
	{
		return (float) Math.acos(normalized().dotProduct(vector.normalized()));
	}

	/**
	 * the product of the vectors values
	 * 
	 * @param vec
	 *            the vector to use
	 * @return returns the product of the vector values
	 */
	public float dotProduct(Vector2f vec)
	{
		return x * vec.getX() + y * vec.getY();
	}

	/**
	 * normalizes the vector
	 * 
	 * @return returns the vector
	 */
	public Vector2f normalized()
	{
		float length = length();
		return new Vector2f(x / length, y / length);
	}

	/**
	 * gets the x value of the vector
	 * 
	 * @return returns the x value
	 */
	public float getX()
	{
		return x;
	}

	/**
	 * gets the y value of the vector
	 * 
	 * @return returns the y value
	 */
	public float getY()
	{
		return y;
	}

	/**
	 * sets the x value of the vector
	 * 
	 * @param x
	 *            the x value to set
	 */
	public void setX(float x)
	{
		this.x = x;
	}

	/**
	 * sets the y value of the vector
	 * 
	 * @param y
	 *            the y value to set
	 */
	public void setY(float y)
	{
		this.y = y;
	}

	/**
	 * sets both the x and y values of the vector
	 * 
	 * @param x
	 *            the x value to set
	 * @param y
	 *            the y value to set
	 */
	public void set(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	/**
	 * sets both x and y values to the same value
	 * 
	 * @param v
	 *            the value to set
	 */
	public void set(float v)
	{
		this.x = v;
		this.y = v;
	}

	/**
	 * sets the values of the vector to that of another vector
	 * 
	 * @param vec
	 *            the vector to copy
	 */
	public void set(Vector2f vec)
	{
		this.x = vec.getX();
		this.y = vec.getY();
	}

	/**
	 * sets the values the array of the vector
	 * 
	 * @param vec
	 *            the vector to use
	 */
	public void set(float[] vec)
	{
		if (vec.length == 2)
		{
			this.x = vec[0];
			this.y = vec[1];
		} else
		{
			throw new IllegalArgumentException("Array incorrect size for vector2f");
		}
	}

	/**
	 * sets the vector values to zero
	 */
	public void zero()
	{
		x = 0;
		y = 0;
	}

	/**
	 * compares two vectors to see if they are equal
	 * 
	 * @param other
	 *            the other vector to compare
	 * @return returns boolean value if vectors are equal or not
	 */
	public boolean equals(Vector2f other)
	{
		if (x == other.getX() && y == other.getY())
		{
			return true;
		} else
		{
			return false;
		}
	}

	/**
	 * compares the object values of the two vectors
	 * 
	 * @param other
	 *            the other vector to compare
	 * @return returns a boolean value if vectors are equal or not
	 */
	@Override
	public boolean equals(Object other)
	{
		if (other instanceof Vector2f)
		{
			return equals((Vector2f) other);
		} else
		{
			return false;
		}
	}

	/**
	 * the toString of the class that shows the state of the object
	 * 
	 * @return returns a string value of the state of the object
	 */
	@Override
	public String toString()
	{
		return "[" + x + ", " + y + "]";
	}

}
