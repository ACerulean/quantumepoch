package quantumepoch.utils.math;

/**
 * 4th dimension vector with 4 values that instantiates and fills the values of
 * the vector
 *
 */
public class Vector4f
{

	private float x;
	private float y;
	private float z;
	private float w;

	/**
	 * constructor that sets the values of the vector to zero
	 */
	public Vector4f()
	{
		this.x = 0;
		this.y = 0;
		this.z = 0;
		this.w = 0;
	}

	/**
	 * constructor that sets the values to the param values
	 * 
	 * @param x
	 * @param y
	 * @param z
	 * @param w
	 */
	public Vector4f(float x, float y, float z, float w)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}

	/**
	 * constructor that sets all the values to the same param value
	 * 
	 * @param v
	 */
	public Vector4f(float v)
	{
		this.x = v;
		this.y = v;
		this.z = v;
		this.w = v;
	}

	/**
	 * constructor that sets the x and y values to another vector
	 * 
	 * @param vec
	 *            the vector to copy
	 * @param z
	 *            the z value
	 * @param w
	 *            the w value
	 */
	public Vector4f(Vector2f vec, float z, float w)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = z;
		this.w = w;
	}

	/**
	 * constructor that sets the x, y and z values of another vector
	 * 
	 * @param vec
	 *            the vector to copy
	 * @param w
	 *            the w value
	 */
	public Vector4f(Vector3f vec, float w)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = vec.getZ();
		this.w = w;
	}

	/**
	 * sets the values to that of another value
	 * 
	 * @param vec
	 *            the vector to copy values from
	 */
	public Vector4f(Vector4f vec)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = vec.getZ();
		this.w = vec.getW();
	}

	/**
	 * sets the values of the vector to another vector array
	 * 
	 * @param vec
	 *            the vector to get values from
	 */
	public Vector4f(float[] vec)
	{
		if (vec.length == 4)
		{
			this.x = vec[0];
			this.y = vec[1];
			this.z = vec[2];
			this.w = vec[3];
		} else
		{
			throw new IllegalArgumentException("Array incorrect size for vector");
		}
	}

	/**
	 * gets the x value of the vector
	 * 
	 * @return returns the x value
	 */
	public float getX()
	{
		return x;
	}

	/**
	 * gets the y value of the vector
	 * 
	 * @return returns the y value
	 */
	public float getY()
	{
		return y;
	}

	/**
	 * gets the z value of the vector
	 * 
	 * @return returns the z value
	 */
	public float getZ()
	{
		return z;
	}

	/**
	 * gets the w value of the vector
	 * 
	 * @return returns the w value
	 */
	public float getW()
	{
		return w;
	}

	/**
	 * sets the x value of the vector
	 * 
	 * @param x
	 *            the x value to set
	 */
	public void setX(float x)
	{
		this.x = x;
	}

	/**
	 * sets the y value of the vector
	 * 
	 * @param y
	 *            the y value to set
	 */
	public void setY(float y)
	{
		this.y = y;
	}

	/**
	 * sets the z value of the vector
	 * 
	 * @param z
	 *            the z value to set
	 */
	public void setZ(float z)
	{
		this.z = z;
	}

	/**
	 * sets the w value of the vector
	 * 
	 * @param w
	 *            the w value to set
	 */
	public void setW(float w)
	{
		this.w = w;
	}

	/**
	 * sets all the values of the vector to the param values
	 * 
	 * @param x
	 *            the x value to set
	 * @param y
	 *            the y value to set
	 * @param z
	 *            the z value to set
	 * @param w
	 *            the w value to set
	 */
	public void set(float x, float y, float z, float w)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}

	/**
	 * sets all the vector values to the same param value
	 * 
	 * @param v
	 *            the value to set the vector values to
	 */
	public void set(float v)
	{
		this.x = v;
		this.y = v;
		this.z = v;
		this.w = v;
	}

	/**
	 * sets the x and y vector values from another vector
	 * 
	 * @param vec
	 *            the vector to copy values from
	 * @param z
	 *            the z value to set
	 * @param w
	 *            the w value to set
	 */
	public void set(Vector2f vec, float z, float w)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = z;
		this.w = w;
	}

	/**
	 * sets the x, y, and z values from another vector
	 * 
	 * @param vec
	 *            the vector to copy values from
	 * @param w
	 *            the w value to set
	 */
	public void set(Vector3f vec, float w)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = vec.getZ();
		this.w = w;
	}

	/**
	 * sets the values of the vector from another vector
	 * 
	 * @param vec
	 *            the vector to copy values from
	 */
	public void set(Vector4f vec)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = vec.getZ();
		this.w = vec.getW();
	}

	/**
	 * sets the values of the vector a different vector array
	 * 
	 * @param vec
	 *            the vector array to copy values from
	 */
	public void set(float[] vec)
	{
		if (vec.length == 4)
		{
			this.x = vec[0];
			this.y = vec[1];
			this.z = vec[2];
			this.w = vec[3];
		} else
		{
			throw new IllegalArgumentException("Array incorrect size for vector");
		}
	}

	// vec operators

	/**
	 * adds to the vector
	 * 
	 * @param vec
	 *            the vector to add to
	 * @return returns the new vector
	 */
	public Vector4f add(Vector4f vec)
	{
		return new Vector4f(this.x + vec.getX(), this.y + vec.getY(), this.z + vec.getZ(), this.w + vec.getW());
	}

	/**
	 * subtracts from the vector
	 * 
	 * @param vec
	 *            the vector to subtract from
	 * @return returns the new vector
	 */
	public Vector4f subtract(Vector4f vec)
	{
		return new Vector4f(this.x - vec.getX(), this.y - vec.getY(), this.z - vec.getZ(), this.w - vec.getW());
	}

	/**
	 * multiplies the values of the vector
	 * 
	 * @param vec
	 *            the vector to use
	 * @return returns the new vector
	 */
	public Vector4f multiply(Vector4f vec)
	{
		return new Vector4f(this.x * vec.getX(), this.y * vec.getY(), this.z * vec.getZ(), this.w * vec.getW());
	}

	/**
	 * override method of the multiplication that multiplies the vector values
	 * 
	 * @param mat
	 *            the matrix to multiply values by
	 * @return returns the new vector
	 */
	public Vector4f multiply(Matrix4f mat)
	{
		Vector4f res = new Vector4f();
		res.setX(mat.getElement(0, 0) * x + mat.getElement(0, 1) * y + mat.getElement(0, 2) * z + mat.getElement(0, 3) * w);
		res.setY(mat.getElement(1, 0) * x + mat.getElement(1, 1) * y + mat.getElement(1, 2) * z + mat.getElement(1, 3) * w);
		res.setZ(mat.getElement(2, 0) * x + mat.getElement(2, 1) * y + mat.getElement(2, 2) * z + mat.getElement(2, 3) * w);
		res.setW(mat.getElement(3, 0) * x + mat.getElement(3, 1) * y + mat.getElement(3, 2) * z + mat.getElement(3, 3) * w);
		return res;
	}

	/**
	 * sets the vector values to a power
	 * 
	 * @param power
	 *            the power to set the values to
	 * @return returns the new vector
	 */
	public Vector4f pow(double power)
	{
		return new Vector4f((float) Math.pow(this.x, power), (float) Math.pow(this.y, power), (float) Math.pow(this.z, power), (float) Math.pow(this.w, power));
	}

	/**
	 * gets the length of the vector
	 * 
	 * @return returns the vectors length
	 */
	public double length()
	{
		return Math.sqrt(x * x + y * y + z * z + w * w);
	}

	/**
	 * gets the product of all the vector values
	 * 
	 * @param vec
	 *            the vector to use
	 * @return returns the product of the values
	 */
	public double dot(Vector4f vec)
	{
		return this.x * vec.getX() + this.y * vec.getY() + this.z * vec.getZ() + this.w * vec.getW();
	}

	/**
	 * normalizes the vector's values
	 */
	public void normalize()
	{
		float length = (float) length();
		this.x /= length;
		this.y /= length;
		this.z /= length;
		this.w /= length;
	}

	/**
	 * multiplies the vector values by a scaler
	 * 
	 * @param scaler
	 *            the value to scale the vector by
	 */
	public void scale(float scaler)
	{
		this.x *= scaler;
		this.y *= scaler;
		this.z *= scaler;
		this.w *= scaler;
	}

	/**
	 * inverts the value of x
	 */
	public void invertX()
	{
		this.x *= -1;
	}

	/**
	 * inverts the value of y
	 */
	public void invertY()
	{
		this.y *= -1;
	}

	/**
	 * inverts the value of z
	 */
	public void invertZ()
	{
		this.z *= -1;
	}

	/**
	 * inverts the value of w
	 */
	public void invertW()
	{
		this.w *= -1;
	}

	/**
	 * inverts all the values of the vector
	 */
	public void invert()
	{
		scale(-1);
	}

	/**
	 * translates the vector values to another location
	 * 
	 * @param x
	 *            the x value to translate by
	 * @param y
	 *            the y value to translate by
	 * @param z
	 *            the z value to translate by
	 * @param w
	 *            the w value to translate by
	 */
	public void translate(float x, float y, float z, float w)
	{
		this.x += x;
		this.y += y;
		this.z += z;
		this.w += w;
	}

	/**
	 * multiplies all the vector values by the param values
	 * 
	 * @param x
	 *            the x value to multiply by
	 * @param y
	 *            the y value to multiply by
	 * @param z
	 *            the z value to multiply by
	 * @param w
	 *            the w value to multiply by
	 */
	public void mult(float x, float y, float z, float w)
	{
		this.x *= x;
		this.y *= y;
		this.z *= z;
		this.w *= w;
	}

}
