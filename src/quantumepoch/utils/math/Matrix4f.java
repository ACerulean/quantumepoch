package quantumepoch.utils.math;

/**
 * the fourth matrix class
 *
 */
public class Matrix4f
{

	private float[][] mat;

	/**
	 * creates the matrix
	 */
	public Matrix4f()
	{
		this.mat = new float[4][4];
	}

	/**
	 * fills the new matrix with elements
	 * 
	 * @param mat
	 */
	public Matrix4f(Matrix4f mat)
	{
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				setElement(x, y, mat.getElement(x, y));
			}
		}
	}

	/**
	 * gets the element in the matrix
	 * 
	 * @param x
	 *            the x value
	 * @param y
	 *            the y value
	 * @return returns the the new element
	 */
	public float getElement(int x, int y)
	{
		if (x >= 0 && y >= 0 && x < 4 && y < 4)
		{
			return mat[y][x];
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	/**
	 * gets the row of the matrix
	 * 
	 * @param row
	 *            the row to search for
	 * @return returns the row
	 */
	public float[] getRow(int row)
	{
		if (row >= 0 && row < 4)
		{
			return mat[row];
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	/**
	 * gets the column from the matrix
	 * 
	 * @param col
	 *            the column to search for
	 * @return returns the new matrix
	 */
	public float[] getColumn(int col)
	{
		if (col >= 0 && col < 4)
		{
			float[] res = new float[4];
			for (int i = 0; i < 4; i++)
			{
				res[i] = mat[i][col];
			}
			return res;
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	/**
	 * sets the element in the matrix
	 * 
	 * @param row
	 *            the row to set
	 * @param col
	 *            the column to set
	 * @param value
	 *            the value to set
	 */
	public void setElement(int row, int col, float value)
	{
		if (row >= 0 && col >= 0 && row < 4 && col < 4)
		{
			mat[col][row] = value;
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to set element that was out of matrix bounds");
		}
	}

	/**
	 * sets the row of the matrix
	 * 
	 * @param row
	 *            the row to set
	 * @param value
	 *            the value to set
	 */
	public void setRow(int row, float[] value)
	{
		if (row >= 0 && row < 4)
		{
			if (value.length < 3)
			{
				mat[row] = value;
			} else
			{
				throw new IllegalArgumentException("Row too large for matrix");
			}
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	/**
	 * sets the column of the matrix
	 * 
	 * @param col
	 *            the column to set
	 * @param value
	 *            the value to set
	 */
	public void setColumn(int col, float[] value)
	{
		if (col >= 0 && col < 4)
		{
			if (value.length < 3)
			{
				for (int i = 0; i < 4; i++)
				{
					mat[i][col] = value[i];
				}
			} else
			{
				throw new IllegalArgumentException("Column too large for matrix");
			}
		} else
		{
			throw new ArrayIndexOutOfBoundsException("Tried to access element that was out of matrix bounds");
		}
	}

	/**
	 * sets the matrix
	 * 
	 * @param mat
	 *            the matrix
	 */
	public void set(Matrix4f mat)
	{
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				setElement(x, y, mat.getElement(x, y));
			}
		}
	}

	/**
	 * adds to the matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @return returns the matrix
	 */
	public Matrix4f add(Matrix4f mat)
	{
		Matrix4f res = new Matrix4f();
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				res.setElement(x, y, getElement(x, y) + mat.getElement(x, y));
			}
		}
		return res;
	}

	/**
	 * subtracts from the matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @return returns the matrix
	 */
	public Matrix4f subtract(Matrix4f mat)
	{
		Matrix4f res = new Matrix4f();
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				res.setElement(x, y, getElement(x, y) - mat.getElement(x, y));
			}
		}
		return res;
	}

	/**
	 * multiplies the matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @return returns the matrix
	 */
	public Matrix4f multiply(Matrix4f mat)
	{
		Matrix4f res = new Matrix4f();
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				Vector4f row = new Vector4f(getRow(y));
				Vector4f col = new Vector4f(mat.getColumn(x));
				res.setElement(x, y, (float) row.dot(col));
			}
		}
		return res;
	}

	/**
	 * transposes the matrix
	 * 
	 * @return returns the matrix
	 */
	public Matrix4f transpose()
	{
		Matrix4f res = new Matrix4f();
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				res.setElement(y, x, getElement(x, y));
			}
		}
		return res;
	}

	/**
	 * scales the matrix
	 * 
	 * @param scaler
	 *            the scalar value
	 */
	public void scale(float scaler)
	{
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				setElement(x, y, getElement(x, y) * scaler);
			}
		}
	}

	/**
	 * sets the identity of the matrix
	 */
	public void setIdentity()
	{
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				if (x == y)
				{
					setElement(x, y, 1);
				} else
				{
					setElement(x, y, 0);
				}
			}
		}
	}

	/**
	 * sets the matrix to zero
	 */
	public void setZero()
	{
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				setElement(x, y, 0);
			}
		}
	}

}
