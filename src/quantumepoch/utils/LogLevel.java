package quantumepoch.utils;

/**
 * the enumeration of the log level
 *
 */
public enum LogLevel
{

	INFO, WARNING, FATAL

}
