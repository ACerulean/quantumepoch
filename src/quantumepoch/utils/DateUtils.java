package quantumepoch.utils;

import java.util.Calendar;

/**
 * gets the different times of the game for calcs
 *
 */
public class DateUtils
{

	/**
	 * gets the seconds for the time
	 * 
	 * @return returns time
	 */
	public static int getSeconds()
	{
		return Calendar.getInstance().get(Calendar.SECOND);
	}

	/**
	 * gets the minutes for the time
	 * 
	 * @return returns minutes
	 */
	public static int getMinutes()
	{
		return Calendar.getInstance().get(Calendar.MINUTE);
	}

	/**
	 * gets the hours for the time (first 12)
	 * 
	 * @return returns hours
	 */
	public static int getHours12()
	{
		return Calendar.getInstance().get(Calendar.HOUR);
	}

	/**
	 * gets the full 24 hours for the time
	 * 
	 * @return returns hours
	 */
	public static int getHours24()
	{
		return Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
	}

	/**
	 * gets the day of the month for the time
	 * 
	 * @return returns the day
	 */
	public static int getDayOfMonth()
	{
		return Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * gets the months for the time
	 * 
	 * @return returns the month
	 */
	public static int getMonth()
	{
		return Calendar.getInstance().get(Calendar.MONTH) + 1;
	}

	/**
	 * gets the year for the time
	 * 
	 * @return returns the year
	 */
	public static int getYear()
	{
		return Calendar.getInstance().get(Calendar.YEAR);
	}
}
