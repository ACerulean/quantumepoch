package quantumepoch.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Overall utilities of the class to be used
 *
 */
public class GeneralUtils
{

	/**
	 * removes empty portions of the string
	 * 
	 * @param array
	 *            the array to remove empty string from
	 * @return returns new updated string
	 */
	public static String[] removeEmptyStrings(String[] array)
	{
		List<String> res = new ArrayList<String>();

		for (String x : array)
		{
			if (!x.equals(""))
			{
				res.add(x);
			}
		}

		String[] resToArray = new String[res.size()];
		res.toArray(resToArray);

		return resToArray;
	}

	/**
	 * corrects the extension and path of the file
	 * 
	 * @param path
	 *            the path for the file
	 * @param ext
	 *            the extension to check for
	 * @return returns true if correct extension
	 */
	public static boolean correctExtension(String path, String ext)
	{
		String[] fileTokens = path.split("\\.");
		if (fileTokens[fileTokens.length - 1].equals(ext))
		{
			return true;
		} else
		{
			return false;
		}
	}

}
