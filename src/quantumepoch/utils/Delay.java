package quantumepoch.utils;

/**
 * delays the time for certain functions and events that occur
 *
 */
public class Delay
{

	private long delay;
	private long lastCheck;

	/**
	 * constructor that sets the delay and time of delay
	 * 
	 * @param delay
	 */
	public Delay(long delay)
	{
		this.delay = delay;
		this.lastCheck = System.currentTimeMillis();
	}

	/**
	 * gets the time to delay for
	 * 
	 * @return returns delay time
	 */
	public long getDelay()
	{
		return delay;
	}

	/**
	 * sets the time to delay for
	 * 
	 * @param delay
	 *            the time to delay for
	 */
	public void setDelay(long delay)
	{
		this.delay = delay;
	}

	/**
	 * the time remaining of the delay
	 * 
	 * @return returns delay time remaining
	 */
	public long remaining()
	{
		return delay - System.currentTimeMillis() + lastCheck;
	}

	/**
	 * resets the time delay
	 */
	public void reset()
	{
		this.lastCheck = System.currentTimeMillis();
	}

	/**
	 * forces the delay to start
	 */
	public void force()
	{
		this.lastCheck = System.currentTimeMillis() - delay;
	}

	/**
	 * asks if the delay is ready to be executed
	 * 
	 * @return
	 */
	public boolean isReady()
	{
		if (remaining() <= 0)
		{
			lastCheck = System.currentTimeMillis();
			return true;
		} else
		{
			return false;
		}
	}

}
