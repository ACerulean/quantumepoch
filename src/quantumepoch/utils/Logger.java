package quantumepoch.utils;

import java.io.IOException;
import java.io.OutputStream;

/**
 * <<<<<<< HEAD logs the input/output stream of files and displays then in crash reports for user. ======= logs the input/output stream of files and displays then in crash reports for user. >>>>>>> stash
 *
 */
public class Logger
{

	private OutputStream[] output;

	/**
	 * the logger to output logs to
	 * 
	 * @param output
	 *            the output of log
	 */
	public Logger(OutputStream[] output)
	{
		this.output = output;
	}

	/**
	 * constructor that returns the output, takes no params
	 * 
	 * @return
	 */
	public OutputStream[] getOutput()
	{
		return output;
	}

	/**
	 * sets the output for the log
	 * 
	 * @param output
	 *            the output to display
	 */
	public void setOutput(OutputStream[] output)
	{
		this.output = output;
	}

	/**
	 * the log to output to the screen with different attributes
	 * 
	 * @param logLevel
	 *            the level of the log
	 * @param message
	 *            the message to be displayed on the log
	 * @param timeStamp
	 *            the time of the log
	 */
	public void log(LogLevel logLevel, String message, boolean timeStamp)
	{
		StringBuilder string = new StringBuilder();

		if (timeStamp)
		{
			string.append("[" + DateUtils.getHours12() + ":" + DateUtils.getMinutes() + ":" + DateUtils.getSeconds() + "]");
		}

		switch (logLevel)
		{
		case INFO:
			string.append("[INFO]: ");
			break;
		case WARNING:
			string.append("[WARNING]: ");
			break;
		case FATAL:
			string.append("[FATAL]: ");
			break;
		}

		string.append(message + "\n");

		try
		{
			for (OutputStream os : output)
				os.write(string.toString().getBytes());
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * the info to be displayed on the log
	 * 
	 * @param message
	 *            the message of the log
	 * @param timeStamp
	 *            the time of the log
	 */
	public void info(String message, boolean timeStamp)
	{
		log(LogLevel.INFO, message, timeStamp);
	}

	/**
	 * the info on the log
	 * 
	 * @param message
	 *            the message of the log
	 */
	public void info(String message)
	{
		info(message, true);
	}

	/**
	 * the warning message displayed on the log
	 * 
	 * @param message
	 *            the message of the log
	 * @param timeStamp
	 *            the time of the log warning
	 */
	public void warning(String message, boolean timeStamp)
	{
		log(LogLevel.WARNING, message, timeStamp);
	}

	/**
	 * the warning message for the log
	 * 
	 * @param message
	 *            the message for the warning
	 */
	public void warning(String message)
	{
		warning(message, true);
	}

	/**
	 * sends a fatal warning message for log
	 * 
	 * @param message
	 *            the fatal message
	 * @param timeStamp
	 *            the fatal error time
	 */
	public void fatal(String message, boolean timeStamp)
	{
		log(LogLevel.FATAL, message, timeStamp);
	}

	/**
	 * the fatal message output
	 * 
	 * @param message
	 *            the fatal message string
	 */
	public void fatal(String message)
	{
		fatal(message, true);
	}
}
