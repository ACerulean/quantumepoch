package quantumepoch.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * counts amount of lines in the file and sets path for files
 *
 */
public class LineCounter
{

	public static final String PATH = "src";

	/**
	 * main for counter files and lines
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{

		System.out.println(PATH);

		List<File> allFiles = listAllFiles(new File(PATH));
		int lines = 0;
		int characters = 0;

		// bubble sort the files in terms of lexicographic order
		for (int i = allFiles.size() - 1; i >= 0; i--)
		{
			for (int j = 0; j < i; j++)
			{
				if (allFiles.get(j).getName().compareTo(allFiles.get(j + 1).getName()) > 0)
				{
					File temp = allFiles.get(j);
					allFiles.set(j, allFiles.get(j + 1));
					allFiles.set(j + 1, temp);
				}
			}
		}

		for (File f : allFiles)
		{
			int i = lineCount(f);
			lines += i;
		}

		for (File f : allFiles)
		{
			characters += charCount(f);
		}

		System.out.println(lines + " lines of code in " + allFiles.size() + " files, avg = " + ((double) lines / allFiles.size()));
		System.out.println("Char count: " + characters + ", avg = " + (characters / allFiles.size()));

	}

	/**
	 * counts the characters in the file
	 * 
	 * @param file
	 *            the file
	 * @return returns the number of characters in the file
	 */
	public static int charCount(File file)
	{
		try
		{
			FileInputStream input = null;
			input = new FileInputStream(file);

			int i = 0;

			while (input.read() != -1)
			{
				i++;
			}

			input.close();

			return i;
		}
		catch (IOException e)
		{
			System.out.println("Caught");
			e.printStackTrace();
			return -1;
		}

	}

	/**
	 * counts the number of lines in the file
	 * 
	 * @param file
	 *            the file
	 * @return returns the number of lines in the file
	 */
	public static int lineCount(File file)
	{
		BufferedReader stream = null;
		try
		{
			stream = new BufferedReader(new FileReader(file));
			int count = 0;
			String line = "";
			while ((line = stream.readLine()) != null)
			{
				if (line.contains("System.out"))
					System.out.println(file.getName() + " has reset graphics");
				// if (line.contains("new SpriteBatch"))
				// {
				// System.out.println("Sprite batch at line in " +
				// file.getName() + " at line " + count);
				// System.out.println(line);
				// System.out.println();
				// }
				if (!line.equals(""))
				{
					count++;
				} else
				{
				}
			}
			stream.close();
			return count;
		}
		catch (Exception e)
		{
			return 0;
		}
	}

	/**
	 * makes a List of all the files
	 * 
	 * @param parent
	 *            the parent file
	 * @return returns the list of files
	 */
	public static List<File> listAllFiles(File parent)
	{
		if (parent != null)
		{
			List<File> res = new ArrayList<File>();
			for (File f : parent.listFiles())
			{
				String n = f.getName();
				if (f.isDirectory())
				{
					res.addAll(listAllFiles(f));
				} else if (n.contains(".java") || n.contains(".vs") || n.contains(".fs") || n.contains(".h") || n.contains(".cpp"))
				{
					System.out.println(n);
					res.add(f);
				}

			}
			return res;
		}
		return null;
	}
}
