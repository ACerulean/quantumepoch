package quantumepoch.utils;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;

import quantumepoch.core.Core;
import quantumepoch.utils.math.Matrix2f;
import quantumepoch.utils.math.Matrix3f;
import quantumepoch.utils.math.Matrix4f;
import sun.awt.image.ToolkitImage;

/**
 * Basic input/output utilities for the class
 *
 */
public class IOUtils
{

	/**
	 * gets the resources from url for utils
	 * 
	 * @param path
	 *            path to set url for
	 * @return returns path for resource
	 */
	public static URL getInternalResourceAsURL(String path)
	{
		return IOUtils.class.getResource(correctPath(path));
	}

	/**
	 * gets the resources from file for utils
	 * 
	 * @param path
	 *            sets path for the file
	 * @return returns the path for file
	 */
	public static File getInternalResourceAsFile(String path)
	{
		return new File(IOUtils.class.getResource(correctPath(path)).getFile());
	}

	/**
	 * gets the resources from the input stream path
	 * 
	 * @param path
	 *            the path for the resources
	 * @return returns path for input stream
	 */
	public static InputStream getInternalResourceAsStream(String path)
	{
		return IOUtils.class.getResourceAsStream(correctPath(path));
	}

	/**
	 * gets the file name from the path
	 * 
	 * @param path
	 *            the path to the file
	 * @return returns the file name as string
	 */
	public static String getFileName(String path)
	{
		return new File(path).getName();
	}

	/**
	 * buffers the image given
	 * 
	 * @param img
	 *            image to buffer
	 * @return returns the new image
	 */
	public static BufferedImage toBufferedImage(ToolkitImage img)
	{
		BufferedImage bimage = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = bimage.createGraphics();
		g.drawImage(img, 0, 0, null);
		g.dispose();
		return bimage;
	}

	/**
	 * gets the root path for the file path
	 * 
	 * @return returns the file from the path
	 */
	public static String getRootPath()
	{
		try
		{
			return URLDecoder.decode(IOUtils.class.getProtectionDomain().getCodeSource().getLocation().getFile(), "UTF-8");
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * gets parent path of file
	 * 
	 * @return returns parent path
	 */
	public static String getRootParentPath()
	{
		return (new File(getRootPath())).getParent();
	}

	/**
	 * loads the file to a string
	 * 
	 * @param path
	 *            the path to the file
	 * @return returns the file as string
	 */
	public static String loadInternalFileAsString(String path)
	{
		try
		{

			BufferedReader in = new BufferedReader(new InputStreamReader(IOUtils.class.getResourceAsStream(correctPath(path))));

			StringBuilder fileSource = new StringBuilder();

			String currentLine = "";

			while ((currentLine = in.readLine()) != null)
			{
				fileSource.append(currentLine + "\n");
			}

			in.close();
			return fileSource.toString();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * loads and image from the file
	 * 
	 * @param path
	 *            the path to the file
	 * @return returns the buffered image
	 */
	public static BufferedImage loadInteralImage(String path)
	{

		String correctPath = correctPath(path);

		try
		{
			return ImageIO.read(IOUtils.class.getResourceAsStream(correctPath));
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}

	}

	/**
	 * sets and gets the right path for files
	 * 
	 * @param path
	 *            the path for the files
	 * @return returns path for files
	 */
	public static String correctPath(String path)
	{
		StringBuilder builder = new StringBuilder();

		path.replace('\\', '/');

		builder.append(path);

		if (!path.startsWith("/"))
		{
			builder.insert(0, '/');
		}

		return builder.toString();
	}

	/**
	 * turns file to string format
	 * 
	 * @param path
	 *            the path to the file
	 * @return returns the file as a string
	 */
	public static String loadFileAsString(String path)
	{
		return loadFileAsString(new File(path));
	}

	/**
	 * loads file as string
	 * 
	 * @param file
	 *            the file to turn into a string
	 * @return returns the file as string
	 */
	public static String loadFileAsString(File file)
	{
		if (file != null && file.exists())
		{
			try
			{
				BufferedReader in = new BufferedReader(new FileReader(file));
				StringBuilder fileSource = new StringBuilder();

				String currentLine = "";

				while ((currentLine = in.readLine()) != null)
				{
					fileSource.append(currentLine + "\n");
				}

				in.close();

				return fileSource.toString();
			}
			catch (IOException e)
			{
				System.err.println("Error loading file as string at: " + file.getAbsolutePath());
				e.printStackTrace();
				return null;
			}
		} else
		{
			return null;
		}
	}

	/**
	 * determines if the file is a jar file
	 * 
	 * @return returns true if jar
	 */
	public static boolean isJar()
	{
		return Core.class.getProtectionDomain().getCodeSource().getLocation().getFile().contains(".jar");
	}

	/**
	 * buffers the bytes for utils
	 * 
	 * @param size
	 *            the size to buffer the bytes
	 * @return returns buffer size
	 */
	public static ByteBuffer createByteBuffer(int size)
	{
		return BufferUtils.createByteBuffer(size);
	}

	/**
	 * size to buffer the in integers
	 * 
	 * @param size
	 *            the size to buffer
	 * @return returns buffer size
	 */
	public static IntBuffer createIntBuffer(int size)
	{
		return BufferUtils.createIntBuffer(size);
	}

	/**
	 * size to buffer in short data types
	 * 
	 * @param size
	 *            the size to buffer
	 * @return returns buffer size
	 */
	public static ShortBuffer createShortBuffer(int size)
	{
		return BufferUtils.createShortBuffer(size);
	}

	/**
	 * size to buffer in float type
	 * 
	 * @param size
	 *            the size to buffer
	 * @return returns buffer size
	 */
	public static FloatBuffer createFloatBuffer(int size)
	{
		return BufferUtils.createFloatBuffer(size);
	}

	/**
	 * array to buffer size
	 * 
	 * @param array
	 *            the array to buffer
	 * @return returns result of buffer
	 */
	public static ByteBuffer byteArrayToBuffer(byte[] array)
	{
		ByteBuffer result = createByteBuffer(array.length);

		for (byte b : array)
		{
			result.put(b);
		}

		result.flip();

		return result;
	}

	/**
	 * array to buffer in short size
	 * 
	 * @param array
	 *            the array to buffer
	 * @return returns result of buffer
	 */
	public static ShortBuffer shortArrayToBuffer(short[] array)
	{
		ShortBuffer result = createShortBuffer(array.length);

		for (short b : array)
		{
			result.put(b);
		}

		result.flip();

		return result;
	}

	/**
	 * matrix to float buffering
	 * 
	 * @param mat
	 *            the matrix to buffer
	 * @return returns result of buffer
	 */
	public static FloatBuffer mat2ToFloatBuffer(Matrix2f mat)
	{
		FloatBuffer res = createFloatBuffer(4);
		for (int x = 0; x < 2; x++)
		{
			for (int y = 0; y < 2; y++)
			{
				res.put(mat.getElement(x, y));
			}
		}
		res.flip();
		return res;
	}

	/**
	 * matrix to float buffer
	 * 
	 * @param mat
	 *            the matrix to buffer
	 * @return returns the result of the buffer
	 */
	public static FloatBuffer mat3ToFloatBuffer(Matrix3f mat)
	{
		FloatBuffer res = createFloatBuffer(9);
		for (int x = 0; x < 3; x++)
		{
			for (int y = 0; y < 3; y++)
			{
				res.put(mat.getElement(x, y));
			}
		}
		res.flip();
		return res;
	}

	/**
	 * matrix to float buffer
	 * 
	 * @param mat
	 *            the matrix to buffer
	 * @return returns the result of the buffer
	 */
	public static FloatBuffer mat4ToFloatBuffer(Matrix4f mat)
	{
		FloatBuffer res = createFloatBuffer(16);
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				res.put(mat.getElement(x, y));
			}
		}
		res.flip();
		return res;
	}

	/**
	 * changes the byte array to and integer
	 * 
	 * @param bytes
	 *            the byte array to convert
	 * @return returns the result of the conversion
	 */
	public static int byteArrayToInt(byte[] bytes)
	{
		int res = 0;
		if (bytes.length == 4)
		{
			for (int i = 0; i < 4; i++)
			{
				res |= bytes[i];
			}
			return res;
		} else
		{
			throw new IllegalArgumentException("Byte array length must be equal to 4");
		}
	}

	/**
	 * converts integer to byte array
	 * 
	 * @param i
	 *            the integer to convert
	 * @return returns the new byte array
	 */
	public static byte[] intToByteArray(int i)
	{
		byte[] bytes = new byte[4];
		bytes[0] = (byte) ((i >> 24) & 0xFF);
		bytes[1] = (byte) ((i >> 16) & 0xFF);
		bytes[2] = (byte) ((i >> 8) & 0xFF);
		bytes[3] = (byte) ((i >> 0) & 0xFF);
		return bytes;
	}
}
