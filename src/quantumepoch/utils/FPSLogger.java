package quantumepoch.utils;

import quantumepoch.core.Core;

/**
 * Keeps track of the fps and displays it on screen
 *
 */
public class FPSLogger
{

	private Delay delay;

	/**
	 * constructor setting the delay until displaying new fps
	 */
	public FPSLogger()
	{
		delay = new Delay(1000);
	}

	/**
	 * asks if ready to display new fps log
	 */
	public void log()
	{
		if (delay.isReady())
		{
			Core.getLog().info("FPS: " + Core.getCore().getFps() + " UPS: " + Core.getCore().getUps());
		}
	}

}
