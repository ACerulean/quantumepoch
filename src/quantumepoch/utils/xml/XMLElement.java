package quantumepoch.utils.xml;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * sets the name, values, and children/attributes of the xml files and elements
 *
 */
public class XMLElement
{

	private String name;
	private String value;
	private List<XMLElement> children;
	private List<XMLAttrib> attribs;

	/**
	 * constructor setting the values to null
	 */
	public XMLElement()
	{
		this(null, null);
	}

	/**
	 * constructor that sets the values to the param values
	 * 
	 * @param name
	 * @param value
	 */
	public XMLElement(String name, String value)
	{
		this.name = name;
		this.value = value;
		children = new ArrayList<XMLElement>();
		attribs = new ArrayList<XMLAttrib>();
	}

	/**
	 * gets the name of the element
	 * 
	 * @return returns the element
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * gets the value of the elements
	 * 
	 * @return returns the value of the elements
	 */
	public String getValue()
	{
		return value;
	}

	/**
	 * gets the children of the elements
	 * 
	 * @return returns the children elements
	 */
	public List<XMLElement> getChildren()
	{
		return children;
	}

	/**
	 * gets a list of the element descendants
	 * 
	 * @return returns the list of element descendants
	 */
	public List<XMLElement> getDescendents()
	{
		List<XMLElement> res = new ArrayList<XMLElement>();
		for (XMLElement child : children)
		{
			res.add(child);
			if (child.getChildren().size() > 0)
			{
				res.addAll(child.getDescendents());
			}
		}
		return res;
	}

	/**
	 * gets the attributes of the elements class
	 * 
	 * @return
	 */
	public List<XMLAttrib> getAttribs()
	{
		return attribs;
	}

	/**
	 * gets the attributes (mainly the name) of the class
	 * 
	 * @param name
	 *            the name to get
	 * @return returns the name or null
	 */
	public XMLAttrib getAttrib(String name)
	{
		for (XMLAttrib attrib : getAttribs())
		{
			if (attrib.getKey().equals(name))
			{
				return attrib;
			}
		}
		return null;
	}

	/**
	 * gets the child through the name given
	 * 
	 * @param name
	 *            the name to search for
	 * @return returns the child of the name
	 */
	public XMLElement getChildByName(String name)
	{
		List<XMLElement> result = getChildrenByName(name);
		if (result.size() > 0)
		{
			return result.get(0);
		} else
		{
			return null;
		}
	}

	/**
	 * gets the child by name given to search for
	 * 
	 * @param name
	 *            the name to search for
	 * @return returns the result of the search
	 */
	public List<XMLElement> getChildrenByName(String name)
	{
		List<XMLElement> result = new ArrayList<XMLElement>();
		for (XMLElement element : children)
		{
			if (element.getName().equals(name))
			{
				result.add(element);
			}
		}
		return result;
	}

	/**
	 * adds a child to the child array
	 * 
	 * @param child
	 *            the child to add
	 */
	public void addChild(XMLElement child)
	{
		this.children.add(child);
	}

	/**
	 * adds a node to the node list
	 * 
	 * @param nodeList
	 *            the node list to add to
	 */
	public void addAllNodesAsChildren(NodeList nodeList)
	{
		for (int i = 0; i < nodeList.getLength(); i++)
		{
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE)
			{

				XMLElement element = new XMLElement();
				element.setName(node.getNodeName());

				if (node.hasChildNodes())
				{
					NodeList children = node.getChildNodes();
					boolean child = true;
					for (int j = 0; j < children.getLength(); j++)
					{
						if (children.item(j).getNodeType() == Node.ELEMENT_NODE)
						{
							child = false;
						}
					}

					if (child)
					{
						element.setValue(node.getTextContent());
					}
				}

				if (node.hasAttributes())
				{
					NamedNodeMap attributes = node.getAttributes();
					List<XMLAttrib> attribs = new ArrayList<XMLAttrib>();
					for (int j = 0; j < attributes.getLength(); j++)
					{
						String key = attributes.item(j).getNodeName();
						String value = attributes.item(j).getNodeValue();
						attribs.add(new XMLAttrib(key, value));
					}
					element.addAllAttributes(attribs);
				}
				addChild(element);

				if (node.hasChildNodes())
				{
					element.addAllNodesAsChildren(node.getChildNodes());
				} else
				{
				}
			}

		}
	}

	/**
	 * removes a child from the array
	 * 
	 * @param child
	 */
	public void removeChild(XMLElement child)
	{
		this.children.remove(child);
	}

	/**
	 * adds an attribute to the class/array
	 * 
	 * @param attrib
	 *            the attribute to add
	 */
	public void addAttrib(XMLAttrib attrib)
	{
		this.attribs.add(attrib);
	}

	/**
	 * adds all the attributes to the class/array
	 * 
	 * @param attribs
	 */
	public void addAllAttributes(List<XMLAttrib> attribs)
	{
		this.attribs.addAll(attribs);
	}

	/**
	 * removes an attribute from the class/array
	 * 
	 * @param attrib
	 *            the attribute to remove
	 */
	public void removeAttrib(XMLAttrib attrib)
	{
		this.attribs.remove(attrib);
	}

	/**
	 * sets the name of the element
	 * 
	 * @param name
	 *            the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * the value to set
	 * 
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value)
	{
		this.value = value;
	}

	/**
	 * tabulates the string and builds a string and counter
	 * 
	 * @param builder
	 *            the builder of the string
	 * @param count
	 *            the value to count
	 */
	public void tabulate(StringBuilder builder, int count)
	{
		for (int i = 0; i < count; i++)
		{
			builder.append('\t');
		}
	}

	/**
	 * converts the child object/class to a string value
	 * 
	 * @param tabs
	 * @return
	 */
	public String toStringWithChildren(int tabs)
	{
		StringBuilder builder = new StringBuilder();

		// add tabs as needed
		tabulate(builder, tabs);

		// add start tag and attributes
		builder.append("<" + name + "");
		if (attribs.size() > 0)
		{
			for (int i = 0; i < attribs.size(); i++)
			{
				XMLAttrib attrib = attribs.get(i);
				builder.append(" " + attrib.getKey() + "=" + "\"" + attrib.getValue() + "\"");
				/*
				 * if (i < (attribs.size() - 1)) { builder.append(" "); }
				 */
			}
		}

		builder.append(">");

		// test if there are children

		if (getChildren().size() > 0)
		{
			builder.append("\n");
			for (XMLElement child : children)
			{
				builder.append(child.toStringWithChildren(tabs + 1));
			}
			tabulate(builder, tabs);
			builder.append("</" + name + ">\n");
		} else
		{
			if (value != null)
			{
				builder.append(value);
			}
			builder.append("</" + name + ">\n");
		}

		return builder.toString();
	}

	/**
	 * the toString method that shows the state of the object and returns it in
	 * a string form
	 * 
	 * @return returns the string value of the state of the object
	 */
	@Override
	public String toString()
	{
		return "";
	}
}
