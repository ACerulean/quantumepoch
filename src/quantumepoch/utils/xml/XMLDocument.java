package quantumepoch.utils.xml;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * gets and sets the files and headers of the XML documents created by the
 * program
 *
 */
public class XMLDocument
{

	private XMLHeader header;
	private File file;
	private XMLElement rootElement;

	/**
	 * constructor that sets the file of the document
	 * 
	 * @param file
	 */
	public XMLDocument(File file)
	{
		this.file = file;
	}

	/**
	 * gets the header of the xml file
	 * 
	 * @return returns the header file
	 */
	public XMLHeader getXmlHeader()
	{
		return header;
	}

	/**
	 * gets the xml file
	 * 
	 * @return returns the file
	 */
	public File getFile()
	{
		return file;
	}

	/**
	 * gets the root element of the xml file
	 * 
	 * @return returns the root element
	 */
	public XMLElement getRootElement()
	{
		return rootElement;
	}

	/**
	 * sets the xml header file
	 * 
	 * @param header
	 *            the header to set
	 */
	public void setXmlHeader(XMLHeader header)
	{
		this.header = header;
	}

	/**
	 * sets the xml file
	 * 
	 * @param file
	 *            the file to set
	 */
	public void setFile(File file)
	{
		this.file = file;
	}

	/**
	 * sets the root element of the file
	 * 
	 * @param rootElement
	 *            the value to set
	 */
	public void setRootElement(XMLElement rootElement)
	{
		this.rootElement = rootElement;
	}

	/**
	 * loads the xml document and header files from the report
	 */
	public void load()
	{

		Document doc = createDOMDocument();

		// initialize root
		Element root = doc.getDocumentElement();
		rootElement = new XMLElement();
		rootElement.setName(root.getNodeName());

		// add atribs
		NamedNodeMap attributes = root.getAttributes();
		List<XMLAttrib> attribs = new ArrayList<XMLAttrib>();
		for (int i = 0; i < attributes.getLength(); i++)
		{
			String key = attributes.item(i).getNodeName();
			String value = attributes.item(i).getNodeValue();
			attribs.add(new XMLAttrib(key, value));
		}
		rootElement.addAllAttributes(attribs);

		// add children
		NodeList children = root.getChildNodes();
		rootElement.addAllNodesAsChildren(children);
	}

	/**
	 * creates the document and header files to be printed
	 * 
	 * @return returns the documents created
	 */
	public Document createDOMDocument()
	{

		if (file != null)
		{
			try
			{
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				// factory.setValidating(true);
				factory.setIgnoringComments(true);
				factory.setIgnoringElementContentWhitespace(true);
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document doc = builder.parse(file);
				// doc.getDocumentElement().normalize();
				header = new XMLHeader(doc.getXmlVersion(), doc.getXmlEncoding());
				return doc;
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			catch (SAXException e)
			{
				e.printStackTrace();
			}
			catch (ParserConfigurationException e)
			{
				e.printStackTrace();
			}

		}

		return null;

	}

	/**
	 * saves the documents and files created
	 */
	public void save()
	{
		if (file != null)
		{
			try
			{
				if (!file.exists())
				{
					file.createNewFile();
				}
				BufferedWriter writer = new BufferedWriter(new FileWriter(file));

				// version and encoding
				if (header != null)
				{
					writer.append("<?xml version=\"" + header.getXmlVersion() + "\" encoding=\"" + header.getXmlEncoding() + "\"?>\n");
				}

				if (rootElement != null)
				{
					writer.append(rootElement.toStringWithChildren(0));
				}

				writer.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

}
