package quantumepoch.utils.xml;

/**
 * gets and sets the attributes of the keys and values
 *
 */
public class XMLAttrib
{

	private String key;
	private String value;

	/**
	 * constructor that sets the values of the key and value
	 * 
	 * @param key
	 *            the key value to set
	 * @param value
	 *            the value to set
	 */
	public XMLAttrib(String key, String value)
	{
		this.key = key;
		this.value = value;
	}

	/**
	 * gets the key value
	 * 
	 * @return returns the key value
	 */
	public String getKey()
	{
		return key;
	}

	/**
	 * gets the value value
	 * 
	 * @return returns the value value
	 */
	public String getValue()
	{
		return value;
	}

	/**
	 * sets the key value
	 * 
	 * @param key
	 *            the key value to set
	 */
	public void setKey(String key)
	{
		this.key = key;
	}

	/**
	 * sets the value value
	 * 
	 * @param value
	 *            the value value to set
	 */
	public void setValue(String value)
	{
		this.value = value;
	}

}
