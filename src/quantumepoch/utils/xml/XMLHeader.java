package quantumepoch.utils.xml;

/**
 * instantiates and creates the header files and value for the document
 *
 */
public class XMLHeader
{

	private String xmlVersion;
	private String xmlEncoding;

	/**
	 * constructor that sets the values of the header version and coding
	 * 
	 * @param xmlVersion
	 *            version to set to header
	 * @param xmlEncoding
	 *            encoding to set to header
	 */
	public XMLHeader(String xmlVersion, String xmlEncoding)
	{
		this.xmlVersion = xmlVersion;
		this.xmlEncoding = xmlEncoding;
	}

	/**
	 * gets the xml header current version
	 * 
	 * @return returns the xml version
	 */
	public String getXmlVersion()
	{
		return xmlVersion;
	}

	/**
	 * gets the xml header encoding
	 * 
	 * @return returns the xml encoding
	 */
	public String getXmlEncoding()
	{
		return xmlEncoding;
	}

	/**
	 * sets the xml header version of the file
	 * 
	 * @param xmlVersion
	 *            the xml version to set
	 */
	public void setXmlVersion(String xmlVersion)
	{
		this.xmlVersion = xmlVersion;
	}

	/**
	 * sets the xml header encoding of the file
	 * 
	 * @param xmlEncoding
	 *            the xml encoding to set
	 */
	public void setXmlEncoding(String xmlEncoding)
	{
		this.xmlEncoding = xmlEncoding;
	}

}
