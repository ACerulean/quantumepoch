package quantumepoch.utils;

import quantumepoch.utils.math.Matrix4f;
import quantumepoch.utils.math.Vector2f;
import quantumepoch.utils.math.Vector3f;

/**
 * the math utilities for the util class used in calculations for the class and
 * other methods and attributes.
 *
 */
public class MathUtils
{

	/**
	 * finds the biggest value of value
	 * 
	 * @param lower
	 *            the lowest value of value
	 * @param higher
	 *            the highest value of value
	 * @param value
	 *            the value
	 * @return returns the highest value of vluae
	 */
	public static float clamp(float lower, float higher, float value)
	{
		if (value < lower)
		{
			return lower;
		} else if (value > higher)
		{
			return higher;
		} else
		{
			return value;
		}
	}

	/**
	 * makes a matrix of floats
	 * 
	 * @param left
	 *            the left float for the matrix
	 * @param right
	 *            the right float for the matrix
	 * @param bottom
	 *            the bottom float for the matrix
	 * @param top
	 *            the top float for the matrix
	 * @param zNear
	 *            the nearest float of the matrix
	 * @param zFar
	 *            the farthest float for the matrix
	 * @return returns the matrix
	 */
	public static Matrix4f orthoMatrix(float left, float right, float bottom, float top, float zNear, float zFar)
	{
		Matrix4f mat = new Matrix4f();
		orthoMatrix(mat, left, right, bottom, top, zNear, zFar);
		return mat;
	}

	/**
	 * sets the matrix for the utilities
	 * 
	 * @param mat
	 *            the matrix
	 * @param left
	 *            the left value
	 * @param right
	 *            the right value
	 * @param bottom
	 *            the bottom value
	 * @param top
	 *            the top value
	 * @param zNear
	 *            the closest value
	 * @param zFar
	 *            the farthest value
	 */
	public static void orthoMatrix(Matrix4f mat, float left, float right, float bottom, float top, float zNear, float zFar)
	{
		mat.setElement(0, 0, 2 / (right - left));
		mat.setElement(1, 0, 0);
		mat.setElement(2, 0, 0);
		mat.setElement(3, 0, 0);

		mat.setElement(0, 1, 0);
		mat.setElement(1, 1, 2 / (top - bottom));
		mat.setElement(2, 1, 0);
		mat.setElement(3, 1, 0);

		mat.setElement(0, 2, 0);
		mat.setElement(1, 2, 0);
		mat.setElement(2, 2, -2 / (zFar - zNear));
		mat.setElement(3, 2, 0);

		mat.setElement(0, 3, -((right + left) / (right - left)));
		mat.setElement(1, 3, -((top + bottom) / (top - bottom)));
		mat.setElement(2, 3, (zFar + zNear) / (zFar - zNear));
		mat.setElement(3, 3, 1);

	}

	/**
	 * the perspective matrix for the utils
	 * 
	 * @param fov
	 *            the field of view
	 * @param aspect
	 *            the aspect ratio
	 * @param zNear
	 *            the closest value
	 * @param zFar
	 *            the farthest value
	 * @return returns the matrix
	 */
	public static Matrix4f perspectiveMatrix(float fov, float aspect, float zNear, float zFar)
	{
		Matrix4f mat = new Matrix4f();
		perspectiveMatrix(mat, fov, aspect, zNear, zFar);
		return mat;
	}

	/**
	 * the perspective matrix for the utils
	 * 
	 * @param mat
	 *            the matrix
	 * @param fov
	 *            the field of view
	 * @param aspect
	 *            the aspect ratio
	 * @param zNear
	 *            the closest value
	 * @param zFar
	 *            the farthest value
	 */
	public static void perspectiveMatrix(Matrix4f mat, float fov, float aspect, float zNear, float zFar)
	{
		// float f = (float) (1 / (Math.tan(fov / 2)));
		float tan = (float) Math.tan(Math.toRadians(fov / 2));
		float zRange = zNear - zFar;

		mat.setElement(0, 0, 1f / (tan * aspect));
		mat.setElement(1, 0, 0);
		mat.setElement(2, 0, 0);
		mat.setElement(3, 0, 0);

		mat.setElement(0, 1, 0);
		mat.setElement(1, 1, 1f / tan);
		mat.setElement(2, 1, 0);
		mat.setElement(3, 1, 0);

		mat.setElement(0, 2, 0);
		mat.setElement(1, 2, 0);
		mat.setElement(2, 2, (zFar + zNear) / zRange);
		mat.setElement(3, 2, -1);

		mat.setElement(0, 3, 0);
		mat.setElement(1, 3, 0);
		mat.setElement(2, 3, (2 * zNear * zFar) / zRange);
		mat.setElement(3, 3, 0);

	}

	/**
	 * the translation matrix for the utils
	 * 
	 * @param translate
	 *            the translation value
	 * @return returns the translated matrix
	 */
	public static Matrix4f translationMatrix(Vector2f translate)
	{
		return translationMatrix(translate.getX(), translate.getY());
	}

	/**
	 * the translation matrix for the utils
	 * 
	 * @param x
	 *            the x value
	 * @param y
	 *            the y value
	 * @return returns the matrix translated
	 */
	public static Matrix4f translationMatrix(float x, float y)
	{
		Matrix4f mat = new Matrix4f();
		translationMatrix(mat, x, y);
		return mat;
	}

	/**
	 * the translation matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @param translate
	 *            the translation value
	 */
	public static void translationMatrix(Matrix4f mat, Vector2f translate)
	{
		translationMatrix(mat, translate.getX(), translate.getY());
	}

	/**
	 * the translation matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @param x
	 *            the x values
	 * @param y
	 *            the y values
	 */
	public static void translationMatrix(Matrix4f mat, float x, float y)
	{
		translationMatrix(mat, x, y, 0);
	}

	/**
	 * the translation matrix
	 * 
	 * @param translate
	 *            the translation value
	 * @return returns the matrix
	 */
	public static Matrix4f translationMatrix(Vector3f translate)
	{
		return translationMatrix(translate.getX(), translate.getY(), translate.getZ());
	}

	/**
	 * the translation matrix
	 * 
	 * @param x
	 *            the x value
	 * @param y
	 *            the y value
	 * @param z
	 *            the z value
	 * @return returns the new matrix
	 */
	public static Matrix4f translationMatrix(float x, float y, float z)
	{
		Matrix4f mat = new Matrix4f();
		translationMatrix(mat, x, y, z);
		return mat;
	}

	/**
	 * the translation matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @param translate
	 *            the translation value
	 */
	public static void translationMatrix(Matrix4f mat, Vector3f translate)
	{
		translationMatrix(mat, translate.getX(), translate.getY(), translate.getZ());
	}

	/**
	 * the translation matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @param x
	 *            the x value
	 * @param y
	 *            the y value
	 * @param z
	 *            the z value
	 */
	public static void translationMatrix(Matrix4f mat, float x, float y, float z)
	{
		mat.setElement(0, 0, 1);
		mat.setElement(1, 0, 0);
		mat.setElement(2, 0, 0);
		mat.setElement(3, 0, 0);

		mat.setElement(0, 1, 0);
		mat.setElement(1, 1, 1);
		mat.setElement(2, 1, 0);
		mat.setElement(3, 1, 0);

		mat.setElement(0, 2, 0);
		mat.setElement(1, 2, 0);
		mat.setElement(2, 2, 1);
		mat.setElement(3, 2, 0);

		mat.setElement(0, 3, x);
		mat.setElement(1, 3, y);
		mat.setElement(2, 3, z);
		mat.setElement(3, 3, 1);
	}

	/**
	 * the rotation matrix
	 * 
	 * @param rot
	 *            the rotation value
	 * @return returns the matrix
	 */
	public static Matrix4f rotationMatrix(Vector3f rot)
	{
		Matrix4f mat = new Matrix4f();
		rotationMatrix(mat, rot.getX(), rot.getY(), rot.getZ());
		return mat;
	}

	/**
	 * the rotation matrix
	 * 
	 * @param rotX
	 *            the x value
	 * @param rotY
	 *            the y value
	 * @param rotZ
	 *            the z value
	 * @return returns the rotated matrix
	 */
	public static Matrix4f rotationMatrix(float rotX, float rotY, float rotZ)
	{
		Matrix4f mat = new Matrix4f();
		rotationMatrix(mat, rotX, rotY, rotZ);
		return mat;
	}

	/**
	 * the rotation matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @param rot
	 *            the rotation value
	 */
	public static void rotationMatrix(Matrix4f mat, Vector3f rot)
	{
		rotationMatrix(mat, rot.getX(), rot.getY(), rot.getZ());
	}

	/**
	 * the rotation matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @param rotX
	 *            the x value
	 * @param rotY
	 *            the y value
	 * @param rotZ
	 *            the z value
	 */
	public static void rotationMatrix(Matrix4f mat, float rotX, float rotY, float rotZ)
	{
		Matrix4f rotXMat = rotationX(rotX);
		Matrix4f rotYMat = rotationY(rotY);
		Matrix4f rotZMat = rotationZ(rotZ);

		mat.set(rotZMat.multiply(rotYMat.multiply(rotXMat)));
	}

	/**
	 * the rotation of the x value matrix
	 * 
	 * @param theta
	 *            the angle of rotation
	 * @return returns the matrix
	 */
	public static Matrix4f rotationX(float theta)
	{
		Matrix4f mat = new Matrix4f();
		rotationX(mat, theta);
		return mat;
	}

	/**
	 * the rotation x value for the matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @param theta
	 *            the angle to rotate
	 */
	public static void rotationX(Matrix4f mat, float theta)
	{
		float sin = (float) Math.sin(theta);
		float cos = (float) Math.cos(theta);

		mat.setElement(0, 0, 1);
		mat.setElement(1, 0, 0);
		mat.setElement(2, 0, 0);
		mat.setElement(3, 0, 0);

		mat.setElement(0, 1, 0);
		mat.setElement(1, 1, cos);
		mat.setElement(2, 1, -sin);
		mat.setElement(3, 1, 0);

		mat.setElement(0, 2, 0);
		mat.setElement(1, 2, sin);
		mat.setElement(2, 2, cos);
		mat.setElement(3, 2, 0);

		mat.setElement(0, 3, 0);
		mat.setElement(1, 3, 0);
		mat.setElement(2, 3, 0);
		mat.setElement(3, 3, 1);
	}

	/**
	 * the y rotation value for the matrix
	 * 
	 * @param theta
	 *            the rotation
	 * @return returns the rotated matrix
	 */
	public static Matrix4f rotationY(float theta)
	{
		Matrix4f mat = new Matrix4f();
		rotationY(mat, theta);
		return mat;
	}

	/**
	 * the y rotation value for the matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @param theta
	 *            the rotation angle
	 */
	public static void rotationY(Matrix4f mat, float theta)
	{
		float sin = (float) Math.sin(theta);
		float cos = (float) Math.cos(theta);

		mat.setElement(0, 0, cos);
		mat.setElement(1, 0, 0);
		mat.setElement(2, 0, sin);
		mat.setElement(3, 0, 0);

		mat.setElement(0, 1, 0);
		mat.setElement(1, 1, 1);
		mat.setElement(2, 1, 0);
		mat.setElement(3, 1, 0);

		mat.setElement(0, 2, -sin);
		mat.setElement(1, 2, 0);
		mat.setElement(2, 2, cos);
		mat.setElement(3, 2, 0);

		mat.setElement(0, 3, 0);
		mat.setElement(1, 3, 0);
		mat.setElement(2, 3, 0);
		mat.setElement(3, 3, 1);
	}

	/**
	 * the matrix 2nd rotation
	 * 
	 * @param theta
	 *            the rotation angle
	 * @return returns the rotated matrix
	 */
	public static Matrix4f rotationZ(float theta)
	{
		Matrix4f mat = new Matrix4f();
		rotationZ(mat, theta);
		return mat;
	}

	/**
	 * the rotation matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @param theta
	 *            the rotation angle
	 */
	public static void rotationZ(Matrix4f mat, float theta)
	{
		float sin = (float) Math.sin(theta);
		float cos = (float) Math.cos(theta);

		mat.setElement(0, 0, cos);
		mat.setElement(1, 0, -sin);
		mat.setElement(2, 0, 0);
		mat.setElement(3, 0, 0);

		mat.setElement(0, 1, sin);
		mat.setElement(1, 1, cos);
		mat.setElement(2, 1, 0);
		mat.setElement(3, 1, 0);

		mat.setElement(0, 2, 0);
		mat.setElement(1, 2, 0);
		mat.setElement(2, 2, 1);
		mat.setElement(3, 2, 0);

		mat.setElement(0, 3, 0);
		mat.setElement(1, 3, 0);
		mat.setElement(2, 3, 0);
		mat.setElement(3, 3, 1);
	}

	/**
	 * the matrix scaling
	 * 
	 * @param scale
	 *            the scaling for the matrix
	 */
	public static void scaleMatrix(Vector2f scale)
	{
		scaleMatrix(scale.getX(), scale.getY());
	}

	/**
	 * the scaling matrix
	 * 
	 * @param scaleX
	 *            the x scalar
	 * @param scaleY
	 *            the y scalar
	 */
	public static void scaleMatrix(float scaleX, float scaleY)
	{
		scaleMatrix(scaleX, scaleY, 1);
	}

	/**
	 * the scaling matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @param scale
	 *            the scalar
	 */
	public static void scaleMatrix(Matrix4f mat, Vector2f scale)
	{
		scaleMatrix(mat, scale.getX(), scale.getY());
	}

	/**
	 * the scaling matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @param scaleX
	 *            the x scalar
	 * @param scaleY
	 *            the y scalar
	 */
	public static void scaleMatrix(Matrix4f mat, float scaleX, float scaleY)
	{
		scaleMatrix(mat, scaleX, scaleY, 1);
	}

	/**
	 * the scaling matrix
	 * 
	 * @param scale
	 *            the scalar
	 * @return returns the scaled matrix
	 */
	public static Matrix4f scaleMatrix(Vector3f scale)
	{
		return scaleMatrix(scale.getX(), scale.getY(), scale.getZ());
	}

	/**
	 * the scalar matrix
	 * 
	 * @param scaleX
	 *            the x scalar
	 * @param scaleY
	 *            the y scalar
	 * @param scaleZ
	 *            the z scalar
	 * @return
	 */
	public static Matrix4f scaleMatrix(float scaleX, float scaleY, float scaleZ)
	{
		Matrix4f mat = new Matrix4f();
		scaleMatrix(mat, scaleX, scaleY, scaleZ);
		return mat;
	}

	/**
	 * the scalar matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @param scale
	 *            the scalar
	 */
	public static void scaleMatrix(Matrix4f mat, Vector3f scale)
	{
		scaleMatrix(mat, scale.getX(), scale.getY(), scale.getZ());
	}

	/**
	 * the scalar matrix
	 * 
	 * @param mat
	 *            the matrix
	 * @param scaleX
	 *            the x scalar
	 * @param scaleY
	 *            the y scalar
	 * @param scaleZ
	 *            the z scalar
	 */
	public static void scaleMatrix(Matrix4f mat, float scaleX, float scaleY, float scaleZ)
	{
		mat.setElement(0, 0, scaleX);
		mat.setElement(1, 0, 0);
		mat.setElement(2, 0, 0);
		mat.setElement(3, 0, 0);

		mat.setElement(0, 1, 0);
		mat.setElement(1, 1, scaleY);
		mat.setElement(2, 1, 0);
		mat.setElement(3, 1, 0);

		mat.setElement(0, 2, 0);
		mat.setElement(1, 2, 0);
		mat.setElement(2, 2, scaleZ);
		mat.setElement(3, 2, 0);

		mat.setElement(0, 3, 0);
		mat.setElement(1, 3, 0);
		mat.setElement(2, 3, 0);
		mat.setElement(3, 3, 1);
	}

	/**
	 * asks if the is squared
	 * 
	 * @param n
	 *            the n power
	 * @return returns true if power of two
	 */
	public static boolean isPowerOfTwo(int n)
	{
		if ((n != 0) && (n & (n - 1)) == 0)
		{
			return true;
		} else
		{
			return false;
		}
	}

}
