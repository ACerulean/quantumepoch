package quantumepoch.input;

import quantumepoch.core.Core;

/**
 * A class to contain instances to various input subsystems to the engine. This
 * class should be a singleton.s
 */
public class Input
{

	/** the keybaord input subsystem */
	private Keyboard keyboard;
	/** the mouse input subsystem */
	private Mouse mouse;

	/**
	 * Creates a new input singleton.
	 */
	public Input()
	{
		if (Core.getInput() != null)
		{
			throw new IllegalStateException("Can not make more than one instance of Input");
		} else
		{
			keyboard = new Keyboard();
			mouse = new Mouse();
		}
	}

	/**
	 * @return the keyboard subsystem
	 */
	public Keyboard getKeyboard()
	{
		return keyboard;
	}

	/**
	 * @return the mouse subsystem
	 */
	public Mouse getMouse()
	{
		return mouse;
	}

	/**
	 * Creates all input subsystems.
	 */
	public void create()
	{
		keyboard.create();
		mouse.create();
	}

	/**
	 * Updates all input subsystems.
	 */
	public void update()
	{
		keyboard.update();
		mouse.update();
	}

	/**
	 * Disposes all input subsystems and frees the resources associated with
	 * them.
	 */
	public void dispose()
	{
		keyboard.dispose();
		mouse.dispose();
	}

}
