package quantumepoch.input;

import org.lwjgl.LWJGLException;

/**
 * A core input subsystem, the mouse provides useful functionality to check for
 * mouse device input events.
 */
public class Mouse
{

	/** the highest amount of buttons possible */
	public static final int NUM_BUTTONS = 0xFF;

	/** the singleton instance of this class */
	private static Mouse instance;

	/** buttons that are currently pressed */
	private boolean[] buttonsDown;
	/** buttons that were pressed just this frame */
	private boolean[] buttonsThisFrame;

	/** the last check for the mouse's dynamic x */
	private int lastDX = -1;
	/** the last check for the mouse's dynamic y */
	private int lastDY = -1;

	/**
	 * Creates a new Mouse input subsystem.
	 */
	public Mouse()
	{
		if (instance == null)
		{
			instance = this;

			buttonsDown = new boolean[NUM_BUTTONS];
			buttonsThisFrame = new boolean[NUM_BUTTONS];
		} else
		{
			throw new IllegalStateException("Can not make more than one instance of Mouse");
		}
	}

	/**
	 * Checks if the mouse the player currently has, contains a scroll wheel.
	 * 
	 * @return Whether the current mouse has a scroll wheel
	 */
	public boolean hasWheel()
	{
		return org.lwjgl.input.Mouse.hasWheel();
	}

	/**
	 * @return Whether the mouse is currently inside of the game window
	 */
	public boolean isInWindow()
	{
		return org.lwjgl.input.Mouse.isInsideWindow();
	}

	/**
	 * @return Whether the mouse is currently grabbed
	 */
	public boolean isGrabbed()
	{
		return org.lwjgl.input.Mouse.isGrabbed();
	}

	/**
	 * @param button
	 *            The button to check input for
	 * @return Whether the specified button is currently pressed
	 */
	public boolean isButtonDown(int button)
	{
		return buttonsDown[button];
	}

	/**
	 * @param button
	 *            The button to check input for
	 * @return Whether the button was just pressed
	 */
	public boolean wasButtonPressed(int button)
	{
		return buttonsThisFrame[button];
	}

	/**
	 * @return the x of the mouse relative to the game window
	 */
	public int getX()
	{
		return org.lwjgl.input.Mouse.getX();
	}

	/**
	 * @return the y of the mouse relative to the game window
	 */
	public int getY()
	{
		return org.lwjgl.input.Mouse.getY();
	}

	/**
	 * @return The dynamic x of the mouse. This is defined as the difference in
	 *         the mouse's x position between this call and the last call of
	 *         this method.
	 */
	public int getDX()
	{
		if (lastDX == -1)
		{
			lastDX = getX();
			return 0;
		} else
		{
			int temp = lastDX;
			lastDX = getX();
			return getX() - temp;
		}
	}

	/**
	 * @return The dynamic y of the mouse. This is defined as the difference in
	 *         the mouse's y position between this call and the last call of
	 *         this method.
	 */
	public int getDY()
	{
		if (lastDY == -1)
		{
			lastDY = getY();
			return 0;
		} else
		{
			int temp = lastDY;
			lastDY = getY();
			return getY() - temp;
		}
	}

	/**
	 * Sets the mouse's dynamic x back to zero.
	 */
	public void resetDX()
	{
		lastDX = getX();
	}

	/**
	 * Sets the mouse's dynamic y back to zero.
	 */
	public void resetDY()
	{
		lastDY = getY();
	}

	/**
	 * @return The integer amount of movement on the scroll wheel
	 */
	public int getDWheel()
	{
		return org.lwjgl.input.Mouse.getDWheel();
	}

	/**
	 * The mouse being grabbed hides the mouse cursor.
	 * 
	 * @param grabbed
	 *            The new value of the mouse being grabbed or not.
	 */
	public void setGrabbed(boolean grabbed)
	{
		org.lwjgl.input.Mouse.setGrabbed(grabbed);
	}

	/**
	 * Sets the [x,y] position of the mouse on the screen
	 * 
	 * @param x
	 * @param y
	 */
	public void setPosition(int x, int y)
	{
		org.lwjgl.input.Mouse.setCursorPosition(x, y);
	}

	/**
	 * Creates the mouse input subsystem.
	 */
	public void create()
	{
		try
		{
			org.lwjgl.input.Mouse.create();
		}
		catch (LWJGLException e)
		{
			System.err.println("Error creating mouse");
			e.printStackTrace();
		}
	}

	/**
	 * Updates the mouse input subsystem.
	 */
	public void update()
	{
		for (int i = 0; i < NUM_BUTTONS; i++)
		{
			if (org.lwjgl.input.Mouse.isButtonDown(i) && !buttonsDown[i])
			{
				buttonsThisFrame[i] = true;
			} else
			{
				buttonsThisFrame[i] = false;
			}
		}

		for (int i = 0; i < NUM_BUTTONS; i++)
		{
			if (org.lwjgl.input.Mouse.isButtonDown(i))
			{
				buttonsDown[i] = true;
			} else
			{
				buttonsDown[i] = false;
			}
		}

	}

	/**
	 * Disposes the mouse input subsystem and frees and resources associated
	 * with it.
	 */
	public void dispose()
	{
		org.lwjgl.input.Mouse.destroy();
	}

}
